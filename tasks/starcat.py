#!/usr/bin/env python
# encoding: UTF8

import time
import itertools as it
import numpy as np
import pandas as pd

from scipy.interpolate import splev, splrep

class starcat:
    """Generate a catalog with star absolute magnitudes."""

    version = 1.0
    config = {\
     'filters': ['vis', 'g', 'r', 'i', 'z', 'Y', 'J', 'H'],
     'norm_mag': 10,
     'Nstars':  40000,
     'star_zs': 0.01,
     'sed_prob': {},
     'star_path': '/data2/photoz/run/data/seds_stars/STAR_PICKLES.list'}

    _star_path = '/data2/photoz/run/data/seds_stars/STAR_PICKLES.list'
    def load_seds_list(self):
        # This information should be passed elsewhere...
        A = open(self._star_path).readlines()
        star_seds = [x.split('.')[0].split('/')[1] for x in A]

        return star_seds

    def ab_spls(self):
        ab = self.job.ab.result
        filters = self.config['filters']

        splD = {}
        for fname, sed in it.product(filters, self.star_seds):
            name = '/{0}/{1}'.format(fname, sed)

            splD[fname,sed] = splrep(ab['z'], ab[name])

        return splD 

    def create_mag_templ(self):
        res_mag = {}
        filters = self.config['filters']
        nr = filters.index('i')

        zs = self.config['star_zs']
        for sed in self.star_seds:
            def find_flux(f):
                return float(splev(zs, self.ab_splD[f, sed]))
 
            flux = np.hstack(map(find_flux, filters))
            mag = -2.5*np.log10(flux)

#            delta_m = self.config['norm_mag'] - mag_unnorm[nr]
#            mag = mag_unnorm + delta_m

            if not np.isfinite(mag).all():
                raise 

            res_mag[sed] = mag

        return res_mag, zs, filters

    def _random_sed(self):
        mes_prob = 'sed_prob' in self.job.depend
        conf_prob = 'sed_prob' in self.config

        # The flat probability. I just want to finish this special case first.
        nstars = self.config['Nstars']
        if not mes_prob or conf_prob:
            nseds = len(self.star_seds)
            sed_ind = np.random.randint(0, nseds, nstars)

            return sed_ind

        if mes_prob:
            # This part could more part to the configuration....
            B = self.job.depend['sed_prob']['starfit']
            p = B[B.chi2 < 10][B.cl < 0.8][B.mag < 21].sed.value_counts()
            sed_prob = p[:10].to_dict()
        else:
            sed_prob = self.config['sed_prob']


        # This part is shared.
        A,B = zip(*[(y,x) for (x,y) in self.config['sed_prob'].iteritems()])
        keys = map(self.star_seds.index, B)
        prob = np.zeros(len(self.star_seds))
        prob[keys] = np.array(A)
        prob /= np.sum(prob)

        A = np.random.multinomial(1, prob, nstars)
        sed_ind = np.array(map(np.argmax, A)) 

        return sed_ind

    def _mag_norm(self, Nstars):
        mag_prob = 'mag_prob' in self.job.depend

        if mag_prob:
            H = self.job.mag_prob.result
            spl = splrep(H.prob.cumsum(), H.mag)
            r = np.random.random(Nstars)
            mag = splev(r, spl)
        else:
            mag = self.config['norm_mag']*np.ones(Nstars)

        return mag

    def create_mag(self):
        sed_ind = self._random_sed()
        res_mag, zs_templ, filters = self.create_mag_templ()
        zs = zs_templ*np.ones_like(sed_ind)
        mag_stack = np.vstack([res_mag[x] for x in self.star_seds])
        mag = mag_stack[sed_ind]

        assert np.isfinite(mag).all(), 'Error: Infinite magnitudes detected!'

        columns = map('mag_{0}'.format, filters)
        cat = pd.DataFrame(mag, columns=columns)

        Nstars = len(sed_ind)
        mag_norm = self._mag_norm(Nstars)
        delta_m = mag_norm - cat.mag_i
        cat = cat.add(delta_m, axis='rows')

        cat['zs'] = zs
        cat['star_sed_ind'] = sed_ind

        return cat

    def run(self):
        self.star_seds = self.load_seds_list()
        self.ab_splD = self.ab_spls()

        self.job.result = self.create_mag()
