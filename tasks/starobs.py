#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd

class starobs:
    """Generate stellar apparent magnitudes."""

    version = 1.03
    config = {}

    def ab_pivot(self, ab_in):
        """Tranform the AB values at z=0.0 to a pivot table."""

        # Here we are trying to use a slightly different
        # approach to storing the flux values.
        row = ab_in.irow(0)
        assert row['z'] == 0., 'Should use the value at z=0.'
        del row['z']

        _, fname, sed = zip(*(x.split('/') for x in row.keys()))
        ab = pd.DataFrame({'fname': fname, 'sed': sed, 'flux': row})
        ab_pivot = ab.pivot(index='sed', columns='fname', values='flux')

        return ab_pivot

    def gen_mag(self, cat, ab_pivot):
        """Generate the magnitudes."""

        mag = -2.5*np.log10(ab_pivot.ix[cat.sed])
        mag = mag.set_index(cat.index)
        mag = mag.add(cat.imag - mag.i, axis='rows')
        mag = mag.rename(columns=lambda x: 'mag_'+x)
        mag = mag.join(cat[['point', 'sed']])

        return mag

    def run(self):
        ab_in = self.job.depend['ab'].result
        cat = self.job.depend['starcat'].result

        ab_pivot = self.ab_pivot(ab_in)
        self.job.result = self.gen_mag(cat, ab_pivot)
