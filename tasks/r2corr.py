#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd
from sklearn.neighbors import  KNeighborsRegressor

class r2corr:
    """Calculate a correction to the nmissing data in the north."""

    version = 1.1
    config = {'corr_filters': ['g', 'r', 'i'],
              'z_corr': False, 
              'use_zb': True}

    def getcat(self, pipel):
        names = ['r2eff', 'r2pred', 'r2true']
        store = pipel.r2train.get_store()

    #    A = pipel.kids['r2train']['r2train']
        cat = pipel.galcatN.result
        for name in names:
            part = store[name]
            part.columns = ['{0}_{1}'.format(name, x) for x in range(len(part.columns))]

            cat[name] = part.mean(axis=1)
            cat = cat.join(part)
        if self.config['z_corr'] and self.config['use_zb']:
            cat = cat.join(pipel.pzcat.result, rsuffix='x')


        return cat

    def calc_correct(self, Nboot, catS, catSX, catN):

        E = catS.join(catSX[['r2eff', 'r2pred', 'r2true']], rsuffix='_red', how='inner')
        E = E[(~np.isnan(E.r2eff)) & (~np.isnan(E.r2eff_red))]

        corr_filters = self.config['corr_filters']
        train_keys = map('mag_{0}'.format, corr_filters)

        if self.config['z_corr']:
            train_keys.append('zb' if self.config['use_zb'] else 'zs')

        # Factor between the different predictions.
        E['f'] = E.r2pred / E.r2pred_red
        subE = E[train_keys]

        reg = KNeighborsRegressor()
        reg = reg.fit(subE, E.f)

#        catN['r2eff_corr'] = (catN.r2pred *fN - catN.r2true) / catN.r2true
#        Nboot = north.kids['r2train'].config['Nboot']
        res = pd.DataFrame()
        for i in range(Nboot):
            print('Predicting', i)
            # Not sure what happens if feeding NaN into the predictor...
            inds = ~np.isnan(catN['r2eff_{0}'.format(i)])

            sub = catN[inds]
            f = reg.predict(sub[train_keys])

            key = 'r2eff_corr_{0}'.format(i)
            S = (f*sub.r2pred - sub.r2true) / sub.r2true
            S.name = key
            res = res.join(S,how='outer')

        return res

    def run(self):

        # Reading in the catalogs.
        depend = self.job.depend
        catS = self.getcat(self.job.south)
        catSX = self.getcat(self.job.southX)
        catN = self.getcat(self.job.north)

        Nboot = self.job.north.r2train.config['Nboot']
        res = self.calc_correct(Nboot, catS, catSX, catN)

        self.job.result = res
        # self.job.store_frametbl('r2corr', res)
