#!/usr/bin/env python
# encoding: UTF8

import pandas as pd

class r2starx:
    """Estimate the effective galaxy PSF."""

    version = 1.0
    config = { 
     'r2_filter': 'vis',
    }

    def r2eff(self, magcat, r2vz_in):
        """Estimate the r2eff for all galaxies."""

        # Create a r2vz pivot table."""
        row = r2vz_in.irow(0)
        assert row.z == 0., 'We need z=0.0 for the stars'
        del row['z']
        _, fname, sed = zip(*(x.split('/') for x in row.keys()))
        r2vz = pd.DataFrame({'fname': fname, 'sed': sed, 'r2eff': row})
        r2vz_pivot = r2vz.pivot(index='sed', columns='fname', values='r2eff')

        # Estimate the r2eff.
        r2vz_pivot = r2vz_pivot[self.config['r2_filter']]
        r2eff = pd.DataFrame({'r2eff': r2vz_pivot.ix[magcat.sed].values}, 
                             index=magcat.index)

        return r2eff

    def run(self):
        magcat = self.job.depend['magcat'].result
        r2vz_in = self.job.depend['r2vz'].result

        self.job.result = self.r2eff(magcat, r2vz_in)
