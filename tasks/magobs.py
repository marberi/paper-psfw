#!/usr/bin/env python
# encoding: UTF8

# Start of cleaning up the code. Moving out the
# configurations.

from __future__ import print_function
import pdb
import tables
import time
import numpy as np
import itertools as it
from scipy.interpolate import splev, splrep
import cosmolopy as cpy
import pandas as pd

import bcnz
import bcnz.model.project as proj

class magobs:
    """Create the noiseless apparent magnitudes from the absolute magnitudes."""

    version = 1.1
    config = {'filters': ['H', 'J', 'Y', 'g', 'i', 'r', 'vis', 'z'],
              'kcorr': False, # Additional k-correction...
              'prior_filter': 'i',
              'norm_filter': 'r'}

    def _conv_splines(self, abdf_in, seds):
        """Constructs splines for the coversion between redshift and
           absolute magnitude for all filters and SEDs. Arbitrary
           normalization.
        """

        lum_dist = cpy.distance.luminosity_distance

        absD = {}
        magD = {}
        add_kcorr = self.conf['kcorr']

        # The distance modulus would require the logarithm of 
        # zero if including all points.
        ab_df = abdf_in[0.001 < abdf_in.z]
        z = ab_df['z']
        Dl = lum_dist(z, **cpy.fidcosmo)
        DM = 25. + 5*np.log10(Dl)

        # Only iterating over the itema we need.
        for key in it.product(self.conf['filters'], seds):
            fname,sed = key
            name = '/{0}/{1}'.format(fname, sed)
            ab = ab_df[name]

            if add_kcorr:
#                kcorr = -2.5*np.log10(ab['ab'][0] / abx / (1+zx))
                kcorr = -2.5*ab.irow(0) / ab / (1+z)
            else:
                kcorr = 0.

            mag = -2.5*np.log10(ab)

            # Absolute and apparent magnitudes, unnormalized.
            absD[key] = splrep(z, mag)
            magD[key] = splrep(z, mag + DM + kcorr)

        return absD, magD

    # - Same normalization shift applies both to the absolute and
    #   apparent magnitudes.
    # - The distance factor in the K-correction ends up being absorbed
    #   in the magnitude normalization.
    # - Normalization can either be done in fluxes of magnitudes. I prefer
    #   the magnitudes since it requires less conversions and more operations
    #   is additions.

    def _proj(self, fname, seds, splD, zs, tt):
        # This could be a function instead of a method.

        print('Projecting in filter:', fname)

        # One could also pass these, but it makes the interface more complicated.
        tt0 = tt.astype(np.int)
        dt = tt - tt0

        # This code is very similar to R2Gal....
        mag_filter = np.nan*np.ones(len(zs))
        for i in range(len(seds)):
            spl0 = splD[fname, seds[i]]

            inds = np.array((tt0 == i))
            spl0 = splD[fname, seds[i]]

            # This ends up giving problems.
            if not np.sum(inds):
                continue

            if i == len(seds)-1:
                mag_filter[inds] = splev(zs[inds], spl0) #, ext=2)
                continue

            spl1 = splD[fname, seds[i+1]]
            part1 = (1. - dt[inds]) * splev(zs[inds], spl0) #, ext=2)
            part2 = dt[inds] * splev(zs[inds], spl1) #, ext=2)

            mag_filter[inds] = part1 + part2

        return mag_filter

    def genmag(self, cat_in, seds):
        """Generate the observed magnitudes."""

        zs = cat_in['zs']
        tt = cat_in['sed']

        filters = self.config['filters']
        cat_unnormed = {}
        for fname in filters:
            cat_unnormed[fname] = self._proj(fname, seds, self._magD, zs, tt)


        norm_filter = self.config['norm_filter']
        mabs_norm = self._proj(norm_filter, seds, self._absD, zs, tt)

        delta_m = cat_in['M'] - mabs_norm
        normalize = lambda x: cat_unnormed[x] + delta_m

        cat_normed = np.vstack(map(normalize, filters)).T

        # Perhhaps not the best. The hirarchical columns is causing me a 
        # nightmare other places. Use this while figuring out what should
        # be done.
        columns = ['mag_{0}'.format(x) for x in filters]
        cat_out = pd.DataFrame(cat_normed, columns=columns)

        # Without the np.array it complains about reindexing from a duplicate
        # axis.
        cat_out['zs'] = np.array(zs)
        cat_out['m0'] = np.array(normalize(self.config['prior_filter']))
# This resulted in a cast from int to float... might have caused some of the
# problems.
#        cat_out['id'] = cat_in['id']


        return cat_out

    def run(self): 
        self.conf = bcnz.libconf(self.config)

        assert 'ab' in self.job.depend, 'Need the AB files.'
        ab_df = self.job.ab.result #.unstack()

#        pdb.set_trace()

        mabs_store = self.job.mabs.get_store()
        seds = mabs_store['seds']
        self._absD, self._magD = self._conv_splines(ab_df, seds)

        path_out = self.job.empty_file('default')
        mag_store = pd.HDFStore(path_out, 'w')

        Rin = mabs_store.select('default', iterator=True, chunksize=1e6)
        for i,mabs_cat in enumerate(Rin):
            print('genmag i', i)
            magcat = self.genmag(mabs_cat, seds)

            mag_store.append('default', magcat)

        mag_store['seds'] = seds
        mabs_store.close()
        mag_store.close()
