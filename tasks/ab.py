#!/usr/bin/env python
# encoding: UTF8

import glob
import os
import shutil
import numpy as np
import tables
import pandas as pd

from scipy.interpolate import splev, splrep

class ab:
    """Apparent magnitude cache."""

    # Part of the code inside here could be moved to yet another layer which
    # allows filter curves to be inside a separate job.

    version = 1.11
    config = {'sed_dir': 'seds',
              'data_dir': '',
              'zmax': 12,
              'dz_ab': 0.001,
              'filter_dir': 'filters'}

    _config_keys = ['data_dir' , 'sed_dir', 'filter_dir', 'dz_ab']

    def load_curves(self, field, suf):
        path = os.path.join(self.config['data_dir'], self.config[field])

        assert os.path.exists(path), 'Path not existing: {0}'.format(path)

        res = {}
        g = os.path.join(path, '*.{0}'.format(suf))
        for file_path in glob.glob(g):
            key = os.path.basename(file_path).split('.')[0]
            x,y = np.loadtxt(file_path).T


            res[key] = x,y

        return res

    def _load_filters(self):
        fout = {}
        df = self.job.depend['filters']['filters']
        print('Actually loading the filters...')
        for fname, val in df.iteritems():
            x = np.array(val[0])
            y = np.array(val[1])

            fout[fname] = x,y

        return fout

    def _convert(self, abD):
        """Change from the nested format within the photo-z code to a single
           dataframe.
        """

        # Later in the loop test it all results has the same 
        # redshift binning.
        ab_array = abD.values()[0].values()[0]
        z_def = ab_array[:,0]

        df = pd.DataFrame()
        df['z'] = z_def

        for fname,val_p1 in abD.iteritems():
            for sed, val in val_p1.iteritems():
                name = '/{0}/{1}'.format(fname, sed)

                assert np.allclose(z_def, val[:,0])

                df[name] = val[:,1]

        return df

    def get_ab(self, sedD, filterD):
        import bcnz.model

        bcnz_myconf = dict([(x,self.config[x]) for x in self._config_keys])
        bcnz_conf = bcnz.libconf(bcnz_myconf)


        zdata = {}
        zdata['sed_spls'] = {}
        zdata['filters'] = filterD.keys()
        zdata['seds'] = sedD.keys()

        zdata['filtersD'] = filterD
        zdata['sedsD'] = sedD

        sed_filters = bcnz.model.sed_filters()
        zdata.update(sed_filters(bcnz_conf, zdata, False))
        model = bcnz.model.model_mag(bcnz_conf, zdata)

        abD = model._all_proj()
        ab_df = self._convert(abD)


        return ab_df

    def run(self):
        sedD = self.load_curves('sed_dir', 'sed')

        if 'filters' in self.job.depend:
            filterD = self._load_filters()
        else:
            filterD = self.load_curves('filter_dir', 'res')

        self.job.result = self.get_ab(sedD, filterD)
