#!/usr/bin/env python
# encoding: UTF8

import os
import numpy as np
import pandas as pd

class csv_bz2:
    """Converting from the CosmoHUB format to the Pandas format."""

    version = 1.0
    config = {'file_name': ''}

    d = '/data2/marberi/data'
    def run(self):
        assert self.config['file_name'], 'No file_name specified'

        path = os.path.join(self.d, self.config['file_name'])
        cat = pd.io.api.read_csv(path)

        self.job.result = cat
