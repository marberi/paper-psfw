#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd

from scipy.interpolate import splev, splrep
from scipy.ndimage.interpolation import zoom
from matplotlib import pyplot as plt

class r2pz:
    """Estimate the PSF size based on the photo-z."""

    version = 1.0
    config = {'r2_filter': 'vis', 'use_ml': False}

    def _load_r2vz_spls(self, seds):
        """Load splines with the relation between redshift and
           the PSF size.
        """

        df = self.job.r2vz.result
        fname = self.config['r2_filter']

        z = df['z']
        spls = {}
        for sed in seds:
            val = df['/{0}/{1}'.format(fname, sed)]
            spls[sed] = splrep(z, val)

        return spls

    def r2_peak(self, spls, pzdata, pzmeta):
        """Reconstruct the peak using the peak from the photoz
           code.
        """
        pzcat = pzdata['default']
        pzcat['fake_id'] = np.arange(len(pzcat))

        r2 = -np.ones(len(pzcat))

        if self.config['use_ml']:
            z_use = 'z_ml'
            t_use = 't_ml'
        else:
            z_use = 'zb'
            t_use = 't_b'


        seds = list(pzmeta['seds'])
        bins = range(len(seds))

        gr = pzcat.groupby(pd.cut(pzcat[t_use], bins, right=False))
        for i,(key,val) in enumerate(gr):

            if not len(val):
                continue

            spl1 = spls[seds[i]]
            spl2 = spls[seds[i+1]]

            dt = val[t_use] - i

            r2[val.fake_id] = (1-dt)*splev(val[z_use], spl1) + \
                                dt*splev(val[z_use], spl2)

        val = pzcat[pzcat[t_use] == len(pzmeta['seds']) - 1]
        if len(val):
            r2[val.fake_id] = splev(val[z_use], spls[seds[-1]])

        # Test what is going on....
        import ipdb
        hmm = -np.ones(len(pzcat))
        A = set()
        for i,(key,val) in enumerate(gr):
            A.update(set(val.index))

            hmm[val.fake_id] = i


        # Catching internal errors.
        assert not (r2 == -1).any()

        return r2

    def r2_pdf(self, spls, pzdata, pzmeta):
        """R^2 calculated from the p(z) and best fit type."""

        pzcat = pzdata['default']
        pzcat['sub_id'] = np.arange(len(pzcat))
        r2 = -np.ones(len(pzcat))

        A = []
        dz = pzmeta['dz']
        z = pzmeta['z']
        for sed in pzmeta['seds']:
            A.append(dz*splev(z, spls[sed]))

        bins = range(len(pzmeta['seds']))

        # Joining since I need the type inside for each subset.
        cat = pzdata['pzpdf'].join(pzcat)
        N = len(pzdata['pzpdf'].columns)

        for i,(key,val) in enumerate(cat.groupby(pd.cut(cat.t_b, bins, right=False))):
            if not len(val):
                continue

            sub = val[range(N)]
            dt = val.t_b - i

            r2[val.sub_id] = (1-dt)*(sub*A[i]).sum(axis=1) + \
                              dt*(sub*A[i+1]).sum(axis=1)

        
        # Special case for the right edge. 
        val = cat[cat.t_b == len(pzmeta['seds']) - 1]
        if len(val):
            sub = val[range(N)]
            r2[val.sub_id] = (sub*A[-1]).sum(axis=1)

        assert not (r2 == -1).any()

        return r2

    def r2_pdf_type(self, spls, pzdata, pzmeta):
        """Weight by a redshift-type 2D pdf."""

        seds = pzmeta['seds']
        X_orig = np.zeros((len(pzmeta['z']), len(seds)))
        for i,sed in enumerate(seds):
            r2sed_spl = spls[sed] 
            X_orig[:,i] = splev(pzmeta['z'], r2sed_spl)

        X_orig = (X_orig.T * np.array(pzmeta['dz'])).T


        # Linear interpolation between splines.
        ntypes_orig = len(seds)
        nt = pzmeta['interp']*(ntypes_orig - 1) + ntypes_orig
        zoom_fac = (1, float(nt) / ntypes_orig)

        X = zoom(X_orig, zoom_fac, order=1)

        # order, galaxy, redshift, sed
        pdfs = pzdata['pzpdf_type']
        r2rec = np.einsum('gzt, zt-> g', pdfs, X)

        return r2rec

    def _discover_dimentions(self): #, pztypes):
        """Determine the number of SEDs and redshift bins used in
           the photo-z run.
        """

        # This part should not be needed. Previously the tables
        # was stored in wide tables and one could directly open
        # them. Unpacking was fine when reading in the full table.
        # When reading in all columns, not so. Especially if reading
        # all the three formats are the same time (default, pzpdf,
        # pzpdf_type).


        pzjob = self.job.depend['pzcat']
        store = pzjob.get_store()

        ND = {}
        if 'default' in store:
            X = store.select('default', stop=1000).unstack()

            assert len(X.shape) == 2, 'Should be a 2D (galid, col) table.'
            ND['ncol'] = X.shape[1]

        # These are always accessed later, although not always needed.
        nsed = len(store['seds'])
        ninterp = self.job.pzcat.config['interp']
        ND['nt'] = nsed+(nsed-1)*ninterp

        ND['nz'] = len(store['z'])

        store.close()

        return ND

    def run(self):
        L = {'default': ('r2peak', self.r2_peak),
             'pzpdf': ('r2pdf', self.r2_pdf),
             'pzpdf_type': ('r2pdf_type', self.r2_pdf_type)}

        pzjob = self.job.depend['pzcat']
        interp = pzjob.config['interp']


        ND = self._discover_dimentions()

#        self.test_panel(ND)

        r2eff_all = self.job.r2gal.result['r2eff']
        pzdf_store = self.job.pzcat.get_store()

#        pzdf_store = self.job.pzcat.result['pzcatdf'] #FISK

        # Some additional information.
        pzmeta = {}
        pzmeta['interp'] = interp
        for field in ['z', 'dz', 'seds']:
            if field in pzdf_store:
                pzmeta[field] = pzdf_store[field]


#        Ngal = int(1e5)

        Ngal = 200
        r2eff_store = self.job.r2gal.get_store()
        r2eff_iter = r2eff_store.select('default', iterator=True, chunksize=Ngal)
        r2eff_iter = iter(r2eff_iter)


#        Nseds = len(pzmeta['seds'])
#        Nt = (interp+1)*(Nseds - 1) + 1
#        Ncol = 12

        # Much lower batches than before to handle the pzpdf_type plots
        NX = {'default': Ngal*ND['ncol'],
              'pzpdf': Ngal*ND['nz'],
              'pzpdf_type': Ngal*ND['nz']*ND['nt']}

#        import ipdb
#        ipdb.set_trace()


        # The important input
        K = {}
        for pztype in L.keys():
            if not pztype in pzdf_store:
                continue

            N = NX[pztype] #Ngal*Nt if pztype == 'pzpdf_type' else Ngal
            K[pztype] = iter(pzdf_store.select(pztype, iterator=True, chunksize=N))

        typeL = list(K.keys())
#        W = K['pzpdf_type']
#        E = W.next()
#        import ipdb
#        ipdb.set_trace()

        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        spls = self._load_r2vz_spls(pzmeta['seds'])

#        typeL = ['pzcat']
        while True:
            try:
                X1 = r2eff_iter.next()
            except StopIteration: 
                break

            r2eff = X1['r2eff']

            # Reading in everything first.
            pzdata = {}
            for pztype in typeL:
                pzdata[pztype] = K[pztype].next()

            pzdata['default'] = pzdata['default'].unstack()


            if 'pzpdf' in pzdata:
                pzdata['pzpdf'] = pzdata['pzpdf'].unstack()

            if 'pzpdf_type' in pzdata:
                pzdata['pzpdf_type'] = pzdata['pzpdf_type'].unstack().to_panel()

                # Important test... this should never fail..
                assert not np.isnan(pzdata['pzpdf_type'].values).any()

            # This should be changed in the photo-z code, but by now it is only
            # changed here.
            assert 'default' in pzdata, 'The peak values is needed'
            pzdata['default'].t_b -= 1           
            pzdata['default'].t_ml -= 1           

            hasnan = np.isnan(pzdata['default'].zs).any()
            assert not hasnan, 'Internal error.'

            D = {}
            for pztype in typeL: 
                r2name, f = L[pztype]
                r2rec = f(spls, pzdata, pzmeta)

                assert not np.isnan(r2rec).any()
                D['{0}_abs'.format(r2name)] = r2rec
                D[r2name] = (r2rec - r2eff) / r2eff

                print(D)

            store_out.append('default', pd.DataFrame(D))

        store_out.close()
