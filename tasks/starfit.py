#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


from numba import jit

@jit
def calc_python(chi2, col_model, mask, col, H):
    """Estimate the chi2 for different stellar SED by minimizing to
       the colors.
    """

    # When benchmarking ~17000 galaxies from 2 KiDS pointins, I found
    #  Python: 51.82
    #  Numba: 0.37
    #  c++: 0.15
    # meaning the numba version is sufficiently fast.

    ngal,nsed  = chi2.shape
    nf = col.shape[1]

    for x in range(ngal):
        for y in range(nsed):
            nobs = 0.
            chi_f = 0.

            for i in range(nf):
                if not mask[x,i]:
                    continue

                chi_f = chi_f + H[x,i]*pow(col[x, i]-col_model[y,i], 2) 
                nobs = nobs + 1.


            if not nobs:
                continue

            chi2[x,y] = chi_f / nobs

    return chi2


class starfit:
    """TODO"""

    version = 1.01
    config = {'filters':  ['u', 'g', 'r', 'i']}

    def limit_cols(self):
        def tolist(pre, L):
            return [pre+x.upper() for x in filters]

        filters = self.config['filters']
        mag_cols = tolist('MAG_AUTO_', filters)
        err_cols = tolist('MAGERR_AUTO_', filters)
        other = ['CLASS_STAR']
        cols = mag_cols + err_cols + other

        return mag_cols, err_cols

    def make_model(self, row):
        """Create a model."""

        sedL = []
        for key in row.keys():
            if not key.startswith('/u'):
                continue

            sedL.append(key.split('/')[2])

        # Use an array for storing the mode at z=0.
        assert row.z == 0.
        filters = self.config['filters']
        model = np.zeros((len(filters), len(sedL)))
        for i,fname in enumerate(filters):
            for j,sed in enumerate(sedL):
                key = '/{0}/{1}'.format(fname, sed)
                model[i,j] = row[key]

        return model, sedL


    def fit(self, model, cat, sedL, mag_cols, err_cols):
        """Perform a chi2 fit to the model."""

        # Converting to magnitudes instead.
        model = model.T
        model = -2.5*np.log10(model)


        mag = cat[mag_cols].values
        err = cat[err_cols].values
        _var = err**2

        # Note: Var(X-Y) = Var(X)+Var(Y)
        col = mag[:, :-1]-mag[:, 1:]
        col_var = _var[:, :-1] + _var[:, 1:]

        col_model = model[:, :-1]-model[:, 1:]

        # Remove some of the observations. Inversting the variance
        # makes handling errors easier.
        np.seterr(divide='ignore')
        mask = 1.*np.logical_not(np.isnan(col))
        H = 1./col_var
        np.seterr(divide='raise')

        ngal = col.shape[0]
        nsed = col_model.shape[0]
        chi2 = np.zeros((ngal, nsed))

        t1 = time.time()
        chi2 = calc_python(chi2, col_model, mask, col, H)
        t2 = time.time()

        chi2_df = pd.DataFrame(chi2, columns=sedL, index=cat.index)
        chi2_df['nobs'] = mask.sum(axis=1)

        print('time fit', t2-t1)
        return chi2_df

    def run(self):
        store = self.job.depend['catalog'].get_store()
        reader = store.select('default', chunksize=1e4)

        # The magnitude model
        ab = self.job.depend['ab'].result
        row = ab.irow(0)
        model, sedL = self.make_model(row)

        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        mag_cols, err_cols = self.limit_cols()
        for i,cat_in in enumerate(reader):
            print('chunk', i)

            chi2_part = self.fit(model, cat_in, sedL, mag_cols, err_cols)
            store_out.append('default', chi2_part)
