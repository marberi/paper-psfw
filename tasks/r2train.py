#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import pdb
import numpy as np
import sklearn
import tables
import pandas as pd
import sklearn.ensemble as en
from sklearn import svm,neighbors
from sklearn import linear_model
from matplotlib import pyplot as plt

from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.pipeline import Pipeline
#from sklearn.cross_validation import Bootstrap

# Pandas is complaining when doing an outer join. Ignoring the
# errors appears to work.
np.seterr(invalid='ignore')

reg_map = {'linear': ('linear_model', 'LinearRegression'),
           'elasticnet': ('linear_model', 'ElasticNet'),
           'svr': ('svm', 'SVR'), 
           'nusvr': ('svm', 'NuSVR'),
           'kneigh': ('neighbors', 'KNeighborsRegressor'),
           'radneigh': ('neighbors', 'RadiusNeighborsRegressor')
}

class r2train:
    """Estimate the PSF size from training on the photometry."""

    version = 1.11
    config = {'filters':  ['vis', 'r', 'i', 'Y', 'J', 'H'],
              'use_stars': False,
              'use_colors': True,
              'Ntrain': 20000,
              'Ntest': 30000,
              'Nboot': 2,
              'all_test': False,
              'reg': 'nusvr',
              'opts': {},
              'lim_vis': 24.5
             }

    def prepare_input(self): 
        """Convert the data structures."""

        # Can probably use a common interface. Created before having a common
        # naming scheme.

        magcat_test = self.job.galcat.result 
        test_cat = magcat_test.join(self.job.r2gal.result) 
        if self.config['use_stars']:
            train_magcat = self.job.train_star_cat.result
            train_cat = train_magcat.join(self.job.train_star_r2.result)
        else:
            train_magcat = self.job.train_gal_cat.result
            train_cat = train_magcat.join(self.job.train_gal_r2.result)


        filters = self.config['filters']
        mag_cols = map('mag_{0}'.format, filters)

        Atrain = train_cat[mag_cols].values
        Atest = test_cat[mag_cols].values

        if self.config['use_colors']:
            Atrain = Atrain[:,1:] - Atrain[:,:-1]       
            Atest = Atest[:,1:] - Atest[:,:-1]       

        Btrain = train_cat.r2eff.values
        Btest = test_cat.r2eff.values

        # Just in case.
        assert len(Atrain) == len(Btrain) 
        assert len(Atest) == len(Btest) 

        return test_cat, Atrain, Atest, Btrain, Btest


    def _bootstrap(self, train_tot, test_tot):
        # Rewrite of a method which was needed when using a subset of the
        # galaxies for training.

        # Number of objects.
        Ntrain = self.config['Ntrain']
        Ntest = test_tot if self.config['all_test'] else self.config['Ntest']
        Nboot = self.config['Nboot']


        rall_train = np.arange(train_tot)
        rall_test = np.arange(test_tot)

        for i in range(Nboot):
            np.random.shuffle(rall_train)
            np.random.shuffle(rall_test)

            train_index = rall_train[:Ntrain].copy()
            test_index = rall_test[:Ntest].copy()

            yield train_index, test_index

                
    def _reg_cls(self):
        """Map from regressor name to the class."""

        reg_type = self.config['reg']
        assert reg_type in reg_map, 'No such regressor: '+reg_type
        reg_cls = sklearn
        for x in reg_map[reg_type]:
            reg_cls = getattr(reg_cls, x)

        assert hasattr(reg_cls, 'fit'), 'Something went wrong'

        return reg_cls

    def estimate_r2(self, test_cat, A_trainX, A_testX, B_trainX, B_testX):
        """Estimate the effective R^2 from input magnituted using training
           methods.
        """

        bs = list(self._bootstrap(len(A_trainX), len(A_testX)))
        reg_cls = self._reg_cls()

        res_pred = []
        res_true = []
        res_delta = []

        df_delta = pd.DataFrame()
        df_pred = pd.DataFrame()
        df_true = pd.DataFrame()


        A = []        
        for i,(train_index, test_index) in enumerate(bs):
            print('i', i)

            # A bit verbose...
            A_train = A_trainX[train_index]
            A_test = A_testX[test_index]
            B_train = B_trainX[train_index]
            B_test = B_testX[test_index]

            reg = reg_cls(**self.config['opts'])
            reg.fit(A_train, B_train)

            B_pred = reg.predict(A_test)
            delta_r2 = (B_pred - B_test) / B_test

            tindx = test_cat.index[test_index]

            res_delta.append(pd.Series(delta_r2, index=tindx))
            res_pred.append(pd.Series(B_pred, index=tindx))
            res_true.append(pd.Series(B_test, index=tindx))


        res = {'r2eff': pd.concat(res_delta, axis=1),
               'r2pred': pd.concat(res_pred, axis=1),
               'r2true': pd.concat(res_true, axis=1)}


        return res


    def write_output(self, file_path, res):
        # I should perhaps update the underlying framework to deal with this
        # in a better way.
        store = pd.HDFStore(file_path, 'w')
        store['r2eff'] = res['r2eff']
        store['r2pred'] = res['r2pred']
        store['r2true'] = res['r2true']
        store.close()

    def run(self):

        X = self.prepare_input()
        df = self.estimate_r2(*X)

        file_path = self.job.empty_file('default')
        self.write_output(file_path, df)
