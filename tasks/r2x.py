#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import pdb
import numpy as np
import sklearn
import tables
import pandas as pd
import sklearn.ensemble as en
from sklearn import svm,neighbors
from sklearn import linear_model
#from matplotlib import pyplot as plt

from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.pipeline import Pipeline
#from sklearn.cross_validation import Bootstrap

# Pandas is complaining when doing an outer join. Ignoring the
# errors appears to work.
np.seterr(invalid='ignore')

reg_map = {'linear': ('linear_model', 'LinearRegression'),
           'elasticnet': ('linear_model', 'ElasticNet'),
           'svr': ('svm', 'SVR'), 
           'nusvr': ('svm', 'NuSVR'),
           'kneigh': ('neighbors', 'KNeighborsRegressor'),
           'radneigh': ('neighbors', 'RadiusNeighborsRegressor')
}

descr = {\
  'filters': 'Filters to train with',
  'use_cal': 'Use calibration sample',
  'coff_start': 'First color offset step',
  'z_cal' : 'Redshift field in the calibration sample',
  'z_test' : 'Redshift field in the test sample',
  'lim_vis': 'Magnitude cut in the calibration sample',
  'to_minimize': 'Which field that should be minimized'
}

class RegShifted:
    """The prediction when including a color shift."""

    def __init__(self, reg, coff, train_cols, bin_by, use_cal):
        self._reg = reg
        self.coff = coff
        self.bin_by = bin_by
        self.use_cal = use_cal

        if use_cal:
            train_cols = copy.copy(train_cols)
            train_cols[0] = 'c0_tmp'
            self.train_cols = train_cols
        else:
            self.train_cols = train_cols

    def predict(self, A):
        if self.use_cal:
            A = A.join(self.coff, on=self.bin_by)
            A.ix[np.isnan(A.col_off), 'col_off'] = 0. # Setting default value.
            A['c0_tmp'] = A.c0 + A.col_off

        r2pred = self._reg.predict(A[self.train_cols])
        r2pred = pd.Series(r2pred, index=A.index)

        return r2pred

class r2x:
    """TODO"""

    # Updated version of r2x which also supports:
    # - Can minimize based on the predicted R^2 size instead of
    #   the r2bias.
    # - Can used the updated star catalogs.
    # - Iterate over batches of galaxies.

    version = 1.1
    config = {'filters':  ['r', 'i', 'z'], 
              'use_stars': False,
              'use_colors': True,
              'Ntrain': 20000,
              'Ntest': 30000,
              'Nboot': 2,
              'all_test': False,
              'reg': 'nusvr',
              'opts': {},
              'lim_vis': 24.5,

              'use_cal': True,
              'coff_start': 0.05,
              'z_cal': 'zs',
              'z_test': 'zs',
              'noise_cal': True,
              'bin_by': 'zbin',
              'to_minimize': 'r2bias',
              'Nsteps': 3
             }


    def _add_colors(self, cat_type, cat):
        """Add colors"""

        filters = self.config['filters']

        # Refuse to overwrite columns. That could cause hard
        # to detect problems.
        new_cols = map('c{0}'.format, range(len(filters)-1))
        overlap = set(new_cols) & set(cat.columns)
        assert not overlap, 'Overlapping columns: {0}'.format(overlap)

        fmt = 'mag_{0}'.format
        for col_name, f1, f2, in zip(new_cols, filters[:-1], filters[1:]):
            cat[col_name] = cat[fmt(f1)] - cat[fmt(f2)]

    def _add_zbins(self, cat_type, cat):
        """Add a z-bin column to all of the catalogs."""

        zedges_cal = np.linspace(0.1, 1.8, 10)
        # We are not binning the training sample.
        if not cat_type in ['cal', 'test']:
            return 

        # Here we manually assign bin labels since it otherwise
        # fails. And is also complained before converting to 
        # numpy array.
        zfield = self.config['z_{0}'.format(cat_type)]
        lbls = range(len(zedges_cal)-1)
        cat['zbin'] = np.array(pd.cut(cat[zfield], zedges_cal, labels=lbls))

        return cat

    def _add_colbins(self, cat_type, cat):
        """Add a color-bin column."""

        c0edges = np.linspace(-1., 2., 10)

        # We are not binning the training sample.
        if not cat_type in ['cal', 'test']:
            return

        # See the commends in '_add_zbins'
        lbls = range(len(c0edges)-1)
        cat['c0bin'] = np.array(pd.cut(cat.c0, c0edges, labels=lbls))

    def _fix_input(self, cat_type, cat):
        """This part is common for all catalogs."""

        self._add_colors(cat_type, cat)
        self._add_zbins(cat_type, cat)
        self._add_colbins(cat_type, cat)

        cat['r2true'] = cat['r2eff']


        lim_vis = self.config['lim_vis']
        if cat_type == 'cal' and lim_vis:
            cat = cat[cat.mag_vis < lim_vis]


        return cat

    def train_input(self):
        """Load the training and the calibration sample."""

        # The code always needs the full training and calibration
        # sample.
        pre = 'train_gal' # example
        pre_train = 'train_star' if self.config['use_stars'] else \
                    'train_gal'

        # Allows for dropping the calibration catalog when it's
        # actually now used.
        toload = [('train', pre_train)]
        if self.config['use_cal']:
            toload += [('cal', 'cal')]

        inputD = {}
        for cat_type,pre in toload:
            # Need to use the noiseless catalog when noise_cal is False.
            cat_job = self.job.depend['{0}_cat'.format(pre)]
            if cat_type == 'cal' and not self.config['noise_cal']:
                print('Disabling noise..')

                cat_job = cat_job.magcat

            cat = cat_job.result
            cat = cat.join(self.job.depend['{0}_r2'.format(pre)].result)

            pzkey = '{0}_pz'.format(pre)
            if pzkey in self.job.depend:
                cat = cat.join(self.job.depend[pzkey].result.unstack(), rsuffix='_ignore')

            inputD[cat_type] = self._fix_input(cat_type, cat)

        return inputD

    def find_cols(self):
        filters = self.config['filters']
        if self.config['use_colors']:
            train_cols = map('c{0}'.format, range(len(filters)-1))
        else:
            train_cols = map('mag_{0}'.format, filters)

        pred_col = 'r2eff'

        return train_cols, pred_col

                
    def _reg_cls(self):
        """Map from regressor name to the class."""

        reg_type = self.config['reg']
        assert reg_type in reg_map, 'No such regressor: '+reg_type
        reg_cls = sklearn
        for x in reg_map[reg_type]:
            reg_cls = getattr(reg_cls, x)

        assert hasattr(reg_cls, 'fit'), 'Something went wrong'

        return reg_cls

    def next_calc_step(self, df_cal):
        """Determine the next step using a Newton downhill method."""

        sub = df_cal.groupby(self.config['bin_by']).mean()


        tomin = self.config['to_minimize']
        sub['a'] = (sub[tomin+'1'] - sub[tomin+'0']) / (sub.coff1 - sub.coff0)
        sub['b'] = sub[tomin+'0'] - sub.a*sub.coff0

        sub['col_off'] = -sub.b/sub.a


        return sub.col_off

    def core_find_offset(self, reg, df_cal):
        """Determine the optimal r-band offset."""

        train_cols = map('c{0}'.format, range(len(self.config['filters'])-1))
        train_cols[0] = 'c0_tmp'

        Nsteps = self.config['Nsteps']
        assert Nsteps
        for istep in range(Nsteps+1):
            if istep == 0:
                df_cal['coff0'] = 0.
                df_cal['c0_tmp'] = df_cal.c0
            elif istep == 1:
                df_cal['coff1'] = self.config['coff_start']
                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1
            elif istep < Nsteps+1:
                coff = self.next_calc_step(df_cal)

                if 'col_off' in df_cal:
                    del df_cal['col_off']

                df_cal = df_cal.join(coff, on=self.config['bin_by'])
                df_cal.ix[np.isnan(df_cal.col_off), 'col_off'] = 0. # Setting default value.
                df_cal['coff1'] = df_cal['col_off']

                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1


            col_tmp = df_cal[train_cols].values
            r2pred = pd.Series(reg.predict(col_tmp), index=df_cal.index)
            r2bias = (r2pred - df_cal.r2true)/df_cal.r2true
            r2diff = (r2pred - df_cal.r2true)

            nr = 0 if istep == 0 else 1
            df_cal['r2pred{0}'.format(nr)] = r2pred
            df_cal['r2bias{0}'.format(nr)] = r2bias
            df_cal['r2diff{0}'.format(nr)] = r2diff

        return coff

    def sample_train(self, train_cat):
        """Sample from the training catalog."""

        Ntrain = self.config['Ntrain']
        Nboot = self.config['Nboot']
        assert not isinstance(Ntrain, bool), 'Ntrain should be an integrer'
        assert not isinstance(Nboot, bool), 'Nboot should be an integrer'

        has_point = 'point' in train_cat.columns
        for i in range(Nboot):
            tmp = train_cat[train_cat.point == i] if has_point else train_cat
            sub_train = tmp.sample(n=Ntrain)
        
            yield sub_train

    def sample_test(self, test_cat):
        """Sample from the test catalog."""

        Ntest = self.config['Ntest']
        Nboot = self.config['Nboot']
        assert not isinstance(Ntest, bool), 'Ntest should be an integrer'
        assert not isinstance(Nboot, bool), 'Nboot should be an integrer'

        all_test = self.config['all_test']
        for i in range(Nboot):
            sub_test = test_cat if all_test else test_cat.sample(n=Ntest)

            yield sub_test


    def create_reg(self, inputD):
        """Estimate the effective R^2 from input magnituted using training
           methods.
        """


        train_cat = inputD['train']
        reg_cls = self._reg_cls()

        xtrain_cols = map('c{0}'.format, range(len(self.config['filters'])-1))
        if self.config['use_cal']:
            xtrain_cols[0] = 'c0_tmp'

        # To avoid passing everything.
        reg_conf = {}
        pass_key = ['bin_by', 'use_cal']
        for key in pass_key:
            reg_conf[key] = self.config[key]

        regD = {}
        cs_train = self.sample_train(train_cat)

        for i, sub_train in enumerate(cs_train):
            print('training', i)

            reg = reg_cls(**self.config['opts'])
            reg.fit(sub_train[self.train_cols], sub_train[self.pred_col])

            # Calibrating against the simulations for each pointing.
            if self.config['use_cal']:
                cal_cat = inputD['cal']
                coff = self.core_find_offset(reg, cal_cat)
            else:
                coff = False

            # We are using this wrapper for all cases.
            regD[i] = RegShifted(reg, coff, self.train_cols, **reg_conf)

        return regD

    def estimate_r2(self, regD, sub_test):
        D = {}
        for key in ['r2true', 'r2pred', 'r2eff', 'r2diff']:
            D[key] = pd.DataFrame(index=sub_test.index)

        fields = regD.keys()

        for i in regD.keys():
            reg = regD[i]

            r2true = sub_test[self.pred_col]
            r2pred = reg.predict(sub_test[self.train_cols+['c0bin', 'zbin']])
            r2bias = (r2pred - r2true) / r2true
            r2diff = (r2pred - r2true) 

            D['r2true'][i] = r2true
            D['r2pred'][i] = r2pred
            D['r2eff'][i] = r2bias
            D['r2diff'][i] = r2diff

        return D

    def run(self):
        # To avoid these being set differently in various
        # parts of the program.

        chunksize = int(1e5)

        # Galaxy catalog
        store_cat = self.job.test_cat.get_store()
        R_cat = store_cat.select('default', iterator=True, chunksize=chunksize)
        I_cat = iter(R_cat)

        # Galaxy R^2 values.
        store_r2 = self.job.test_r2.get_store()
        R_r2 = store_r2.select('default', iterator=True, chunksize=chunksize)
        I_r2 = iter(R_r2)

        if self.config['z_test'] == 'zb':
            Ncols = 12
            store_pz = self.job.test_pz.get_store()
            R_pz = store_pz.select('default', iterator=True, chunksize=chunksize*Ncols)
            I_pz = iter(R_pz)

        self.train_cols, self.pred_col = self.find_cols()
        inputD = self.train_input()
        regD = self.create_reg(inputD)

        # Where to store the output.
        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        i = 0
        while True:
            try:
                print('iteration', i)
                cat_in = I_cat.next()
                cat_in = cat_in.join(I_r2.next())
                if self.config['z_test'] == 'zb':
                    pz_part = I_pz.next().unstack()
                    cat_in = cat_in.join(pz_part, rsuffix='_ignore')

                    assert len(pz_part.columns) == Ncols, 'Internal test..'
                    assert not np.isnan(cat_in).any().any(), 'Internal test..'

                self._fix_input('test', cat_in)
                X = self.estimate_r2(regD, cat_in)

                for key,df in X.iteritems():
                    print('key', key)
                    store_out.append(key, df)

                i += 1
            except StopIteration: 
                break
        store_cat.close()
        store_r2.close()
        store_out.close()
