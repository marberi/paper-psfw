#!/usr/bin/env python
# encoding: UTF8

import os
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import splrep, splev, splint
from sklearn.linear_model import LinearRegression
from scipy.integrate import simps

class r2fit:
    """TODO"""

    version = 1.0
    config = {}

    def load_filter(self, fname):
        filter_dir = '/data2/photoz/run/data/des_euclid'
        fmt = '{0}.res'

        file_path = os.path.join(filter_dir, fmt.format(fname))
        return np.loadtxt(file_path).T

    def mean_wave(self, filters):
      
        xmD = {}
        for fname in filters:
            x,y = self.load_filter(fname)
            spl0 = splrep(x,y)
            spl1 = splrep(x,x*y)
            xmD[fname] = splint(x[0], x[-1], spl1) / splint(x[0], x[-1], spl0)

        return xmD

    def find_factors(self, l0):
        l, phi_l = self.load_filter('vis')

        # Assumes the same normalization and values as in the other module.
        R_l = l**0.55 
        K0 = simps(phi_l*R_l, l)
        K1 = simps(phi_l*R_l*(l-l0), l)
        L0 = simps(phi_l, l)
        L1 = simps(phi_l*(l-l0), l)

        return K0, K1, L0, L1

    def width(self, filters):
        for fname in filters:
            x,y = self.load_filter(fname)

            raise

    def test1(self, xm, Y):
        N = 10 
        for i in range(N):
            plt.plot(xm, Y[i,:])

        plt.show()


    def run(self):
        cat_store = self.job.kids['galcat']['magcat']
        cat = cat_store['cat']
        cat_store.close()

        filters = ['r', 'i', 'z']
        xmD = self.mean_wave(filters+['vis'])
        xm = np.array([xmD[fname] for fname in filters])
        xm = xm - xmD['vis']

        fmt_mag = 'mag_{0}'

        F = map(fmt_mag.format, filters)
        sub = cat[F]
        subA = np.array(sub)

        fA = 10**(subA/2.5)

        x0 = xmD['vis']
        wD = self.width(filters)
        K0, K1, L0, L1 = self.find_factors(x0)
        res = np.zeros((len(cat), 2))
        t1 = time.time()
        N = 1000
        for i in range(N):
            res[i] = np.polyfit(xm, fA[i,:], 1)

        res = res[:N]
        
        R2 = (res[:,0]*K0 +res[:,1]*K1)/(res[:,0]*L0 + res[:,1]*L1)

        S = pd.Series(R2)
        S = S[S.abs() < 2e9]

#        S = S[S.abs() < 0.2]
        S.hist(bins=30)
        plt.show()

        t2 = time.time()
        print('time', t2-t1)

        reg = LinearRegression(fit_intercept=True)

