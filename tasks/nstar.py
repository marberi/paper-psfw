#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd
from sklearn import neighbors, svm

class nstar:
    """TODO"""

    version = 1.0
    config = {}

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def run(self):
        starcat = self.job.starcat.result
        gc = self.job.galcat.result

        Ntrain = 400
        fL = ['r', 'i', 'z']

        Astar, _ = self.get_col(starcat, fL)
        col_gal, mag_gal, err_gal = self.get_col(gc, fL, True)

        inds_train = np.random.permutation(range(len(Astar)))[:Ntrain]
        Atrain = Astar[inds_train]
        _fake = np.ones(len(Atrain)) 
        reg = neighbors.KNeighborsRegressor(n_neighbors=1)
        reg.fit(Atrain, _fake)

        # Neded some index magic.
        inds = reg.kneighbors(col_gal)[1][:,0]
        Adiff = Atrain[inds] - col_gal

        cols = ['coldiff_{0}_{1}'.format(fL[i], fL[i+1]) for i in \
                range(len(fL)-1)]

        self.job.result = pd.DataFrame(Adiff, columns=cols)
