#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import pdb
import numpy as np
import sklearn
import tables
import pandas as pd
import sklearn.ensemble as en
from sklearn import linear_model
#from sklearn.cross_validation import Bootstrap

from matplotlib import pyplot as plt


class r2diag:
    """Diagnostic test on the input data."""

    version = 1.0
    config = {'filters':  ['vis', 'r', 'i', 'Y', 'J', 'H'],
              'use_colors': True,
              'Ntrain': 20000,
              'Ntest': 30000,
              'Nboot': 2,
              'opts': {}
             }


    def load_train(self):
        cat_store = self.job.kids['train_cat']['magcat']
        cat = cat_store['cat']
        cat_store.close()

        r2_store = self.job.kids['train_r2']['r2eff']
        r2 = r2_store['r2eff']
        r2_store.close()

        return cat, r2


    def prepare_input(self): 
        """Convert the data structures."""

        cat, r2= self.load_train()
        filters = self.config['filters']

        A = np.array(cat[['mag_{0}'.format(x) for x in filters]])
        if self.config['use_colors']:
            A = A[:,1:] - A[:,:-1]

        B = np.array(r2['r2eff'])

        return A, B


    def _bootstrap(self, train_tot, test_tot):
        # When having stars the training and validation sample is separate.

        Ntrain = self.config['Ntrain']
        Ntrain = Ntrain if Ntrain else train_tot
        Ntest = self.config['Ntest']
        Nboot = self.config['Nboot']

        if self.config['use_stars']:
            Ntest = Ntest if Ntest else test_tot
            for i in range(Nboot):
                train_index = self._rsub(train_tot, Ntrain)
                test_index = self._rsub(test_tot, Ntest)

                yield train_index, test_index
        else:
            Ntest = train_tot - Ntrain if not Ntest else Ntest
            bs = Bootstrap(train_tot, Nboot, Ntrain, Ntest)
            for x in bs:
                yield x


    def estimate_fit(self, A, B):
        """Estimate the linear fit to R^2 and store coefficients."""


        Nboot = self.config['Nboot']
        Ntrain = self.config['Ntrain']
        bs = Bootstrap(len(A), Nboot, Ntrain)

        df = pd.DataFrame()

        # The number of coefficients is determined by the number of
        # input pararmaters.
        columns = ['b']+['a'+str(i) for i in range(A.shape[1])]
        print('Before')
        for train_index, test_index in bs:
            reg = linear_model.LinearRegression()
            reg.fit(A[train_index], B[train_index])
            L = [reg.intercept_] + list(reg.coef_)

            df = df.append(pd.Series(L, index=columns), ignore_index=True) 

        return df

    def run(self):
        A,B = self.prepare_input()

        self.job.result = self.estimate_fit(A,B)
