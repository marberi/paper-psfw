#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import pdb
import numpy as np
import sklearn
import tables
import pandas as pd
import sklearn.ensemble as en
from sklearn import svm,neighbors
from sklearn import linear_model
from matplotlib import pyplot as plt

from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.pipeline import Pipeline
#from sklearn.cross_validation import Bootstrap

# Pandas is complaining when doing an outer join. Ignoring the
# errors appears to work.
np.seterr(invalid='ignore')

reg_map = {'linear': ('linear_model', 'LinearRegression'),
           'elasticnet': ('linear_model', 'ElasticNet'),
           'svr': ('svm', 'SVR'), 
           'nusvr': ('svm', 'NuSVR'),
           'kneigh': ('neighbors', 'KNeighborsRegressor'),
           'radneigh': ('neighbors', 'RadiusNeighborsRegressor')
}

descr = {\
  'filters': 'Filters to train with',
  'use_cal': 'Use calibration sample',
  'coff_start': 'First color offset step',
  'z_cal' : 'Redshift field in the calibration sample',
  'z_test' : 'Redshift field in the test sample',
  'lim_vis': 'Magnitude cut in the calibration sample',
  'to_minimize': 'Which field that should be minimized'
}

class r2next:
    """TODO"""
    # This version has the following differences compared to r2x:
    # - Can minimize based on the predicted R^2 size instead of
    #   the r2bias.
    # - Can used the updated star catalogs.

    version = 2.006
    config = {'filters':  ['r', 'i', 'z'], 
              'use_stars': False,
              'use_colors': True,
              'Ntrain': 20000,
              'Ntest': 30000,
              'Nboot': 2,
              'all_test': False,
              'reg': 'nusvr',
              'opts': {},
              'lim_vis': 24.5,

              'use_cal': True,
              'coff_start': 0.05,
              'z_cal': 'zs',
              'z_test': 'zs',
              'noise_cal': True,
              'bin_by': 'zbin',
              'to_minimize': 'r2bias',
              'Nsteps': 3
             }


    def _add_colors(self, cat):
        """Add colors"""

        filters = self.config['filters']

        # Refuse to overwrite columns. That could cause hard
        # to detect problems.
        new_cols = map('c{0}'.format, range(len(filters)-1))
        overlap = set(new_cols) & set(cat.columns)
        assert not overlap, 'Overlapping columns: {0}'.format(overlap)

        fmt = 'mag_{0}'.format
        for col_name, f1, f2, in zip(new_cols, filters[:-1], filters[1:]):
            cat[col_name] = cat[fmt(f1)] - cat[fmt(f2)]

    def _add_zbins(self, inputD):
        """Add a z-bin column to all of the catalogs."""

        zedges_cal = np.linspace(0.1, 1.8, 10)
        for key, cat in inputD.iteritems():
            # We are not binning the training sample.
            if not key in ['cal', 'test']:
                continue

            # Here we manually assign bin labels since it otherwise
            # fails. And is also complained before converting to 
            # numpy array.
            zfield = self.config['z_{0}'.format(key)]
            lbls = range(len(zedges_cal)-1)
            cat['zbin'] = np.array(pd.cut(cat[zfield], zedges_cal, labels=lbls))


    def _add_colbins(self, inputD):
        """Add a color-bin column."""

        c0edges = np.linspace(-1., 2., 10)
        for key, cat in inputD.iteritems():
            # We are not binning the training sample.
            if not key in ['cal', 'test']:
                continue

            # See the commends in '_add_zbins'
            lbls = range(len(c0edges)-1)
            cat['c0bin'] = np.array(pd.cut(cat.c0, c0edges, labels=lbls))


    def prepare_input(self): 
        """Read in all data needed for the PSF R^2 predictions."""

        pre = 'train_gal' # example
        pre_train = 'train_star' if self.config['use_stars'] else \
                    'train_gal'

        inputD = {}
        for key,pre in [('train', pre_train), ('test', 'test'), ('cal', 'cal')]:
            # Need to use the noiseless catalog when noise_cal is False.
            cat_job = self.job.depend['{0}_cat'.format(pre)]
            if key == 'cal' and not self.config['noise_cal']:
                print('Disabling noise..')

                cat_job = cat_job.magcat

            cat = cat_job.result
            cat = cat.join(self.job.depend['{0}_r2'.format(pre)].result)

            pzkey = '{0}_pz'.format(pre)
            if pzkey in self.job.depend:
                cat = cat.join(self.job.depend[pzkey].result, rsuffix='_ignore')

            cat['r2true'] = cat['r2eff']

            self._add_colors(cat)
            inputD[key] = cat


        self._add_zbins(inputD)
        self._add_colbins(inputD)

        lim_vis = self.config['lim_vis']
        if lim_vis:
            cal = inputD['cal']
            inputD['cal'] = cal[cal.mag_vis < lim_vis]

        return inputD

    def find_cols(self):
        filters = self.config['filters']
        if self.config['use_colors']:
            train_cols = map('c{0}'.format, range(len(filters)-1))
        else:
            train_cols = map('mag_{0}'.format, filters)

        pred_col = 'r2eff'

        return train_cols, pred_col

                
    def _reg_cls(self):
        """Map from regressor name to the class."""

        reg_type = self.config['reg']
        assert reg_type in reg_map, 'No such regressor: '+reg_type
        reg_cls = sklearn
        for x in reg_map[reg_type]:
            reg_cls = getattr(reg_cls, x)

        assert hasattr(reg_cls, 'fit'), 'Something went wrong'

        return reg_cls

    def next_calc_step(self, df_cal):
        """Determine the next step using a Newton downhill method."""

        sub = df_cal.groupby(self.config['bin_by']).mean()


        tomin = self.config['to_minimize']
        sub['a'] = (sub[tomin+'1'] - sub[tomin+'0']) / (sub.coff1 - sub.coff0)
        sub['b'] = sub[tomin+'0'] - sub.a*sub.coff0

        sub['col_off'] = -sub.b/sub.a


        return sub.col_off

    def core_find_offset(self, reg, df_cal):
        """Determine the optimal r-band offset."""

        train_cols = map('c{0}'.format, range(len(self.config['filters'])-1))
        train_cols[0] = 'c0_tmp'

        Nsteps = self.config['Nsteps']
        assert Nsteps
        for istep in range(Nsteps+1):
            if istep == 0:
                df_cal['coff0'] = 0.
                df_cal['c0_tmp'] = df_cal.c0
            elif istep == 1:
                df_cal['coff1'] = self.config['coff_start']
                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1
            elif istep < Nsteps+1:
                coff = self.next_calc_step(df_cal)

                if 'col_off' in df_cal:
                    del df_cal['col_off']

                df_cal = df_cal.join(coff, on=self.config['bin_by'])
                df_cal.ix[np.isnan(df_cal.col_off), 'col_off'] = 0. # Setting default value.
                df_cal['coff1'] = df_cal['col_off']

                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1


            col_tmp = df_cal[train_cols].values
            r2pred = pd.Series(reg.predict(col_tmp), index=df_cal.index)
            r2bias = (r2pred - df_cal.r2true)/df_cal.r2true
            r2diff = (r2pred - df_cal.r2true)

            nr = 0 if istep == 0 else 1
            df_cal['r2pred{0}'.format(nr)] = r2pred
            df_cal['r2bias{0}'.format(nr)] = r2bias
            df_cal['r2diff{0}'.format(nr)] = r2diff

        return coff

    def catalog_sample(self, train_cat, test_cat):
        # If these types are booleans it can give problems.
        Ntrain = self.config['Ntrain']
        Ntest = self.config['Ntest']
        Nboot = self.config['Nboot']
        assert not isinstance(Ntrain, bool), 'Ntrain should be an integrer'
        assert not isinstance(Ntest, bool), 'Ntest should be an integrer'
        assert not isinstance(Nboot, bool), 'Nboot should be an integrer'

        all_test = self.config['all_test']

        has_point = 'point' in train_cat.columns
        for i in range(Nboot):
            tmp = train_cat[train_cat.point == i] if has_point else train_cat
            sub_train = tmp.sample(n=Ntrain)
        
            sub_test = test_cat if all_test else test_cat.sample(n=Ntest)


            yield sub_train, sub_test

    def estimate_r2(self, inputD):
        """Estimate the effective R^2 from input magnituted using training
           methods.
        """

        train_cols, pred_col = self.find_cols()

        train_cat = inputD['train']
        test_cat = inputD['test']
        cal_cat = inputD['cal']

        reg_cls = self._reg_cls()

        # Again testing storing the information directly in DataFrames.
        df_r2true = pd.DataFrame(index=test_cat.index)
        df_r2pred = pd.DataFrame(index=test_cat.index)
        df_r2bias = pd.DataFrame(index=test_cat.index)
        df_r2diff = pd.DataFrame(index=test_cat.index)
 
        # I didn't find out how Pandas handled this.
        res_r2bias = []
        res_pred = []
        res_true = []
        test_cat = inputD['test']

        xtrain_cols = map('c{0}'.format, range(len(self.config['filters'])-1))
        if self.config['use_cal']:
            xtrain_cols[0] = 'c0_tmp'

        cs = self.catalog_sample(train_cat, test_cat)
        for i, (sub_train, sub_test) in enumerate(cs):
            reg = reg_cls(**self.config['opts'])
            reg.fit(sub_train[train_cols], sub_train[pred_col])

            # Calibrating against the simulations for each pointing.
            if self.config['use_cal']:
                coff = self.core_find_offset(reg, cal_cat)
                if 'col_off' in test_cat:
                    del test_cat['col_off']

                sub_test = sub_test.join(coff, on=self.config['bin_by'])
                sub_test['coff'] = sub_test['col_off']
                sub_test.ix[np.isnan(sub_test.coff), 'coff'] = 0. # Setting default value.
                sub_test['c0_tmp'] = sub_test.c0 + sub_test.coff


            r2true = sub_test[pred_col]
            r2pred = reg.predict(sub_test[xtrain_cols])
            r2bias = (r2pred - r2true) / r2true
            r2diff = (r2pred - r2true) 

            df_r2true[i] = r2true
            df_r2pred[i] = pd.Series(r2pred, index=sub_test.index)
            df_r2bias[i] = r2bias
            df_r2diff[i] = r2diff


        res = {'r2eff': df_r2bias, 'r2pred': df_r2pred, 'r2true': df_r2true, 
               'r2diff': df_r2diff}

        return res


    def write_output(self, file_path, res):
        # I should perhaps update the underlying framework to deal with this
        # in a better way.
        store = pd.HDFStore(file_path, 'w')
        for key,val in res.iteritems():
            store[key] = val

        store.close()

    def run(self):

        inputD = self.prepare_input()
        df = self.estimate_r2(inputD)

        file_path = self.job.empty_file('default')
        self.write_output(file_path, df)
