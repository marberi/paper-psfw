#!/usr/bin/env python
# encoding: UTF8

import sys
import time
import numpy as np
import pandas as pd
import cosmolopy as cpy
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep, RectBivariateSpline
from scipy.integrate import romberg

descr = {
  'sed_types': 'The types corresponding to only_seds'
}

class galabs:
    """Code for generating the apparent magnitude catalogs from
       luminousity functions.
    """

    version = 1.34
    config = {
    'Ngal': 200007,
    'lum': \
    {'ell': {\
        'phi': [2.4, 1.1, -2.7],
        'Ms': [5.0, 1.6, -21.9],
        'alpha': [1.7, 1.6, -1]},

     'sp': {\
        'phi': [0.5, 0.1, -2.28],
        'Ms': [3.2, 2.5, -21.0],
        'alpha': [0.7, -0.9, -1.5]},

     'irr': {\
        'phi': [1.0, -3.5, -3.1],
        'Ms': [5.0, 1.3, -20.0],
        'alpha': [1.8, 0.9, -1.85]}},

    'vis_cut': 24.5, # Not used...

    'only_seds': ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy'],
    'sed_types': ['ell', 'sp', 'sp', 'irr', 'irr', 'irr'],
    'limit_seds': False,
    'intp_seds': True
    }

    # intp_seds: If interpolating between the SEDs.
    
    labels = ['ell', 'sp', 'irr']

    def _load_fjc_seds(self):
        """The sed numbers used in the input catalogs."""

        fjc_path = '/nfs/pic.es/user/e/eriksen/papers/psfw/data/fjc_temp'
        fjc_seds = np.loadtxt(fjc_path, dtype=np.str).tolist()

        return fjc_seds

    def f(self, z, a,b,c):
        return a*np.exp(-(1+z)**b) + c

    def find_N(self, lum, z, M):
        Dl = cpy.distance.luminosity_distance(z, **cpy.fidcosmo)
        Kzt = -2.5*np.log10(1/(1+z))
        Mconv = 5*np.log10(Dl) + 25 #+ Kzt

        N = {}
        for i, Ki in lum.iteritems():
            phi = self.f(z, *Ki['phi'])
            Ms = self.f(z, *Ki['Ms'])
            alpha = self.f(z, *Ki['alpha'])

            # The last factor in Ni comes from converting dx->dm
            x = 10**(-0.4*np.subtract.outer(M, Ms))
            Ni = 10**(phi)*x**alpha*np.exp(-x) #*x

            Mtile = np.tile(M, (len(z), 1)).T
            mag = Mtile + Mconv
            touse = np.logical_and(17. < mag, mag < 26)
            Ni[np.logical_not(touse)] = 0.

            Mtmp = np.tile(M, (len(z),1)).T
            mag = Mtmp + Mconv


            N[i] = Ni

        return N

    def sample_types(self, N, z, M, Ngal):
        w = {}
        for tgal, Ni in N.iteritems():
            spl = RectBivariateSpline(M,z,Ni)

            w[tgal] = spl.integral(min(M), max(M), min(z), max(z))

        tr = np.array([w[x] for x in self.labels])

        E = tr.cumsum()
        E /= E[-1]

        rt = np.random.random(Ngal)

        tt = np.digitize(rt, E)

        return tt, w

    def sample_zM(self, N, z, M, tt, w):
        # More inefficient version, but can be used to test!
        N_spls = {}
        for tgal, Ni in N.iteritems():
            spl = RectBivariateSpline(M, z, Ni / w[tgal])
            N_spls[tgal] = spl

        counts = pd.Series(tt).value_counts()
        def rand_lin(X, N):
            return min(X) + (max(X) - min(X))*np.random.random(N)

        z_gal = -np.ones(len(tt))
        M_gal = -np.ones(len(tt))

        Nbatch = 1e6

        for i,tgal in enumerate(self.labels):
            Ntarget = counts[i]
            spl = N_spls[tgal]

            zL = []
            ML = []

            Nfound = 0
            while Nfound < Ntarget:
                rz = rand_lin(z, Nbatch)
                rm = rand_lin(M, Nbatch)
                r = np.random.random(Nbatch)
                p = spl.ev(rm, rz)

                touse = r < p

                zL.append(rz[touse])
                ML.append(rm[touse])

                Nfound += touse.sum()

            inds = (tt == i)
            z_gal[inds] = np.hstack(zL)[:Ntarget]
            M_gal[inds] = np.hstack(ML)[:Ntarget]

        return z_gal, M_gal

    def TESTsample_zM(self, N, z, M, tt, w):
        # More inefficient version, but can be used to test!
        N_spls = {}
        z_spls = {}
        for tgal, Ni in N.iteritems():
            z_spls[tgal] = splrep(Ni.sum(axis=0), z)

        r = np.random.random(20000)


    def _seds_all(self, tt):
        """Sample among all templates in the library."""

        sedl = [0,17,55,65]

        if self.config['intp_seds']:
            seds = -np.ones(len(tt)) #, dtype=np.int)
        else:
            seds = -np.ones(len(tt), dtype=np.int)

        for i,sed in enumerate(self.labels):
            x0,x1 = sedl[i],sedl[i+1]

            inds = (tt == i)
            if self.config['intp_seds']:
                seds[inds] = (x1-x0)*np.random.random(inds.sum()) + x0
            else:
                seds[inds] = np.random.randint(x0,x1, inds.sum())



        assert not (seds == -1).any()

        fjc_seds = self._load_fjc_seds()

        return seds, fjc_seds

    def OLD_seds_selected(self, tt): 
        """Only select seds from a subset given in the configuration."""

        # Used to divide in types(ell,spl,irr) to get the right
        # distribution.
        sedl = [0,17,55,65]
        sedl = [0,17,51,65]

        only_seds = self.config['only_seds']
        only_nr = map(self.fjc_seds.index, only_seds)

        gal_type = np.digitize(only_nr, sedl) - 1
        gal_type = np.clip(gal_type, 0, len(sedl)-2)

        gal_df = pd.DataFrame({'sed': only_seds, 'sed_nr': only_nr, 'gal_type': gal_type})


        seds = -np.ones(len(tt), dtype=np.int)
        for key, val in gal_df.groupby('gal_type'):
            if len(val) == 1:
                seds[tt == key] = val.sed_nr
            else:
                inds = (tt ==key)
                rind = np.random.randint(0, len(val), inds.sum())

                seds[inds] = val.sed_nr.values[rind]

        assert not (seds == -1).any()

        return seds


    def _seds_selected(self, tt): 
        """Only select seds from a subset given in the configuration."""

        seds = -np.ones(len(tt), dtype=np.int)
        only_seds = self.config['only_seds']
        sed_types = self.config['sed_types']

        for j,sedx in enumerate(['ell', 'sp', 'irr']): #set(sed_types):
            # SEDs beloning to a given type.

            tmp = [(sed,i) for i,(sed, sedt) in enumerate(zip(only_seds, sed_types)) if sedt == sedx]
            if not len(tmp):
                continue

            sed_sel, nr_sel = zip(*tmp)

            toset = (tt == j).nonzero()[0]
            E = np.random.randint(0, len(nr_sel), len(toset))
            seds[toset] = np.array(nr_sel)[E]

        assert not (seds == -1).any()

        return seds, only_seds


    def sample_sed(self, tt):
        f = self._seds_selected if self.config['limit_seds'] \
            else self._seds_all

        return f(tt)

    def create_catalog(self):
        """Creates the galaxy catalog."""

        # Some of these values should have been moved into the
        # configuration..
        z = np.linspace(0.01, 6.0, 500)
        M = np.linspace(-24, -14, 100)
        M = np.linspace(-24, -14, 100)
        M = np.linspace(-24, -18, 200)

        lum = self.config['lum']
        N = self.find_N(lum, z, M)

        tt,w = self.sample_types(N, z, M, self.config['Ngal'])
        z_gal, M_gal = self.sample_zM(N, z, M, tt, w)

        seds,seds_names = self.sample_sed(tt)
        df = pd.DataFrame({'zs': z_gal, 'M': M_gal, 'sed': seds})
        df['id'] = np.arange(len(df))

        return df, seds_names

    def run(self):
        df,seds_names = self.create_catalog()

        # The nameing here reflects what is expected when using
        # the result decorator.
        file_path = self.job.empty_file('default')
        store = pd.HDFStore(file_path, 'w')
        store['seds'] = pd.Series(seds_names)
        store.append('default', df)
        store.close()
