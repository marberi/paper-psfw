#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

def new_pipel():
    photoz_data = '/data2/photoz/run/data/'
    filter_dir = '/data2/photoz/run/data/des_euclid2'

    abkids = xd.Job('ab')
    abstar = xd.Job('ab')
    abkids.task.config.update({'data_dir': photoz_data,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abstar.task.config.update({'data_dir': photoz_data,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})

    r2vz_star = xd.Job('r2vz')
    r2vz_star.config['filter_dir'] = filter_dir
    r2vz_star.config['data_dir'] = photoz_data
    r2vz_star.config['obj_type'] = 'star'

    j1 = xd.Job('kidscat')

    j2 = xd.Job('starsel')
    j2.depend['catalog'] = j1

    j3 = xd.Job('starfit')
    j3.depend['catalog'] = j2
    j3.depend['ab'] = abkids

    j4 = xd.Job('starabs')
    j4.depend['starcat'] = j2
    j4.depend['starfit'] = j3


    j5 = xd.Job('starobs')
    j5.depend['starcat'] = j4
    j5.depend['ab'] = abstar

    j6 = xd.Job('r2starx')
    j6.depend['magcat'] = j5
    j6.depend['r2vz'] = r2vz_star

    pipel = xd.Job()
    pipel.depend['starabs'] = j4
    pipel.depend['starcat'] = j5
    pipel.depend['r2star'] = j6

    return pipel

import trainpipel
defpipel = trainpipel.pipel()


def check_r2eff():
    """Compare the R^2 values for different ways of generating the
       SED distribution.
    """

    pipel = new_pipel()
    top = xd.Topnode(pipel)
    layer = [{'starabs.sed_distr': x} for x in ['uni', 'mean', 'point']]
    layer += [{'starabs.resample': True}]
    top.add_layer(layer)

    for i,(key, pipel) in enumerate(top.iteritems()):
        if pipel.starabs.config['resample']:
            lbl = 'Resample'
        else:
            lbl = 'Distr: {0}'.format(pipel.config['starabs.sed_distr'])

        cat = pipel.starcat.result
        cat = cat.join(pipel.r2star.result)

        cat['r_i'] = cat.mag_r - cat.mag_i

        plt.plot(cat.r_i, cat.r2eff+5*i, '.', label=lbl)

    plt.xlabel('r - i', size=16)
    plt.ylabel('$R^2$ (shifted, weird normalization)', size=16)
    plt.legend()
    plt.grid(True)
    plt.savefig(tosave.path('x81_r2eff'))

def check_seddistr():
    """Check that the SED distribution looks normal."""

    pipel = new_pipel()
    top = xd.Topnode(pipel)
    top.add_layer([{'starabs.sed_distr': 'point'}, 
                   {'starabs.resample': True}])

    titles = ['Distr', 'Resample']
    fig, A = plt.subplots(ncols=2)
    for i in range(2):
        pipel = top[(i,)]

        cat = pipel.starcat.result
        cat = cat.join(pipel.r2star.result)

        cat['r_i'] = cat.mag_r - cat.mag_i

        ax = A[i]
        for j,(key, sub) in enumerate(cat.groupby('point')):
            ax.plot(sub.r_i, sub.r2eff+.5*j, '.') #, ms=2.0, color='black', alpha=0.7)

        ax.set_xlabel('r - i', size=16)
        ax.set_title(titles[i], size=16)
        ax.grid(True)

    A[0].set_ylabel('$R^2$ (shifted, weird normalization)', size=16)
    plt.savefig(tosave.path('x81_seddistr'))

if __name__ == '__main__':
    check_r2eff()
    check_seddistr()
