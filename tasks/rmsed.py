#!/usr/bin/env python
# encoding: UTF8

import pandas as pd
import numpy as np

descr = {
  'Nobj': 'Number of objects to include',
  'how': 'Method for removing the SEDs',
  'lkeep': 'Length of the interval to keep',
  'lrem':  'Length of the interval to remove',
  'torem': 'Where to start removing the SEDs',
  'dsed': 'Interval length',
}

class rmsed:
    """TODO"""

    version = 1.02
    config = {'Nobj': 'all',
              'how': 'frac',
              'lkeep': 1.,
              'lrem': 1.,
              'torem': '', 'dsed': 1.}

    def frac_bins(self, sed):
        lkeep = self.config['lkeep']
        lrem = self.config['lrem']
        assert (0. <= lkeep), 'lkeep should be non-negative'
        assert (0. <= lrem), 'lrem should be non-negative'

        toincl = pd.Series(index=sed.index, dtype=np.bool)

        i = 0
        x0,x1 = 0., -100
        edges = []
        sed_max = sed.max()

        while x1 < sed_max:
            length = lkeep if not i % 2 else lrem
            incl = True if not i % 2 else False

            x1 = x0 + length
            print(i, x0, x1, length)

            toincl.ix[(x0 <= sed) & (sed < x1)] = incl

            x0 = x1
            i += 1

        assert not np.isnan(toincl).any(), 'Internal error'

        return toincl

    def run(self):
        store_in = self.job.default.get_store()
        seds = store_in['seds']
        cat = store_in['default']
        store_in.close()

        # Assuming some interpolation between the seds.
        if self.config['how'] == 'interv':
            remove = (torem <= cat.sed) & (cat.sed < torem + 1.)
            cat_out = cat[~remove]
        elif self.config['how'] == 'frac':
            toincl = self.frac_bins(cat.sed)

            cat_out = cat[toincl]

        Nobj = self.config['Nobj']
        if not Nobj == 'all':
            cat_out = cat_out.sample(Nobj, replace=False)

        # Because I also need to store the seds.
        file_path = self.job.empty_file('default')
        store_out = pd.HDFStore(file_path, 'w')
        store_out['seds'] = seds
        store_out.append('default', cat_out)
        store_out.close()
