#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd

class magcut:
    """Cut the input catalog."""


    # Previously there was another layer of configuration. Removed for simplicity.
    version = 1.0
    config = {'vis_cut': 24.5,
              'HACK': 0.}

#    def add_offset(self, cat_in):
#        filters = [x.replace('mag_', '') for x in cat_in.columns if x.startswith('mag_')]
#        cat_out = cat_in.copy()
#
#        for fname in filters:
#            cat_out['mag_{0}'.format(fname)] += self.config[fname]
#
#        return cat_out

    def cut_catalog(self, cat_in):
        inds = cat_in.mag_vis < self.config['vis_cut']
        cat_out = cat_in[inds]

        return cat_out

    def run(self):
        store_in = self.job.kids['magcat']['magcat']
        Rin = store_in.select('cat', iterator=True, chunksize=1e6)

        path_out = self.job.empty_file('magcat')
        store_out = pd.HDFStore(path_out, 'w')

        for cat_in in Rin:
            store_out.append('cat', self.cut_catalog(cat_in))

        store_in.close()
        store_out.close()

