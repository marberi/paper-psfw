#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import os
import pdb
import sys
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep
import pandas as pd

class r2star:
    """Estimate the PSF size for the stars."""

    version = 1.0

    config = {
     'r2_filter': 'vis',
    }
   
    _star_path = '/data2/photoz/run/data/seds_stars/STAR_PICKLES.list'
    def load_seds_list(self):
        # This information should be passed elsewhere...
        A = open(self._star_path).readlines()
        star_seds = [x.split('.')[0].split('/')[1] for x in A]
 
        return star_seds 

    def _load_r2(self):
        """Load the R^2(z) splines."""
 
        r2 = {}
        r2vz = self.job.r2vz.result
        fname = self.config['r2_filter']

        z = r2vz['z']
        for sed in self.star_seds:
            key = '/{0}/{1}'.format(fname, sed)
            r2[fname, sed] = splrep(z, r2vz[key])

        return r2

    def _load_starcat(self):
        starcat = self.job.magcat.result

        return starcat

    def create_r2(self, r2_spls, starcat):
        """Estimate the effective R^2(z) value for the star."""

        r2filter = self.config['r2_filter']

        res = []
        sed_ind = starcat['star_sed_ind'].astype(np.int)
        zs = starcat['zs']

        # Unless I end up needing it, the core only supports
        # one redshift.
        assert len(set(zs)) == 1
        zs_val = zs[0]

        for sed in self.star_seds:
            spl = r2_spls[r2filter, sed]
            res.append(splev(zs_val, spl))

        r2_seds = np.hstack(res)
        r2 = r2_seds[np.array(sed_ind)]

        return r2

#    def _write_file(self, file_path, r2eff):
#        """Write the calculations to an HDF5 file."""

#        store = pd.HDFStore(file_path, 'w')
#        store['r2eff'] = pd.DataFrame({'r2eff': r2eff})
#        store.close()

    def main(self): 
        self.star_seds = self.load_seds_list()

        r2_spls = self._load_r2()
        starcat = self._load_starcat()
        r2eff = self.create_r2(r2_spls, starcat)


        df = pd.DataFrame({'r2eff': r2eff})
        self.job.result = df

#        file_path = self.job.empty_file('r2eff')
#        self._write_file(file_path, r2eff)

    def run(self):
        self.main()
