#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import numpy as np
import pandas as pd

import glob

class stardistr:
    """Construct a magnitude distribution from KiDS stars."""

    version = 1.0
    config = {'nfields': 'all',
              'imag': 21.}

    def _read_cat(self, file_path, filters):
        # Should this not be moved elsewhere???

        import astropy.io.fits
        W = astropy.io.fits.getdata(file_path)

        mag_in = np.vstack([W['MAG_GAAP_'+f] for f in filters]).T
        mask = np.logical_or(mag_in == 99, mag_in == -99)

        mkheader = lambda pre, L: [pre+x for x in L]
        header_mag = mkheader('mag_', filters)

        mag = pd.DataFrame(mag_in, columns=header_mag)

        # The panda did at not first like me..
        mag = mag.where(pd.notnull(mag), None)
        mag = mag.where(np.abs(mag) != 99, None)

        cat = mag
        cat['cl'] = np.array(W['CLASS_STAR'], dtype=np.float64)

        return W, cat

    def create_distr(self):
        """Create a distribution of stars."""

        g = '/data2/data/kids/kidscat/KIDS*'
        filters = ['i']
        df = pd.DataFrame()

        fL = glob.glob(g)
        N = self.config['nfields']
        if not N == 'all':
            fL = fL[:N]

        for i,file_path in enumerate(fL):
            print('i', i)
            w, cat = self._read_cat(file_path, filters)
            stars = cat[0.8 < cat.cl]
       
            D = pd.DataFrame({'mag_i': stars.mag_i.convert_objects(), 
                              'field': len(stars)*[i]})
            df = df.append(D)

        return df

    def run(self):

        df = self.create_distr()

        # This did not appear to be used.
#        self.job.store_frametbl('stardistr', df)

        mag = df.mag_i[df.mag_i.notnull()]
        mag = mag[mag < self.config['imag']]

        H, edges = np.histogram(mag, bins=30)
        w = edges[1:] - edges[:-1]
        prob = w*H
        prob = prob / np.sum(prob)

        df_hist = pd.DataFrame({'H': H,
                                'mag': 0.5*(edges[:-1] + edges[1:]),
                                'w': w, 'prob': prob})

        self.job.result = df_hist
