#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd
import itertools as it

from scipy.interpolate import splev, splrep
from matplotlib import pyplot as plt

class r2test:
    """TODO"""

    version = 1.0
    config = {}

    def _find_seds(self, r2vz):
        seds = []
        for key in r2vz.columns:
            if not '/' in key:
                continue

            _, fname, sed = key.split('/')
            if fname == 'vis':
                seds.append(sed)

        return seds

    def test_fit(self, cat, r2vz, ab, seds):
#        N = 3000
        ax = 0.0001
        z = np.arange(0., 5, 0.01)


        if False:
            plt.plot(cat.zs, col1, '.', label='r - i')
            plt.plot(cat.zs, col2, '.', label='i - z')

        if False:
            plt.plot(col1, cat.r2eff, '.', label='r - i')
            plt.plot(col2, cat.r2eff, '.', label='i - z')

            plt.legend()
            plt.show()


        all_seds = self._find_seds(r2vz)
        sednr = cat.sed.unique()

        # Creating a grid!
        toload = pd.Series(seds)[sednr]
        z_spl = r2vz['z']
        r2D = {}
        for sed in toload:
            y_r2 = r2vz['/vis/{0}'.format(sed)]
            spl_r2 = splrep(z_spl, y_r2)

            r2D[sed] = splev(z, spl_r2)

        filters = ['r', 'i', 'z']
        magD = {}
        for sed,fname in it.product(toload, filters):
            y_ab = ab['/{0}/{1}'.format(fname, sed)]
            y_mag = -2.5*np.log10(y_ab)

            spl_ab = splrep(z_spl, y_mag)

            magD[sed,fname] = splev(z, spl_ab)

        # A very direct test of the precision....
#        sed = 'Ell_01'
        mod1D = {}
        mod2D = {}
        for sed in toload:
            mod1D[sed] = magD[sed, 'r'] - magD[sed, 'i']
            mod2D[sed] = magD[sed, 'i'] - magD[sed, 'z']

#        cat['c1'] = col
        cat['c1'] = cat.mag_r - cat.mag_i
        cat['c2'] = cat.mag_i - cat.mag_z

        sub = cat[cat.sed == 1]

        norm = magD['Ell_01', 'i'][0]

        k = 1.
#        y = :
        y = []
        for i,gal in cat.iterrows():
            print(i)
            sed = seds[gal.sed]


            mod_c1 = mod1D[sed]
            mod_c2 = mod2D[sed]
            diff = k*(gal.c1 - mod_c1)**2 + (gal.c2 - mod_c2)**2

            z_best = z[diff.argmin()]

            r2fit = r2D[sed][diff.argmin()]
            R = (r2fit - gal.r2eff) / gal.r2eff

            if 0.005 < np.abs(R):
                print(seds[gal.sed], gal.zs)
                plt.plot(z, diff, 'o-')
                plt.axvline(gal.zs)

                plt.show()

            y.append(R)

        S = pd.Series(y)
        T = S[S.abs() < 0.005]

        T.hist(bins=400)
        plt.show()

 
#
#            plt.plot(z, diff, 'o-')
#            plt.axvline(gal.zs)
#
#            plt.xlabel('z')
#
#            plt.show()



        


    def run(self):
#        self.job.kids['r2vz']
        r2vz = self.job.depend['r2vz']['r2vz']['r2vz']
        galcat = self.job.depend['galcat']['magcat']['cat']
        r2eff = self.job.depend['r2gal']['r2eff']['r2eff']
        cat = galcat.join(r2eff)
        cat = cat.join(self.job.depend['mabs']['mabs']['cat'], rsuffix='x')

        seds = self.job.depend['mabs']['mabs']['seds']
        ab = self.job.depend['ab']['ab']['ab']

        galcatN = self.job.depend['galcatN']['magcat']['magcat']


        self.test_fit(cat, r2vz, ab, seds)
