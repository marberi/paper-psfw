#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd

class offset:
    """Add an offset to the input catalog"""

    version = 1.0

    # Previously there was another layer of configuration. Removed for simplicity.
    # Here HACK is not a magnitude, but a mechanism for cache invalidation.
    config = {'vis': 0., 'g': 0., 'r': 0., 'i': 0., 'z': 0., 'Y': 0., 'J': 0., 'H': 0.}

    def add_offset(self, cat_in):
        filters = [x.replace('mag_', '') for x in cat_in.columns if x.startswith('mag_')]
        cat_out = cat_in.copy()

        for fname in filters:
            # To avoid adding default values to the PAU filters.
            if not fname in self.config:
                continue

            cat_out['mag_{0}'.format(fname)] += self.config[fname]

        return cat_out

    def run(self):
        store_in = self.job.magcat.get_store()
        Rin = store_in.select('default', iterator=True, chunksize=1e6)

        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        for cat_in in Rin:
            store_out.append('default', self.add_offset(cat_in))

        store_in.close()
        store_out.close() 
