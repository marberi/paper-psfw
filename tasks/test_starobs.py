#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

photoz_data = '/data2/photoz/run/data/'
abkids = xd.Job('ab')
abkids.task.config.update({'data_dir': photoz_data,
                           'filter_dir': 'kids_filters',
                           'zmax': 1.,
                           'sed_dir': 'seds_stars'})


j1 = xd.Job('kidscat')


j2 = xd.Job('starsel')
j2.depend['catalog'] = j1

j3 = xd.Job('starfit')
j3.depend['catalog'] = j2
j3.depend['ab'] = abkids

j4 = xd.Job('starabs')
j4.depend['starcat'] = j2
j4.depend['starfit'] = j3


j5 = xd.Job('starobs')
j5.depend['starcat'] = j4
j5.depend['ab'] = abkids

def check_magdistr():
    """Plot the magnitude distribution"""

    cat = j5.result
    for key,sub in cat.groupby('point'):
        sub.i.hist(normed=True, histtype='step', alpha=0.5)

    cat.i.hist(normed=True, histtype='step', lw=3.0, color='black')

    plt.xlabel('i-band magnitude', size=16)
    plt.ylabel('Probability', size=16)

    plt.savefig(tosave.path('x78_magdistr'))


def check_seduni():
    """Check it the SED distribution is uniform."""

    cat = j5.result
    counts = cat.sed.value_counts()
    counts.plot(kind='bar')
    plt.savefig(tosave.path('x78_seduni'))

def check_seddistr():
    """Check the SED when using KiDS data."""

    j4.config['resample'] = True
    cat = j5.result
    cat.sed.value_counts()[:10].plot(kind='bar')
    plt.savefig(tosave.path('x78_seddistr'))

if __name__ == '__main__':
    check_magdistr()
    check_seduni()
    check_seddistr()
