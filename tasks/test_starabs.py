#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

photoz_data = '/data2/photoz/run/data/'
abkids = xd.Job('ab')
abkids.task.config.update({'data_dir': photoz_data,
                           'filter_dir': 'kids_filters',
                           'zmax': 1.,
                           'sed_dir': 'seds_stars'})


j1 = xd.Job('kidscat')


j2 = xd.Job('starsel')
j2.depend['catalog'] = j1

j3 = xd.Job('starfit')
j3.depend['catalog'] = j2
j3.depend['ab'] = abkids

j4 = xd.Job('starabs')
j4.depend['starcat'] = j2
j4.depend['starfit'] = j3

def check_uniform():
    """Checks that the SEDs are actually uniformly distributed."""

    cat = j4.result
    counts = cat.sed.value_counts()
    diff = (counts - counts.mean()) / counts.mean()

    diff.hist(normed=True)
    plt.xlabel('(counts_sed - counts_mean) / counts_mean', size=16)
    plt.ylabel('Probability')

    plt.savefig(tosave.path('x77_seduni'))

def check_mdistr():
    """Check that the magnitude distribution makes sense."""

    cat = j4.result
    for key, sub in cat.groupby('point'):
        sub.imag.hist(histtype='step', normed=True)

    plt.xlabel('i-band magnitude', size=16)
    plt.ylabel('Probability', size=16)
    plt.savefig(tosave.path('x77_magdistr'))

def check_resample():
    """Only executing when using resampling."""

    j4.config['resample'] = True
    cat = j4.result

if __name__ == '__main__':
    check_uniform()
    check_mdistr()
    check_resample()    
