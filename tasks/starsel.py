#!/usr/bin/env python
# encoding: UTF8

import pandas as pd
import numpy as np

class starsel:
    """TODO"""

    version = 1.01
    config = {}

    def run(self):

        store = self.job.depend['catalog'].get_store()
        t1 = pd.Term('CLASS_STAR', '>', 0.8)
        t2 = pd.Term('MAG_AUTO_I', '<', 21.)

        self.job.result = store.select('default', [t1,t2])
