#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import os
import time
import pandas as pd
from astropy.table import Table

class kidscat:
    """TODO"""

    version = 1.108
    config = {'limited': False}


    d = '/data2/data/kids/astrowise'
    d = '/nfs/astro/eriksen/kids/astrowise'
    source_list = 'kids_dr2.0_cat_wget.txt'
   
    def read_srclist(self):
        """Read in list of files"""

        res = []
        list_path = os.path.join(self.d, self.source_list)
        for line in open(list_path).readlines():
            assert '.fits' in line

            file_name = line.strip().split('/')[1]
            res.append(file_name)

        # For testing
        if self.config['limited']:
            res = res[:4]

        return res

    def assert_downloaded(self, names):
        """Assert that all files are downloaded."""

        # We could trigger a download, but that is not
        # really needed at the current stage.
        for file_name in names:
            path_in = os.path.join(self.d, file_name)
            assert os.path.exists(path_in)


    def run(self):
        names = self.read_srclist()
        self.assert_downloaded(names)

        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        # Here the index is set manually. If only appending, there
        # will be a collision of indices.
        data_columns = ['CLASS_STAR', 'MAG_AUTO_I']
        n = 0
        for i, file_name in enumerate(names):
            t1 = time.time()
            path_in = os.path.join(self.d, file_name)

            print('Reading in part:', i)
            df = Table.read(path_in).to_pandas()
            df.index = range(n,n+len(df))
            df['fieldnr'] = i

            store_out.append('default', df, data_columns=data_columns, index=False)

            t2 = time.time()
            print('time', t2-t1)

            n += len(df)
        store_out.close()
