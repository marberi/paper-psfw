#!/usr/bin/env python
# encoding: UTF8

import copy
import time
import numpy as np

import bcnz
import bcnz.io
import bcnz.model
import bcnz.tasks
import bcnz.zdata

import pzcat

class local_task:
    # Potentially no longer needed. Already moved away from
    # subclassing old bcnz task.

    # This is having a different call signature than the original tasks.
    def __init__(self, myconf, zdata, in_iter, out_table, prior_cat=False):
        self.config = bcnz.libconf(myconf)
        self.zdata = zdata
        self.in_iter = in_iter
        self.out_table = out_table

        self.prior_cat = prior_cat

    def _run_iter(self, in_iter, out_table):
        in_iter.open()
        out_table.open()

#        zdata = bcnz.zdata.zdata(self.config)
        zdata = self.zdata
        zdata = bcnz.model.add_model(self.config, zdata)

        if not isinstance(self.prior_cat, bool):
            priors = bcnz.priors.ext(self.config, self.zdata, self.prior_cat)
        else:
            priors = None

        for data in in_iter:
            data = bcnz.observ.post_pros(self.config, zdata, data)

            fit = bcnz.fit.chi2(self.config, zdata, data, priors)
            for block in fit.blocks():
                out_table.append(block)

        in_iter.close()
        out_table.close()

    def run(self):
        print('Running photoz')
        self._run_iter(self.in_iter, self.out_table)

class GalIter:
    def __init__(self, conf, store):
        self.conf = conf
        self.store = store

        # These only needs to be calculated once..
        filters = self.conf['filters']
        mag_fmt = self.conf['mag_fmt']
        self.mag_cols = map(mag_fmt.format, filters)

        if not self.conf['add_noise']:
            err_fmt = self.conf['err_fmt']
            self.err_cols = map(err_fmt.format, filters)

    def open(self):
        # It should not be needed to tune here.
        #Rin = self.store.select('default', iterator=True, chunksize=500) #1e4)
        Rin = self.store.select('default', iterator=True, chunksize=200) #1e4)
        self.Rin = iter(Rin)

    def close(self):
        self.store.close()

    def __iter__(self):
        return self

    def next(self):
        print('In the GalIter..')
        df = self.Rin.next()

        data = {}
        data['mag'] = np.array(df[self.mag_cols])
        if not self.conf['add_noise']:
            data['emag'] = np.array(df[self.err_cols])


        for key in ['id', 'zs', 'ra', 'dec', 'm0', 'spread_model_i', 'm_0']:
            if key in df:
                data[key] = np.array(df[key])

# just disbled this check..
#        assert not 'id' in data, 'Need to check this case.'
        data['id'] = df.index

        return data

class pzcat:
    """Run bcnz to determine the photo-z."""

    version = 1.14 # Before editing locally...
    version = 1.15

    # - Some of these are Eulicd related and should be moved elsewhere.
    # - Overriding e.g. interp is done here to always have access to these
    #   parameters.
    config = {'filters': ['g', 'r', 'i', 'z', 'Y', 'J', 'H'],
              'seds': ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy'],
#              'order': [],
              'mag_fmt': 'mag_{0}',
              'err_fmt': 'err_{0}',
              'add_noise': False,
              'flat_amp_prior': True,
              'use_priors': True,
              'interp': 2,
              'out_pdf': False,
              'out_pdftype': False,
              'hh_fac': 0.0,
              'dz': 0.01,
              'zmax': 10,
              'min_rms': 0.05,
              'min_magerr': 0.,
              'max_magerr': 10.,
              'sky_spec': 'sky_spectrum.txt',
              'frame_name': 'magcat',
              'sed_dir': 'seds',
              'filter_dir': 'des_euclid'}

    def old_paths(self):
        # Used for the pure HDF5 output. No longer in use, but to soon to delete.

        out_paths = {}
        for obj_name, to_incl in [('pzcat', True), ('pzpdf', self.conf['out_pdf']),
                                  ('pzpdf_type', self.conf['out_pdftype'])]:

            if not to_incl:
                continue

            # The photo-z code use the same names.
            out_paths[obj_name] = self.job.empty_file(obj_name)

        return out_paths

    def run(self):
        # A bit of a patch work...
        ext_priors = 'prior_cat' in self.job.depend
        myconf = copy.deepcopy(self.config)
        if ext_priors:
            myconf['prior'] = 'ext'

        conf = bcnz.libconf(self.config)
        self.conf = conf

        zdata = bcnz.zdata.zdata(conf, minimal=False) 
        zdata['ab_df'] = self.job.ab.result

        zdata = bcnz.model.add_model(conf, zdata)
        nz, nt, _ = zdata['f_mod'].shape

        # Trying to add external priors to the BCNZ code.
        if ext_priors:
            assert 'prior_mabs' in self.job.depend

            magcat = self.job.prior_cat.result['magcat']['magcat'] #FISK
            mabs = self.job.prior_mabs.result['mabs']['cat'] #FISK
            prior_cat = mabs.join(magcat, rsuffix='x')
        else:
            prior_cat = False

        if self.conf['cat']:
            in_iter = bcnz.io.hdf5.read_cat(conf, zdata, self.conf['cat'])
        else:
            store = self.job.galcat.get_store()

            in_iter = GalIter(conf, store)

        paths = {'pzcatdf': self.job.empty_file('default')}
        out_table = bcnz.io.dataframe.write_cat(conf, zdata, paths, nz, nt)
        task = local_task(conf, zdata, in_iter, out_table, prior_cat)

        task.run()
