#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd

class magnoise:
    """TODO"""

    version = 2.0
    config = {'sn': 50,

              # Update 1
              'sn_lims': {'vis': 25.25, 'g': 24.65, 'r': 24.15, 'i': 24.35, 'z': 23.95,
                          'Y': 24.0, 'J': 24.0, 'H': 24.0},
              'sn_val': 5.,
              'isindep': False,
              'add_noise': True,
              # The rest..
              'sn_min': 0, 
              'sn_max': 10000, 
              'not_scale': [],
              'r': 1.,
              'const_sn': False}

    config['sn_val'] = 10.


    # This should be a configuration option. Leaving it here for the moment.
    ground = ['g', 'r', 'i', 'z']

    descr = {'sn_min' : 'Below this SN level we remove the observations'}

    def _const_SN(self, cat_in, filters):
        # This part should later be coupled with the photo-z code.
        SN = pd.DataFrame()
        sn_const = self.config['sn']*np.ones(len(cat_in))
        for fname in filters:
            SN[fname] = sn_const

        return SN

    def _scale_filters(self, filters):
        not_scale = self.config['not_scale']
        scale_filters = [x for x in filters if not x in not_scale]

        return scale_filters

    def _exposure_SN(self, cat_in, filters):
        sn_lims = self.config['sn_lims']

        missing_filters = set(filters) - set(sn_lims)
        assert not missing_filters, 'Missing filters: {0}'.\
                   format(', '.join(missing_filters))

        lims = [sn_lims[x] for x in filters]
        sn_val0 = self.config['sn_val']

        R = self.config['r']

        scale_filters = self._scale_filters(filters)
        sn_val = [(R*sn_val0 if x in scale_filters else sn_val0) for x in filters]


        D = sn_val*10**(np.array(lims) / 5.)

        mag = np.array(cat_in[['mag_{0}'.format(x) for x in filters]])
        SN_arr =  D*10**(-0.2*mag)


        sn_min = self.config['sn_min']
        sn_max = self.config['sn_max']
        

        SN_arr = np.clip(SN_arr, sn_min, sn_max)
        SN = pd.DataFrame(SN_arr, columns=filters)

        return SN

    def scale_SN(self, filters, SN):

        # Scale noise for selected filters.

        scale_filters = self._scale_filters(filters)
        fmask = [(x in scale_filters) for x in filters]

        k = np.sqrt(self.config['r'])
        SN_ratio = np.where(fmask, k, 1.)

        return SN.mul(SN_ratio)

    def add_noise(self, cat_in):
        # Not exactly nice..
        filters = []
        for col in cat_in.columns.tolist():
            if 'mag_' in col:
                filters.append(col.split('_')[1])

        if self.config['const_sn']:
            SN = self._const_SN(cat_in, filters)
        else:
            SN = self._exposure_SN(cat_in, filters)

        SN = self.scale_SN(filters, SN)

        cat_out = cat_in.copy()

        err_mag = 2.5*np.log10(1.+ 1./SN)
        err_real = err_mag*np.random.normal(size=SN.shape)

        nonobs = SN < self.config['sn_min']
        for fname in filters:
            mag = cat_out['mag_{0}'.format(fname)] 
            if self.config['add_noise']:
                mag += err_real[fname]

            err = err_mag[fname]

            mag[nonobs[fname]] = 99
            err[nonobs[fname]] = 99
            cat_out['mag_{0}'.format(fname)] = mag
            cat_out['err_{0}'.format(fname)] = err

        return cat_out

    def run(self):
        # Ok, this should perhaps be abstracted, but it is still early
        # in finding out how the iterators work.
        store_in = self.job.magcat.get_store() 

        Rin = store_in.select('default', iterator=True, chunksize=1e6)
        path_out = self.job.empty_file('default')
        store_out = pd.HDFStore(path_out, 'w')

        for i,cat_in in enumerate(Rin):
            print('magnoise i', i)

            store_out.append('default', self.add_noise(cat_in))

        store_in.close()
        store_out.close()
