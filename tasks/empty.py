#!/usr/bin/env python
# encoding: UTF8

class empty:
    """Empty task. Used for passing dependencies."""

    version = 1.0
    config = {}

    def run(self):
        pass
