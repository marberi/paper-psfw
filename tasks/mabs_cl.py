#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd

class mabs_cl:
    """TODO"""

    version = 1.0
    config = {'sed': 'Ell_01',
              'zs': 0}

    def _load_fjc_seds(self):
        """The sed numbers used in the input catalogs."""

        fjc_path = '/home/marberi/drv/work/papers/psfw/data/fjc_temp'
        fjc_seds = np.loadtxt(fjc_path, dtype=np.str).tolist()

        return fjc_seds

    def _create_cluster(self, cat):
        # This defines the input table. Only a small wrapper reading in the files
        # Fabian sent.. 

        M = cat[0]
        M += 5*np.log10(0.7)

        # This is because of the units which is assumed, at z = 0.25

        # Starting the numbering at 0, even if FJC started at 1.
        fjc_seds = self._load_fjc_seds()
        sed = self.config['sed']
        assert sed in fjc_seds
        sednr_all = fjc_seds.index(sed)

        ngal = len(cat)
        sednr = sednr_all*np.ones((ngal))

        cat['M'] = M
        cat['sed'] = sednr

        if self.config['zs']:
            cat['zs'] = self.config['zs']*np.ones((ngal))

        return cat

    def run(self):
        print('Cluster, Generating absolute magnitudes.')
        file_path = self.job.kids['file']['file']
        store = pd.HDFStore(file_path, 'r')
        cat_in = store['mabs']
        store.close()

        cat_out = self._create_cluster(cat_in)
        self.job.store_frametbl('mabs', cat_out, 'cat')
