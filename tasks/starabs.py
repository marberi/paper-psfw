#!/usr/bin/env python
# encoding: UTF8

import pdb
import os
import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde

descr = {
  'resample': 'Generate the catalog by reampling the KiDS galaxies.',
  'nstars': 'Number of stars in each field.',
  'max_nsed': 'Number of SED used for each field.'
}

class starabs:
    """Genarate the stellar distribution."""

    # By default this code generate a stellar distribution sampling
    # uniformly from the Pickles library and with an i-band distribution
    # following KiDS.

    version = 1.06
    config = {
      'resample': False,
      'nstars': 400,
      'max_nsed': 15,
      'sed_distr':  'uni',
      'mag_distr': 'mean',
    }

    # sed_distr options:
    # uni - Uniform drawn from Pickles.
    # mean - Use the mean for all pointings.
    # point - Use a separate distribution for all pointings.
    #
    # mag_distr options:
    # mean - Use the mean for all KiDS poining.
    # point - Use the pointing by pointing distribution.

    data_dir = '/nfs/pic.es/user/e/eriksen/data/psfw'
    _star_path = os.path.join(data_dir, 'seds_stars/STAR_PICKLES.list')

    def find_star_seds(self):
        """Find the list of the SEDs in the Pickles library."""

        sedL = []
        for line in open(self._star_path).readlines():
            # I forget regexp between each time I need them..
            sed = line.replace('PICKLES/','').split('.sed')[0]
            sedL.append(sed)

        sedS = pd.Series(sedL)

        return sedS

    def get_cat(self, kids_cat, sedf):
        """Add the information on the best SED fit to the KiDS
           catalog.
        """

        # Only look at the objects with 3 colors.
        sedf = sedf[sedf.nobs == 3.]
        nobs = sedf.nobs
        sedf = sedf.multiply(nobs, axis='rows')
        del sedf['nobs']

        #xmin = np.argmin(sedf.values, axis=1)

        best_fit = sedf.idxmin(axis=1)
        best_fit.name = 'sed'

        cat = kids_cat.join(best_fit)
        cat['chi2'] = pd.Series(sedf.lookup(best_fit.index, best_fit),
                                index=best_fit.index)

        return cat

    def gen_resample(self, input_cat):
        """Generate the catalog by resampling the KiDS catalog."""
        
        input_cat = input_cat[~np.isnan(input_cat.chi2)]

        df = pd.DataFrame()
        nstars = self.config['nstars']
        for point, sub in input_cat.groupby('fieldnr'):
            part = sub.sample(n=400)[['sed', 'MAG_AUTO_I']]
            part = part.rename(columns={'MAG_AUTO_I': 'imag'})
            part['point'] = point

            part.index = range(nstars)
            part.index.name = 'star'


            df = df.append(part, ignore_index=True)

        return df 

    def mag_kdes(self, cat):
        """Generate the magnitude KDEs."""

        kdeD = {'all': gaussian_kde(cat.MAG_AUTO_I)}
        if self.config['mag_distr']:
            for key, sub in cat.groupby('fieldnr'):
                kdeD[key] = gaussian_kde(sub.MAG_AUTO_I)

        return kdeD

    def find_topten(self, cat):
        """Limit the result to the most common stellar SEDs."""


        max_nsed = self.config['max_nsed']
        D = {}
        for key, sub in cat.groupby('fieldnr'):
            D[key] = sub.sed.value_counts()[:max_nsed]

        topten = pd.concat(D, axis=1)

        topten['all'] = cat.sed.value_counts()[:max_nsed]

        return topten

    def gen_distr(self, cat):
        """Generate the distribution."""

        # The code here might be verbose, but works.
        kdeD = self.mag_kdes(cat)
        topten = self.find_topten(cat)
        sedL = self.find_star_seds()

        nstars = self.config['nstars']
        df = pd.DataFrame(columns=['point', 'star', 'sed', 'imag'])
        for point in topten.columns:
            if point == 'all': continue

            part = pd.DataFrame(index=range(400))
            part.index.name = 'star'

            part['point'] = point


            # Generating the SEDs.
            nstars = self.config['nstars']
            sed_distr = self.config['sed_distr']
            if sed_distr == 'uni':
                sed = sedL.sample(n=nstars, replace=True).values
            elif sed_distr == 'mean':
                sed = topten['all'].sample(n=nstars, replace=True, weights=topten['all']).index
            elif sed_distr == 'point':
                sed = topten[point].sample(n=nstars, replace=True, weights=topten[point]).index
            else:
                raise

            part['sed'] = sed

            # Generate the magnitude distribution.
            mag_distr = self.config['mag_distr']
            if mag_distr == 'mean':
                imag = kdeD['all'].resample(nstars)[0]
            elif mag_distr == 'point':
                imag = kdeD[point].resample(nstars)[0]
            else:
                raise


            part['imag'] = imag
            part = part.reset_index()
            df = df.append(part, ignore_index=True)

        df['point'] = df.point.astype(np.int)

        return df

    def run(self):
        assert not self.config['mag_distr'] == 'uni', \
          'A uniform magnitude distribution is not supported'

        # Experimenting more with the best SED fits.
        kids_cat = self.job.depend['starcat'].result
        sedf = self.job.depend['starfit'].result
        input_cat = self.get_cat(kids_cat, sedf)


        if self.config['resample']:
            cat = self.gen_resample(input_cat)
        else:
            cat = self.gen_distr(input_cat)

        self.job.result = cat

