#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import os
import pdb
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep
import pandas as pd
import tables

class r2gal:
    """Estimate the effective R^2 for a galaxy."""

    version = 1.0
    config = {
       'r2_filter': 'vis'
    }

    def _load_r2vz(self, seds):
        """Splines representing R^2(z) for difference seds."""

        fname = self.config['r2_filter']

        r2vz = self.job.r2vz.result
        z = r2vz['z']

        spls = []
        for sed in seds:
            name = '/{0}/{1}'.format(fname, sed)
            spls.append(splrep(z, r2vz[name]))

        return spls

    def r2eff(self, galcat, r2vz):
        """The effective R^2 for a galaxy."""

        # Ok, the joining gives strange results..
        zs = np.array(galcat['zs'])
        gal_type = np.array(galcat['sed'])

        tt0 = gal_type.astype(np.int)
        dt = gal_type - tt0


        r2_true = np.nan*np.ones(len(gal_type))

        for i in range(len(r2vz)):
            inds = tt0 == i
            spl0 = r2vz[i]

            # This ends up giving problems.
            if not np.sum(inds):
                continue

            if i == len(r2vz)-1:
                r2_true[inds] = splev(zs[inds], spl0)
                continue

            spl1 = r2vz[i+1]
            part0 = (1. - dt[inds]) * splev(zs[inds], spl0)
            part1 = dt[inds] * splev(zs[inds], spl1)

            r2_true[inds] = part0 + part1


        assert not np.isnan(r2_true).any(), 'Something went wrong.'

        return r2_true

    def run(self):
        galcat = self.job.mabs.result
        store = self.job.mabs.get_store()
        r2vz = self._load_r2vz(store['seds'])
        store.close()

        r2eff = self.r2eff(galcat, r2vz)

        self.job.result = pd.DataFrame({'r2eff': r2eff})
