#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

descr = {
 'filters': 'Filters to train on',
 'use_col': 'If using colors'
}

class r2sed:
    """Estimate the R^2 value based on an input mock
       generated from SEDs.
    """

    version = 1.0
    config = {'filters': ['r', 'i', 'z'], 
              'Ntrain': 1000,
              'use_col': False}


    def fit(self, train, test):
        assert not self.config['use_col'], 'Only magnitude fits are implemented.'

        from sklearn import svm

        fL = self.config['filters']
        cols_A = map('mag_{0}'.format, fL)

        Ntrain = self.config['Ntrain']
        inds_train = np.random.permutation(range(len(train)))[:Ntrain]

        Atrain = train.ix[inds_train, cols_A].values
        Btrain = train.ix[inds_train, 'r2eff'].values

        Atest = test[cols_A].values
        Btest = test['r2eff'].values

        t1 = time.time()
        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)
        t2 = time.time()

        t3 = time.time()
        r2pred = reg.predict(Atest)
        t4 = time.time()

        print('time fit', t2-t1, 'time pred', t4-t3, 'ngal', len(Atest))


        r2eff = (r2pred - test.r2true) / test.r2true
        r2sed = pd.DataFrame({'r2eff': r2eff, 
                              'r2pred': r2pred,
                              'r2true':test.r2true}, 
                             index=test.index)


        return r2sed

    def run(self):
        train_cat = self.job.train_cat.result
        train = train_cat.join(self.job.train_r2gal.result)
        
        test_cat = self.job.test_cat.result
        test = test_cat.join(self.job.test_r2gal.result)

        # I want to switch to this notation.
        train['r2true'] = train['r2eff']
        test['r2true'] = test['r2eff']

        self.job.result = self.fit(train, test)
