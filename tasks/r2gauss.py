#!/usr/bin/env python
# encoding: UTF8

import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep

data_dir = '/nfs/pic.es/user/e/eriksen/data/psfw/'

class r2gauss():
    """Create Gaussian R^2 simulations."""

    version = 1.0
    config = {'r2_filter': 'vis',
              'z0': 0.49,
              'zmax': 0.5,
              'pz': 0.0,
              'bias': 0.0,
              'N': 10000,
              'seds': ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']} 


    def gencat(self, z0, zmax, pz,bias):
        """Generate the catalogs."""

        n = self.config['N']

        zs = z0 + (zmax - z0)*np.random.random(n)
        zp = zs + bias + (1+zs)*pz*np.random.normal(size=n)

        # Not exactly record arrays, but sufficiently close.
        return {'zs': zs, 'zp': zp}

    def convolve(self, cat, spls):
        """Estimate r2 after adding the photo-z."""

        df = pd.DataFrame(cat)
        for sed in self.config['seds']:
            r2true = splev(df.zs, spls[sed])
            r2obs = splev(df.zp, spls[sed])

            r = (r2obs - r2true) / r2true

            df[sed] = r

        return df

    def load_spls(self):
        """Load cached splines with the relation between R^2 and z."""

        from xdolphin import Job
        # This is not the usual approach... [DAMN, this is dirty...]
        r2vz_job = Job('r2vz')
        r2vz_job.config['data_dir'] = data_dir
        r2vz_job.config['filter_dir'] = os.path.join(data_dir, 'des_euclid2')
        r2vz = r2vz_job.result
        fname = self.config['r2_filter']

        spls = {}
        for sed in self.config['seds']:
            r2 = r2vz['/{0}/{1}'.format(fname, sed)]
            spls[sed] = splrep(r2vz['z'], r2)

        return spls

    def run(self):
        spls = self.load_spls()

        par = ['z0', 'zmax', 'pz','bias']
        cat = self.gencat(*[self.config[x] for x in par])

        self.job.result = self.convolve(cat, spls)
