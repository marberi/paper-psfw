#!/usr/bin/env python
# encoding: UTF8

import numpy as np
import pandas as pd
#import r2train

import xdolphin as xd
from sklearn import svm
from matplotlib import pyplot as plt

class r2cal:
    """Calibrate the r2bias predictions."""

    version = 1.0
    config = {'cal_sample': True}

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def find_caldf(self, cat):
        """Find the R^2 bias in various bins."""

        zedges = np.linspace(0.1, 1.8, 10)
        medges = np.linspace(-3., 1., 10)

        gr1 = pd.cut(cat.c1, medges)
        gr2 = pd.cut(cat.c2, medges)
        gr3 = pd.cut(cat.zs, zedges)

        df = pd.DataFrame()
        for key,val in cat.groupby([gr1, gr3]):
            # Here I could be only setting selected fields,
            # but this is simpler.
            df = df.append(val.mean(), ignore_index=True)


        return df

    def get_input(self, job):
        """Get the input to the training method."""

        store = self.job.r2train.get_store()
        r2eff = store['r2eff']
        store.close()

        gc = self.job.r2train.galcat.result
        gc['c1'] = gc.mag_r - gc.mag_i
        gc['c2'] = gc.mag_i - gc.mag_z

        gc['r2bias'] = r2eff[0] # HACK

        cat = gc.join(r2eff)

        return cat

    def calibrate(self, cat, cal_df):
        fit_cols = ['c1', 'c2', 'zs']

        reg = svm.NuSVR()
        reg.fit(cal_df[fit_cols].values, cal_df.r2bias.values)

        r2corr = reg.predict(cat[fit_cols])

        return r2corr


    def run(self):
        cat = self.get_input(self.job.r2train)

        if self.config['cal_sample']:
            train_cat = self.get_input(self.job.cal)
            cal_df = self.find_caldf(train_cat)
        else:
            cal_df = self.find_caldf(cat)


        r2corr = self.calibrate(cat, cal_df)
        Nboot = self.job.r2train.config['Nboot']

        r2eff = (cat[range(Nboot)].T - r2corr).T

        cat['rup'] = r2eff[0]

        z = np.linspace(0.1, 1.8, 10)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.plot(mean.zs, mean.r2bias, 'o-', label='Normal')
        plt.plot(mean.zs, mean.rup, 'o-', label='Corrected')

        plt.legend()
        plt.show()

