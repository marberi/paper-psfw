#!/usr/bin/env python
# encode: UTF8

import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep
from scipy.integrate import simps

import sys
sys.path.append('/home/marberi/drv/work/papers/psfw/tasks')
import ab

class ab_nb:
    """Combines the BB filters with a set of syntetic NB filters."""

    version = 1.25
    config = {'sed_dir': 'seds',
              'data_dir': '', 
              'zmax': 12.,
              'dz_ab': 0.001,
              'N': 1,
              'xa': 5500,
              'xb': 9200,
              'filter_dir': 'filters'
             }

    load_curves = ab.ab.__dict__['load_curves']

    def get_ab(self, sedD):

        # Artifical and needs improvement.
        xedges = np.linspace(self.config['xa'], self.config['xb'],
                             1 + self.config['N'])

        splD = {}
        for sed_name in sedD.keys():
            splD[sed_name] = splrep(*sedD[sed_name])

#        trans = 0.5

        z = np.arange(0, self.config['zmax'], self.config['dz_ab'])
        a = 1./(1+z)

        ab = pd.DataFrame()
        ab['z'] = z

        clight_AHz=2.99792458e18

        # The narrow band filters.
        for i,(xai,xbi) in enumerate(zip(xedges[:-1], xedges[1:])):

            # Ok, I could have done the math..
            lmb_edges = np.linspace(xai, xbi, 100)
            lmb = 0.5*(lmb_edges[:-1] + lmb_edges[1:])
            dlmb = lmb[1] - lmb[0]
            w = dlmb*lmb
#            w = dlmb*np.ones_like(lmb) # TEST

            r = 1./simps(1/lmb_edges, lmb_edges) / clight_AHz

            X = np.einsum('i,j->ij', a, lmb)
            for sed in sedD.keys():
                Y = splev(X.flatten(), splD[sed]).reshape(X.shape)
                F = r*np.einsum('ij,j->i', Y, w)

                name = '/{0}/{1}'.format(i, sed)
                ab[name] =  F

        return ab
       
    def run(self):
        sedD = self.load_curves('sed_dir', 'sed')

        ab_bb = self.job.depend['ab']['ab']['ab']

        if not self.config['N']:
            ab = ab_bb
        else:
            ab_nb = self.get_ab(sedD)
            assert (ab_bb.z == ab_nb.z).all(), 'Mismatch in redshift binning.'
            ab = ab_nb.join(ab_bb, rsuffix='_rem')
            del ab['z_rem']

        self.job.store_fixed('ab', ab)
