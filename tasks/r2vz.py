#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import os
import pdb
import sys
import pandas as pd

import bcnz
import bcnz.model
import bcnz.zdata


import numpy as np
from scipy.interpolate import splrep, splev
from scipy.integrate import simps, trapz, quad
from scipy.ndimage.interpolation import zoom

data_dir = '/nfs/pic.es/user/e/eriksen/data/psfw'

class r2vz(bcnz.model.model_mag):
    """Cache the relation between R^2 and z."""

    version = 1.0
    config = {'r2_exp': 0.55, 
              'obj_type': 'gal',
              'dz_ab': 0.001,
              'Nint': 1000,
              'filters': ['vis'],
              'filter_dir': None,
              'data_dir': None}

    _fjc_path = '../data/fjc_temp'

    def __init__(self):
        # Overriding the __init__ from model_mag.
        pass


    def r2_lambda(self, x):
        """The R^2 wavelength dependence."""

        gamma = self.config['r2_exp']
        return x**gamma * 7350**(0.55 - gamma)

    def proj_filter(self, zdata, z_ab, filter_name, Nint=False):
        """Project into filter."""

        Nint = self.config['Nint'] if not Nint else Nint
        
        xrlim = zdata['rlim'][filter_name]
        xedges = np.linspace(xrlim[0], xrlim[1], Nint)
#                             self.ninterp[filter_name])

        xm = .5*(xedges[:-1] + xedges[1:])
        xw = xedges[1:] - xedges[:-1]

        phi_spl = zdata['resp_spls'][filter_name]
        xpart = zdata['r_const'][filter_name]*xw*splev(xm, phi_spl)

        r2_x = self.r2_lambda(xm) #**2.
        a_ab = 1./(1.+z_ab)

        print('r2vz', filter_name)

        res = {}
        for sed in zdata['seds']:
            sed_spl = zdata['sed_spls'][sed]

            ker = np.outer(a_ab, xm)
            ker_shape = ker.shape

            hmm = splev(ker.flatten(), sed_spl).reshape(ker_shape)
        
            I1 = np.sum(r2_x * xpart * hmm, axis=1)
            I2 = np.sum(xpart * hmm, axis=1)

            res[sed] = I1 / I2 

        return res


    def _r2vz_calculate(self, conf, zdata):
        """The actual calulation."""

        z_ab = np.arange(0., conf['zmax_ab'], conf['dz_ab'])

        r2vz = pd.DataFrame()
        r2vz['z'] = z_ab
        for filter_name in zdata['filters']:
            f = self.proj_filter(zdata, z_ab, filter_name)

            for sed in f.keys():
                name = '/{0}/{1}'.format(filter_name, sed)
                r2vz[name] = f[sed]


        return r2vz

    def _r2vz(self, sed_dir, seds, other={}):
        """Create the r2vz array for one set of seds."""

#        # ok, this could be cleaner...
#        myconf = copy.deepcopy(euclid_base.euclid_base.myconf)

        myconf = {}
        dz_ab = self.config['dz_ab']
        myconf.update({'sed_dir': sed_dir, 
                       'filter_dir': self.config['filter_dir'],
                       'filters': self.config['filters'],
                       'data_dir': self.config['data_dir'],
                       'seds': seds, 'dz_ab': dz_ab})
        myconf.update(other)


        conf = bcnz.libconf(myconf)
        zdata = bcnz.zdata.zdata(conf, False)

        return self._r2vz_calculate(conf, zdata)

    def _galaxies(self):
        """R2vz for the galaxies."""

        # Galaxies
        seds_gal = map(str.strip, open(self._fjc_path).readlines())
        seddir_gal = 'seds'

        return self._r2vz(seddir_gal, seds_gal)

    def _stars(self):
        """R2vz for the stars."""

        # Stars.
        seddir_stars = 'seds_stars'
        star_path = os.path.join(data_dir, 'seds_stars/STAR_PICKLES.list')
        A = open(star_path).readlines()
        seds_stars = [x.split('.')[0].split('/')[1] for x in A]

        other = {'zmax_ab': 1.}
        return self._r2vz(seddir_stars, seds_stars, other)

    def run(self):
        # Ok, this part is actually a bit strange. When calling, I always make
        # sure to pass the rigth parameters.

        type_f = {'gal': self._galaxies, 'star': self._stars}
        obj_type = self.config['obj_type']
        if not obj_type in type_f:
            raise ValueError('No such object type: {0}'.format(obj_type))

        print('In the end of r2vz')
        self.job.result = type_f[obj_type]()
