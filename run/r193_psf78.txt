\begin{tabular}{lrrrr}
\toprule
     Filters &     Gal &  Gal-noisy &    Star &  Star-noisy \\
\midrule
       r,i,z & 2.1e-04 &    3.4e-04 & 6.2e-05 &     1.6e-04 \\
         r,i & 1.1e-04 &    1.1e-04 & 1.1e-03 &     1.3e-03 \\
         i,z & 1.4e-03 &    1.4e-03 & 3.7e-03 &     3.6e-03 \\
   vis,r,i,z & 2.4e-04 &    2.9e-04 & 5.3e-05 &     1.3e-04 \\
     vis,r,i & 1.8e-04 &    7.3e-05 & 7.4e-04 &     8.9e-04 \\
     vis,i,z & 1.8e-04 &    6.1e-04 & 2.4e-03 &     2.4e-03 \\
         All & 6.3e-05 &    2.0e-04 & 4.8e-04 &     3.8e-04 \\
   All - vis & 4.5e-05 &    1.3e-04 & 2.4e-04 &     3.8e-04 \\
     All - r & 6.0e-05 &    3.6e-04 & 1.1e-04 &     2.6e-04 \\
     All - z & 5.4e-05 &    1.7e-04 & 3.2e-04 &     1.4e-04 \\
 All - vis,z & 3.7e-05 &    1.0e-04 & 1.2e-04 &     1.2e-04 \\
\bottomrule
\end{tabular}
