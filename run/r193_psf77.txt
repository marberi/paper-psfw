\begin{tabular}{lrrrrrrrr}
\toprule
{} &    g &    r &    i &    z &  vis &    Y &    J &    H \\
\midrule
Galaxies & 24.4 & 24.1 & 24.1 & 23.7 & 24.5 & 23.2 & 23.2 & 23.2 \\
Stars    & 25.1 & 24.8 & 24.8 & 24.4 & 25.2 & 23.9 & 23.9 & 23.9 \\
\bottomrule
\end{tabular}
