Sample &Ell& Sp& Irr\\
\midrule
$R^2$[\text{Peak}] &1.47e-03& -5.82e-05& -1.43e-03\\
$R^2$[\text{Pdf(z)}] &1.15e-03& -2.35e-03& -3.74e-03\\
$R^2$[\text{Pdf(z,sed)}] &1.19e-03& -1.47e-03& -3.01e-03\\
