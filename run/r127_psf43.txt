Sample &Ell& Sp& Irr\\
$R^2$[\text{Peak}] &1.78e-03& 5.33e-04& -5.19e-04\\
$R^2$[\text{Pdf(z)}] &1.92e-03& -2.07e-03& -3.77e-03\\
$R^2$[\text{Pdf(z,sed)}] &1.25e-03& -1.35e-03& -3.00e-03\\
