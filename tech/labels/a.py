#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

import matplotlib

def plot1():
    matplotlib.rcParams['ps.useafm'] = True
    matplotlib.rcParams['pdf.use14corefonts'] = True
    matplotlib.rcParams['text.usetex'] = True

    x = np.linspace(0., 2.)

    plt.plot(x, x**2)
    plt.grid()

    plt.xlabel('Fisk($x^2 F$)', size=16)
    plt.ylabel('Fugl($z^2$)', size=16)

    ax = plt.gca()

    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    #ipdb.set_trace()

    plt.show()

# Second try..
import latexify
latexify.figsize()

x = np.linspace(0., 2.)

plt.plot(x, x**2, label='xxx')
plt.grid()

plt.xlabel('Fisk($x^2 F$)')#, size=16)
plt.ylabel('Fugl($z^2$)')#, size=16)

ax = plt.gca()

#ax.spines['right'].set_visible(False)
#ax.spines['top'].set_visible(False)

#ipdb.set_trace()
plt.savefig('test1.pdf', bbox_inches="tight")
#plt.show()
