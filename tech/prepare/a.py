#!/usr/bin/env python
# encoding: UTF8

# Testing if we can pass mock objects to register which
# entries that will be used.

import ipdb
import numpy as np
import pandas as pd
import sys

import xdolphin as xd

d = '/home/marberi/drv/work/papers/psfw/plots'
sys.path.append(d)

def prepare(pipel):
    cat = pipel.r2train.galcat.result['magcat']['cat']
    cat = cat.join(pipel.r2train.result['r2train']['r2eff'])

    S = pd.Series()
    S['r2bias'] = cat[cat.mag_vis < 24.5][0].mean()

    return S

import trainpipel
pipel = trainpipel.pipel()
pipel.config.update({\
 'r2train.Nboot': 3,
 'r2train.Ntrain': 400})


#S = prepare(pipel)

import register
fpipel = register.Pipeline()
prepare(fpipel)

req = fpipel.requested()

print('requested', req)

xtop = xd.Topnode(pipel)
xtop.runsel(prepare)


ipdb.set_trace()


