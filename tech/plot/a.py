#!/usr/bin/env python
# encoding: UTF8

import ipdb
from matplotlib import pyplot as plt
import numpy as np

x = np.linspace(0.,1.)
y = 10*x

print('hei', x)
plt.plot(x, 2*y, label='line') #, lw=2, color='r')

xline = np.linspace(*plt.xlim())
y_lower = 0*np.ones_like(xline)
y_upper = 10*np.ones_like(xline)

plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.2, label='fill')
plt.legend()
plt.savefig('bals.pdf')
#plt.show()

#ipdb.set_trace()
