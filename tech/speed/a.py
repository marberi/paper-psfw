#!/usr/bin/env python
# encoding: UTF8

import time

N = int(81e6)
t1 = time.time()
for i in range(N):
    pass

t2 = time.time()

print(t2-t1)
