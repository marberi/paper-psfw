#!/usr/bin/env python

import ipdb

class Config:
    pass

class Job:
    def __init__(self):
        self.config = Config()
        self.id = id(self)
        self._depend = {}

    def __getitem__(self):
        ipdb.set_trace()

    def __setitem__(self, key, val):
        self._depend[key] = val
        setattr(self, key, val)

#        ipdb.set_trace()

    depend = __setitem__

def test():
    job1 = Job()
    job2 = Job()

    job2.depend('and', job1)
    job2.depend('fisk', job1)

    ipdb.set_trace()

test()
