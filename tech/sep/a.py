#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

x = np.linspace(0.,3.)
fig, A = plt.subplots(1,2)

for i in range(2):
    A[i].plot(x, x**2, 'o-')
    t = A[i].set_title('Plot: {0}'.format(i))
    t.set_y(1.03)

#    ipdb.set_trace()

#    A[i]
#plt.subplots_adjust(top=0.86) 
plt.show()
#ipdb.set_trace()
