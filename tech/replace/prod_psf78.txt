\begin{tabular}{lrrrr}
\toprule
     Filters &     Gal &  Gal-noisy &    Star &  Star-noisy \\
\midrule
       r,i,z & \num{1.2e-04} &    \num{3.0e-04} & \num{2.1e-04} &     \num{2.8e-04} \\
         r,i & \num{4.2e-05} &    \num{9.8e-05} & \num{9.7e-04} &     \num{1.2e-03} \\
         i,z & \num{1.1e-03} &    \num{1.3e-03} & \num{4.0e-03} &     \num{3.8e-03} \\
   vis,r,i,z & \num{1.4e-04} &    \num{1.8e-04} & \num{1.2e-04} &     \num{2.0e-04} \\
     vis,r,i & \num{2.0e-04} &    \num{1.7e-05} & \num{7.4e-04} &     \num{8.5e-04} \\
     vis,i,z & \num{1.3e-04} &    \num{7.7e-04} & \num{2.7e-03} &     \num{2.6e-03} \\
         All & \num{1.1e-05} &    \num{1.7e-04} & \num{9.9e-04} &     \num{8.3e-04} \\
   All - vis & \num{1.0e-05} &    \num{1.8e-04} & \num{1.7e-04} &     \num{2.5e-04} \\
     All - r & \num{4.0e-05} &    \num{4.7e-04} & \num{1.8e-04} &     \num{1.8e-04} \\
     All - z & \num{1.1e-05} &    \num{9.8e-05} & \num{6.1e-04} &     \num{4.7e-04} \\
 All - vis,z & \num{1.1e-05} &    \num{1.8e-04} & \num{4.4e-04} &     \num{3.7e-04} \\
\bottomrule
\end{tabular}
