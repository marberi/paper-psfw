#!/usr/bin/env python
# encoding: UTF8

class test:
    def __init__(self, x):
        self.__x = x

    def _getx(self):
        print('getting x', self.__x)

        return self.__x


    def _setx(self, val):
        print('setting x')
        self.__x = val

    x = property(_getx, _setx)


A = test(9)
A.x
A.x = 10

print(A.x)
