#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

a = -2.4
b = 0.14

x = np.linspace(0., 1.)
y = a*x + b

plt.plot(x, y, 'o-')
plt.show()

#ipdb.set_trace()
