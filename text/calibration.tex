\subsection{Impact of calibration errors.}
\label{sec:cal_errors}

So far we have assumed that the flux measurements in the various bands
used to infer the effective PSF size are perfectly calibrated and that
the wavelength dependence of the PSF is known. In practice the
zeropoints in the \riz\ bands will vary across the survey, although we
note that (some) modern surveys can achieve impressive homogeneity
\citep[e.g.][]{Finkbeiner16}. Moreover, the wavelength dependence of
the PSF is expected to be well-known, but it will vary with time due
to variations in the optical system. 

A machine-learning algorithm trained on the observed stars establishes the 
mapping  between the observed colours and PSF size, rendering it insensitive 
to zero-point offsets  and errors in the PSF model. Interestingly, problems
with the photometry and PSF model could be identified by examining the
observed sizes to the ones expected based on their SED. For instance, the colour-$R^2_{\rm PSF}$
relation is shifted by a magnitude offset and can thus be used to detect 
biases in the ground based photometry. Deviations from the expected wavelength dependence
of the PSF can be directly tested by comparing the observed PSF to the
wavelength dependent PSF model, $R^2_{\rm PSF}(\lambda)$, convolved with stellar spectra.

Unfortunately we found in the previous section that it is not possible 
to estimate  the effective PSF using the stars alone, because it resulted 
in strong redshift dependent biases.  We therefore need to quantify the impact of 
calibration errors on the estimate of the effective PSF size. We compare the
sensitivity of the two machine-learning implementations discussed in the previous 
section. Although both approaches ultimately rely on simulated galaxies,
the hybrid method may retain some of the advantages of self-calibration,
and thus the sensitivity to calibration errors may still be reduced. In this subsection 
we use the \riz\ observations of stars to train the algorithm, although we note that the results 
are qualitatively the same when only $r,i$ data are used, albeit with a somewhat larger 
scatter in the latter case.

\xbf
\xfigure{prod_psf93.pdf}
\caption{The allowed range in offset in the power law slope of
the PSF size as a function of wavelength, $\gamma$, such that the bias 
in effective PSF size does not exceed the {\it Euclid} requirement. The
black bars indicate the results for the hybrid approach, and the blue bars
when we train using galaxy SEDs. To obtain
these results we applied the  offset to the simulated galaxy and star data, while 
the galaxy training and calibration sample use the nominal value of $\gamma = 0.55$.
This corresponds  to an unknown $\gamma$ shift in the data. 
On the y-axis we show the requirement for the full sample (all) and
  when splitting in redshift bins. The five redshift bins are:
  [0.10, 0.44), [0.44, 0.78), [0.78, 1.12), [1.12, 1.46)
  and [1.46, 1.80), in increasing order.}
\label{cal_gammaoffset}
\xef

We first examine the sensitivity to the wavelength of the PSF, which we 
assume to be a power law $R_{\rm PSF}^2\propto \lambda^\gamma$, 
where $\gamma=0.55$ is the nominal value used so far. We assume
that an error in the PSF model is captured by a change in the value of
$\gamma$. For the full sample and different redshift bins, we
find that the relative bias in effective PSF is a linear function of $\delta\gamma$,
the change in the power law slope. We use these relations to determine the
maximum change $\delta\gamma$ that can be tolerated such that the bias in 
effective PSF size is smaller than the {\it Euclid} requirement. The results are shown in 
Fig.~\ref{cal_gammaoffset} for both training approaches. Note that this
shift is applied to the simulated observations, but that the calibration sample
is not changed.

We find that the lowest redshift bin is remarkably insensitive to changes
in the PSF model, and that the  requirements are rather similar for the two 
training approaches. We do note that the galaxy-only case for the fourth
redshift bin represents a challenge: as the sign of $\delta\gamma$ is in
principle unknown, we obtain $|\delta\gamma/0.55|<2.4\times 10^{-3}$.
When we consider the hybrid method that trains on stars
first, we find that $|\delta\gamma/0.55|<5\times 10^{-3}$ is needed to meet the requirement on 
the relative bias in effective PSF size. Although this may appear challenging, 
such a deviation represents noticeable deviation in the optical model of the PSF.
Moreover, the {\it Euclid} PSF is expected to be very stable in time, and thus
changes in $\gamma$ can be easily monitored.

\xbf
\xfigure{prod_psf92.pdf}
\caption{The allowed range in magnitude offsets that can be tolerated
for {\it Euclid}. The black bars indicate the results for the hybrid method,
and the blue bars when training on galaxies only. In both cases only
  \riz\ data were used to train on. In the top, middle and bottom panels, 
  the offsets are applied to the \bi{r},\bi{i} and \bi{z}-band independently. On
  the y-axis we show the requirement for the full sample (all) and
  when splitting in redshift bins. The five redshift bins are:
  [0.10, 0.44), [0.44, 0.78), [0.78, 1.12), [1.12, 1.46)
  and [1.46, 1.80), in increasing order.}
\label{cal_magoffset}
\xef

We now proceed to examine the sensitivity to errors in the calibration
of the ground-based observations, which may occur because of varying
observing conditions. Repeated observations of the same field
allow for exquisite homogeneity \citep{Finkbeiner16}. In the near
future, however, {\it Gaia} \citeind{gaia} spectrophotometric measurements 
should provide an excellent reference across the full sky in this wavelength range.
Nonetheless it is important to evaluate what errors in photometric
zeropoint can be tolerated. 

To examine this we apply an offset to the simulated observations in each of 
the optical bands used  and calculate the relative changes 
in the effective PSF size. To do so, we change the zeropoint in one
filter while keeping the other measurements unchanged, and 
determine the maximum shifts in zeropoint that are still within
requirements and show the results in Figure~\ref{cal_magoffset} for the full 
sample of galaxies and the five tomographic bins in true redshift.
The results show that the effective PSF is most sensitive to photometric
calibration errors in the $r$ band: for the hybrid method we find
that  $|\delta m|<0.005$  in the $r$-band, whereas $|\delta m|<0.01$ appears 
adequate for the other filters.  Such a level of photometric homogeneity seems quite
achievable. Training on galaxy simulations alone would increase the sensitivity to
magnitude offsets in the \bi{r}-band such that $|\delta m| < 0.003$.
Hence the hybrid method of training on stars, followed by a redshift
dependent adjustment determined using simulated galaxies retains some 
of the self-calibration properties. 

\subsection{Sensitivity to template library}
\label{subsec:detect_cal}

\xbf
\xfigure{prod_psf94.pdf}
\xfigure{prod_psf95.pdf}
\caption{\emph{Top:} The relative $R^2_{\rm PSF}$ bias when limiting the
SEDs in the galaxy training and the calibration sample. Solid and dashed
lines show $\delta R^2_{\rm PSF} \, / \, R^2_{\rm PSF}$ when training
on stars and galaxies, respectively. The template removal is done uniformly
over the 66 SEDs and legend shows the fraction remaining. On
the x-axis it the spectroscopic redshift. \emph{Bottom:} The mean difference
between the $R^2_{\rm PSF}$ predicted when training on simulated observed 
stars and simulated galaxies. The errorbars for both panels are estimated
from 100 {\it Euclid} pointings.}
\label{detect_main}
\xef

Given that both machine-learning approaches rely on a library of simulated
galaxy SEDs, a key remaining question is whether the results are sensitive
to limitations of the template library. In \S\ref{subsec:sedfit} we already
saw that the performance of the template fitting code depends on the
number of templates used, especially if the number was too low. We therefore
return to this issue here and examine the impact of an incomplete SED
library on the machine-learning approaches. 

The top panel in Fig.~\ref{detect_main} shows $\delta R^2_{\rm PSF} \, / \, R^2_{\rm PSF}$
when we limit the SED coverage in the galaxy training sample and
the calibration sample used to adjust the predictions when training on stars first. 
The legend shows the fraction of SEDs which remain after uniformly removing 
templates from the simulations. For a complete sample ($100\%$), the 
bias in effective PSF size is small for both approaches. When omitting galaxy templates, 
the hybrid method performs better at high redshift. Overall, both methods
appear fairly robust to an incomplete template library, although this may need
to explored further; especially the impact of emission-line galaxies and dusty
galaxies has not been explored.

As the hybrid method requires the same template library, one may be tempted
to omit this approach. Regardless, we need to somehow ensure that the
template library is adequate. This can be done by comparing the results
from the two methods, as their dependencies on the template library is 
different. This is evident from the top panel in  Fig.~\ref{detect_main}. The bottom 
panel of Fig.~\ref{detect_main} shows the difference in the estimate of the effective PSF size, 
relative to that of an early type galaxy, as a function of redshift. If the template library is highly 
incomplete, the two PSF estimates differ significantly, especially for high redshift galaxies.
This can thus be used as a way to validate the machine-learning approach.

