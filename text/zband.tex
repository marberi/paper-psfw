\xbf
\xfigure{prod_psf79.pdf}
\caption{Histogram of $\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ values when
training using either \riz\ (blue) or \bi{r} and \bi{i} (red) data.
The solid lines correspond to the case where the algorithm is trained
on stars, whereas the dashed lines show the results when training on
the galaxy simulations. The vertical band marks the {\it Euclid}
requirement for the mean bias.}
\label{riz_ri_hist}
\xef

When training on galaxies, the best performance was obtained when
we used $r$ and $i$ photometry only, which can be understood because 
the VIS passband overlaps only with the blue half of the full $z$-band. 
It is therefore interesting to examine whether $z$-band data can be omitted, 
especially given the relatively large amount of observing time needed to obtain 
these data from the ground.

To compare the performances of the various filter combinations, we
show in Fig.~\ref{riz_ri_hist} the distributions of $\delta R_{\rm
PSF}^2/R^2_{\rm PSF}$. The main impact of omitting the $z$-band is an
increase in the scatter, but this has a negligible impact on the
overall precision of the weak lensing analysis.  We note that the
approach discussed in Appendix~\ref{app_rshift} also works well using
only $r$ and $i$ data (see Fig.~\ref{mag_depth}).

\xbf
\xfigure{prod_psf22.pdf}
\caption{The effect of the magnitude depth on the photometric redshift and
the $R_{\rm PSF}^2$ bias. The points indicate the ratio in the
exposure times relative to the fiducial setup (with values indicated).
The blue lines indicate the results when training using the \riz\ data,
whereas the red lines are for the case where the \bi{z}-band is
omitted. The solid lines are for the results when the algorithm is
trained on stars, and the dashed lines are when the simulated galaxies
are used to train. Note that when the \bi{z}-band is omitted, it is
also not used in the photo-$z$ determination. The vertical band marks
the {\it Euclid} requirement bias in effective PSF size.}
\label{mag_depth}
\xef

The depth of the supporting multi-band photometry for {\it Euclid} is
determined by the need for sufficiently precise photometric redshifts,
but also affects our ability to determine the effective PSF size. It
is therefore important to examine the impact of changes in depth (and
filter coverage) on both of these key aspects of the lensing
measurements.

This is captured in Fig.~\ref{mag_depth} which shows $\sigma_{68}$
and $\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ as a function of exposure time, where
$\sigma_{68}$ is the (average two-sided) 68 per cent limit of $(z_p -
z_s)$, which corresponds to $1-\sigma$ for a Gaussian distribution,
but is less sensitive to the outliers than the rms. The magnitude
errors enter in the estimates of the photometric redshift and the
effective PSF size. In the figure `r' indicates the relative change in
exposure time in the \riz\ bands, relative to the nominal values used
throughout the paper. The photo-$z$ estimate does not include the
{\it Euclid} VIS-band. Not surprisingly, longer exposures
significantly decrease the photo-$z$ scatter and vice versa.  We also
show results without $z$-band observations and find only small
differences in the precision of the photometric redshifts and the bias
in effective PSF size.

The most noticeable result is how insensitive $\delta R_{\rm
PSF}^2/R_{\rm PSF}^2$ is to the change in depth. For the fiducial
exposures all lines are well within the requirement. This can be
attributed to the calibration sample, which also simulates the
measurement noise: when changing the exposure time we also change the noise
level in the calibration sample and the calibration therefore helps to
remove the noise bias. We note, however, that this does increase the
noise in the PSF estimate.  Nonetheless it is clear that the required
precision in the determination of photometric redshifts is the main
driver for the depth of the photometric data.
