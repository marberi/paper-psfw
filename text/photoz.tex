\subsection{Photo-$z$ estimation}
\label{subsec:pz_estim}
Cosmic shear studies rely on photometric redshifts (photo-$z$s) derived from
deep broad-band imaging to relate the lensing signal to the underlying
cosmological model. In this section we explore whether the algorithms
used to determine photo-$z$s can also be used to estimate the size of
the effective PSF.

These algorithms can broadly be divided into two classes. Machine
learning methods train on a set of galaxies where the redshift is
known from spectroscopy to predict the redshift for a larger ensemble
of galaxies only observed using broad-band photometry.  Examples of
learning methods include the neural network algorithms {\tt ANNz}
\citep{annz} and {\tt Skynet} \citep{skynet}. Template based photo-$z$
methods \citep[e.g. {\tt BPZ};][]{benitez2000,coe2006} use libraries of the
restframe galaxy SEDs. For each redshift and galaxy type, one can
model the observed galaxy colours, and the best fit model is found by
\change{maximizing the probability}

\change{
\begin{align}
p(z,\tau) &\propto \exp\left(-\frac{1}{2} \chi^2(z,\tau)\right) 
           p_{\text{Priors}}(z,\tau)
\label{photoz_pz} \\
\chi^2(z,\tau) &\equiv \sum_i \left[ \frac{{\left(\tilde{f}_i -
      f_i(z,\tau)\right)}^2}{\sigma_{\tilde{f}_i}^2} \right] 
\label{thr_photoz_chi2}
\end{align}
}

\noindent
after marginalizing (summing) over the galaxy types ($\tau$). Here
$\tilde{f_i}$ and $f_i(z,\tau)$ are the observed and predicted fluxes
in the $i$th filter, with $z$ and $\tau$ being the galaxy redshift and
template, respectively.  The uncertainty in the flux is assumed to be
Gaussian with a standard deviation $\sigma_{\tilde{f}_i}$.  An
optional prior term $p_{\text{Priors}}(z,\tau)$ adjusts the
probabilities based on galaxy redshift and type, to reflect additional
constraints, for instance the fact that bright galaxies are more
likely to be at low redshift.  This term reduces the number of
degenerate solutions and catastrophic photo-$z$ outliers. In this paper
we use the default {\tt BPZ} priors specified in \citedir{benitez2000}.
The template library is based on the same set of templates used to 
create the simulations.

Unlike learning methods, template based photo-$z$ methods also provide
an estimate of the galaxy SED. From the best fit galaxy restframe SED
($S_{\rm best}(\lambda)$) and photometric redshift ($z_{\rm best}$),
one can estimate the effective PSF size

\be R^2_{\tau,{\rm best}} = \frac{\int d\lambda
  \lambda T(\lambda) S_{\rm best} (\lambda (1+z_{\rm best}))
  \rpsf(\lambda)} {\int d\lambda \lambda T(\lambda) S_{\rm
    best}\left(\lambda (1+z_{\rm best}) \right)},
\label{r2pz}
\ee

\noindent
where the integration is over the observed-frame wavelength. However,
because of measurement uncertainties in the photometry, the template
based codes not only provide the best fit redshift for each galaxy,
but also a redshift probability distribution (eq. \ref{photoz_pz}).
Instead of using the best fit redshift (Eq. \ref{r2pz}), one can instead
estimate a PSF size, weighted by the redshift probability distribution:

\be R^2_{\tau,p(z)} = 
\frac{\int dz p(z) \int d\lambda \lambda
  T(\lambda) S_{\rm best}\left(\lambda (1+z)\right) \rpsf(\lambda)}
{\int dz p(z)\int d\lambda \lambda T(\lambda) S_{\rm best}\left(\lambda (1+z)
  \right)}.
\label{r2pdf}
\ee

\noindent This can be extended further by also including the
probabilities of the galaxy types.  We compare the performance of
these choices for the effective PSF size estimates in
\S\ref{subsec:pzconv}.

The template library uses the following six templates: Ell\_01,
Sbc\_01, Scd\_01, Irr\_01, Irr\_14, I99\_05Gy, which represent a
subset of the SEDs in the galaxy mocks. This limited set of SEDs
reflects the conventional use of photo-$z$ algorithms and the fact that
the real SEDs are not perfectly known. We explore this in more detail
in \S\ref{subsec:sedfit}, but note that the photo-$z$ code does
include two linear interpolation steps between consecutive templates
to mimic a smooth transition between templates. When determining
photometric redshifts from the mock galaxy catalogs we exclude the VIS
band, since it only yields a minor improvement in the photo-$z$
precision, while it would increase the covariance between the
photometric redshift and the shape measurements.

\subsection{Simple scenario}
\label{subsec:pzgauss}

The photo-$z$ algorithm provides an estimate of the restframe SED, while the
calculation of the effective PSF size is done using the galaxy SED in
the observed frame.  The conversion between the two frames requires
the redshift, which causes the redshift biases and uncertainties to
directly affect the estimate of $R^2_{\rm PSF}$ from a template based
photo-$z$ code. The photo-$z$ probability density distribution that is
provided by a template fitting algorithm can be a complex function of
redshift, as degenerate solutions may be found with different best-fit
SEDs. Before examining this complex, but more realistic situation, we
consider a number of simpler cases that allow us to disentangle the
different effects.

\xbf
\xfigure{prod_psf5.pdf}
\xfigure{prod_psf4.pdf}
\caption{Effect of Gaussian photo-$z$ uncertainties on the estimate of
  $R^2_{\rm PSF}$ for a known rest-frame SED. The lines show the
  relative bias $\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ for different
  galaxy templates at $z = 0.5$. In the top panel the photo-$z$ bias
  varies (no photo-$z$ scatter), while the bottom panel varies the
  photo-$z$ scatter (no photo-$z$ bias).  The solid (gray) shaded region
  shows the required accuracy. In the top panel the hatched (blue)
  region indicates a photo-$z$ bias $<0.002(1+z)$ within a redshift bin,
  while it marks a photo-$z$ scatter $<0.05(1+z)$ in the bottom panel.}
\label{r2_gauss}
\xef

The top panel in Fig.~\ref{r2_gauss} shows the absolute value of the 
relative bias in effective PSF size if we assume that the SED is known a 
priori, but where the best-fit photo-$z$ is biased by $|\Delta z_p|$.  While the size
of the bias varies with redshift, we limit the discussion here to
$z=0.5$ for simplicity, and use the full redshift range for the
realistic simulations (see \S\ref{subsec:pzconv}). The amplitude of
the bias increases with increasing redshift bias, with the different
templates yielding rather similar results.  We find that if
$|\Delta z_p|<0.005$ the resulting bias in PSF size is within the
adopted allocation for {\it Euclid} (indicated by the grey shaded
region). This may appear challenging, but a correct interpretation of
the cosmic shear signal requires that the bias in the mean redshift
for a given tomographic bin is known to better than
$|\Delta z|<0.002(1+z)$ \citep{euclidrb} and thus for an ensemble of
galaxies the resulting bias in the PSF size may be sufficiently
small. However, the redshift sampling of photometric redshift codes
is typically $\Delta z\sim 0.01$, which may introduce biases. We examine
this in detail in Appendix~\ref{app_dzresol} and find this is not a concern.

The situation is more problematic when we consider the uncertainty in
the photometric redshift estimate, which we assume to be a Gaussian
with a dispersion $\sigma_z$ around the correct redshift (i.e. no
bias).  The bottom panel in Fig.~\ref{r2_gauss} shows the relative
bias in the effective PSF size as a function of $\sigma_z/(1+z)$ for
galaxies at $z=0.5$, demonstrating that a small photo-$z$ scatter can
cause a substantial bias in the estimate of the effective PSF
size. The bias increases with increasing uncertainty, with the largest
bias occurring for the Irr\_01 template.  The requirements for the
cosmic shear tomography for {\it Euclid} are that
$\sigma_z/(1+z)<0.05$ \citep{euclidrb}, and hence the resulting biases
in PSF size are somewhat larger than can be tolerated. \change{
However, in reality one does not only use a galaxy at a single redshift (here
$z=0.5$), but an average over a sample of galaxies within a tomographic bin. This
can potentially reduce the bias and thus these numbers should not be considered
appropriate requirements. Nonetheless they indicate that the statistical
uncertainties in the photometric redshifts are important.}

\subsection{Conventional template fitting method}
\label{subsec:pzconv}

After considering the simplistic case of galaxies with a known SED and
Gaussian photo-$z$ errors, we now examine the performance
of a template fitting method using the more realistic galaxy
simulations described in \S\ref{subsec:pzmocks}. As a consequence, the
results include redshift outliers and misestimates of the galaxy
rest-frame SED.  


%\makebox[\textwidth][\c]{
\begin{table}
\begin{center}
\begin{tabular}{ l c c c c }
\hline
\xinput{prod_psf43.txt}
\hline
\end{tabular}
\end{center}
\caption{The mean relative bias in effective PSF size times \change{$10^{4}$} when using
  a photo-$z$ template fitting method. Columns show the results
  by galaxy type: Elliptical (Ell), Spiral (Sp) or Irregular (Irr) galaxies.
  The rows list the relative bias when using the best fit photo-$z$ (Peak) and
  when weighting using the redshift pdf (PDF).}
\label{tbl_pzbias}
\end{table}
%}

%\newcommand{\fracerr}{$(R^2(\text{obs}) - R^2) / R^2$ }
\xbf
\xfigure{prod_psf6.pdf}
\caption{The distribution of relative biases
  $\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ for different galaxy types
  using a photometric redshift template fitting code. The galaxies are
  split into Elliptical, Spiral and Irregular types based on
  the definition in the input mock catalogue. The vertical band marks
  the {\it Euclid} requirement for the mean relative bias.}
\label{r2pz_hist}
\xef

As mentioned earlier, the template fitting algorithm provides an
estimate for the best fit redshift and rest-frame SED, but also a
probability density distribution for the redshift (which may be
combined with a distribution of templates $\tau$). The different
outputs can be used to estimate the average bias in the effective PSF
size. Table~\ref{tbl_pzbias} lists the resulting average values for
$\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ when splitting by the true galaxy
types. If we consider the best fit redshift estimate, the biases are
small for both the spiral and irregular galaxies, whereas the biases
are large for early type galaxies, irrespective of the weighting
scheme.

It is also instructive to examine the distribution of biases for the
different galaxy types. Figure~\ref{r2pz_hist} shows that the
distribution of effective PSF sizes is much broader than the
requirement. For all three galaxy types the distribution peaks close to zero, 
and the bias for all three types is caused by the skewness towards larger $R_{\rm PSF}^2$
values, because the redshift errors and SED misestimates do not
fully cancel.

\xbf
\xfigure{prod_psf9.pdf}
\caption{Redshift dependence of the relative bias in the effective PSF
  size when a photo-$z$ template fitting method is used. The plot shows
  results split by input galaxy type and for two approaches to
  determine $R^2_{\rm PSF}$, i.e. using the best fit photometric
  redshift, or using the full $p(z)$. In both cases we do not
  marginalize over the uncertainty in the SED. The horizonal band
  marks the {\it Euclid} requirement.}
\label{r2pz_redshift}
\xef

The results in Table~\ref{tbl_pzbias} are averages over the full
redshift range, but the broad distributions in Fig.~\ref{r2pz_hist}
for the three galaxy types suggest that other parameters play a
role. Figure~\ref{r2pz_redshift} shows the relative bias in
$R^2_{\rm PSF}$ for different estimators as a function of redshift.
Note that the number of irregular galaxies in our simulations is
negligible at $z>1.5$, because they are fainter than our magnitude
cut, and the measurements therefore only extend to this redshift. The variation 
as a function of redshift is the main cause of the broad
distributions in Fig. ~\ref{r2pz_hist}, and is much larger than can
be tolerated.  This demonstrates that a simple average over the full
galaxy sample is not adequate.  In general using the best-fit redshift
and the redshift weighting yields very similar results, although the
averages differ somewhat. We therefore focus on the results using
the best-fit redshift below.


\subsection{Restricted template fitting}
\label{subsec:sedfit}

It is interesting to investigate which parameters of the experimental setup
affect the bias in effective PSF size the most, as they may provide clues
how to improve the performance. We therefore examine a range
of scenarios where we modify the set of filters used, examine the SED coverage
of the algorithm, as well as the role of the photo-$z$ priors.

\xbf
\xfigure{prod_psf7.pdf}
\caption{Effect of a redshift error on the estimate for
  $R_{\rm PSF}^2$. The plot shows the relative bias
  $\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ when using a photo-$z$ template
  fitting method for three input galaxy types as a function of the
  difference between estimated and true redshift. The horizonal band
  marks the {\it Euclid} requirement.}
\label{r2pz_deltaz}
\xef

\change{The
PSF size depends on the observed frame SED in the VIS band, which is used for
shape measurements. One might naively assume that the photo-z method to select
a template and redshift combination with an accurate observed frame color, even
when the redshift is wrong. To test this hypothesis, Fig.~\ref{r2pz_deltaz}
shows the relative bias in the effective PSF size as a function of the
difference between the best-fit photo-$z$ $z_p$ and the true redshift $z_s$.
For the late type SEDs the results look qualitively similar to what was found
in \S\ref{subsec:pzgauss}, but for the early type galaxies the effective PSF size is
overestimated consistently. Hence it is incorrect to assume that errors in the
photo-z estimate are compensated by selecting a different SED.  We find that
only using \riz\ data does not reduce the bias.}


In template based photo-$z$ methods the flux measurements in the various filters
are compared to the model, and additional priors are used to restrict the range
of solutions. The former can be split further into the contributions that arise
from the \riz\ filters that overlap with the VIS band, and the out-of-band filters
($\bi{g},\bi{Y},\bi{J},\bi{H}$ in our case). Minimising $\chi^2$ using the contributions from
the out-of-band filters and the photo-$z$ priors only provides indirect information
on the SED in the VIS band and may thus lead to biases in the estimate for
the PSF size. It is therefore of interest to examine whether the performance
improves by restricting the filters used. Reducing the statistical uncertainties
in the flux measurements may provide another way to improve the performance.

\begin{figure*}
\yfigure{prod_psf31.pdf}
\caption{$\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ for the SED template
    fitting method. On the x-axis the exposure time is scaled relative
    to the fiducial setup. In the left panel the simulated galaxy
    catalog only include the 6 templates used in the photo-$z$ code,
    while for the right panel it includes the full 66 templates. The
    lines show which combination of filters was used (All or
    $r,i,z$), whether priors were used (No priors or Priors),
    and how many SEDs were used. The vertical line indicates
    the fiducial setup, while the shaded (gray) band marks the
    {\it Euclid} requirement.}
\label{r2sed}
\end{figure*}

Figure~\ref{r2sed} shows the relative bias for the full sample as a
function of exposure time (relative to the nominal case), where we
assumed that the increase in exposure time is the same for all
filters, including the {\it Euclid} VIS and NIR filters. We do so for
different setups. In the left panel we create simulated catalogs using
only six distinct SEDs, whereas in the right panel the full range of
SEDs is used. We do keep the luminosity functions for the various
galaxy types unchanged, but rather assign slightly different SEDs to
each type.

For the simulations with six SEDs (left panel) there are several
combinations of filters that meet the requirement on the average
relative bias (as indicated by the grey region).  If the filter set is
restricted to \riz, the priors are needed to reduce the average bias
for the fiducial exposure time, but otherwise applying the photo-$z$
code with only six templates performs well.  As expected, in the noiseless limit
the bias vanishes for all setups with six templates.  While
the galaxy priors and the \bi{g},\bi{Y},\bi{J},\bi{H} only contribute
indirectly to constrain the SED within the VIS band, including these
does reduce the bias in the PSF size. 

Of particular interest are the two cases where the photo-$z$ algorithm
uses all 66 templates in the analysis: the biases are larger, although
they do vanish in the noiseless case (with the \riz\ scenario
converging outside the plot). The larger bias can be understood,
because the noise causes the algorithm to select SEDs not in the
simulations. While adding more templates in the fitting code may be
tempting, this result cautions us that it can also lead to biases.

The right panel in Fig.~\ref{r2sed} shows the results when the full
range of SEDs is used to create the simulated catalogs. In this case
we find that using only six templates in the photo-$z$ algorithm leads
to biases, even in the noiseless case. The \riz\ setup without photo-$z$
priors accidentally meets the requirements for the nominal exposure
time. Using the full range of SEDs in the analysis improves the
performance, as expected, with the best results for the case where all
filters are used. These results highlight the need for the photo-$z$
templates to span the full set of SEDs in the observations, which may
be challenging in practice.


