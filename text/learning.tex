The results presented in the previous section suggest that modifications to the 
template fitting codes are needed if these are to be used to determine the effective PSF.
Motivated by the fact that Fig.~\ref{gal_star_triangle} shows that the PSF sizes for galaxies
correlate strongly with the observed colour, we explore the use of machine learning methods 
as a possible alternative to map between the observed colours and the effective PSF. 

\change{Although machine learning techniques are fast, flexible and easy to
implement, the spatial variation of the PSF introduces additional complications
which may be less straightforward to implement. In contrast, if the system PSF
is known, the effective PSF is readily computed from the PSF model, which
predicts the dependence of wavelength, and the SED from a template fitting
code.} Regardless of which approach may be best suited for the analysis of {\it
Euclid} data, quantifying the performance of the machine-learning methods
allows us to assess whether the supporting ground-based observations for {\it
Euclid} are adequate to infer the effective PSF.


\subsection{Training on simulated galaxy photometry}
\label{subsec:basic_train}

\xbf
\xfigure{prod_psf80.pdf}
\caption{Histogram of $\delta R^2_{\rm PSF}/R^2_{\rm PSF}$ values when
using a template fitting photo-$z$ code (dashed green) and when
training on \riz\ data from a simulated galaxy catalog (blue).}
\label{r2pz_r2train}
\xef

To explore the performance of machine learning we create a training sample of galaxy 
simulations using the same procedure as the test catalog (as described in
\S\ref{subsec:pzmocks}). We start with a best case scenario where the
training set does not contain noise. The {\tt NuSVR} algorithm from
{\tt scikit-learn} \citeind{scikitlearn} is used to train on a
sample of 4000 galaxies with multi-wavelength measurements.
The galaxy training and test catalogs are generated with
the same algorithm, but they are separate realisations. 

The results are used to estimate the effective PSF sizes of the test
catalog (which does contain noise). A histogram of the residuals
when we train on 4000 galaxies with \riz\ photometry
is presented in Fig.~\ref{r2pz_r2train} (solid histogram). The
distribution of residuals is fairly symmetric and centred around zero
bias: we observe an average value of
$\delta R_{\rm PSF}^2/R_{\rm PSF}^2=3.5\times 10^{4}$ (also see
Table~\ref{training_table}). For comparison we also show (dashed
histogram) the residuals from the template fitting method (all
filters, priors, photo-$z$ peak). The residuals from the SED fitting
method also peak around zero, but the distribution is noticeably
skewed, which leads to a significant bias when averaged over the full
sample.

We also consider a number of scenarios where different filter combinations
are used to train. The resulting average biases are listed in Table~\ref{training_table}. 
The results in the second column are for noiseless photometry, whereas
noisy data were used in the training for the results listed in the third column.
We find that all configurations perform well, with the exception of the case
where only $i,z$ data are used to train.  This can be understood from
Fig.~\ref{gal_star_triangle}, which shows that for all colours the
colour-$R^2_{\rm PSF}$ relation is tightest for colours that include the
\bi{r}-band.  Interestingly, we find that the combination of $r,i$
performs better than the \riz\ setup. This is because the VIS filter
covers only the blue half of the $z$-band. Hence the information
contained in the $z$-band measurements is of limited value as some of
the flux falls outside the VIS filter. We verified this with a test
where the $z$-band was restricted to the blue half: the bias was
reduced, whereas the bias increased when we used the redder half.

Although perhaps somewhat counterintuitive, we advocate to exclude the
$z$-band when a restricted set of filters is used to estimate the
effective PSF when training using galaxy templates. We explore this
setup in more detail in Appendix~\ref{app_zband}. Including more
filters does reduce the bias, but it is not clear whether this would
work in practice because of variations in photometric
calibration. Especially including the NIR data would lead to
additional requirements on the relative calibration between the
ground-based optical data and the {\it Euclid} NIR data.

\begin{table}
\begin{center}
\xinput{prod_psf78.txt}
\caption{
  The absolute value times $10^{4}$ of the relative bias  $|\delta R^2_{\rm PSF}|/R_{\rm PSF}^2$ when 
  using a machine learning method
  for different instrumental setups and training approaches. The first
  two columns (Gal) list results when the algorithm is trained on
  galaxy simulations, while the last two columns (Star) correspond to the
  biases when the algorithm is trained on stars. The columns marked
  `-noisy' include noise and the others are noiseless.  The first column
  lists the filters used. Here 'All' means \bi{g},\bi{r},\bi{i},\bi{z},VIS,\bi{Y},\bi{J},\bi{H}.
  and `All - filter' means that the filter was omitted.}
\label{training_table}
\end{center}
\end{table}

\subsection{Training on observed photometry of stars}
\label{subsec:star_train}

So far, our attempts to predict the effective PSF size have focused on
galaxy templates. The predicted SED is then used with a model of the
wavelength dependence of the PSF to compute the value for
$R_{\rm PSF}^2$. On the other hand, the PSF properties, including
higher order moments of the surface brightness distribution, can be measured 
directly for stars in the data.  Moreover, Fig.~\ref{gal_star_triangle} shows that 
galaxies and stars with the same colour have very similar effective PSF sizes. 
It is therefore interesting to examine whether it is possible to train on a sample of
stars instead. 

The main benefit of such an approach is the potential of constructing a self-calibrating method: 
when training on, and applying the results to the same pointing, this implementation would
be rather insensitive to calibration errors in the photometric zero-points. The effective PSF 
can be computed using the wavelength-dependent model of the PSF and the SEDs of the stars. 
The resulting mapping between effective PSF parameters and observed star colours could then 
also be applied using the observed colours of galaxies. As before, we focus on the effective PSF size
for simplicity.

We start with noise-free measurements of the colours of stars. For the
star catalogs we use 400 stars, uniformly sampled from stellar SEDs in
the Pickles \citeind{pickles} library. A typical {\it Euclid} pointing
is expected to contain more stars, and thus these results give a
conservative indication whether self-calibration is feasible. On the
other hand, the distribution of stellar SEDs is wider than in actual
data. We explore this further in \S\ref{subsec:tomo} where we use a
realistic distribution of spectral types from the second data release
of the KiloDegree Survey \citep[KiDS;][]{Kuijken15}. 

To mimic a self-calibration procedure, we create 100 independent
pointings, and train the NuSVR algorithm on the star simulations.  
The last two columns in Table~\ref{training_table} list the results
when we train using the star catalogs. The biases are generally small,
and in some (noise-free) cases this approach outperforms the training
on galaxy templates. This the consequence of the fact that the stars
show a remarkably simple relation between the colour and
$R_{\rm PSF}^2$ (see Fig.~\ref{gal_star_triangle}). An important
difference is the increase in bias when excluding the \bi{z}-band,
which did not occur when training with galaxies. Including the
VIS band reduces the bias, albeit with limited effect. As was the
case for the galaxies, including VIS and NIR data can help, provided
the relative calibrations between the various data sets can be
ensured.

\subsection{Tomography and calibration sample}
\label{subsec:tomo}

% Previous Fig.10, which the reviewer did not like.
\begin{comment}
\xbf
\xfigure{prod_psf66.pdf}
\caption{Effective PSF size $R^2_{\rm PSF}$, relative to the value
for the Ell\_01 SED at $z=0$, versus colour for filters
that overlap with VIS. Stars are marked as dots, while galaxies
are binned in redshift ranges and shown as lines. The hex-bins shows
the density for the full galaxy population. No magnitude noise is
included.}
\label{stars_failure}
\xef
\end{comment}

The constraints on cosmological parameters from cosmic shear surveys
are improved significantly if the source sample is split in a number
of narrow redshift bins, such that they are sensitive to the matter
distribution at different redshifts.  Such `tomographic' analyses are
now standard for cosmic shear studies \citep[e.g.][]{Heymans13, Becker16, Jee16, 
Hildebrandt17}, and therefore it is not sufficient to consider the bias for the full
sample, especially because the effective PSF size varies strongly with
redshift. Hence, even though training on stars results in an overall
small bias in the effective PSF size, we need to ensure that the bias
does not vary significantly with redshift. We already saw that this
was problematic for template fitting methods (see
Fig.~\ref{r2pz_redshift}).

\xbf
\xfigure{prod_psf74.pdf}
\caption{The relative bias in effective PSF size for different
  calibration methods, when 400 stars that are uniformly sampled from
  the Pickles SED library \citeind{pickles} are used for the training
  step. The dashed-dotted blue line shows the results without further
  calibration, showing a strong redshift dependence. The dotted red
  line indicates the results when the bias is adjusted based on the
  \bi{r}-\bi{i} colour. The dashed green line shows that the bias can
  be reduced significantly when an redshift-dependent offset in the
  \bi{r} magnitude is applied. The solid black line uses a stellar SED
  distribution from fitting to KiDS data (see text). The errors are estimated from 100
  pointings and the horizontal band marks the required accuracy.}
\label{learning_calibrated}
\xef

\change{
To examine this in more detail, we train on a star catalog using the \riz\
bands and compute the residual bias in the effective PSF size as a function of
redshift. The results are presented in Fig.~\ref{learning_calibrated} by the
blue dashed line, and show a clear redshift dependence. At low and high
redshift there is a high bias from the stars and galaxies having a different
relation between color and $R^2$, which is problematic for a machine learning
method. This result is similar to the conclusion reached by \cite{cypriano} who
considered only a single colour at a time. Such a redshift dependent bias is
problematic as it may mimic an interesting cosmological signal, and our results
demonstrate that it is not sufficient to specify a requirement for the full
sample.}

We explored various approaches to model the redshift-dependent bias,
but were unable to do so directly. Instead we opted for a hybrid
approach using a simulated galaxy catalog as a `calibration'
sample. We train on the stars as before, but use the simulated galaxy
catalog to adjust the method to create an unbiased estimate of the
effective PSF size.  Although such an implementation is no longer
fully self-calibrating, training on the observed stars may still
valuable, if it can reduce the sensitivity to errors in the photometric
calibration and the wavelength dependence of the PSF model, as well as
the adopted library of galaxy templates (see \S\ref{calibration} for
more details). On the other hand, the performance may still be limited
by the fidelity of the calibration sample that is used.

One possibility is to adjust the bias in effective PSF size by
accounting for the difference in size as a function of $r-i$ colour
between stars and galaxies, as is indicated in
Fig.~\ref{stars_failure}. The dotted red line in
Fig.~\ref{learning_calibrated} demonstrates, however, that this does
not alleviate the strong redshift dependence. Instead, as is explained
in more detail in Appendix~\ref{app_rshift}, we found that it is
possible to reduce the bias effectively by introducing an \bi{r}-band
offset that depends on redshift, without increasing the scatter
significantly. However, in practice photometric redshifts are used,
and we examine the consequences of this in the next subsection.

The adopted redshift-resolution for this correction allows us to adjust the 
importance of the galaxy templates on the results: if we assume no redshift
dependence the method reverts back to training on stars, whereas a very fine
redshift sampling is identical to training on galaxy templates. As discussed
in Appendix~\ref{app_rshift} we adopted a redshift sampling of $\Delta z = 0.18$, which
appeared to be a reasonable compromise between the two extremes.
In \S\ref{sec:cal_errors} we compare the performance of the hybrid approach
to the training on galaxy templates in the presence of calibration errors in the 
photometric data.

The distribution of stellar SEDs is expected to vary, in particular as a function of Galactic  coordinates.
To explore the impact of such variations we determine the SED distribution by fitting the
Pickles library to the \bi{g}, \bi{r}, \bi{i}, \bi{z} for
stars ($i_{AB}< 21$, $\text{CLASS\_GAL} > 0.8$) in KiDS DR2. For
each {\it Euclid} pointing we simulate 400 stars generated
using the stellar distribution in a KiDS pointing limited to
the 15 most frequent templates. We find that the bias is essentially
unchanged compared to the case of training on a uniform distribution of templates.

\subsection{Correlations between photometric redshift and effective PSF size}
\label{subsec:pzcorr}

\xbf
\xfigure{prod_psf17.pdf}
\xfigure{prod_psf82.pdf}
\caption{\emph{Top:} Pearson correlation coefficient between the
  residual bias in the effective PSF size,
  $\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ and photo-$z$ error, $\delta z$,
  as a function of true redshift. The effective PSF size was estimated
  using the \riz\ bands only, whereas the photometric redshifts use
  measurements from all available filters. The blue lines show the
  results when galaxy mocks are used for the training, whereas the red
  lines show the results when stars are used (but no additional
  calibration applied). The solid lines indicate the results for the
  realistic case when the same \riz\ data are used to measure both PSF
  size and photometric redshift (i.e., the noise is in common),
  whereas the dashed lines are for independent realisations of the
  data. The error bars are estimated from simulating 100 pointings.
  \emph{Bottom:} Histogram of $\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ for
  the redshift range [0.4, 0.5] when selecting either on spec-z
  (solid black) or photo-$z$ (dashed blue). The training uses stars in
  the \riz\ bands and the histograms comprise 100 independent
  pointings.}
\label{pz_corr}
\xef

The results presented in Fig.~\ref{learning_calibrated} show that
the biases as a function of {\it true} redshift can be reduced to the
required level. However, in practice, tomographic bins are based on
the photometric redshifts. The large uncertainties and potential
outliers may affect the performance of the approach outlined
above. Moreover, as the photometric redshift estimate and the
determination of the effective PSF make use of the same data (at least
in part), correlations may be introduced. We explore these more
practical complications here.

To quantify the correlation between the relative error in the inferred
effective PSF size $\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ and the error
in the best fit photometric redshift
$\delta z\equiv (z_{\rm b}-z_{\rm true})/(1+z_{\rm true})$ we define
the Pearson correlation coefficient 

\newcommand{\var}{\text{Var}} 
\be
r(\delta R_{\rm PSF}^2, \delta z) \equiv \frac{\left< \delta R_{\rm PSF}^2, \delta
    z\right>} {\sqrt{\var(\delta R_{\rm PSF}^2) \var(\delta z)}}
\label{rpz_coeff}
\ee

\noindent
where $\var(.)$ is the variance. The solid lines in Fig.~\ref{pz_corr} show the
correlation coefficient between the error in the effective PSF size
and $\delta z$ as a function of true redshift when training on
galaxies or stars, respectively. In both cases we find a significant
positive correlation between the errors between $0.4<z<1$ and an
anti-correlation at high redshifts. In contrast, the dashed lines show
the results when the effective PSF size and photometric redshift
determinations are obtained using independent data sets, i.e.
independent noise realisations.  In this case the correlation
coefficient is close to zero for both training cases.

\xbf
\xfigure{prod_psf76.pdf}
\caption{$\delta R_{\rm PSF}^2/R_{\rm PSF}^2$ as a function of
  photometric redshift, using either the photometric redshift or the
  true redshift in the calibration catalog. The training uses
  observations of stars in the \riz\ bands and the uncertainties are
  estimated from 100 pointings.}
\label{pz_calib}
\xef

These results demonstrate that the (re-)use of photometric data for
different measurements may lead to redshift dependent correlations
between residuals. To examine this in more detail we consider a single
redshift bin with $0.4<z<0.5$, where the selection can be done either
based on spectroscopic or photometric redshift. The resulting
distributions of the relative bias in effective PSF size are presented
in the bottom panel of Fig.~\ref{pz_corr}.  When the galaxies are
selected by spectroscopic redshift the distribution is symmetric, and
the mean bias meets our requirement.  However, selecting galaxies
based on their photometric redshift leads to a skewed distribution
with a significant bias in the mean. This is the result of the
correlation between $\delta z$ and
$\delta R_{\rm PSF}^2/R_{\rm PSF}^2$: a selection in photometric
redshift leads to a selection in effective PSF size. Hence an unbiased
estimate for tomographic bins needs to account for this correlation.

This is achieved naturally when correcting the bias in effective PSF
size using the simulated galaxy catalog: the redshift dependence of
the applied offset in $r$ magnitude can be determined by splitting the
galaxies as a function of photometric redshift. As shown by the solid
black line in Fig.~\ref{pz_calib}, this removes the impact of the
correlation between the $\delta z$ and
$\delta R_{\rm PSF}^2/R_{\rm PSF}^2$, since this is also included in
the calibration sample.  In contrast, when the calibration sample is
instead split based on the true redshift, $z_s$, the bias exceeds
requirements (blue dashed line). These results indicate that the
calibration step can reduce the bias when using photometric redshifts,
provided that the simulations are sufficiently accurate. We note, however,
that the training needs be done {\it after} the tomographic bins have been
defined.
