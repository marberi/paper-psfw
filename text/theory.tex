\label{theory}
\subsection{Effective PSF size}

To infer cosmological parameters from the lensing data we need to
measure the correlations in the shapes of galaxies before
they were modified by instrumental and atmospheric effects. In the
following we ignore detector effects, such as charge transfer
inefficiency, which have been studied separately
\citep[e.g.][]{Massey14, Israel15}. Instead we examine how well we can
estimate the size of the effective PSF given available broad-band
observations.

We start by defining the nomenclature and notation. Throughout the
paper we implicitly assume that measurements are done on images
produced by a photon counting device, such as a charge-coupled device
(CCD). In this case the observed (photon) surface brightness or image
at a wavelength $\lambda$, $I({\bf x};\lambda)$, is related to the
intensity $S ({\bf x};\lambda)$ through
$I({\bf x};\lambda)=\lambda S ({\bf x};\lambda)T(\lambda)$, where
$T(\lambda)$ is the normalised transmission. For the results presented
here we assume that the {\it Euclid} VIS filter has a uniform transmission
between 5500-9200\AA, and that all the light is blocked at other
wavelengths. The image of an object is then given by

\be I^{\rm obs}({\bf x})=\int I^{\rm 0}({\bf x};\lambda)\otimes P({\bf
  x};\lambda){\rm d}\lambda, \ee

\noindent where $P({\bf x};\lambda)$ is the wavelength-dependent PSF,
and $I^{\rm 0}$ is the image of the object before
convolution. Following \cite{massey13}, we use unweighted quadrupole
moments $Q_{ij}$, which are defined as

\be Q_{ij}=\frac{1}{F}\int {\rm d}\lambda \int x_i x_j I({\bf
  x};\lambda) {\rm d}^2{\bf x}, \ee

\noindent where $F$ is the total observed photon flux of an image
$I({\bf x})$.

The moments can be used to estimate the shape and size of an object. A
complication is that the observed moments are measured from the noisy
PSF-convolved images, and the challenge for weak lensing algorithms is
to relate these to the unweighted quadrupole moments of the true
galaxy surface brightness distribution.  Throughout the paper we
assume that this is possible, and thus that we can use the fact that
the unweighted quadrupole moments ($Q_{ij}^{\rm 0}$) are related to the observed
quantities ($Q_{ij}^{\rm obs}$) through \citep{semboloni_colorgrad}:

\be
Q_{ij}^{\rm 0}=Q_{ij}^{\rm obs}-\frac{1}{F}\int F(\lambda)P_{ij}(\lambda){\rm d}\lambda,
\ee

\noindent where $F(\lambda)\equiv \lambda S(\lambda)T(\lambda)$
explicitly indicates the wavelength dependence of the observed photon
flux in terms of the transmission $T(\lambda)$ and $S(\lambda$), the
spectral energy distribution (SED) of the object.  $P_{ij}(\lambda)$
are the quadrupole moments of the PSF as a function of wavelength. The
second term defines the quadrupole moments of the effective PSF and
the main focus of this paper is to quantify the bias in the
measurements of galaxy shapes that arise from the limited knowledge of
the galaxy SEDs. Clearly errors in the PSF model itself contribute as
well, but we assume that these are determined sufficiently well
\citep[see e.g.][]{cropper}, although we briefly return to this in
\S\ref{calibration}.

The main complication for weak lensing measurements is that the
estimate for the effective PSF depends on the rest-frame SED of the
galaxy and its redshift, whilst neither are known a
priori. Importantly, given the large number of sources that need to be
observed to reduce the statistical uncertainties due to shape noise,
only broad-band photometry is available to estimate the SEDs and
photometric redshifts. The aim of this paper is to quantify whether
this limited information is sufficient for the accuracy we require in
the case of {\it Euclid}.

The biases in shape measurement algorithms are commonly quantified by
relating the inferred shear $\gamma$ (or measured ellipticity) to the true
value \citep{Heymans06}

\be
\gamma^{\rm obs}=(1+m)\gamma^{\rm true}+c,
\ee

\noindent where $m$ is the multiplicative bias and $c$ the additive
bias. \cite{massey13} examined the various terms that contribute,
including errors in the PSF model. PSF errors will lead to both
additive and multiplicative biases, although the former can be studied
from the data themselves \citep[e.g.][]{heymans2012}. Relevant here is
the bias in the estimate of the effective PSF size. We define the size
of the PSF in terms of the quadrupole moments as:

\be R_{\rm PSF}^2(\lambda)=P_{11}(\lambda)+P_{22}(\lambda), \ee

\noindent where $\rpsf(\lambda)$ is the size\footnote{Throughout the paper we refer to 
this definition as size, but note here that it  really corresponds to an area.} of the wavelength
dependent PSF, which we assume to be described by a power law

\be
\rpsf(\lambda) \propto \lambda^{0.55},
\label{r2def}
\ee

\noindent where the value of the slope is found to be a good fit to
results from simulated {\it Euclid} PSF models. In principle the wavelength
dependence can be predicted from a physical model of the optical
system, or it can be determined from careful modelling of calibration
observations of star fields. The PSF modelling greatly benefits from
the fact that stellar SEDs are well-known and well-behaved. The
observed effective PSF size $R^2_{\rm PSF}$ is then given by

\be
R^2_{\rm PSF} = \frac{1}{F}\int {\rm d}\lambda F(\lambda) \rpsf(\lambda).
\label{r2calc}
\ee


\begin{figure}
\xfigure{prod_psf2.pdf}
\caption{The change in $R_{\rm PSF}^2$ as a function of redshift for
  different galaxy types, normalised to an early type (Ell\_01)
  spectrum at $z=0$. The six lines corresponds to the templates
  Irr\_01, Irr\_14, Ell\_01, Scd\_01, Sbc\_01 and I99\_05Gy from the
  CWW template library. The two vertical lines at z=0.4,1.3 indicate
  where the $\lambda = 4000$\AA\ break is entering and leaving the VIS
  filter.}
\label{r2_zdep}
\xef

\cite{cropper} presented a detailed breakdown of the various
systematic effects based on the expected performance of {\it
  Euclid}. It includes an allocation with the description `wavelength
variation of PSF contribution' in their Table~1, for which a
contribution to the relative bias in effective PSF size of
$|\delta R^2_{\rm PSF}/R^2_{\rm PSF}|=3.5\times 10^{-4}$ is listed.  To
include margin for additional uncertainties, we adopt here a slightly
more stringent requirement of

\be \left|\frac{\delta R_{\rm PSF}^2}{R^2_{\rm PSF}}\right| \equiv \left | \left
    <\frac{R^2_{\text{Pred}} - R^2_{\rm PSF}}{R^2_\text{PSF}}
  \right> \right |< \euclidreq,
\label{r2requirement}
\ee

\noindent where we explicitly average over an ensemble of galaxies
($\left< . \right>$).  The predicted value of the effective PSF size,
$R^2_{\text{Pred}}$, is the one we will attempt to estimate using
supporting broad-band observations in multiple passbands, whereas the
correct value is given by $R^2_{\text{PSF}}$. Note that this
requirement is considerably tighter than what was studied in
\cite{cypriano}.

This requirement is to be contrasted with the expected variation in
effective PSF size for different galaxy types and
redshifts. Figure~\ref{r2_zdep} shows the relative change in
$R^2_{\rm PSF}$ for five different galaxy SEDs as a function of
redshift. Both the variation between galaxy types at a given redshift,
and the variation with redshift for a given SED template are about two
orders of magnitude larger than the requirement given by
Eqn.~\ref{r2requirement}. This figure highlights that incorrect
estimates of the spectral type or photometric redshift can result in
considerable biases in the adopted effective PSF.  To explore this
problem in detail we create simulated multi-wavelength catalogs, which
we discuss next.

\subsection{Simulated data}
\label{subsec:pzmocks}

To quantify how well the effective PSF size can be determined from
broad-band imaging data, we create simulated catalogs. In this paper
we explore several approaches, which use observations of galaxies and
stars. In our forecasts we consider the combination of {\it Euclid}
observations in the VIS and NIR filters, with ground-based DES
data. This is the baseline discussed in \cite{euclidrb} and also used
in \cite{cypriano}.  We use the extended CWW library \citeind{cww} from
{\tt LePHARE} \citeind{Ilbert06} which contains 66 SEDs. We  split these into 
elliptical (Ell), spiral (Sp) and irregular (Irr) galaxies as described  in \citedir{polpz}; 
they also describes how the SEDs are assigned. These galaxy realisations with 
absolute magnitudes, redshifts and SEDs are then converted into apparent photon 
fluxes ($f_i$):

\begin{align}
f_i &= \int d\lambda \, \lambda T_i(\lambda) S(\lambda (1+z)) %\\
%m &= 2.5 \log_{10}
\end{align}

\xbf
\zfigure{prod_psf1.pdf}
\caption{The {\it Euclid} (VIS, and near infrared \bi{Y},\bi{J},\bi{H}
  bands) and DES (ground based \bi{g},\bi{r},\bi{i},\bi{z}) effective
  filter response curves used to create simulate data. The effective
  filter response curves combine the atmospheric (for DES), telescope,
  filter and CCD transmission.}
\label{filter:res}
\xef

\noindent
where $S(\lambda)$ is the rest-frame galaxy SED, $T_i(\lambda)$ is the
response function in filter $i$ and the integration is over the
observed frame wavelength. Figure~\ref{filter:res} shows the adopted filter
response functions ($T_i$) for the {\it Euclid} VIS and NIR filters, as well
as the optical DES filters\footnote{The DES filter curves are
  obtained from http://www.ctio.noao.edu/, while the {\it Euclid} filters
  are approximated from the values presented in \citet{euclidrb}.}.

The simulated catalogs include realistic statistical uncertainties for
the magnitudes. We assume that the measurements are limited by the
noise from the sky background, which is independent between
filters. Table \ref{mag_lim} lists the adopted limiting magnitudes for
DES and {\it Euclid} for point sources with a signal-to-noise ratio
S/N=10. For galaxies, which are extended, we take a limit 0.7
magnitude brighter. We generate mock catalogs of galaxies that include
galaxies that are fainter than $\text{VIS} < 24.5$, but restrict
the analysis to this limiting magnitude when estimating the relative
bias in $R_{\rm PSF}^2$.

We explore various scenarios to estimate the effective PSF size, such as
different combinations of broad-band imaging data.  Of particular interest is
the question whether it is possible to use the observed sizes and colours of
stars to estimate the effective PSF sizes of galaxies: if a star and a galaxy
would have the same SED, they would also have the same effective PSF. Figure
\ref{gal_star_triangle} shows $R^2_{\rm PSF}$ as a function of colour for
simulated galaxies and stars.  The galaxies (yellow points) are a random subset
of the simulated (noiseless) catalog, while the stars (black points) are
generated by uniformly sampling all SEDs in the Pickles library
\citeind{pickles}. For the filters that overlap in wavelength with the VIS band
(panels with blue background) the relation between $R^2_{\rm PSF}$ and colour
is indeed quite similar for galaxies and stars. \change{The performance of this
approach, extending the different single colour estimates explored by
\cite{cypriano} to include the more extensive colour information, is explored
in \S\ref{learning} using machine learning algorithms.}


\begin{table}
\begin{center}
\setlength{\tabcolsep}{4.7pt}
%\xinput{prod_psf77.txt}
\begin{tabularx}{\columnwidth}{lrrrrrrrr}
\toprule
{} &  VIS &    $g$ &   $ r$ &    $i$ &    $z$ &   $ Y$ &    $J$ &    $H$ \\
\midrule
Galaxies & 24.5 & 24.4 & 24.1 & 24.1 & 23.7 & 23.2 & 23.2 & 23.2 \\
Stars    & 25.2 & 25.1 & 24.8 & 24.8 & 24.4 & 23.9 & 23.9 & 23.9 \\
\bottomrule
\end{tabularx}

\end{center}
\caption{The adopted limiting magnitudes for detections with a 
  signal-to-noise ratio $S/N = 10$. The limiting magnitudes for extended 
  objects are assumed to be 0.7 magnitudes  shallower than for point sources. 
  The ground based observations (\bi{g},\bi{r},\bi{i},\bi{z}) correspond to 
  DES data, whereas the VIS,  and \bi{Y},\bi{J},\bi{H} correspond to 
  the {\it Euclid} optical and NIR limits taken from \citet{euclidrb}.}
\label{mag_lim}
\end{table}




\begin{figure*}
\yfigure{prod_psf23.pdf}
\caption{Relation between colour and the effective PSF size ($R^2_{\rm PSF}$)
  for different filter combinations (indicated in the title of each subplot). The
  black points show the results for stars
  with SEDs from the Pickles library \citeind{pickles}. The yellow
  points correspond to a random subset of galaxies covering a range of SEDs and
  redshifts. Subplots with a blue background indicate filters
  overlapping the VIS band.  }
\label{gal_star_triangle}
\end{figure*}

