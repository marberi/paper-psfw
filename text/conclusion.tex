The convolution of galaxy images by the PSF is the dominant source of
bias for weak gravitational lensing studies, and an accurate estimate
of the PSF is thus essential for a successful measurement.  In this
paper we studied the bias caused by the combination of a wavelength
dependent PSF and limited information about the SED of a galaxy of
which we wish to measure the shape. We quantified the impact on the
performance of {\it Euclid}, for which the impact is exacerbated
because of the combination of a (near-)diffraction limited PSF and the
broad VIS pass-band. We note, however, that the
wavelength dependence of the PSF cannot be ignored for other stage IV
ground-based surveys such as LSST.

Based on the analysis of biases by \cite{massey13}, we restricted the
study to the determination of the effective (or SED-weighted) PSF
size, which is different for each galaxy. Under the assumption that an
accurate model of the PSF as a function of wavelength can be derived,
we explored several approaches to estimate the effective PSF size from
a number of broad-band images.  Given the exquisite precision with
which {\it Euclid} can measure the cosmic shear signal, the corresponding
accuracy with which the PSF properties need to be determined makes
this challenging. Following \cite{cropper} we consider a maximum
relative error of $\euclidreq$, whereas the variation in effective PSF
size with source redshift is two orders of magnitude larger (see
Figure~\ref{r2_zdep}).

We simulated catalogs of stars and galaxies based on the expected depth and 
wavelength coverage of DES and {\it Euclid}. We used these to examine how 
well a standard template fitting photo-$z$ code can predict the effective PSF for a
galaxy. We found that the resulting distribution of predicted PSF sizes is skewed,
leading on biases in the estimate of the effective PSF that exceed the requirements.
Hence, modifications to the template fitting codes are needed if these are to be
used for this purpose. We note that this may be worthwhile, because the
effective PSF can be computed using a multi-wavelength model of the spatially
varying PSF and the SED from a template fitting code. 

To quantify the expected performance for different scenarios of multi-wavelength
data, we used machine-learning methods instead. We used a NuSVR algorithm to
train on simulated galaxy catalogs and found  that it is possible to reduce the skewness in the predicted effective PSF sizes, 
and thus also the bias in the mean value. We considered various filter configurations and
found  that good results can be obtained when using the $\bi{r}-\bi{i}$ colour only.
A potential complication is the fact that part of the photometric data
are used to estimate both the effective PSF size and the photometric
redshift of a source.  This leads to a correlation between the error
in PSF size and photometric redshift: galaxies with a large error in
PSF size are also more likely to migrate redshift bins when binned in
photometric redshift, causing a selection bias.  Fortunately, we found that when the training is
also done using photometric redshifts, the correlation is naturally
accounted for.

We also examined whether it is possible to predict the effective PSF size using observations of stars in the
data. Such an approach would be immune to photometric calibration errors. To test this, we trained and 
applied a NuSVR algorithm on simulated data. Interestingly, training on \riz\ observations of stars resulted in sufficiently small residuals for the full sample, but the bias varied with redshift significantly. To account for this, we introduced a correction  based on a calibration sample of simulated galaxies.  The adopted redshift-resolution for this correction 
allows us to adjust the importance of the galaxy templates on the results: if we assume no redshift 
dependence the method reverts back to training on stars, whereas a very fine redshift sampling 
is identical to training on galaxy templates.

In the case of perfectly calibrated data the two machine-learning implementations
are similar, but in the presence of calibration errors the performances may differ. 
We examined the sensitivity to errors in the PSF model and the photometric calibration,
and found a slightly better performance for the hybrid method. In this case the power-law 
slope of the wavelength dependence of the PSF size needs to be known to better than
$|\delta\gamma|<5\times 10^{-3}$ which is quite achievable. Moreover,
the estimated effective PSF size is most sensitive to the zeropoint errors in
the $r$-band, which needs to be accurate to $|\delta m|<0.005$, whereas
$|\delta m|<0.01$ is sufficient for the $i$ and $z$ bands. Such levels
of photometric homogeneity have already been achieved, and we expect
the situation to improve further thanks to spectrophotometric
observations with {\it Gaia}. 


As both implementations rely on a simulated template library, we explored whether limitations 
of the template library may pose a problem. The hybrid method
is less sensitive to an incomplete set of SEDs, but given our knowledge of 
galaxy SEDs this does not seem to result in a major bias. However, further work
may be needed to examine the impact of emission line galaxies, which we did
not consider here. Although we note this caveat, we conclude that it is possible to 
estimate the effective  PSF to the required level of accuracy for {\it Euclid} with 
the anticipated photometric data.


