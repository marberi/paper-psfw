The measurement of the distance-redshift relation using distant type
Ia supernovae led to the remarkable discovery that the expansion of
the Universe is accelerating \citeind{sn1,sn2}. Since then, this
finding has been confirmed by a wide range of observations, but there
is still no consensus on the underlying theory: options range from a
cosmological constant to a change of fundamental
physics.  To restrict the range of explanations, significant
observational progress is required, and to this end a wide variety of
observational probes and facilities are being studied and employed
\citeind{weinberg_probes}.

Of particular interest is weak gravitational lensing
\change{\citeind[e.g.][]{Bartelmann2001,HJ08, Kilbinger15}}: the statistics of the coherent distortions of
the images of distant galaxies by intervening structures can be
related to the underlying cosmological model. Measuring this lensing
signal as a function of source redshift can in principle lead to some
of the tightest constraints on cosmological parameters. The typical
change in galaxy shape is tiny compared to its intrinsic ellipticity,
and a precise measurement involves averaging over large samples of
galaxies. Moreover, gravitational lensing is not the only phenomenon
that can lead to observed correlations in the galaxy shapes: tidal
effects during structure formation may lead to intrinsic alignments,
which complicate the interpretation of the measurements
\change{\citep[e.g.][]{Catelan2000, Heavens2000, Croft2000, Hirata2004,
Joachimi15,Kirk15}}.

Perhaps the biggest challenge is that a range of instrumental effects
can overwhelm the lensing signal, unless carefully corrected for.  Of
these, the convolution of the galaxy images by the point spread
function (PSF) is typically dominant, but other effects may contribute
as well \citeind{massey13,cropper}. Hence, much effort has focussed on
an accurate correction for the PSF, which circularises the images, but
can also introduce alignments if it is anisotropic.  Despite these
technical difficulties, the lensing signal by large-scale structure,
commonly referred to as `cosmic shear', has now been robustly measured
\change{\citep[see e.g.][]{heymans2012,
Hildebrandt17, Troxel2017}}.

The next generation of lensing surveys will cover much larger areas of
sky and aim to measure shapes of billions of galaxies. The Large
Synoptic Survey Telescope\footnote{http://www.lsst.org/}
\citep[LSST;][]{lsst1} will survey the sky repeatedly from the ground,
whereas {\it Euclid}\footnote{http://www.euclid-ec.org/}
\citep{euclidrb}, and the Wide-Field Infrared Survey
Telescope\footnote{http://wfirst.gsfc.nasa.gov/} \citep[WFIRST;][]{wfirst} will observe from space to avoid the blurring of
the images by the atmosphere. The dramatic reduction in statistical
uncertainties afforded by these new surveys needs to be matched by a
reduction in the level of residual systematics. Consequently, even in
diffraction-limited space-based observations, the PSF cannot be
ignored \citep{cropper}.

The PSF varies spatially due to misalignments of optical elements,
which also typically vary with time due to changes in thermal conditions
and, in the case of ground-based telescopes, due to changing
gravitational loads.  This can be modelled using the observations of
stars in the field-of-view. A complication is that the PSF generally
depends on wavelength; this effect is stronger for diffraction-limited
optics, but atmospheric differential chromatic refraction and the
turbulence in the atmosphere also depend on wavelength
\citep{meyers2015}. Hence, the observed PSFs depend on the spectral
energy distribution (SED) of the stars. Fortunately the SEDs of stars
are well-studied and relatively smooth, such that with limited
broad-band colour information the wavelength dependence can also be
included in the PSF model.

Each galaxy, however, is convolved by a PSF that depends on its SED in
the observed frame, the `effective' PSF. An incorrect estimate of this
PSF will lead to biases in the galaxy shape estimates and consequently
in the cosmological parameters. Hence it is not only important that
the wavelength dependent model for the PSF is accurate, but also that
the galaxy SED can be inferred sufficiently well. In this paper we
focus on the spatially averaged, or global, SED of the galaxy, but we
note that spatial variations lead to additional complications
\citep{voigt,semboloni_colorgrad}, which we do not consider
here. Examining the impact of the wavelength dependence is
particularly relevant for {\it Euclid}, because the PSF is not only
diffraction limited, but the shape measurements are based on optical
data obtained using an especially broad passband \citep[5500-9200\AA;][]{euclidrb} 
to maximise the number of galaxies for which shapes can be measured.

To study the expansion history and growth of structure, lensing
surveys measure the cosmic shear signal as a function of source
redshift. Measuring spectroscopic redshifts for such large numbers of
faint galaxies is too costly, but fortunately photometric redshifts
are adequate. These are obtained by complementing the shape
measurements with photometry in multiple filters, which can also
provide information on the observed SEDs. Whether such data are
adequate for the determination of the effective PSF for galaxies was
first studied by \citedir{cypriano} in the context of {\it Euclid}.

\cite{cypriano} examined two approaches to account for the wavelength
dependent PSF. First, they explored whether stars with similar colours
as the galaxies could be used. In general one does not expect the SEDs
of stars to match those of galaxies well over the broad redshift range
covered by {\it Euclid}. Nonetheless, this approach performed
reasonably well, albeit with significant biases for high redshift
galaxies. \cite{cypriano} obtained better results by training a neural
network on simulated SEDs and combining this with a model for the
wavelength dependence of the PSF. This allowed them to to predict the
PSF size as a function of the observed galaxy colours. Similarly,
\citedir{meyers2015} explored how machine learning techniques can be
used to account for atmospheric chromatic effects in ground-based
data.

In this paper we revisit the problem studied by \citet{cypriano} and
\citet{meyers2015}, with a particular focus on what data are required
to meet the requirements for {\it Euclid}.  This paper examines the
performance of the various approaches to estimate the effective PSF
size, using a more up-to-date formulation of requirements, as
presented in \cite{massey13}.  The detailed break down of various
sources of bias presented in \cite{cropper} indicates that the actual
requirements are more stringent than those assumed by
\cite{cypriano}. \change{We also use a more realistic model for the wavelength
dependence of the PSF, that is based on fits to simulated Euclid PSF models.}
Importantly, we examine how well the supporting broad band imaging data need to
be calibrated, as zero-point variations will lead to coherent biases in the
inferred PSF sizes. The photometric data are also used to determine photometric
redshifts, and as a result we expect covariance between photometric redshift
errors and the inferred PSF size. The break down presented in \cite{cropper}
ignores such interdependencies, and here we examine the validity of this
assumption.

The outline of this paper is as follows. In \S\ref{theory} we present
the problem and describe the simulations we use to study the impact of
the wavelength dependence of the PSF.  In \S\ref{photoz} we explore
how well we can determine the PSF size using a conventional photometric
redshift methods, whereas we investigate machine learning techniques in
\S\ref{learning}. In \S\ref{calibration} we quantify the impact of
calibration errors and limitated SED templates. \change{Appendix
\ref{app_dzresol} studies the requirement on the redshift resolution when using
conventional photo-z method, while appendix \ref{app_rshift} describes the $R^2$
bias calibration when using a machine learning technique.} Appendix 
\ref{app_zband} investigates the implication of omitting \bi{z}-band
observations.
