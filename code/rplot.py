#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pdb
import numpy as np
from matplotlib import pyplot as plt
import tables

nboot = 1000
fb = tables.openFile('r2out.h5')
r2 = fb.getNode('/r2')

fields = ['r2_zs', 'r2_peak', 'r2_pdf']
fields = ['r2_peak', 'r2_pdf']
def find_boot(cat):

    def g(field):

        return (cat[field] - cat['r2_true']) / cat['r2_true']

    res = {}
    for field in fields:
        d = g(field)
        d = d[d < 0.2] # Apply an additional cut...
        rint = np.random.randint(0, len(d), (nboot, len(d)))
        res[field] = np.average(d)

        res['{0}_err'.format(field)] = np.std(np.average(d[rint], axis=1))

    return res

def merge(res):
#    d1 = (cat['r2_zs'] - cat['r2_true']) / cat['

    ipdb.set_trace()

# Calculate the deltas with error...

zmL = np.linspace(0.1, 1.5,10)
#zmL = np.linspace(0.5, 1.0,10)
dz = 0.05
partial = []
for zm in zmL:
    za = zm - 0.5*dz
    zb = zm + 0.5*dz

#    ta = 5.5
#    tb = 6.1
#    ia = 18.
#    ib = 25.
    cuts = '({0} < z_s) & (z_s < {1})'.format(za, zb)
#    cuts += ' & ({0} < tb) & (tb < {1})'.format(ta, tb)
#    cuts += ' & ({0} < m_0) & (m_0 < {1})'.format(ia, ib)

#    ipdb.set_trace()
    subcat = r2.readWhere(cuts)
    partial.append(find_boot(subcat))

fb.close()


# Merge
keys = partial[0].keys()
merged = {}
for key in keys:
    merged[key] = np.array([X[key] for X in partial])

for field in fields:
    y = merged[field]
    yerr = merged['{0}_err'.format(field)]
    plt.errorbar(zmL, y, yerr, label=field)

plt.yscale('symlog')
plt.legend(loc=0)
plt.hlines([-1e-3,-3e-4, 3e-4,1e-3], min(zmL), max(zmL))
plt.grid()
plt.grid(which='minor')
plt.show()
#ipdb.set_trace()

