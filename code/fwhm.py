#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import ipdb
import glob
import os
import pdb
import sys
import numpy as np
import tables

from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep

def load_data():
    """Load the psfw(z) estimations."""

    g = '../sfw_data/*PSF'
    res = {}
    for file_path in glob.glob(g):
        sed, filter_name,_ = os.path.basename(file_path).split('.')

        x,y = np.loadtxt(file_path).T

        res[sed,filter_name] = splrep(x, y)
    #    pdb.set_trace()

    return res

def sel_filter(spls, to_sel):
    """Select the estimations for one filter."""

    sel = {}
    for (sed,filter_name), spl in spls.iteritems():
        if not filter_name == to_sel:
            continue
        sel[sed] = spl

    return sel

seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
def to_nr(spls, seds):
    """Convert from sed names to numbers."""

    innum = {}
    for key, spl in spls.iteritems():
        innum[seds.index(key)+1] = spl

    return innum

spls = load_data()
in_vhs = sel_filter(spls, 'vhs')
in_num = to_nr(in_vhs, seds)

def load_cat_adv(sel):

    def f((field, X)):
        return '({0} < {1}) & ({1} < {2})'.format(X[0], field, X[1])

    select = ' & '.join(map(f, sel.iteritems()))

    cat_path = '../cat/faint.h5'
    fb = tables.openFile(cat_path)
    cat_table = fb.getNode('/cat/cat')
    cat = cat_table.readWhere(select)

    return cat
#    pdb.set_trace()


def find_r(spls, t, z):
#    pdb.set_trace()
    t0 = np.floor(t)
    dt = t - t0

    r = -10*np.ones((len(z)))
    for i in np.arange(1,6):
        inds = t0 == i
        if not inds.sum():
            continue

        spl0 = spls[i]
        spl1 = spls[i+1]

        zi = z[inds]
        dti = dt[inds]
        r1 = splev(zi, spl0)
        r2 = splev(zi, spl1)

        r[inds] = (1-dti)*r1 + dti*r2

    end = (r == -10)
    if np.sum(end):
        r[end] = splev(z[end], spls[6])

    return r
#    pdb.set_trace()


def rprint(x,r, y):
    z = np.average(np.abs(y-r) / r) * 100
    print(x, np.average(y), z)

def E(r, y, err=False):
    #return np.average(np.abs(y-r) / r) #* 100
    delta_r = (y-r) / r 

    x = np.abs(np.average(delta_r))
    if not err:
        return x

    nboot = 100
    S = np.random.randint(0, len(delta_r), (nboot, len(delta_r)))
    error = np.std(np.average(delta_r[S], axis=1))

    return x, error

def plot1():
    """Redshift dependence."""

    dz = 0.1
    r1,r2,r3,r4 = [],[],[],[]
    z_mean = np.linspace(0.2, 1.5)
    for zmi in z_mean:
        za = zmi - 0.5*dz
        zb = zmi + 0.5*dz

        cat = load_cat_adv({'z_s': [za,zb]})
        r = find_r(in_num, cat['t_t'], cat['z_s'])
        r1.append(E(r, find_r(in_num, cat['t_t'], cat['z_s'])))
        r2.append(E(r, find_r(in_num, cat['t_b'], cat['z_s'])))
        r3.append(E(r, find_r(in_num, cat['t_t'], cat['zb'])))
        r4.append(E(r, find_r(in_num, cat['t_b'], cat['zb'])))

    print(r1)
    print(r2)
    print(r3)
    print(r4)

    plt.plot(z_mean, r2, label='Photo-z')
    plt.plot(z_mean, r3, label='Photo-type')
    plt.plot(z_mean, r4, label='Photo-both')
    plt.xlabel('z')
    plt.ylabel('R^2 rel. bias')
    plt.yscale('log')
    plt.hlines(3e-4, min(z_mean), max(z_mean))
    
    plt.legend()
    plt.show()


def plot2():
    """Dependence on odds paramenter."""

    do = 0.1
    r1,r2,r3,r4 = [],[],[],[]
    o_mean = np.linspace(0.1, 1.)
    for omi in o_mean:
        o1 = omi - 0.5*do
        o2 = omi + 0.5*do

        cat = load_cat_adv({'odds': (o1, o2)})
        r = find_r(in_num, cat['t_t'], cat['z_s'])
        r1.append(E(r, find_r(in_num, cat['t_t'], cat['z_s'])))
        r2.append(E(r, find_r(in_num, cat['t_b'], cat['z_s'])))
        r3.append(E(r, find_r(in_num, cat['t_t'], cat['zb'])))
        r4.append(E(r, find_r(in_num, cat['t_b'], cat['zb'])))

    print(r1)
    print(r2)
    print(r3)
    print(r4)

    plt.plot(o_mean, r2, label='Photo-z')
    plt.plot(o_mean, r3, label='Photo-type')
    plt.plot(o_mean, r4, label='Photo-both')
    plt.xlabel('Odds')
    plt.ylabel('R^2 rel. bias')
    plt.yscale('log')
    plt.hlines(3e-4, min(o_mean), max(o_mean))
    plt.legend()
    plt.show()


def plot4():
    """Dependence on magnitude."""

    dx = 0.1
    r1,r2,r3,r4 = [],[],[],[]
    x_mean = np.linspace(22.5, 23.7)
    for xmi in x_mean:
        x1 = xmi - 0.5*dx
        x2 = xmi + 0.5*dx

#        pdb.set_trace()
        cat = load_cat_adv({'m_0': (x1, x2)})
        r = find_r(in_num, cat['t_t'], cat['z_s'])
        r1.append(E(r, find_r(in_num, cat['t_t'], cat['z_s'])))
        r2.append(E(r, find_r(in_num, cat['t_b'], cat['z_s'])))
        r3.append(E(r, find_r(in_num, cat['t_t'], cat['zb'])))
        r4.append(E(r, find_r(in_num, cat['t_b'], cat['zb'])))

    print(r1)
    print(r2)
    print(r3)
    print(r4)

    plt.plot(x_mean, r2, label='Photo-z')
    plt.plot(x_mean, r3, label='Photo-type')
    plt.plot(x_mean, r4, label='Photo-both')
    plt.xlabel('i AB')
    plt.ylabel('R^2 rel. bias')
    plt.yscale('log')
    plt.hlines(3e-4, min(x_mean), max(x_mean))
    plt.legend()
    plt.show()


def plot5():
    dx = 0.1
    field = 'z_s'
    #x_mean = np.linspace(0.2, 1.5)
    x_mean = np.linspace(0.2, 2.)

    
#    dx = 0.1
#    field = 'm_0'
#    x_mean = np.linspace(22.5, 23.7)
    gtypes = [('ell', 1., 1.5), ('spl', 1.5, 3.5), ('irr', 3.5, 6.0)]

    for tname, t1, t2 in gtypes:
        r1,r2,r3,r4 = [],[],[],[]
        for xmi in x_mean:
            x1 = xmi - 0.5*dx
            x2 = xmi + 0.5*dx


            cat = load_cat_adv({field: [x1, x2], 't_t': [t1, t2]})

        
            r = find_r(in_num, cat['t_t'], cat['z_s'])
            r1.append(E(r, find_r(in_num, cat['t_t'], cat['z_s'])))
            r2.append(E(r, find_r(in_num, cat['t_b'], cat['z_s'])))
            r3.append(E(r, find_r(in_num, cat['t_t'], cat['zb'])))
            r4.append(E(r, find_r(in_num, cat['t_b'], cat['zb'])))


#        plt.plot(z_mean, r2, label='Photo-z')
#        plt.plot(z_mean, r3, label='Photo-type')
#        plt.plot(z_mean, r4, label='Photo-both')

        plt.plot(x_mean, r4, label=tname)

    plt.xlabel(field)
    plt.ylabel('R^2 rel. bias')
    plt.yscale('log')
    plt.hlines(3e-4, min(x_mean), max(x_mean))
    plt.legend()
    plt.show()

def plot6():

    gtypes = [('ell', 1., 1.5), ('spl', 1.5, 3.5), ('irr', 3.5, 6.0)]

    for tname, t1, t2 in gtypes:
        cat = load_cat_adv({'t_t': [t1, t2]})
        r = find_r(in_num, cat['t_t'], cat['z_s'])
        robs = find_r(in_num, cat['t_b'], cat['zb'])

        dr = robs - r

        H, edges = np.histogram(dr/r, bins=30)
        x = 0.5*(edges[:-1] + edges[1:])
        plt.plot(x, H, label=tname)

    plt.xlabel('[r^2(obs) - r^2]/r^2')
    plt.legend()
    plt.show()

#    pdb.set_trace()


def F(r, r2):
    return (r - r2) / r

def plot7():
    dx = 0.1
    field = 'z_s'
    #x_mean = np.linspace(0.2, 1.5)
    x_mean = np.linspace(0.2, 2.)

    
#    dx = 0.1
#    field = 'm_0'
#    x_mean = np.linspace(22.5, 23.7)
    gtypes = [('ell', 1., 1.5), ('spl', 1.5, 3.5), ('irr', 3.5, 6.0)]

    edges = np.linspace(-0.5, 0.5, 20)

    def fisk(x, Y):
        res = []
        for i in range(len(edges) - 1):
            inds = np.logical_and(edges[i] < x, x < edges[i+1])
#            pdb.set_trace()
            res.append(np.average(np.abs(Y[inds])))

        return res

    for tname, t1, t2 in gtypes:
#        r1,r2,r3,r4 = [],[],[],[]
#        for xmi in x_mean:
#            x1 = xmi - 0.5*dx
#        x2 = xmi + 0.5*dx


        cat = load_cat_adv({'t_t': [t1, t2]})

        
        r = find_r(in_num, cat['t_t'], cat['z_s'])
        r1 = F(r, find_r(in_num, cat['t_t'], cat['z_s']))
        r2 = F(r, find_r(in_num, cat['t_b'], cat['z_s']))
        r3 = F(r, find_r(in_num, cat['t_t'], cat['zb']))
        r4 = F(r, find_r(in_num, cat['t_b'], cat['zb']))

        x = cat['zb'] - cat['z_s']
        Y = fisk(x, r4) #(r4-r)/r)
        xm = 0.5*(edges[:-1] + edges[1:])
#        pdb.set_trace()
#        H, edges = np.linspace(
#        plt.plot(z_mean, r2, label='Photo-z')
#        plt.plot(z_mean, r3, label='Photo-type')
#        plt.plot(z_mean, r4, label='Photo-both')

#        plt.plot(x_mean, r4, label=tname)
        plt.plot(xm, Y, label=tname)

    plt.xlabel(field)
    plt.ylabel('R^2 rel. bias')
    plt.yscale('log')
#    plt.hlines(3e-4, min(x_mean), max(x_mean))
    plt.legend()
    plt.show()

def gencat(z0, zmax, pz,bias):
    n = 10000

    z_s = z0 + (zmax - z0)*np.random.random(n)
    z_p = z_s + bias + np.random.normal(scale=pz, size=n)

    # Not exactly record arrays, bur close enough....
    return {'z_s': z_s, 'zb': z_p}

def plot8():
#    pzL = np.linspace(0.01, 0.1)
    pzL = np.logspace(-3., -1.) #0.01, 0.1)
#    pzL = np.logspace(-3., -0.5)
    seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
#    seds = [seds[1]]
    for name, ti in zip(seds, np.arange(1., 7.)):
        r2 = []
        for pz in pzL:

            z0 = 0.49
            zmax = 0.5

            cat = gencat(z0, zmax, 0.001, pz)
#            cat = gencat(z0, zmax, pz, 0.0)
    
            t = ti*np.ones(len(cat['z_s']))
            r =  find_r(in_num, t, cat['z_s'])
            r2.append(E(r, find_r(in_num, t, cat['zb']), True))

#        pdb.set_trace()
#        plt.plot(pzL, r2, 'o-', label=name)
        y, yerr = zip(*r2)
        plt.errorbar(pzL, y, yerr, label=name)

#    plt.xlabel('pz-sigma')
    plt.xlabel('pz-bias')
    plt.ylabel('R^2 rel. bias')
    plt.xscale('log')
    plt.yscale('log')
    plt.hlines(3e-4, min(pzL), max(pzL))
    plt.legend()
    plt.show()

def plot9():
    z = np.linspace(0.1, 2.0) # , 1000)
    seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
#    seds = [seds[0]]
    r2_seds = dict((x, []) for x in seds)

    sigmaz = 0.02
    for name, ti in zip(seds, np.arange(1., 7.)):
        r2 = []
        for zi in z:
            cat = gencat(zi, zi+0.01, sigmaz, 0.)

            t = ti*np.ones(len(cat['z_s']))
            r = find_r(in_num, t, cat['z_s'])
            r2.append(E(r, find_r(in_num, t, cat['zb']), True))

        y, yerr = zip(*r2)
        plt.errorbar(z, y, yerr, label=name)

    plt.title('Sigma z - {0}'.format(sigmaz))
    plt.hlines(3e-4, np.min(z), np.max(z))
    plt.legend()
    plt.show()

assert len(sys.argv) == 2, 'One need to specify the plot number.'
f = 'plot{0}'.format(sys.argv[1])

assert f in globals(), 'No such plot: {0}'.format(f)

globals()[f]()
