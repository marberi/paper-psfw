#!/usr/bin/env python
# encoding: UTF*

# Fitting priors to the observed magnitude distributions.

import ipdb
import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import sys
d = '/home/marberi/drv/work/papers/psfw/plots'
sys.path.append(d)

import trainpipel
pipel = trainpipel.pipel()

prior_cat = pipel.kids['galcatN'].clone()

pipel.kids['galmabs'].config['Ngal'] = 10000
#ipdb.set_trace()
#prior_cat.config['Ngal'] = 10000
prior_mabs = pipel.kids['galmabs']

L = []
L.append(pipel.kids['pzcat']['pzcatdf']['pzcat'])

J = pipel.kids['pzcat']
J.depend('prior_cat', prior_cat)
J.depend('prior_mabs', prior_mabs)

pipel.kids['pzcat']()

L.append(pipel.kids['pzcat']['pzcatdf']['pzcat'])

def plot1():
    fig,A = plt.subplots(len(L))

    for pzcat,ax in zip(L, A):
        ax.plot(pzcat.zs, pzcat.zb, '.')

    plt.show()

lbls = ['Normal', 'New']
for lbl,pzcat in zip(lbls, L):
    dx = (pzcat.zb - pzcat.zs)
    dx.hist(bins=30, histtype='step', normed=True, label=lbl)

plt.legend()
plt.show()

ipdb.set_trace()

plt.plot(pzcat.zs, pzcat.zb, '.')
plt.show()


sys.exit()

#ipdb.set_trace()


mabs = pipel.kids['galmabs']['mabs']['cat']
cat_mag = pipel.kids['galcatN']['magcat']['magcat']
cat_full = mabs.join(cat_mag, on='id', rsuffix='x')

cat = cat_full[cat_full.sed <= 10][['zs','mag_i']]

def plot1():
    ibins = np.linspace(22.5, 25., 10)
    for key,val in cat.groupby(pd.cut(cat.mag_i, ibins)):
        val.zs.hist(bins=30, histtype='step', label=key)

    plt.legend()
    plt.show()




# =======
#plot1()

# Wait with this...
from scipy.stats import gaussian_kde
A = gaussian_kde(np.array(cat).T)

ibins = np.linspace(22.5, 25., 2)
zbins = np.linspace(0.1, 1.0, 10)
Hist,a,b = np.histogram2d(cat.zs, cat.mag_i, bins=[zbins, ibins])

zx = 0.5*(a[:-1] + a[1:])
mx = 0.5*(b[:-1] + b[1:])

zw = 0.5*(a[1:] - a[:-1])
mw = 0.5*(b[1:] - b[:-1])

intw = np.outer(zw, mw)

def mod(X):
    A,alpha, z0, k = X
    zm = z0 + k*(mx-20.)

#    print('valuating mod..')
    print(X)
    # Let redshift be the first index.
    P = A*intw*np.exp(-np.outer(zx, 1./zm)**alpha)
    P = (zx**alpha*P.T).T

    return P

def diff(X):
    P = mod(X)
    D = (Hist*(P-Hist)**2).mean()

    return D


from scipy import optimize

def plot2():
    # Initial points.
    A = 900
    z0 = 0.1
    k = 1.
    alpha = 1.

    x0 = np.array([A, alpha, z0, k])
    res = optimize.minimize(diff, x0)

    # Doing some test plots...
    P = mod(res['x'])



    i = 0
    plt.plot(zx, Hist[:,i], 'o-', label='Input')
    plt.plot(zx, P[:,i], 'o-', label='Fit')
    plt.legend()
    plt.show()


