#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np

from xdolphin import Job

path = '/home/marberi/drv/work/papers/psfw/plots'
sys.path.append(path)

import pzpipel
pipl = pzpipel.create_pipl()

galcat = Job('magnoise')
galcat.depend('magcat', pipl.galcatN)
galcat.task.config['const_sn'] = False

magcat = galcat['magcat']

ipdb.set_trace()
