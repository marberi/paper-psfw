#!/usr/bin/env python
# encoding: UTF8

# Ok, this is a more general test of different things
# in pandas.

import ipdb
import numpy as np
import pandas as pd

def plot1():
    N = 1000
    r = 65*np.random.random(N)
    r = pd.Series(r)

    bins = np.arange(100)
    for key,val in r.groupby(pd.cut(r, bins)):
        print(key)

def plot2():
    N = 1000
    r = 65*np.random.random(N)
    x = np.random.randint(0, 10, N)
    df = pd.DataFrame({'r': r, 'x': x})

    bins = np.arange(100)
    for key,val in df.groupby(pd.cut(df.r, bins)):
        ipdb.set_trace()

        print(key)

plot2()
#ipdb.set_trace()
