#!/usr/bin/env python
# encoding: UTF8

# Exploring the effect of small errors in the filter width
# when trying to do model the galaxy SED by a straight line.

from __future__ import print_function
import ipdb
import os
import numpy as np
from matplotlib import pyplot as plt
from scipy.integrate import simps

filter_dir = '/data2/photoz/run/data/des_euclid'
fmt = '{0}.res'

def plot1():
    for fname in ['r','i','z','vis']:
        file_path = os.path.join(filter_dir, fmt.format(fname))

        x,y = np.loadtxt(file_path).T

        to_width = np.where(0.1 < y, 1, 0)
        width = simps(to_width, x)

        print(fname, 100*(10./width))

xmD = {}
for fname in ['r','i','z','vis']:
    file_path = os.path.join(filter_dir, fmt.format(fname))

    x,y = np.loadtxt(file_path).T

    xmD[fname] = simps(x*y, x)/ simps(y, x)

A = 1e5

k = 1.0075
s = A*(k-1) / 2682.

l0 = 5500
l1 = 9200
l = np.linspace(l0, l1)
f0 = A*np.ones_like(l)
f1 = A + s*(l - l0)
R_psf = l**0.55

R0 = simps(R_psf*f0, l) / simps(f0, l)
R1 = simps(R_psf*f1, l) / simps(f1, l)

ipdb.set_trace()

if False:
    plt.plot(l, f0, label='f0')
    plt.plot(l, f1, label='f1')
    plt.show()

ipdb.set_trace()
