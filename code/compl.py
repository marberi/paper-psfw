#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


filter_dir = '/data2/photoz/run/data/des_euclid'

def load_filters(filters):
    fD = {}
    for fname in filters:
        path = os.path.join(filter_dir, '{0}.res'.format(fname))

        x,y = np.loadtxt(path).T

        fD[fname] = x,y 

    return fD

filters = ['r', 'i', 'z', 'vis']
fD = load_filters(filters)

for key, val in fD.iteritems():
    lmb, compl = val

    dmag = 2.5*np.log10(np.abs(compl))

    plt.plot(lmb, dmag, 'o-')

ax = plt.gca()
ax.xaxis.grid(True, which='both')
ax.yaxis.grid(True, which='both')

plt.xlim(3000, 12000)
plt.ylim(-4, 0)

plt.xlabel('Wavelength[A]', size=16)
plt.ylabel('$\Delta$ mag = 2.5 log10(Filter response)', size=16)

plt.legend()
plt.show()

#    ipdb.set_trace()
