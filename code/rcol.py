#!/usr/bin/env python
# encoding: UTF8

import pdb
import ipdb
import numpy as np
import tables

from matplotlib import pyplot as plt

fb = tables.openFile('../data/r2out.h5')
r2 = fb.getNode('/r2').read()

r2_bias = (r2['r2_peak'] - r2['r2_true'])/ r2['r2_true']

def col(x,y):
    return r2['mag_'+x] - r2['mag_'+y]

u = 'up'
g = 'g'
r = 'r'
i = 'i'
z = 'z'

col1 = col(g, r)
col1 = col(r, i)
col1 = col(i, z)
y = r2['r2_true']
inds = y < 100

#H,xedges,yedges = np.histogram(col1, y)
#xm = 0.5*(xedges[:-1] + xedges[1:])
#ym = 0.5*(yedges[:-1] + yedges[1:])
def plot1():
    for X,(a,b) in enumerate([(u,g), (g,r), (r, i), (i, z)]):
        label = '{0}, {1}'.format(a,b)

        xcol = col(a,b)
        plt.plot(xcol, r2['r2_true']+X*10, '.', label=label)


    #plt.plot(col(u,'g'), r2_bias, '.')
    plt.legend()
    plt.ylim((60., 130.))
    print('Starting to store..')
#    plt.savefig('../plots/r6_scatter_types.pdf')
    plt.show()
    #ipdb.set_trace()

def plot2(z):
    tb = r2['tb']
    L = [0, 1, 2, 3, 4,5,6]
    xcol = col(u,g)
    zinds = np.logical_and(z < r2['z_s'], r2['z_s'] < z + 0.02)

    for X, (a,b) in enumerate(zip(L[:-1], L[1:])):
        inds = np.logical_and(a < tb, tb < b)
        inds = np.logical_and(inds, zinds)

        plt.plot(xcol[inds], y[inds]+X*0, '.', label=X)

    plt.legend()
#    plt.show()

def plot3():
    for z in [0.2, 0.3, 0.5, 0.9]:
        plot2(z)

    plt.show()

def plot4():
    xcol = col(u,g)
    for z in [0.2, 0.3, 0.4, 0.5, 06, 0.7, 0.9]:
        zinds = np.logical_and(z < r2['z_s'], r2['z_s'] < z + 0.01)
        plt.plot(xcol[zinds], y[zinds], '.', label=z, ms=0.5)
#        plt.scatter(xcol[zinds], y[zinds])

    plt.legend()
    plt.show()

plot4()
