#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
from matplotlib import pyplot as plt

def plot1():
    job = Job('stardistr')
    #job.config['nfields'] = 20
    job()

    # Test generating the magnitude distribution...
    from scipy.interpolate import splev, splrep
    df = job['hist']['hist']
    spl = splrep(df.prob.cumsum(), df.mag)

    N = 1000000
    r = np.random.random(N)
    y = pd.Series(splev(r, spl))

    y.hist(bins=100)
    plt.show()
    #ipdb.set_trace()
    #y1 = df.H*df.w

import sys
sys.path.append('/home/marberi/drv/work/papers/psfw/plots')

import trainpipel
pipel = trainpipel.pipel(smag_distr=True)

starcat = pipel.kids['starcat']
starcat()

#ipdb.set_trace()
