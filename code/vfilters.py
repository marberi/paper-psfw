#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np

from matplotlib import pyplot as plt

filter_dir = '/data2/photoz/run/data/all_filters'

for file_name in os.listdir(filter_dir):
    path = os.path.join(filter_dir, file_name)
    fname = file_name.replace('.res','')

    x,y = np.loadtxt(path).T

    plt.plot(x,y, label=fname)

plt.show()
#    ipdb.set_trace()
