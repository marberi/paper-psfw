#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

fname = 'DECam_filters.csv'

df = pd.io.api.read_csv(fname)
df.wavelength *= 10

def plot1():
    w = df.wavelength
    filters = ['g','r','i','z']
    for fi in filters:
        plt.plot(w, df[fi], label=fi)

    plt.legend()
    plt.grid(True)
    plt.xlabel('Wavelength [A]', size=16)
    plt.ylabel('Throughput', size=16)

    plt.savefig('../run/new_filters.pdf')

d = '/data2/photoz/run/data/des_euclid2'

filters = ['g','r','i','z']
w = df.wavelength
for fi in filters:
    path = os.path.join(d, '{0}.res'.format(fi))

    A = np.array([w, df[fi]]).T
    np.savetxt(path, A)
