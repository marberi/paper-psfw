for part in res:
    cov = np.cov(part['delta_r2'], part['delta_z'])
    print(cov)

part = res[0]
def pz_cut(xa, xb, field='zs', edges=False):

    z = part[field]
    inds = np.logical_and(xa <= z, z <= xb)

    Y = part['delta_r2'][inds]

    return Y

def pz_plot(xa, xb, field='zs', edges=False):

    Y = pz_cut(xa, xb, field)
    if not edges:
        H, edges = np.histogram(Y, bins=100, range=(-0.02, 0.02))
    else:
        H, edges = np.histogram(Y, bins=edges)

    xm = 0.5*(edges[:-1] + edges[1:])

    print(field, np.average(Y))
    plt.plot(xm, H, label=field)
    return edges

def plot2(xa=0.2, xb = 0.6):
    pz_cut(0.4, 0.5, field='zs')
    pz_cut(0.4, 0.5, field='zb')

    plt.legend()
    plt.show()

def plot3():
    field = 'zs'
    for xa in [0.2, 0.4, 0.6, 0.8, 1.1]:
        pz_cut(xa, xa+0.2, field)

    plt.legend()
    plt.show()

def plot4():
    def fishy(xaL, dz):
        res = []
        for xa in xaL:
            f = lambda field: pz_cut(xa, xa+dz, field) #'zs') 
    #        Ys = pz_cut(xa, xa+0.2, 'zs') 
    #        Yb = pz_cut(xa, xa+0.2, 'zb') 

            Ys = f('zs') # pz_cut(xa, xa+0.2, 'zs') 
            Yp = f('zb') #pz_cut(xa, xa+0.2, 'zb') 
            bs = np.average(Ys)
            bp = np.average(Yp)

            res.append((bs,bp, bp/bs, np.abs(bp) - np.abs(bs)))

        return xaL, zip(*res)

    xaL = np.arange(0.2, 1.5, 0.05)
    for dz in [0.1, 0.2, 0.3]:
        xaL, Y = fishy(xaL, dz)
        plt.plot(xaL, Y[3], label=dz)

    plt.legend()
#    plt.show()
    plt.savefig('../run/r9_pzbias.pdf')

#ipdb.set_trace()
