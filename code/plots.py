#!/usr/bin/env python
# encoding: UTF8

import os
import pdb
from matplotlib import pyplot as plt
import numpy as np

seds = ['Ell_01', 'I99_05Gy', 'Irr_01', 'Irr_14', 'Sbc_01', 'Scd_01']
filters = ['up', 'vhs']

#seds = [seds[2]]
#filters = [filters[1]]

d = '../sfw_data/'
def load_psfw(sed, filter_name):
    file_name = '{0}.{1}.PSF'.format(sed, filter_name)
    path = os.path.join(d, file_name)

    x,y = np.loadtxt(path, unpack=True)

    return x,y

def plot1():
    for sed in seds:
        for filter_name in filters:
            x,y = load_psfw(sed, filter_name)
            plt.plot(x,y, 'o-', label=filter_name)

        plt.title(sed)
        plt.yscale('log')
        plt.xlim(0., 4.)
        plt.legend()
        plt.show()

def plot2():
    filter_name = 'vhs'
    for sed in seds:
        x,y = load_psfw(sed, filter_name)
        plt.plot(x, y, 'o-', label=sed)


    plt.xlabel('z')
    plt.ylabel('FWHM(arcsec)')
    plt.xlim(0., 4.)
    plt.legend()

    ax = plt.gca()
    ax.xaxis.grid(True, which='major')
    ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    ax.yaxis.grid(True, which='minor')

    plt.show()

plot2()
