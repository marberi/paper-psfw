#!/usr/bin/env python
# encoding: UTF8
# Simple script for plotting photo-z errors.

import pdb
import sys
import tables
from matplotlib import pyplot as plt

path_in = sys.argv[1]
fb = tables.openFile(path_in)
cat = fb.getNode('/cat/cat')

odds = cat.read(field='odds')
inds = 0.8 < odds
z_b = cat.read(field='zb')
z_s = cat.read(field='z_s')

plt.plot(z_s[inds], z_b[inds], '.')
plt.show()
#pdb.set_trace()
