#!/usr/bin/env python
# encoding: UTF8

# Very basic tests of scaling probability functions.

import ipdb
import pdb
import numpy as np
from matplotlib import pyplot as plt

N = 100000
t = 0.3
r1 = np.random.normal(scale=1.0, size=N)
r2 = np.random.normal(scale=t, size=N)
r3 = r1*t

ran = (-3,3)
nbins = 20
mean = lambda x: 0.5*(x[:-1] + x[1:])

H1, edges1 = np.histogram(r1,nbins, range=ran)
H2, edges2 = np.histogram(r2,nbins, range=ran)
H3, edges3 = np.histogram(r3,nbins, range=ran)

x1 = mean(edges1) #0.5*
x2 = mean(edges2) #0.5*
x3 = mean(edges3) #0.5*

plt.plot(x1, H1, label=1)
plt.plot(x2, H2, label=2)
plt.plot(x3, H3, label=3)
plt.legend()
plt.show()

#ipdb.set_trace()
