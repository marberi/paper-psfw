#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import ipdb
import os
import time
import pyfits

import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

d = '/Users/marberi/data/kids/cat'
d = '/disks/shear9/KiDS/LF_cat_wPz/ManMask'
d = '/data2/data/kids/cat'

d = '/data2/data/kids/kidscat'
fmt = 'KIDS_{0}p0_m0p5_V0.5_blinded_extmaglim_photoz_cut.cat'

file_name = fmt.format(30) #224) #129) #30)
#29)
file_name = 'KIDS_219p0_m0p5_V0.5_blinded_extmaglim_photoz_cut.cat'
file_path = os.path.join(d, file_name)

filters = ['u', 'g', 'r', 'i'] #, 'z']
filters = ['g', 'r', 'i'] #, 'z']

def load_cat(file_path):
    W = pyfits.getdata(file_path)

    mag_in = np.vstack([W['MAG_GAAP_'+f] for f in filters]).T
    err_in = np.vstack([W['MAGERR_GAAP_'+f] for f in filters]).T

    mask = np.logical_or(mag_in == 99, mag_in == -99)
#    mag_in[mask] = np.nan
#    err_in[mask] = np.nan

    mkheader = lambda pre, L: [pre+x for x in L]
    header_mag = mkheader('mag_', filters)
    header_err = mkheader('magerr_', filters)

    mag = pd.DataFrame(mag_in, columns=header_mag)
    err = pd.DataFrame(err_in, columns=header_err)

    # The panda did at not first like me..
    mag = mag.where(pd.notnull(mag), None)
    mag = mag.where(np.abs(mag) != 99, None)
    err = err.where(np.abs(err) != 99, None)

#    ipdb.set_trace()
    cat = mag.join(err)

    return W, cat

def modify_cat(cat):
    for f in filters:
    #    ipdb.set_trace()
        mag = cat['mag_'+f]
        emag = cat['magerr_'+f]

        fobs = 10**(-0.4*mag)
        ferr = (10**(0.4*emag)-1)*fobs

#        ipdb.set_trace()
        cat['fobs_'+f] = fobs
        cat['ferr_'+f] = ferr

    return cat

def load_ab():
    #cat = load_cat(file_path)
    abJ = Job('AB')
    #abJ.task.config['data_dir'] = '/Users/marberi/en/photoz/run/data'
    abJ.task.config['data_dir'] = '/data2/photoz/run/data'
    abJ.task.config['sed_dir'] = 'seds_stars'
    abJ.task.config['filter_dir'] = 'kids_filters'

    ab = abJ['ab']

    return ab

W, cat = load_cat(file_path)
cat = modify_cat(cat)
abD = load_ab()

star_types = list(set([x[1] for x in abD.keys()]))
star_types.sort()

t1 = time.time()
abX = np.zeros((len(filters), len(star_types)))
for i,fname in enumerate(filters):
    for j, sed in enumerate(star_types):
        abX[i,j] = abD[fname,sed]['ab'][0]

abX = abX

t2 = time.time()
print('time ab', t2-t1)

ab_mag = -2.5*np.log10(abX)

colX = []
colvarX = []
colwX = []
colmodX = []

for i in range(len(filters))[:-1]:
#for i,(f1, f2) in enumerate(zip(filters[:-1], filters[1:])):
    f1 = filters[i]
    f2 = filters[i+1]
    colX.append(cat['mag_'+f1] - cat['mag_'+f2])
#    colvarX.append(cat['magerr_'+f1]**2 + cat['magerr_'+f2]**2)
    var = cat['magerr_'+f1]**2 + cat['magerr_'+f2]**2

    colvarX.append(var)
    colwX.append(1./var)

    colmodX.append(ab_mag[i] - ab_mag[i+1])

#    colerrX.append(cat['errmag_'+f1] - cat['errmag_'+f2])

chi2_bpz = W['CHI_SQUARED_BPZ']
chi2_starsX = []

for j,sed in enumerate(star_types): #[0,1]:
#    j = 0
    chi2 = 0.
    for i in range(len(colX)):
        Y = colwX[i]*(colX[i] - colmodX[i][j])**2
        chi2 += Y

    chi2_starsX.append(chi2)
#    found_star = (chi2 < chi2_bpz).any()
#@    nstars = (chi2 < chi2_bpz).sum()
#    print(j, sed, nstars)

chi2_stars = np.vstack(chi2_starsX)
A = chi2_stars.argmin(axis=0)

seds = np.array(star_types)

#B = pd.DataFrame({'sed': seds[A].tolist(), 'class':i2 < 1000][B.cl < 0.8][B.mag < 20].sed.value_counts()[:10])
#i['CLASS_STAR']})
chi2_min = chi2_stars.min(axis=0).astype('f8')
chi2_min[np.isnan(chi2_min)] = 1000

# B = pd.DataFrame({'sed': A, 'cl': W['CLASS_STAR'].astype('f8')}) #, 'chi2': chi2_min.astype('f8')})
#B = pd.DataFrame({'sed': A, 'cl': W['CLASS_STAR'].astype('f8'), 'chi2': chi2_min})

#B = pd.DataFrame({'sednr': A, 'cl': W['CLASS_STAR'].astype('f8'), 'chi2': chi2_min, 'sed': seds[A].tolist()})
B = pd.DataFrame({'sednr': A, 'cl': W['CLASS_STAR'].astype('f8'), 'chi2': chi2_min, 'sed': seds[A].tolist(), 'mag': W['MAG_GAAP_i'].astype('f8')})

#B[B.chi2 < 10].sed.value_counts()
#    iolmodX.append(ab_mag[i+1] - ab_mag[i])

#print(B[B.chi2 < 1000][B.cl < 0.8][B.mag < 20].sed.value_counts()[:10])

print(B[B.chi2 < 10][B.cl < 0.8][B.mag < 26].sed.value_counts())

ipdb.set_trace()

def plot1():
    for x in [0.8, 0.5, 0.2]:
        B[B.cl < x].sed.hist()

    plt.show()

def plot2():
    chi2_min = chi2_stars.min(axis=0).astype('f8')
    inds = np.isfinite(chi2_min)

    plt.plot(W['ClASS_STAR'][inds], chi2_min[inds], '.')

    plt.yscale('log')
    plt.show()

plot1()
ipdb.set_trace()
