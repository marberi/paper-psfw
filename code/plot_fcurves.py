#!/usr/bin/env python
# encoding: UTF8

# Plot the filter curves..
import os
import numpy as np
from matplotlib import pyplot as plt

curve_dir = '/data2/photoz/run/data/des_euclid'

filters = ['H', 'J', 'Y', 'g', 'i', 'r', 'vis', 'z']
filters = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

for f in filters:
    file_path = os.path.join(curve_dir, '{0}.res'.format(f))
    x,y = np.loadtxt(file_path).T

    plt.plot(x, y, label=f)

plt.xlabel('A')
plt.ylabel('Response')
plt.title('Filter response curves')
plt.xlim(3000,21000)
plt.ylim(0., 0.7)
plt.legend()

ax = plt.gca()
ax.xaxis.grid(True, which='major')
ax.xaxis.grid(True, which='minor')
ax.yaxis.grid(True, which='major')
ax.yaxis.grid(True, which='minor')

plt.savefig('../run/r11_filtercurves.pdf')
#plt.show()
