#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import ipdb
import os
import pdb
import tables

import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splrep, splev, splint

d = '/disks/shear9/marberi/cache/'
r = 'b46d37b599dcfa59d7024548d388dffa'
r = 'b0ea77b393770afbb9df713f8498e95f'

f1 = tables.openFile(os.path.join(d, 'pzcat', r))
f2 = tables.openFile(os.path.join(d, 'pzpdf', r))

c1 = f1.getNode('/photoz/photoz').read()
c2 = f2.getNode('/pdfs') #.read()

#ngal,nz,nt = c2.shape
ngal, nz = c2.shape

#ipdb.set_trace()

def norm(x,y):
    spl = splrep(x,y)
    return splint(0., 2., spl)

#    ipdb.set_trace()
zs = c1['z_s']

dx = 0.01
x0 = 0.2
xa = x0 - 0.5 * dx
xb = x0 + 0.5 * dx

inds = np.logical_and(xa < zs, zs < xb)
#ipdb.set_trace()

nbins = 80
H1,edges1 = np.histogram(c1['z_s'][inds], bins=nbins)
xm1 = 0.5*(edges1[:-1] + edges1[1:])
H1 = H1 / norm(xm1, H1)

H2,edges2 = np.histogram(c1['zb'][inds], bins=nbins)
xm2 = 0.5*(edges2[:-1] + edges2[1:])
H2 = H2 / norm(xm2, H2)

#plt.plot(xm1, H1, 'o-')
plt.plot(xm2, H2, 'o-', label='Peaks')

plt.vlines(0.5*(xa + xb), 0., 8.)
#ipdb.set_trace()

p3 = 0.
for i in inds.nonzero()[0]:
#for i in range(ngal): #[:20000]:
#    pi = c2[i].sum(axis=1)
    pi = c2[i]
#    print(i)
#    print(i, pi.sum(axis=0))
    p3 += pi

#p = c2[:80].sum(axis=1)

x = np.array(range(nz)) * 0.001 #/ 2.1
p3 = p3 / norm(x, p3)
plt.plot(x, p3, label='Pdfs')
plt.legend()
plt.show()

#pdb.set_trace()
