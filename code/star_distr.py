#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import glob
import os
import ipdb
import numpy as np
import pandas as pd
import pyfits
from matplotlib import pyplot as plt

def load_cat(file_path, filters):
    # Should this not be moved elsewhere???

    W = pyfits.getdata(file_path)

    mag_in = np.vstack([W['MAG_GAAP_'+f] for f in filters]).T
    err_in = np.vstack([W['MAGERR_GAAP_'+f] for f in filters]).T

    mask = np.logical_or(mag_in == 99, mag_in == -99)

    mkheader = lambda pre, L: [pre+x for x in L]
    header_mag = mkheader('mag_', filters)
    header_err = mkheader('magerr_', filters)

    mag = pd.DataFrame(mag_in, columns=header_mag)
    err = pd.DataFrame(err_in, columns=header_err)

    # The panda did at not first like me..
    mag = mag.where(pd.notnull(mag), None)
    mag = mag.where(np.abs(mag) != 99, None)
    err = err.where(np.abs(err) != 99, None)

    cat = mag.join(err)

#    cat['cl'] = np.array(W['CLASS_STAR'])
    cat['cl'] = np.array(W['CLASS_STAR'], dtype=np.float64)

#    ipdb.set_trace()

    return W, cat


def plot1():
    _kids_path = '/data2/data/kids/kidscat'
    _cat_fmt_old = 'KIDS_{0}p0_m0p5_V0.5_blinded_extmaglim_photoz_cut.cat'
    _cat_fmt_new = 'KIDS_{0}_V0.5_blinded_extmaglim_photoz_cut.cat'

    x = '140p0_m0p5'
    file_path = os.path.join(_kids_path, _cat_fmt_new.format(x))

    filters = ['u','g','r','i'] #,'z']

    g = '/data2/data/kids/kidscat/KIDS*'
    for i,file_path in enumerate(glob.glob(g)[:1]):
        w, cat = load_cat(file_path, filters)

        print('field', i, cat.mag_i.mean())

        gal = cat[cat.cl < 0.8]
        stars = cat[0.8 < cat.cl]

        gal.mag_i.hist(bins=20, histtype='step', normed=True, label='Gal')
        stars.mag_i.hist(bins=20, histtype='step', normed=True, label='Stars')
        cat.mag_i.hist(bins=20, histtype='step', normed=True, label='All')

#        plt.plot(gal.mag_i,
#        plt.plot(stars.mag_i)


        plt.legend()
        plt.show()


# cat.mag_i.hist(bins=20, histtype='step', normed=True, label=str(i))
        ipdb.set_trace()

    plt.legend()
    plt.show()

def plot2():
    import pandas as pd

    g = '/data2/data/kids/kidscat/KIDS*'
    filters = ['i']
    df = pd.DataFrame()
    for i,file_path in enumerate(glob.glob(g)[:30]):
        print('i', i)
        w, cat = load_cat(file_path, filters)
        stars = cat[0.8 < cat.cl]
  
        D = pd.DataFrame({'mag_i': stars.mag_i, 'field': len(stars)*[i]})
        df = df.append(D)
 
    ipdb.set_trace()
 
plot2()
