#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
from scipy.interpolate import splrep, splev
from matplotlib import pyplot as plt

d_fjc = '/Users/marberi/data/photoz/seds'
d_fjc = '/data2/photoz/run/data/seds/'

z = 0.9

def get_sed(path):
    x,y = np.loadtxt(path).T
    lmb = (1+z)*x
    spl = splrep(lmb, y)
    y *= 0.2/splev(8000, spl)

    return lmb, y

def fjc_seds():
    seds = ['Ell_01', 'Irr_01', 'Irr_02', 'Irr_03', 'Irr_04', 'Sbc_01', 'Sbc_02', 'Sbc_03']
    for sed in seds:
        path = os.path.join(d_fjc, '{0}.sed'.format(sed))

        lmb, y = get_sed(path)
        plt.plot(lmb, y, 'o-', label=sed)

#d_bpz = '/Users/marberi/data/bpz/bpz-1.99.3/SED'
d_bpz = '/data2/marberi/source/bpz-1.99.3/SED'
d_sed = d_fjc
for fname in os.listdir(d_sed): #bpz):
    if not fname.endswith('.sed'):
        continue

    if not 'Irr' in fname: continue
    if 'CWW' in fname: continue

    sed = fname.replace('.sed', '')
    path = os.path.join(d_sed, fname)
    lmb, y = get_sed(path)
    plt.plot(lmb, y, 'o-', label=sed)

#ipdb.set_trace()

d_f = '/Users/marberi/data/photoz/des_euclid'
d_f = '/data2/photoz/run/data/des_euclid/'

for fname in ['vis', 'r','i','z']:
    x,y = np.loadtxt(os.path.join(d_f, '{0}.res'.format(fname))).T

    plt.plot(x, y, label=fname)


plt.xlim(3000, 12000)

plt.legend()
plt.show()

#    ipdb.set_trace()
