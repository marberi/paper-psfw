#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import time
import numpy as np
import pandas as pd

Nfilters = 50
Nseds = 127
Nz = 1200
path_old = '/tmp/ab_old.hdf5'
path_new = '/tmp/ab_new.hdf5'

def create_ab():
    z = np.linspace(0., 10, Nz)
    r2vz = np.random.random(Nz)
    R = pd.DataFrame({'z': z, 'r2vz': r2vz})

    return R

def storeab_old(file_path, R):

    store = pd.HDFStore(file_path, 'w')
    for i in range(Nfilters):
#        print('i', i)
        for j in range(Nseds):
            store['/f{0}/s{1}'.format(i,j)] = R

    store.close() 

def R_bundle(R):
    bundle = pd.DataFrame()
    bundle['z'] = R.z
    for i in range(Nfilters):
        for j in range(Nseds):
            name = 'f{0}s{1}'.format(i,j)
            bundle[name] = R.r2vz

    return bundle

def storeab_new(file_path, bundle):

    store = pd.HDFStore(file_path, 'w')
    store['r2vz'] = bundle
    store.close() 

def test_write():
    if not os.path.exists(path_old):
        R = create_ab()
        t1 = time.time()
        storeab_old(path_old, R)
        dt1 = time.time() - t1

    if not os.path.exists(path_new):
        R = create_ab()
        bundle = R_bundle(R)
        t2 = time.time()
        storeab_new(path_new, bundle)
        dt2 = time.time() - t2

    print('Write old', dt1)
    print('Write new', dt2)

def readab_old(file_path):
    store = pd.HDFStore(file_path) 

    for i in range(Nfilters):
        for j in range(Nseds):
            R = store['/f{0}/s{1}'.format(i,j)]

    store.close()

def readab_new(file_path):
    store = pd.HDFStore(file_path) 
    bundle = store['r2vz']
    store.close()

t3 = time.time()
readab_old(path_old)
dt3 = time.time() - t3

t4 = time.time()
readab_new(path_new)
dt4 = time.time() - t4

print('Read old', dt3)
print('Read new', dt4)
