#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import glob
import os
import pdb
import ipdb
import numpy as np
from matplotlib import pyplot as plt

from scipy.interpolate import splev, splrep

d = '/disks/shear9/marberi/work/photoz/run/data/filters'

x = np.linspace(0., 10000, 20000)
splD = {}
yD = {}
for f in glob.glob(os.path.join(d, '*.res')):
    x_in, y_in = np.loadtxt(f).T
    filter_name = os.path.basename(f).split('.')[0]
    print('Filter', filter_name)

    spl = splrep(x_in, y_in)
    y = splev(x, spl, ext=1) 
    y = y / np.sum(y)

    splD[filter_name] = spl
    yD[filter_name] = y


nbfilters = map(str, np.arange(4500, 8401, 100, dtype=np.int).tolist())
bb_filters = ['up', 'g', 'r', 'i', 'z', 'y']

A = np.vstack([yD[f] for f in nbfilters]).T

w_init = np.ones(len(nbfilters)) #/ len(nbfilters)

fit_to = 'g'
y_tar = yD[fit_to]

#pdb.set_trace()
def f(w):
    y_mod = (w*A).sum(axis=1)
    diff = 1000*np.sum((y_tar - y_mod)**2)

    punish = 1e6*np.sum(w[w < 0]**2)
    print('diff', diff, punish)

    diff += punish



#    plt.plot(x, y_tar, label='Orig')
#    plt.plot(x, y_mod, label='Sum')
    
#    plt.legend()
#    plt.show()


    return diff

from scipy.optimize import minimize, fmin, fmin_bfgs

bounds = len(w_init)*[(0, 20)]
#Q = minimize(f, w_init, options={'maxiter': 200}) #, od='SLSQP', bounds=bounds) #(0, None))
Q = fmin_bfgs(f, w_init, maxiter=50)

y_mod = (Q*A).sum(axis=1)

plt.plot(x, y_tar, label='Orig')
plt.plot(x, y_mod, label='Sum')
plt.title(fit_to)
plt.legend()
plt.savefig('../plots/r7_sedfit_{0}.pdf'.format(fit_to))
plt.show()


#A*w
ipdb.set_trace()
for f in bb_filters:
    ipdb.set_trace()
