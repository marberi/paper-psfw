#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
from matplotlib import pyplot as plt
import scipy.sparse
import scipy.sparse.linalg



def create_sparse(r1):
    # Here r1 is not the full dense matrix. There was simply not
    # enough memory for doing that.
    N,k = r1.shape

    toperm = np.arange(100)
    L = []
    for i in range(N):
#        print('i', i)
        toperm = np.random.permutation(toperm)
        L.append(toperm[:k])

    x = np.hstack(L)
    y = np.tile(np.arange(k), (1, N))[0]

    data = r1.flatten()

    pdiag = np.arange(N, dtype=np.int)
    #x = np.hstack(
    x = np.hstack([x, pdiag])
    y = np.hstack([y, pdiag])
    data = np.hstack([data, np.ones(N)])

    sp = scipy.sparse.csc_matrix((data, (x,y)), shape=(N, N))

    return sp

# The first performance test.
if False:
    t1 = time.time()
    np.linalg.inv(r1)
    t2 = time.time()

    dt = t2-t1
    print('time dense', dt)

    sp = create_sparse()
    t1 = time.time()
    sp_inv = scipy.sparse.linalg.inv(sp)
    t2 = time.time()

    dt = t2-t1
    print('time sparse', dt)

# Testing various system sizes.
if False:
    k = 10
    for N in [1000, 5000, 10000, 20000, 50000]:
        r1 = np.random.random((N, N))

        sp = create_sparse(r1, k)

        t1 = time.time()
        sp_inv = scipy.sparse.linalg.inv(sp)
        t2 = time.time()

        dt = t2-t1
        print('time sparse', N, dt)

# Test performance for solving the sparse system.
if False:
    k = 10
    for N in [1000, 5000, 10000, 20000, 50000]:
        r1 = np.random.random((N, k))

        A = create_sparse(r1)
        b = np.random.random(N)

        t1 = time.time()
        x = scipy.sparse.linalg.spsolve(A, b)
        t2 = time.time()

        dt = t2-t1
        print('time sparse solve', N, dt)

k = 10
r1 = np.random.random((N, k))
A = create_sparse(r1)


ipdb.set_trace()
