#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import shutil
import numpy as np
import pandas as pd
from scipy.interpolate import splrep, splev, splint
from matplotlib import pyplot as plt

fmt = '../run/r204_code_zx_{0}.pdf'

# Config.
old_dir = '/data2/photoz/run/data/des_euclid2'
new_dir = old_dir.replace('euclid2', 'euclid3')

# These needs to be triggered manually.
# - copy_old_filters - New catalog with a copy of the old filters.
# - create_zx - Creates the z-band filters.


def copy_old_filters():
    # In this code we are not giving a warning if
    # the directory already exists and just overwrite
    # the content.
    if not os.path.exists(new_dir):
        os.mkdir(new_dir)

    # Copy the other filters.
    for fname in os.listdir(old_dir):
        old_path = os.path.join(old_dir, fname)
        new_path = os.path.join(new_dir, fname)

        if not os.path.exists(new_path):
            shutil.copy(old_path, new_path)


def create_zx():
    """Both construct and store the zx filter."""
    
    new_path = os.path.join(new_dir, 'zx.res')
    respD = load_bands(['z'])
    zband = respD['z']

    # The z-band cut to only include the VIS range.
    y1 = zband.resp
    y2 = sig(zband.w, 1., 9200, -20.)
    y_zx = y1*y2

    A = np.array([zband.w, y_zx]).T
    np.savetxt(new_path, A)

def load_bands(bands):
    respD = {}
    for band in bands:
        # And then a special threatment of the z-band.
        path = os.path.join(new_dir, '{0}.res'.format(band))

        resp = pd.read_csv(path, sep=' ')
        resp.columns = ['w', 'resp']

        respD[band] = resp

    return respD


def plot1():
    """Show the response of the different curves."""

    plt.clf()
    respD = load_bands(['z', 'zx', 'vis'])
    for band, resp in respD.iteritems():
        plt.plot(resp.w, resp.resp, '.', label=band)

    plt.grid(True)
    plt.xlim((4000, 11000))
    plt.xlabel('Wavelenght [A]', size=16)
    plt.ylabel('Response', size=16)
    plt.legend()

    path = fmt.format('plot1')
    plt.savefig(path)
#    plt.show()

def get_yvals(resp, add_lmb=True):
    y = resp.w*resp.resp if add_lmb else resp.resp
    spl = splrep(resp.w, y)

    xvals = np.linspace(8000, 10500)
    yvals = np.zeros_like(xvals)

    for i,xi in enumerate(xvals):
        yvals[i] = splint(8000, xi, spl)

    yvals /= yvals[-1]

    return xvals, yvals

def plot2():
    """The cumulative flux in the band."""

    plt.clf()
    respD = load_bands(['z', 'vis'])

    for add_lmb in [False, True]:
        lbl = 'Add $\lambda$ {0}'.format(add_lmb)
        xvals, yvals = get_yvals(respD['z'], add_lmb)
        plt.plot(xvals, yvals, '-', label=lbl)

#        ipdb.set_trace()

    splc = splrep(xvals, yvals)

    plt.axvline(9200, *plt.ylim(), color='black', lw=2, alpha=0.3)
    plt.axhline(splev(9200, splc), *plt.ylim(), color='black', lw=2, alpha=0.3)

    plt.grid(True)
    plt.xlabel('Wavelenght [A]', size=16)
    plt.ylabel('Cumulative flux (fraction)', size=16)
    plt.legend(loc=4)

    path = fmt.format('plot2')
    plt.savefig(path)

def sig(x, amp, cen, dl):
    """Sigmoid function with amplitude, shifted center 
       and adjusted width.
    """
    x = (x - cen)*(5. / dl) # (dl / 5.)
    return amp / (1.0 + np.exp(-1.0 * x))

def plot3():
    """Checking the resolution of the z-band."""

    # experimenting with the zx band.
    respD = load_bands(['z', 'vis'])
    zband = respD['z']

    y1 = zband.resp
    y2 = sig(zband.w, 1., 9200, -20.)

    whres = np.linspace(8000, 10000, 300)
    spl_y1 = splrep(zband.w, y1, k=3)
    y1_hres = splev(whres, spl_y1)

    spl_y2 = splrep(zband.w, y2)
    y2_hres = splev(whres, spl_y2)

    plt.plot(zband.w, y1, '.', label='z-band')
    plt.plot(zband.w, y2, '.', label='sigmoid')
    plt.plot(whres, y1_hres, '-', label='z-band interp')
    plt.plot(whres, y2_hres, '-', label='sigmoid interp')

    plt.xlim((8000, 10500))
    plt.ylim(0., 1.2)
    plt.grid(True)
    plt.legend()

    plt.xlabel('Wavelenght [A]', size=16)
    plt.ylabel('Response', size=16)

    path = fmt.format('plot3')
    plt.savefig(path)

plot1()
