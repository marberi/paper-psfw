#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

lmb = np.linspace(3000, 12000)

if False:
    kappa = -1.2
    plt.plot(lmb, lmb**kappa)
    plt.show()

ll = 5500
lh = 9200
ls = 6500
gamma = 0.55

kL = [-1.1, -1.2, -2., -3]

for k in kL:
    gp = gamma+k+1
    kp = k+1
    A1 = (lh**gp - ll**gp)/gp
    A2 = (lh**kp - ll**kp)/kp

    alpha = np.linspace(0., 0.6*A2)
    R2 = (A1 - alpha*ls**alpha)/(A2 - alpha)

    R2 /= R2[0]

    plt.plot(alpha/A2, R2, 'o-', label='k: '+str(k))


plt.ylabel('$R^2/R^2(\\alpha=0)$', size=16)

#plt.xlim(0.,0.5)
plt.legend()
plt.show()

#ipdb.set_trace()
