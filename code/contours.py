#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
#import seaborn as sns
import seaborn.apionly as sns

from matplotlib import pyplot as plt
# Experimention (again) 
a = 0.1
b = 0.08
b = 0.00
N = 10000

def gen_rand():
    mean = np.array([0., -.0])
    cov = np.array([[a, b], [b, a]])
    R = np.random.multivariate_normal(mean, cov, N).T

    return R

R = gen_rand()

if False:
    fig, A = plt.subplots(2)
    #plt.plot(R[0], R[1], '.')
    #plt.show()
    #sns.set_style("white")
    sns.kdeplot(R[0], R[1], ax=A[0])
    x = np.linspace(0., 1)
    plt.plot(x, x**2)

from scipy.stats import gaussian_kde

#kde = gaussian_kde(R)

k1 = gaussian_kde(R[0])

S = pd.Series(R[0])
if False:
    S.hist(bins=100)
    plt.show()

#S.hist(bins=100, histtype= 'step', normed= True)

desc = S.describe()
#plt.axvline(desc['25%'])
#plt.axvline(desc['75%'])

#if False:
#    plt.show()

#th0 = np.pi / 4. / 10.

def find_contours(R):
    L1 = []
    L2 = []

    mean = R.mean(axis=1)
    S = (R.T - mean).T 

    for th in np.linspace(0., 2*np.pi):
        X = np.cos(th)*S[0] - np.sin(th)*S[1]
        X = pd.Series(X)

        descr = X.describe(percentiles=[0.02, 0.16])


        A = descr['2%']
        L1.append((A*np.cos(th), -A*np.sin(th)))

        A = descr['16%']
        L2.append((A*np.cos(th), -A*np.sin(th)))

    cont = []
    for L in [L1, L2]:
        X,Y = zip(*L)
        X = X+mean[0]
        Y = Y+mean[1]

        cont.append((X,Y))

    return cont

cont = find_contours(R)
for X,Y in cont:
    plt.plot(X, Y, 'o-')

ipdb.set_trace()

plt.show()

#plt.plot(thL, L4)
#plt.show()

ipdb.set_trace()


#ipdb.set_trace()
#plt.show()
