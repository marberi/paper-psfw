#!/usr/bin/env python
# encoding: UTF8

# Simple script for checking the variance

from __future__ import print_function
import ipdb
import numpy as np
from matplotlib import pyplot as plt

a = 4.5
b = 3.7
N = 10000

x = np.random.normal(scale=a, size=N)
y = np.random.normal(scale=b, size=N)

print(a, np.sqrt(np.var(x)))
print(b, np.sqrt(np.var(y)))

print(np.sqrt(a**2 + b**2), np.sqrt(np.var(x-y)))
