#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
import time

def plot1():
    N = 500
    r = np.random.random(N)

    t1 = time.time()
    d1 = 0.
    for i in range(len(r)):
        for j in range(len(r)):
            d1 += (r[j] - r[i])**2

    d1 /= N**2

    lt1 = time.time() - t1

    t2 = time.time()
    d2 = 0
    avg = r.mean()
    for i in range(len(r)):
        d2 += (r[i] - avg)**2

    d2 = 2./N*d2

    lt2 = time.time() - t2

    t3 = time.time()
    d3 = 2*r.std()**2
    lt3 = time.time() - t3

    # In case the algorithm failes
    assert np.allclose(d1,d2)
    assert np.allclose(d2,d3)

    print(N, 'ratio[1/3]', lt1/lt3)
    print(N, 'ratio[2/3]', lt2/lt3)

    #ipdb.set_trace()

def plot2():
    Nx = 1000
    Ny = 1000

    x = np.random.random(Nx)
    y = np.random.random(Ny)

    t1 = time.time()
    d1 = 0.
    for i in range(Nx):
        for j in range(Ny):
            d1 += (x[i] - y[j])**2

    d1 /= float(Nx*Ny)
    lt1 = time.time() - t1

    t2 = time.time()
    d2 = x.std()**2 + y.std()**2 + (x.mean() - y.mean())**2
    lt2 = time.time() - t2

    assert np.allclose(d1,d2)

    print(Nx, Ny, 'ratio[1/2]', lt1/lt2)


N = 1000
r = np.random.random(N)


ipdb.set_trace()
