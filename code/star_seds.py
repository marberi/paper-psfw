#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import glob
import numpy as np
from matplotlib import pyplot as plt

d = '/disks/shear9/marberi/work/source/lephare/lephare_dev/sed/STAR/'
r = 'LAGET'
r = 'PICKLES'

path = os.path.join(d, r)
for file_path in glob.glob(path+'/*'):
    if 'README' in file_path:
        continue

    if '.list' in file_path:
        continue

    print(file_path)
    x,y = np.loadtxt(file_path, usecols=[0,1]).T

    sed = os.path.basename(file_path).replace('.dat', '')
    plt.plot(x,y, label=sed)

plt.xlim(0, 10000)
plt.yscale('log')
plt.legend()
plt.show()

#    print(file_path)
#ipdb.set_trace()
