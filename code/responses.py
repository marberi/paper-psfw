#!/usr/bin/env python
# encoding: UTF8

import pdb
import numpy as np


def f_d(delta, D):
    return 0.154 * (D/1.2)**(-1.) * (delta / 7350.)

def f_mtf(delta):
    part1 = (0.11 - 0.027*(delta/7000.))*(delta <= 7000.)
    part2 = (0.09 - 0.007*(delta/7000.))*(7000 < delta)

    return part1 + part2

def f_whmj(delta):
    return 0.08*np.ones_like(delta)

def f_comb(delta, D):
    f1 = f_d(delta, D)
    f2 = f_mtf(delta)
    f3 = f_whmj(delta)

    return np.sqrt(f1**2 + f2**2 + f3**2)

def f_comb(delta, D):
    return delta**0.55

def make_plots():
    from matplotlib import pyplot as plt

    delta = np.linspace(4000., 11000.)
    D = 1.2

    f1 = f_d(delta, D)
    f2 = f_mtf(delta)
    f3 = f_whmj(delta)
    f4 = f_comb(delta, D)

    plt.plot(delta, f1, label='Diffraction limit')
    plt.plot(delta, f2, label='CCD MTF')
    plt.plot(delta, f3, label='Jitter')
    plt.plot(delta, f4, label='Combined')

    plt.xlim(4500., 10000.)
    plt.ylim(0.05, 0.235)
    plt.minorticks_on()
    plt.legend()

    plt.show()

if __name__ == '__main__':
    make_plots()
