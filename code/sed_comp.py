#!/usr/bin/env python
# encoding: UTF8

import ipdb
import glob
import os
import pdb
import numpy as np
from matplotlib import pyplot as plt

d = '/disks/shear9/marberi/work/source/lephare/lephare_dev/sed/'

g = lambda x: glob.glob(os.path.join(d, x, '*.sed'))
h = lambda x: glob.glob(os.path.join(d, x, '*.dat'))
S1 = g('GAL/COSMOS_SED')
S2 = g('STAR/BD')
S2 = h('STAR/SPEC_PHOT')

for f in S2: #[:4]: #[:8]:
    print(f)
    x,y = np.loadtxt(f).T[:2]
    inds = np.logical_and(4500 < x, x < 7000)

    a,b = np.polyfit(x[inds], y[inds],1)
    
#    ipdb.set_trace()
    plt.plot(x,y)
    plt.plot(x,a*x + b)

    print('a', a)
    plt.show()
