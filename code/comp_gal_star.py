#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import os
import glob
import ipdb

import numpy as np

file_path = '../data/r2vz'

g = os.path.join(file_path, '*')

#print(file_path)
for f in glob.glob(g):
#    file_name = os.path.basename(f)
    sed, fname, _ = os.path.basename(f).split('.')
    if not fname == 'vis':
        continue

    x,y = np.loadtxt(f).T

    print(sed, y[0] / 133.78724279938933)
#    ipdb.set_trace()
