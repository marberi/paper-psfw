#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from scipy.stats import norm
from scipy.integrate import simps
from matplotlib import pyplot as plt

def bias_gauss(loc, scale):
    """Bias for a Gaussian distribution."""

    R = np.linspace(10, 200, 100000)
    p = norm.pdf(R, loc=loc, scale=scale)

    s1 = simps(R*p, R)
    s2 = simps(p/R, R)

    bias = s1*s2 - 1.

    return bias

def bias_flat(loc, width):
    mean = 100

    Ra = mean - 0.5*width
    Rb = mean + 0.5*width

    R = np.linspace(Ra, Rb, 1000)
    p = np.ones_like(R)/width

    s1 = simps(R*p, R)
    s2 = simps(p/R, R)


    bias = s1*s2 - 1.

    return bias

def plot1():
    loc = 100
    scale = np.logspace(-0.5, 1, 50) #linspace(1., 20, 50)
    bias = [bias_gauss(loc, x) for x in scale]

    ax = plt.gca()
    ax.xaxis.grid(which='both')
    ax.yaxis.grid(which='both')

    plt.plot(scale / loc, bias, 'o-')
    plt.yscale('log')

    plt.xlabel('$\sigma_R / <R>$', size=16)
    plt.ylabel('$R^2$ bias', size=16)
    plt.title('$R^2$ bias in Gaussian distribution.', size=16)
    plt.show()

def plot2():
    loc = 100
    width = np.linspace(1., 20, 50)
    bias = [bias_flat(loc, x) for x in width]

    ax = plt.gca()
    ax.xaxis.grid(which='both')
    ax.yaxis.grid(which='both')

    plt.plot(width/loc, bias)
    plt.yscale('log')

    plt.xlabel('$\sigma_R / <R>$', size=16)
    plt.ylabel('$R^2$ bias', size=16)
    plt.title('$R^2$ bias in uniform distribution.', size=16)

    plt.show()

plot2()
#ipdb.set_trace()
