#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import glob
import os
import pdb
import numpy as np
import tables

from scipy.interpolate import splev, splrep

def load_data():
    """Load the psfw(z) estimations."""

    g = '../sfw_data/*PSF'
    res = {}
    for file_path in glob.glob(g):
        sed, filter_name,_ = os.path.basename(file_path).split('.')

        x,y = np.loadtxt(file_path).T

        res[sed,filter_name] = splrep(x, y)
    #    pdb.set_trace()

    return res

def sel_filter(spls, to_sel):
    """Select the estimations for one filter."""

    sel = {}
    for (sed,filter_name), spl in spls.iteritems():
        if not filter_name == to_sel:
            continue
        sel[sed] = spl

    return sel

seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
def to_nr(spls, seds=seds):
    """Convert from sed names to numbers."""

    innum = {}
    for key, spl in spls.iteritems():
        innum[seds.index(key)+1] = spl

    return innum

def load_z(za, zb):
    """Load redshift slice."""

    cat_path = '../cat/faint.h5'
    fb = tables.openFile(cat_path)
    select = '{0} < z_s < {1}'.format(za, zb)
    cat_table = fb.getNode('/cat/cat')
    cat = cat_table.readWhere(select)


    return cat

def load_i(i1, i2):
    """Load magnitude limited selection."""

    cat_path = '../cat/faint.h5'
    fb = tables.openFile(cat_path)
    select = '({0} < m_0) & (m_0 < {1})'.format(i1, i2)
    cat_table = fb.getNode('/cat/cat')
    cat = cat_table.readWhere(select)

#    pdb.set_trace()

    return cat


def find_r(spls, t, z):
#    pdb.set_trace()
    t0 = np.floor(t)
    dt = t - t0

    r = -10*np.ones((len(z)))
    for i in np.arange(1,6):
        inds = t0 == i
        spl0 = spls[i]
        spl1 = spls[i+1]

        zi = z[inds]
        dti = dt[inds]
        r1 = splev(zi, spl0)
        r2 = splev(zi, spl1)

        r[inds] = (1-dti)*r1 + dti*r2

    end = (r == -10)
    if np.sum(end):
        r[end] = splev(z[end], spls[6])

    return r

