#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import pdb
import numpy as np
import sklearn
import tables
import sklearn.ensemble as en
from sklearn import svm
from sklearn import linear_model
from matplotlib import pyplot as plt

from sklearn.linear_model import LinearRegression, ARDRegression
from sklearn.pipeline import Pipeline
from sklearn.cross_validation import Bootstrap
# Config
#filters = ['up', 'g', 'r', 'i', 'z']
#filters = ['g', 'r','i']
conf = {
  'filters': ['up', 'g', 'r', 'i', 'z']
}


off = 'z'
xoff = 0.0

def load_r2():
    fb = tables.openFile('../data/r2out.h5')
    r2_in = fb.getNode('/r2').read()

    return r2_in

r2_in = load_r2()

Ntrain = 40000
ind_raw = np.random.randint(0, len(r2_in), 2*Ntrain)
ind = np.array(list(set(ind_raw))[:Ntrain])

bins = np.arange(-0.02, 0.02, 0.0001)
tohist = []

frac = 0.1

A = np.vstack([r2_in['mag_{0}'.format(x)] for x in conf['filters']]).T
#A += A*frac*np.random.normal(size=A.shape)

if False:
    plt.plot(r2_in['z_s'], r2_in['zb'], '.')
    plt.show()


ipdb.set_trace()


bs = Bootstrap(len(r2_in), 1, 0.3) #Ntrain / len(r2_in))
res = []
for train_index, test_index in bs:
#    reg = en.GradientBoostingRegressor()
    reg = en.RandomForestRegressor()

    A_train = A[train_index]
    A_test = A[test_index]

    B = r2_in['r2_true']
    B_train = B[train_index]
    B_test = B[test_index]

    print('Before fit.')
    reg.fit(A_train, B_train)
    print('After fit.')

    B_pred = reg.predict(A_test)

    delta_r2 = (B_pred - B_test) / B_test
    delta_z = (r2_in['zb'] - r2_in['z_s'])[test_index]

    tindx = test_index
#    ipdb.set_trace()
    res.append({'zs': r2_in['z_s'][tindx], 'zb': r2_in['zb'][tindx], 
                'inds': test_index, 
                'delta_r2': delta_r2, 'delta_z': delta_z})

#    H, edges = np.histogram(delta_r2, bins=3000, range=(-0.02,0.02))
#    xm = 0.5*(edges[:-1] + edges[1:])
#    plt.plot(xm,H, label=str(N))
#    print(N, np.average(delta))
#    ipdb.set_trace()
#    tohist.append((xm,H, delta_r2))

for part in res:
    cov = np.cov(part['delta_r2'], part['delta_z'])
    print(cov)

part = res[0]
def pz_cut(xa, xb, field='zs', edges=False):

    z = part[field]
    inds = np.logical_and(xa <= z, z <= xb)

    Y = part['delta_r2'][inds]

    return Y

def pz_plot(xa, xb, field='zs', edges=False):

    Y = pz_cut(xa, xb, field)
    if not edges:
        H, edges = np.histogram(Y, bins=100, range=(-0.02, 0.02))
    else:
        H, edges = np.histogram(Y, bins=edges)

    xm = 0.5*(edges[:-1] + edges[1:])

    print(field, np.average(Y))
    plt.plot(xm, H, label=field)
    return edges

