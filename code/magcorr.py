#!/usr/bin/env python 
# encoding: UTF8
import ipdb
import numpy as np
import pandas as pd
import itertools as it
from matplotlib import pyplot as plt
from scipy.stats import linregress

from xdolphin import Job
import sys
sys.path.append('/home/marberi/drv/work/papers/psfw/code')
import pipel as psf_pipel

def modify_pipel(pipel):
    raise NotImplementedError()


pipel = psf_pipel.pipel()
pipel, r2train = modify_pipel(pipel)
offset = pipel['starcat']

dm = [0., 0.01, 0.05, 0.1]
#dm = np.linspace(-0.1, 0.1, 21)
dm = np.linspace(-0.01, 0.01, 41)
bins = np.linspace(-0.01, 0.01, 60)
bins = False

filters = ['vis', 'r', 'i', 'z', 'Y', 'J', 'H']
#filters = ['r', 'i', 'z']

def find_df(filters, dm):
    df = pd.DataFrame()
    inds = pd.DataFrame()
    A = pd.DataFrame()

    for f1,f2 in it.combinations(filters, 2):
        for fname in [f1,f2]:
            for x in dm:
                off = dict(zip(filters, np.zeros(len(filters))))
                off[fname] = x
                offset.task.config['offset'].update(off)

                r2train.task.config['filters'] = [f1, f2]
                store = r2train['r2train']
                r2 = store['r2abs']
                store.close()

                N = len(r2)
                inds = inds.append(pd.Series({'fname': fname, 'f1': f1, 'f2': f2, 'dm': x}),
                                   ignore_index=True)

                ipdb.set_trace()
                A = A.append(r2[0], ignore_index=True)

    df = inds.join(A)
    df['mean'] = A.mean(axis=1)
    df['median'] = A.median(axis=1)

    return df

def nooffset():
    # Just to find the mean quantity...
    all_filters = ['vis', 'r', 'i', 'z', 'Y', 'J', 'H']

    off = dict(zip(filters, np.zeros(len(all_filters))))
    offset.task.config['offset'].update(off)

    r2train.task.config['filters'] = [f1, f2]
    store = r2train['r2train']
    r2 = store['r2abs']

    ipdb.set_trace()

def plot1():
    filters = ['vis', 'r', 'i']
    df = find_df(filters, dm)

#    fig, A = plt.subplots(4,2)
    fig, A = plt.subplots(2,2)
    A_flat = A.flatten()

    f = lambda val,field: val[field] / float(val[val.dm==0][field])

    for i,fname in enumerate(filters):
        ax = A_flat[i]

        for (f1,f2), val in df[df.fname==fname].groupby(['f1', 'f2']):
            val = val.drop(['fname', 'f1', 'f2'],axis=1)
            valD =  val.drop(['dm'],axis=1)

            y1 = f(val, 'mean')
            y2 = f(val, 'median')

#            y = val['mean']
#            ipdb.set_trace()
#            y = val.drop(['dm'],axis=1).mean(axis=1)
#            y = y / float(valD[val.dm == 0].mean(axis=1))

            ax.plot(val['dm'], y1, label=f1+'-'+f2+' Mean')
            ax.plot(val['dm'], y2, label=f1+'-'+f2+' Median')
            ax.set_title(fname)
            ax.legend()

    #plt.legend()
    plt.show()
    #ipdb.set_trace()

    print(R.corr())

    #plt.plot(dm, df.std(axis=1), 'o-')
    plt.plot(dm, df.mean(axis=1), 'o-', label='Mean')
    plt.plot(dm, df.median(axis=1), 'o-', label='Median')

    plt.legend()
    plt.show()

def plot2():
    filters = ['r', 'i', 'z']
    df = find_df(filters)
    dm_interp = np.linspace(np.min(dm), np.max(dm), 400)

    fname = 'r'
    for (f1,f2), val in df[df.fname==fname].groupby(['f1', 'f2']):
        slope, intercept, r_value, p_value, std_err = linregress(val.dm, val['mean'])

        plt.plot(val.dm, val['mean'], 'o-')

        y = intercept + slope*dm_interp
        plt.plot(dm_interp, y)

        plt.show()
        ipdb.set_trace()

def plot3():
    AAA = nooffset()

    filters = ['r', 'i'] #, 'z']
    df = find_df(filters, dm)
    #df_test = find_df(filters, dm)
    val = df[(df.fname=='r') & (df.f1 == 'r') & (df.f2 =='i')]
    A = np.array([val['mean']]).T
    B = np.array(val['dm'])

    dm_interp = np.linspace(np.min(dm), np.max(dm), 400)

    from sklearn import linear_model
    pred = linear_model.LinearRegression()
    pred = pred.fit(A, B)

plot1()
