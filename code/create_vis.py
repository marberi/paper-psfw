#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import os
import numpy as np

from matplotlib import pyplot as plt

a = 5500
b = 9200
N = 10000

to_dir = '/data2/photoz/run/data/des_euclid'

def sig(x, amp, cen, dl):
    """Sigmoid function with amplitude, shifted center 
       and adjusted width.
    """
    x = (x - cen)*(5. / dl) # (dl / 5.)
    return amp / (1.0 + np.exp(-1.0 * x))

def create_vis():
    x = np.linspace(3000, 12000, N)

    dl = 10
    qe = 0.4

    resp = qe * sig(x, 1., a, dl) * sig(x, 1., b, -dl)

    return x, resp

def write_vis(lmb, resp):
    file_path = os.path.join(to_dir, 'vis.res')

    np.savetxt(file_path, np.vstack([lmb, resp]).T)

if __name__ == '__main__':
    write_vis(*create_vis())
