#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
#import seaborn as sns
import seaborn.apionly as sns

from matplotlib import pyplot as plt
# Experimention (again) 
a = 0.1
b = 0.08
b = 0.01
N = 100000

def gen_rand():
    mean = np.array([0., -.0])
    cov = np.array([[a, b], [b, a]])
    R = np.random.multivariate_normal(mean, cov, N).T

    return R

R = gen_rand()

#ipdb.set_trace()
import scipy.stats
gkde = scipy.stats.gaussian_kde(R)

k = 1.0
x,y = np.mgrid[-k:k:0.05, -k:k:0.05]
z = gkde.evaluate([x.flatten(), y.flatten()]).reshape(x.shape)

from matplotlib import cm
plt.contour(x,y,z, linewidths=1, alpha=.5, cmap=cm.Greys)
 
plt.show()

#ipdb.set_trace()
