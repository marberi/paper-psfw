#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd

def test1():
    Ngal = 10000
    Ncol = 50

    R = np.random.random((Ngal, Ncol))

    t1 = time.time()
    np.cov(R.T)
    dt1 = time.time() - t1

    dR = pd.DataFrame(R)
    t2 = time.time()
    dR.cov()
    dt2 = time.time() - t2

    print('Ngal, Ncol, t1, t2', Ngal, Ncol, dt1, dt2)

# Testing again...
Ngal = 10000
Ncol = 50

A = np.random.random((Ncol, Ncol))
R = np.random.random((Ngal, Ncol))

cov1 = np.cov(np.dot(R,A.T).T)
cov2 = np.dot(A, np.dot(np.cov(R.T), A.T))

print(cov1)
print(cov2)

#cov1 = np.cov(R.T)

ipdb.set_trace()
