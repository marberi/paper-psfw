#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import flib

from matplotlib import pyplot as plt

def test1():
    ilist = np.linspace(22.5, 24.)
    res = []

    for i in ilist:
        cat = flib.load_i(18., i)
        res.append(len(cat))

    plt.plot(ilist, res, 'o-')
    plt.show()


spls = flib.load_data()
in_vhs = flib.sel_filter(spls, 'vhs')
in_num = flib.to_nr(in_vhs)

cat = flib.load_i(18., 22.6)
print('nr gal:', len(cat))
ipdb.set_trace()
