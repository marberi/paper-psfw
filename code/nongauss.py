#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

#f = 10
f = 10**(0.4*22)
m = 2.5*np.log10(f)

x = 0.05
N = 100000
#sn = 20

for sn in [1,3,5,10,20, 30, 50]:
    ef = f / sn

    fobs = f + ef*np.random.normal(size=N)
    mobs = 2.5*np.log10(fobs)

    xlim = (m*(1-x), m*(1+x))
    H,edges = np.histogram(mobs, 60, range=xlim)

    xm = 0.5*(edges[:-1] + edges[1:])

    plt.xlim(*xlim)
    plt.plot(xm, H, label=str(sn))

plt.legend()
plt.show()
#ipdb.set_trace()
