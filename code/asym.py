#!/usr/bin/env python
# encoding: UTF8

import glob
import os
import pdb
import ipdb
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep

sfw = os.path.join(os.path.dirname(__file__), '..', 'sfw_data')
g = os.path.join(sfw, '*.PSF')

spls = {}
for file_path in glob.glob(g):
    if not 'vhs' in file_path:
        continue

    x,y = np.loadtxt(file_path).T 
#    ipdb.set_trace()
    sed = os.path.basename(file_path).split('.')[0]
    spls[sed] = splrep(x, y, k=3)

d = 2

z = np.linspace(0., 2., 2000)
for key, spl in spls.iteritems():
    #y = splev(z, spl, der=d) 
    y = splev(z, spl, der=d) #/ splev(z, spl)
    if d:
        y = y / splev(z, spl)

    label = '{0}, deriv: {1}'.format(key, d) if d else key
    plt.plot(z, y, label=key)

title = 'r^2 splines'
title = '{0}, deriv {1}'.format(title, d) if d else title

plt.title(title)
plt.legend()
plt.show()
#ipdb.set_trace()
