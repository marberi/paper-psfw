#!/usr/bin/env python
## encoding: UTF8

from __future__ import print_function
import ipdb
import xdolphin
from matplotlib import pyplot as plt

import numpy as np

from xdolphin import Job

def test1():
    job = xdolphin.Job('rstar')
    rstar = job['rstar']

    H, edges = np.histogram(rstar['r2'])
    xm = 0.5*(edges[:-1] + edges[1:])

    plt.plot(xm, H)
    plt.show()

def test2():
    #ipdb.set_trace()
    job = xdolphin.Job('R2Train')
    #job.run()

    r2train = job['r2train']
    H, edges = np.histogram(r2train[0]['delta_r2'])
    xm = 0.5*(edges[:-1] + edges[1:])

    plt.plot(xm,H)
    plt.show()
    #ipdb.set_trace()

def test3():
    job1 = xdolphin.Job('R2Star')
    job2 = xdolphin.Job('R2Train')
    job2.task.config['use_colors'] = True
    job2.depend('star', job1)

    r2train = job2['r2train']

    for use_star in [True, False]:
        job2.task.config['use_stars'] = use_star
        delta_r2 = job2['r2train'][0]['delta_r2']

        H, edges = np.histogram(delta_r2)
        xm = 0.5*(edges[:-1] + edges[1:])
        plt.plot(xm, H, label=str(use_star))

    #    ipdb.set_trace()
        print('len', len(delta_r2))
        print('avg', np.average(delta_r2))

    plt.legend()
    plt.show()

def load_gal():
    fb = tables.openFile('../data/r2out.h5')
    r2_in = fb.getNode('/r2').read()
    fb.close()

    return r2_in

import tables

def test4():
    r2vz = Job('R2vz')
    job1 = Job('R2Star')
    job2 = Job('R2Gal')
    job3 = Job('MagObs')

    job1.depend('r2vz', r2vz)
    job2.depend('r2vz', r2vz)

    r2star = job1['r2star']

    job1.task.config['norm_mag'] = 20.

    x1 = 'vis'
    y1 = 'r'

    x2 = 'vis'
    y2 = 'z'

    def colors(cat):
        col1 = cat['mag_'+x1] - cat['mag_'+y1]
        col2 = cat['mag_'+x2] - cat['mag_'+y2]

        return col1, col2

    r2gal = job2['r2gal'] 
    galcat = job3['galcat']

#    ipdb.set_trace()
    f = lambda x,y: ', {0}-{1}'.format(x,y)
    for name, magcat, r2cat in [('gal', galcat, r2gal), ('star', r2star, r2star)]:
        field = 'r2gal' if name == 'gal' else 'r2_true'
        col1, col2 = colors(magcat)
#        plt.plot(col1, r2cat[field], '.', label=name+f(x1, y1))
#        plt.plot(col2, r2cat[field], '.', label=name+f(x2, y2))

        plt.plot(r2cat[field], col1, '.', label=name+f(x1, y1))
        plt.plot(r2cat[field], col2, '.', label=name+f(x2, y2))
    plt.legend()
#    plt.savefig('../run/r10_r2_color.pdf')
    plt.show()

import sys

def fisk():
    r2vz = Job('R2vz')

    # Layer 1
    r2gal = Job('R2Gal')
    r2star = Job('R2Star')
    r2gal.depend('r2vz', r2vz)
    r2star.depend('r2vz', r2vz)

    # Layer 2
#    galcat = Job('GalCat')
    galcat = Job('MagObs') #GalCat')

    r2train = Job('R2Train')
    r2train.depend('galcat', galcat)
    r2train.depend('r2gal', r2gal)
    r2train.depend('r2star', r2star)

    return r2train

def test5():
#    galcat = Job('GalCat')
#    r2gal = Job('R2Gal')
#    r2star = Job('R2Star')
#    r2train = Job('R2Train')
#
#    r2train.depend('galcat', galcat)
#    r2train.depend('r2gal', r2gal)
#    r2train.depend('r2star', r2star)

    r2train = fisk()

    for use_star in [False, True]:
        r2train.task.config['use_stars'] = use_star
        delta_r2 = r2train['r2train'][0]['delta_r2']

        delta_r2L = [x['delta_r2'] for x in r2train['r2train']]
        biasL = map(np.average, delta_r2L)
        print('Bias', np.average(biasL), np.std(biasL))

#        print(map(np.average, delta_r2L))
#        ipdb.set_trace()

        H, edges = np.histogram(delta_r2, bins=20)
        xm = 0.5*(edges[:-1] + edges[1:])
        plt.plot(xm, H, 'o-', label=str(use_star))

#        ipdb.set_trace()
        assert not np.isnan(delta_r2).any(), 'Something went wrong.'

#        print('len', len(delta_r2))
#        print('avg', np.average(delta_r2))

#    print('here...')
    plt.legend()
#    plt.yscale('log')
#    plt.savefig('../run/r11_test5_svr_vriz.pdf')
    plt.savefig('../run/r11_test5_nusvr_ri.pdf')
    plt.show()

def test6():
    galcat = Job('GalCat')
    r2vz = Job('R2vz') #['r2vz']
    r2gal = Job('R2Gal')
    r2star = Job('R2Star')
    r2gal.depend('r2vz', r2vz)
    r2star.depend('r2vz', r2vz)
    r2gal.depend('galcat', galcat)


    r2star()

def test7():
#    r2vz = Job('R2vz')

    A = Job('R2vz')['r2vz']
#    for sed in ['Scd_10', 'rk2iii']:
    for fname,sed in A.keys():
        if not fname == 'vis': continue

        X = A['vis', sed]
        plt.plot(X['z'], X['r2vz'], label=sed)

    plt.xlabel('z')
    plt.ylabel('R^2(z)')
    plt.legend()
    plt.show()

def test8():
    ab = Job('AB')['ab']
    seds = list(set([x[1] for x in ab.keys()]))
    for sed in seds:
        z,abz = ab['vis', sed][100]
        print(sed, z, abz)

#ipdb.set_trace()
test4()
#agobs = Job('MagObs')
#agobs()
