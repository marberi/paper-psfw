#!/usr/bin/env python
# encoding: UTF8

# Testing the effect of the break.
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.integrate import simps

#lm = np.linspace(5500, 9200)
lm = np.linspace(3000, 12000, 400)
R = lm**0.55

gL = [0.2, 0.5]
gL = [0.1]
bL = [3000, 4000]
bL = [3967, 3442]
zL = np.linspace(0., 2., 100)
df = pd.DataFrame()

fname = '/data2/photoz/run/data/des_euclid/vis.res'
vis_in = np.loadtxt(fname).T

from scipy.interpolate import splev, splrep
spl = splrep(*vis_in)
phi = splev(lm, spl)
#ipdb.set_trace()

vL = []
for g in gL:
    for b in bL:
        for zi in zL:
            f = 1 + (1+g)*(lm > (1+zi)*b)
            val = simps(phi*f*R) / simps(phi*f, lm)

            df = df.append(pd.Series({'g': g, 'z': zi, 'b': b, 'R':val}), 
                           ignore_index=True)
            vL.append(val)

df = df.sort('z')
for key,val in df.groupby(['g', 'b']):
    row = val.irow(0)
    y = val.R / row.R

    lbl='g {0}, b {1} A'.format(row.g, row.b)
    plt.plot(val.z, y, 'o-', label=lbl)

ax = plt.gca()
ax.xaxis.grid(True, which='both')
ax.yaxis.grid(True, which='both')

plt.xlabel('z', size=16)
plt.ylabel('$R^2$/ $R^2$[z=0]', size=16)
plt.suptitle('Effect of the 4000 A break', size=18)
#    ipdb.set_trace()
plt.xlim(0., 2.5)
plt.legend()
plt.show()

