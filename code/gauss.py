#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
# Testing some aspects of having a Gaussian.

import ipdb
import numpy as np
import scipy
import scipy.stats
from matplotlib import pyplot as plt

A = scipy.stats.norm()

x = np.linspace(-10, 10)
y = A.cdf(x)

if False:
    plt.grid(True)
    plt.plot(x,y)
    plt.savefig('test.pdf')

for i in [1,2,3]:
    cdf = A.cdf(i)

    frac = 2.*(cdf - 0.5)
    print(i, '{0:.4f}'.format(frac))


ipdb.set_trace()
