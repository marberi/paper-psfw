#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep

from xdolphin import Job

r2vz = Job('r2vz')['r2vz']

config = {'r2_filter': 'vis', 'norm_sed': 'Ell_01'}
seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
f = config['r2_filter']

X = r2vz[f, config['norm_sed']]
assert X['z'][0] == 0.
norm_val = X['r2vz'][0]

#ipdb.set_trace()
for sed in seds: 
    r2 = r2vz[f, sed]
    plt.plot(r2['z'], r2['r2vz'] / norm_val, label=sed)

#    ipdb.set_trace()
plt.xlim(0., 4.)
plt.legend()
ax = plt.gca()
ax.xaxis.grid(True, which='major')
ax.xaxis.grid(True, which='minor')
ax.yaxis.grid(True, which='major')
ax.yaxis.grid(True, which='minor')

plt.xlabel('z')
plt.ylabel('R^2 / R^2(Ell_01, z=0)')
plt.title('R^2 redshift dependence.')

plt.savefig('../run/r11_r2zdep.pdf')
#plt.show()
