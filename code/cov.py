#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

r_cross = 0.7
sig_r = 0.001
sig_pz = 0.05

N = 10000000
o = r_cross*sig_r*sig_pz

zs = np.linspace(0., 2., N)
cov = np.array([[sig_pz**2, o], [o, sig_r**2]])
A = np.random.multivariate_normal([0,0], cov, size=len(zs)).T

delta_pz, r = A

zp = zs + delta_pz
df = pd.DataFrame({'zs': zs, 'zp': zp, 'delta_pz': delta_pz,
                   'r': r, 'r_orig': r.copy()})

#za = 0.5
#zb = 0.55

def plot1():
    zmean = 0.5
    zL = [0.3, 0.5, 0.7]
    wL = np.logspace(-4, -0.5, 100)
    for zmean in zL:
        mean = [] 
        for w in wL:
            za = zmean - 0.5*w
            zb = zmean + 0.5*w
        
            df_cut = df[(za < df.zp) & (df.zp < zb)]
            mean.append(df_cut.r.mean())
        
        plt.plot(wL, mean, label=str(zmean))

    plt.xscale('log')
    plt.legend()
    plt.show()

zmean = 0.5
wL = [0.001, 0.01, 0.05, 0.1]
for w in wL:
    za = zmean - 0.5*w
    zb = zmean + 0.5*w

    #kL = [0, .1]
    kL = np.linspace(0., 1.5)
    mean = []
    for k in kL:
        delta2 = df.delta_pz*(1+k*df.zs)
        ratio = delta2 / delta_pz

    #    ipdb.set_trace()
        df.pz = df.zs + delta2
        df.r = df.r_orig * ratio
        df_cut = df[(za < df.zp) & (df.zp < zb)]

        mean.append(df_cut.r.mean())

    plt.plot(kL, np.abs(mean), 'o-', label=str(w))

plt.yscale('log')
plt.legend()
plt.show()
#ipdb.set_trace()
