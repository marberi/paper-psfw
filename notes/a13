Dropping the z-band has been an important driver for 
this paper. Can the method work without the z-band? This
will have important implications in the northern sky.

In Fig. \ref{riz_ri_hexbin} is the histogram of the $R^2$
bias values. The most important part is the $R^2$ estimator
being unbiased. One however also wish to maintain a low
scatter. Galaxies does have an intrinsic scatter in the
ellipticity.

This scatter also contributes. We see the \rz\ scatter
is very similar when training on either stars or galaxies. 
Dropping the \bi{z}-band increase the scatter. That is
no surprise since on is the loosing information. It should
be noted that the width of this histogram is very sensitive
to the depth of the groubd based data (not shown). Dropping
the \bi{z}-bandi s not a dritical detail determing the
$R^2$ scatter. The question is if one can tradeoff no
z-band observations with deeper r and i band observations.
Here we only looked at the difference is having the z-band
or not. This means the option of not having the \bi{z}-band
is having less information. We have not investigated the
possibility of tradeoff between the exposures in the 
\bi{r},\bi{i}-bands and the \bi{z}-band.

The peak is slightl more peaked with the stars when using
the \riz\ filters. The most important part is if the 
$R^2$ bias on average a lower number.

Fig. \ref{mag_depth} shows how the $R^2$ bias and photo-z
is affected by changing the depth. Here, unlike subsection 
\myn{2.x} we assume a fixed Euclid NIR deptth. What is 
varying is the depth of the ground based data. This would 
therefore correspond to different choice of the ground
based data.

In this figure we show the photo-z scatter. Could could
also show the photo-z bias, but we focus on the bias for
brevity. Here one see the photo-z scatter is incresing with
the noise level. That is entirely expected since the noise
makes it more difficult to infer the right photo-z when
fitting the templates.

The ($\sigma_{68}, $R^2$) values are calculated from many
different configurations. We can clearly not mark all.
Forturnaltely the \riz\ and dropping the z-band (marked \bi{r},\bi{i})
give roughly the same answer. One should therefore read out the
exposure scaling value from the text in the plot. It also
follow from the order of the points. 

Comparing \riz and \bi{r}.\bi{i{ one see the \bi{z}-band has an
impact, but only a minor for the \sigma_{68} of the full
sample. The effect of the \bi{z}-band is the largest at high
redshifts while this definition of $\sigma_{68}$ is weighted
by the redshift density, which peaks at lower densities.

The upper panel shows some of the $R^2$ bias going wrong. This
is included to show that inlcuding photo-z in the calibration
also has its limits.

Previously the simulation was used for the calibration. Here the 
important part was how accurate the simulations could produce
the right $R^2$. Here one also indirectly have a requirement
on how accurate the photo-z simulations represent reality. We
do however find that the \bi{r}-band offset is a slowly varying
functions, so this should not be the most critical part.
