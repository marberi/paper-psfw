#!/usr/bin/env python
# encoding: UTF8

# To remember how to create a twin axis.
import ipdb
import numpy as np

from matplotlib import pyplot as plt

x = np.linspace(0., 1.)
y1 = 1.2*x
y2 = 4 + x**2

ax1 = plt.gca()
ax2 = plt.twinx()


ax1.plot(x, y1)
ax2.plot(x, y2)

plt.savefig('test.pdf')
