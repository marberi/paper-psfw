#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
from matplotlib import pyplot as plt


x = np.linspace(0., 1)
o1 = plt.plot(x, x**2.)[0]

xline = np.linspace(*plt.xlim())
y_lower = 0*np.ones_like(xline)
y_upper = 0.5*np.ones_like(xline)
o2 = plt.fill_between(xline, y_lower, y_upper, color='black', alpha=1.0, label='fill')

yline = np.linspace(*plt.ylim())
o3 = plt.fill_betweenx(yline, 0, 0.1, color='b', alpha=0.1, hatch='/')

print('o1', o1.zorder)
print('o2', o2.zorder)
print('o3', o3.zorder)
ipdb.set_trace()


plt.grid(True)
plt.savefig('test.pdf')
