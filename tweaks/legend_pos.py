#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(0., 2.)

plt.plot(x, x**2., label='fisk')
plt.legend(loc=(0.5, 0.5))
plt.savefig('test.pdf')
#ipdb.set_trace()
