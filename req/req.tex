\documentclass{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{color}

\newcommand{\citeme}[1]{{\color{red} #1}}

\newcommand{\xfigure}[1]{
%$#1$
\begin{center}
%\includegraphics[width=0.5\textwidth]{../run/#1}
\includegraphics[width=0.5\textwidth]{../links/#1}
\end{center}
}

\begin{document}
The point spread function (PSF) for each galaxy is a convolution of the system
PSF and the galaxy spectral energy distribution (SED). The observed PSF
($R^2_{Obs}$) for a galaxy can be written

\begin{equation}
R^2_{Obs} = \int d\lambda F(\lambda) R^2_{PSF}
\label{r2obs}
\end{equation}

\noindent
where $F(\lambda) \equiv \lambda S(\lambda) T(\lambda)$ and $T(\lambda)$ 
and $S(\lambda)$ is the filter transmission and galaxy SED respectively.
Euclid system PSF is wavelength dependent, where the expected wavelength
dependence is 

\begin{equation}
R^2_{PSF}(\lambda) \equiv \lambda^\gamma = \lambda^{0.55}
\label{wavedep}
\end{equation}

\noindent
The $R^2_{PSF}$ can both be modeled and observed using stars. What enters
in the shape measurement is $R^2_{Obs}$, which is a convolution with the
galaxy SED. In the Euclid science requirement document, one expect that

\newcommand{\psf}{\text{PSF}}
\begin{equation}
\frac{\delta R^2_{\psf}}{R^2_{\psf}} \equiv |<\frac{R^2_{\text{Pred}} - R^2_{\psf}}{R^2_{PSF}}>| < 3 \times {10}^{-4}
\end{equation}

\noindent
where $R^2_{\text{Pred}}$ is a prediction of the effective PSF for each
galaxy. For Euclid the full galaxy sample will not have spectroscopy, but
rely on broad band photometry to estimate distance through photometric
redshift techniques. Estimating the effective PSF for each galaxy can
therefore only use the broad band observations.


\begin{figure}
\xfigure{prod_psf2.pdf}
\caption{Redshift dependence of $R_{Obs}$ in the Euclid VIS filter for
different spectral energy distributions (SED). All lines are normalized
to the an elliptical galaxy template at $z=0$.}
\label{zdep}
\end{figure}

In \citeme{Eriksen et.al} we investigated different approaches and their
limitations to estimate the $R^2_{Obs}$. Figure \ref{zdep} shows how the
$R^2_{Obs}$ changes with redshift. A template based photo-z code would 
return both a best fit redshift and the rest frame SED, while estimating
$R^2_{Obs}$ require the observed frame SED. Converting between the two
relies on the photo-z. The redshift error therefore enters into the
PSF $R^2$ estimation. Using simulations we found an asymmetric error
progation leading to a significan bias. Further, the bias require having
a complete template library.

One can also predict the effective PSF for galaxies using a machine
learning approach. There are two different approaches for training
the color-$R^2$ relation. The PSF is measurable for stars, which
are pointlike sources. A galaxy and star with the same SED would
have the same PSF. The stars might not cover the full color space
of the galaxies. Training directly on observed galaxies is not possible
since the PSF for galaxies is not directly observed. One can however
construct a training set using a galaxy simulations and assuming
an Euclid PSF wavelength dependence (see Eq. \ref{wavedep}). This
approach is similar to an SED fitting, but the algorithm is faster
and one can directly include effects of e.g. extinction in the 
simulated catalog.


\begin{figure}
\xfigure{prod_psf74.pdf}
\caption{The relative bias for different calibration methods.}
\label{r2train}
\end{figure}

Training on stars would be insensitive to $\gamma$ and
magnitude offsets, since the method maps directly between
observed quantities. For the full sample the $R^2$ bias reach
the requirement, but the $R^2$ is strongly redshift dependent.
Figure \ref{r2train} first shows $R^2$ bias without calibration.
By using a simulated galaxy sample we can remove the redshift
dependence. This however makes the method sensitive to $\gamma$
and magnitude offsets and assumptions of the galaxy sample, like
which SEDs are included. Both training on galaxies or stars using
the r,i,z bands has a bias lower than $3 \times {10}^{-4}$. 
Training on stars has a reduced sensitivity to magnitude offsets
and is therefore preferred. For training on stars the galaxy
sample only affects the calibration step. By comparing the
predicted PSF $R^2$ when training on stars or galaxies one
can detect problems with the galaxy simulations.

In \citeme{Eriksen et.al} the fiducial setup combines Euclid
with DES data. The optimal configuration use the r,i,z bands
to predict the effective PSF size. A missing z-band would
increase the scatter in the $R^2_{\text{Pred}}$, but the
bias will still be within the requirement. 

\end{document}
