#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.colors import ColorConverter

from xdolphin import Job

def plot4(x1='vis', y1='g', ax=False, vis=True):
    print('Plotting:', y1)
#    plt.clf()

    ab_dir = '/data2/photoz/run/data/'
    filter_dir = 'des_euclid'

    abgal = Job('AB')
    abstar = Job('AB')
    abgal.task.config.update({'data_dir': ab_dir,
                              'filter_dir': filter_dir,
                              'sed_dir': 'seds'})

    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    mabs = Job('Mabs')
    galcatJ = Job('MagObs') #GalCat')
    galcatJ.depend('mabs', mabs)
    galcatJ.depend('ab', abgal)

    # Stars
    starcatJ = Job('StarCat')
    starcatJ.depend('ab', abstar)
    starcatJ.task.config['norm_mag'] = 20.


    r2vz = Job('R2vz')
    r2starJ = Job('R2Star')
    r2galJ = Job('R2Gal')

    r2starJ.depend('r2vz', r2vz)
    r2galJ.depend('r2vz', r2vz)
    r2starJ.depend('magcat', starcatJ)
    r2galJ.depend('mabs', mabs)

    r2star = r2starJ['r2eff']


#    x1 = 'vis'
#    y1 = 'g'

    x2 = 'vis'
    y2 = 'z'

    def colors(cat):
        col1 = cat['mag_'+x1] - cat['mag_'+y1]
        col2 = cat['mag_'+x2] - cat['mag_'+y2]

        return col1, col2

#    ipdb.set_trace()
    print('next')
    r2gal = r2galJ['r2eff']
    r2star = r2starJ['r2eff']
    galcat = galcatJ['magcat']
    starcat = starcatJ['magcat']

    r1 = np.random.randint(0, len(r2gal), 1000)
    r2 = np.random.randint(0, len(r2star), 1000)
    print('next 2')

#    ipdb.set_trace()
    f = lambda x,y: ', {0}-{1}'.format(x,y)

    colg1,colg2 = colors(galcat)
    cols1,cols2 = colors(starcat)

    ax.plot(colg1[r1], r2gal['r2eff'][r1], '.', color='orange', label='Galaxy', visible=vis)
    ax.plot(cols1[r2], r2star['r2eff'][r2], 'k.', label='Star', visible=vis)

#    plt.
#    plt.savefig('../../run/r14_scatter_{0}_{1}.pdf'.format(x1, y1))
#    plt.show()

#    for name, magcat, r2cat in [('gal', galcat, r2gal), ('star', r2star, r2star)]:
#        field = 'r2gal' if name == 'gal' else 'r2_true'
#        col1, col2 = colors(magcat)
#
#        plt.plot(r2cat[field], col1, '.', label=name+f(x1, y1))
##        plt.plot(r2cat[field], col2, '.', label=name+f(x2, y2))
#    plt.legend()
##    plt.savefig('../run/r11_r2_color.pdf')
#    plt.show()

F = ['g', 'r', 'i', 'z', 'vis']
F += ['Y', 'J', 'H']

#F = ['Y', 'J', 'H']
#F += ['g', 'r', 'i', 'z', 'vis']

N = len(F)
fig, A = plt.subplots(N-1,N-1, sharex='col', sharey=True)
conv = ColorConverter()
col = conv.to_rgb('#b0e0e6') ##87ceeb') #6495ed')

optical =  ['g', 'r', 'i', 'z', 'vis']

for k1,(f,B) in enumerate(zip(F[1:], A)):
    for k2, (g,ax) in enumerate(zip(F[:-1], B)):
#        ipdb.set_trace()
#        if k1 == 1:
#            plt.xlabel(f)

        if k2 < k1+1:
            plot4(f, g, ax)
        else:
            plot4(f, g, ax, False)
            ax.axis('off')

        if f in optical and g in optical:
            ax.set_axis_bgcolor(col)

        if k1 == N - 2:
            ax.set_xlabel(g)

        if k2 == 0:
            ax.set_ylabel(f)

#'row '+str(k1))
#

ipdb.set_trace()
#
#for k1,B in enumerate(A):
#    for k2,ax in enumerate(B):
##        ax.xaxis.label = str(i)
##        ax.yaxis.label = str(j)
##        ipdb.set_trace()
#        if k1 == N - 2:
#            ax.set_xlabel('col '+str(k2))
#       
#        if k2 == 0:
#            ax.set_ylabel('row '+str(k1))

plt.subplots_adjust(wspace=0., hspace=0.)
plt.show()
