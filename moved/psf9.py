#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pipel as psf_pipel
from scipy.stats import gaussian_kde
from matplotlib import pyplot as plt

pipel = psf_pipel.pipel()
pipel, r2cal = psf_pipel.modify_pipel(pipel, 8)

r2cal.task.config['col_weigth'] = True
#r2cal()

D = r2cal.task.job.kids
nfields = sum(['magcat' in x for x in D.keys()])

cats = []
for i in range(nfields):
    magcatJ = D['magcat'+str(i)]
    store = magcatJ.kids['magcat']['magcat']
    df = store['/cat']
#    cats.append(store['/cat'])
    store.close()

    store = D['r2c'+str(i)]['r2eff']
    df_r2 = store['/r2eff']
    store.close()

    df = df.join(df_r2)
    cats.append(df)

def prepare_kdes(filters):
    kdes = {}
    for f1 in filters:
        for f2 in filters:
            if f1 == f2: continue

            all_col = []
            for i,cat in enumerate(cats):
                col = np.array(cat['mag', f1] - cat['mag', f2])
                all_col.append(col)
                kdes[i,f1,f2] = gaussian_kde(col, 0.2) #factor=0.2)

            kdes['avg', f1, f2] = gaussian_kde(np.hstack(all_col))

    return kdes

filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']

kdes = prepare_kdes(filters)

f1 = 'i'
f2 = 'H'

g2 = kdes[2, f1, f2]
g1 = kdes[1, f1, f2]

a1 = 1
a2 = 1
b1 = 0
b2 = 0

cat1 = cats[1]
r1 = np.array(cat1['r2eff'])
c1 = np.array(cat1['mag', f1] - cat1['mag', f2])

#delta_c = [-0.2, -0.1, 0., 0.1, 0.2]
delta_c = np.linspace(-2., 2., 200)
h1 = g1(c1)
y = []
for dc in delta_c:
    print('dc', dc)
    h2 = g2(c1+dc)
    T = h2 / h1

    y.append((r1*(h2/h1)).sum()/ len(r1))

plt.plot(delta_c, y, 'o-')
plt.show()
ipdb.set_trace()

x = np.linspace(-2., 2.)

