#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

from xdolphin import Job

def set_config(pzcat):

    pzcat.task.config['use_priors'] = False
#    pzcat.task.config['D_tel'] = 50.
    pzcat.task.config['D_tel'] = 10.

    pzcat.task.config.update({\

#   'max_magerr': 0.5,

#   'RN': 5.002,
   'RN': 5.0,
   'RN_space': 0., # Not actually used..

#  'from_space': ['Y', 'J', 'H'],
#  'filters': ['g', 'i', 'r', 'z'],
#  'from_space': ['g', 'i', 'r', 'z', 'Y', 'J', 'H'],
  'filters': ['vis', 'g', 'i', 'r', 'z', 'Y', 'J', 'H'],
  'use_snlim_space': True,
#  'snlim_space': 5.,
  'snlim_space': 5.,
#  'snlim_space_mag': 24.,
  'snlim_space_mag': 24.,

  'n_exp': 10,

  'seds': ['CWW_Ell', 'Ell_09', 'CWW_Sbc', 'CWW_Scd', 'CWW_Irr', 'I99_05Gy'],

  'exp_g': 80,
  'exp_r': 80,
  'exp_i': 100,
  'exp_z': 100,
  'exp_y': 50})


    return pzcat

def fisk():
    r2vz = Job('R2vz')

    # Layer 1
    r2gal = Job('R2Gal')
    r2star = Job('R2Star')
    r2gal.depend('r2vz', r2vz)
    r2star.depend('r2vz', r2vz)

    # Layer 2
#    galcat = Job('GalCat')
    mabs = Job('Mabs')
    galcat = Job('MagObs') #GalCat')
    galcat.depend('mabs', mabs)

    r2train = Job('R2Train')
    r2train.depend('galcat', galcat)
    r2train.depend('r2gal', r2gal)
    r2train.depend('r2star', r2star)

    pzcat = Job('PzCat')
    pzcat.depend('galcat', galcat)

    pzcat = set_config(pzcat)

    return pzcat

def plot_mag():
    pzcat = fisk()
    cat = pzcat['pzcat']

    H,edges = np.histogram(cat['m_0'])
    xm = 0.5*(edges[:-1] + edges[1:])

    plt.plot(xm, H)
    plt.show()
#    ipdb.set_trace()

def plot_odds():
    pzcat = fisk()
    cat = pzcat['pzcat']

    H,edges = np.histogram(cat['odds'], bins=20)
    xm = 0.5*(edges[:-1] + edges[1:])

    plt.plot(xm, H, 'o-')
    plt.show()

def plot1():
    pzcat = fisk()

    cat = pzcat['pzcat']
#    inds = cat['m_0'] < 23.0
    m0 = cat['m_0']

    inds = np.logical_and(22.5 < m0, m0 < 24.5)
    inds = np.logical_and(inds,  0.01 < cat['odds'])

    plt.plot(cat['z_s'][inds], cat['zb'][inds], '.', ms=2.)

    x = np.linspace(0., 1.5)
    plt.plot(x,x)
    plt.xlim(0.05, 1.5)
#    plt.ylim(0.05, 1.5)

    plt.xlabel('zs', size=16)
    plt.ylabel('zb', size=16)
    plt.show()

def plot2():
    pzcat = fisk()
    cat = pzcat['pzcat']
    zs = cat['z_s']
    zb = cat['zb']
    delta = zb - zs

    edges = np.arange(0.2, 1.5, 0.02)

    y = []
    m0 = cat['m_0']
    base = np.logical_and(22.5 < m0, m0 < 24.5)
    base = np.logical_and(base,  0.2 < cat['odds'])

    # These two requirement is probably not requally relevant with priors,
    # since the distribution then appears more well behaved.
    base = np.logical_and(base, 0.05 <= zb)
    base = np.logical_and(base, zb <= 1.5)
    for xa, xb in zip(edges[:-1], edges[1:]):
        inds = np.logical_and(base, xa <= zs)
        inds = np.logical_and(inds, zs <= xb)

        A = delta[inds]
        N = len(A)
        A.sort()
#        sig68 = 0.5*(A[int(0.68*N)] - A[int(0.32*N)])
        sig68 = 0.5*(A[int(0.84*N)] - A[int(0.16*N)])

#        ipdb.set_trace()
#        y.append(sig68)
#        A = delta[inds]
#        ipdb.set_trace()
#np.std(delta[inds]))
        y.append(sig68) #


    xm = 0.5*(edges[:-1] + edges[1:])
#            ipdb.set_trace()

    plt.plot(xm, y / (1. + xm), 'o-')
    plt.show()
#    plt.savefig('../run/r17_sig68.pdf')

plot1()
#plot_mag()
#plot_odds()
