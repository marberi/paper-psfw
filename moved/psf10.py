#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pipel as psf_pipel
from scipy.stats import gaussian_kde
from matplotlib import pyplot as plt

pipel = psf_pipel.pipel()
y = []
#g = [0.5, 0.55, 0.56, 0.6]
#g = np.arange(0.5, 0.6, 0.01)

n = 5
x = 0.005
g = np.arange(0.55-n*x, 0.55+n*x, x)

#ipdb.set_trace()

for gi in g:
    print('GI', gi)
    A = pipel['r2vz_star']
    A.task.config['r2_exp'] = gi

#    ipdb.set_trace()
#r2_exp

    job = pipel['r2star']
    store = job['r2eff']
    r2eff = store['r2eff']
    store.close()

    y.append(r2eff['r2eff'].mean())

plt.plot(g, y, 'o-')
plt.show()
#ipdb.set_trace()
