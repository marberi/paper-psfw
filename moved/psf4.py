#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import numpy as np
from xdolphin import Job

from matplotlib import pyplot as plt

# Reorganizing so multiple projects can use this code.
import pipel
fisk = pipel.pipel

r2train, starcat, pipel = fisk()

filters = ['r', 'i', 'z']

sconf = copy.deepcopy(starcat.task.config)
r2train.task.config['use_stars'] = True
r2train.task.config['filters'] = filters
#'use_stars'] = True

#ofilter = 'i'
#filters = ['vis', 'r', 'i', 'z', 'Y', 'J', 'H']


#filters = [filters[1]]
Os = [0.0001, 0.001, 0.002, 0.005, 0.007, 0.01, 0.02, 0.05, 0.1] #, 0.5]

#Os = [0.01, 0.02]

#Os = [0.0001, 0.001]
#Os = [Os[3]]
#filters = ['i']
#filters = [filters[6]]
def plot1():
    global filters

#    filters = [filters[2]]
    for fname in filters:
        print('Filter:', fname)
        avg = []
        for O in Os:
            starcat.task.config = copy.deepcopy(sconf)
    #    for O in [0.5]:
            starcat.task.config['offset'][fname] = O

            delta_r2 = r2train['r2train'][0]['delta_r2']
    #        ipdb.set_trace()
            avg.append((np.average(delta_r2)))

        plt.plot(Os, np.abs(avg), 'o-', label=fname)

    plt.legend()
    plt.xlabel('Mag offset')
    plt.xscale('log')
    plt.yscale('log')
#    plt.savefig('../run/r21_magoffset.pdf')

    plt.show()

def plot2():
    Ns = [1000, 2000, 3000, 5000, 6000, 7000, 8000, 10000, 15000, 20000]
    Ns = [10, 100, 200, 300, 500, 800]

    Ns = [10, 20, 40, 60, 80, 100, 200]
    Ns = [120, 140, 160, 180]

    Ns = [Ns[3]]
    for use in [True, False]:
        r2train.task.config['use_stars'] = use

        avg = []
        for N in Ns:
            r2train.task.config['Ntrain'] = N
            delta_r2 = r2train['r2train'][0]['delta_r2']

            avg.append((np.average(delta_r2)))

        plt.plot(Ns, np.abs(avg), 'o-', label=use)

    plt.legend()
    plt.show()

def plot3():
    r2train.task.config['use_stars'] = True
    r2train.task.config['Ntrain'] = 2000
    r2train.task.config['Nboot'] = 30

    mS = [0.0,  0.001, 0.002, 0.005, 0.01] #, 0.05, 0.1]
    for moff1 in mS:
        avg = []
        std = []
        for moff2 in mS:
            print('moff1', moff1)

            offD = {'r': moff1, 'i': moff2, 'j': 0}

            starcat.task.config['offset'].update(offD)
            delta_r2 = r2train['r2train'][0]['delta_r2']

            T = []
            for cat in r2train['r2train']:
                T.append(np.average(cat['delta_r2']))

            avg.append(np.average(T))
            std.append(np.std(T))
#            ipdb.set_trace()
#            avg.append(delta_r2)
#            avg.append((np.average(delta_r2)))

        plt.errorbar(mS, avg, std)

    plt.show()

def plot4():
    r2train.task.config['use_stars'] = True
    r2train.task.config['Ntrain'] = 2000
#    r2train.task.config['Nboot'] = 30

    SN = [10, 100]
    SN = np.linspace(5, 100)
    SN = np.arange(1, 40)


    pipel['snoise'].task.config['sn'] = 50

    avg = []
#    for sn in SN:
#        pipel['gnoise'].task.config['sn'] = sn
#        delta_r2 = r2train['r2train'][1]['delta_r2']
#
#        avg.append(np.average(delta_r2))

    for sn in [20, 50,200]:
        pipel['gnoise'].task.config['sn'] = sn
        delta_r2 = r2train['r2train'][0]['delta_r2']
        H, edges = np.histogram(delta_r2, range=(-0.01, 0.01), bins=100)
        xm = 0.5*(edges[:-1] + edges[1:])

        plt.plot(xm, H, 'o-', label=sn)

#    plt.plot(SN, avg)
    plt.legend()
    plt.show()
#        print('bias', sn, np.average(delta_r2))

def plot5():
    A1 = {'wk3iii': 2, 'rk5iii': 36, 'k5iii': 33, 'm5iii': 1, 'm2i': 4, 'g8iii': 1,
    'rk1iii': 1, 'k4v': 1, 'k2i': 1, 'm1iii': 1, 'k4i': 34, 'k1iii': 1, 'rk3iii':
    1, 'wk4iii': 1, 'rg5v': 1, 'm4iii': 1, 'm0iii': 5, 'm2iii': 2, 'k4iii': 2,
    'g8v': 2, 'k7v': 41, 'k1iv': 1, 'wg8iii': 3, 'm0v': 5, 'k3v': 1, 'wk2iii': 3,
    'k3i': 1, 'm2v': 1}

    A2 = {'m0iii': 5, 'm2iii': 2}
    A3 = {'m0iii': 5}
    A3 = {'wk3iii': 5, 'rk5iii': 36}
    A3 = {'k5iii': 33, 'm5iii': 1, 'm2i': 4, 'g8iii': 2}

    Ntrain = [500, 1000, 2000]
    Ntrain = np.arange(100, 4000, 100)
#    pipel['starcatN'].task.config['sed_prob'] = A1

#    Ntrain = [1500]
    Ntrain = np.arange(100, 1000, 20)
    Ntrain = np.arange(10, 100, 10).tolist()
    Ntrain += np.arange(100, 4000, 100).tolist()

    abkids = pipel['abkids']
    starfit = Job('StarFit')
    starfit.depend('ab', abkids)

    starcat = pipel['starcatN'] 
    starcat.depend('sed_prob', starfit)

#    r2train.task.config['Nboot'] = 30
#    starfit.task.config['fieldnr'] = 129

    L = [129, 130, 133]
    L = [238]

    L = ['129', '130', '132', '133', '136', '137', '139', '140', '141', '172', '175', '176', '177', '178', '179', '180', '182', '183', '184', '185', '186', '187', '216', '219', '220', '222', '223', '231', '238'] 

    L = ['129', '130', '132', '133', '136', '137', '139', '140', '141', '172', '175', '176', '177']
    for i, nr in enumerate(L): #[129, 130, 133]): #, 135]):
        print('NRRRRR', nr)
        avg = []
        starfit.task.config['fieldnr'] = nr

#        pipel['starcatN'].task.config['sed_prob'] = A
    
        for N in Ntrain:
            r2train.task.config['Ntrain'] = N
            hmm = []
            for cat in r2train['r2train']:
                hmm.append(np.average(cat['delta_r2']))

    #j        delta_r2 = r2train['r2train'][0]['delta_r2']

            avg.append(np.average(hmm)) #delta_r2))
    #        print(N, np.average(delta_r2))

        plt.plot(Ntrain, np.abs(avg), label=str(i))

    plt.axhline(3e-4)
    plt.xscale('log')
    plt.yscale('log')
#    plt.legend()
    plt.xlabel('Nstars')
    plt.ylabel('Abs (r^2 eff -r^2) / r^2')
    plt.savefig('../run/r24_r2train_kidsstars.pdf')
    plt.show()

    ipdb.set_trace()

def plot6():
    abkids = pipel['abkids']
#    abstar.task.config['filters'] = ['u', 'g', 'r', 'i', 'z']

    starfit = Job('StarFit')
    starfit.depend('ab', abkids)

    starfit.task.config['fieldnr'] = 129
    starcat = pipel['starcatN'] 
    starcat.depend('sed_prob', starfit)
    starcat()

plot5()
