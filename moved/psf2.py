#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from xdolphin import Job
from matplotlib import pyplot as plt

def fisk():
    ab_dir = '/data2/photoz/run/data/'
    filter_dir = 'des_euclid'

    r2vz = Job('R2vz')
    abgal = Job('AB')
    abstar = Job('AB')
    abgal.task.config.update({'data_dir': ab_dir,
                              'filter_dir': filter_dir,
                              'sed_dir': 'seds'}) 

    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'}) 

    # Stars
    starcat = Job('StarCat')
    starcat.depend('ab', abstar)

    # Galaxies.
    mabs = Job('Mabs')
    galcat = Job('MagObs') #GalCat')
    galcat.depend('mabs', mabs)
    galcat.depend('ab', abgal)

    # R2 galaxies.
    r2gal = Job('R2Gal')
    r2gal.depend('r2vz', r2vz)
    r2gal.depend('mabs', mabs)
#    r2gal.depend('magcat', galcat)

    # R2 stars
    r2star = Job('R2Star')
    r2star.depend('r2vz', r2vz)
    r2star.depend('magcat', starcat)

    # Training.
    r2train = Job('R2Train')
    r2train.depend('galcat', galcat)
    r2train.depend('starcat', starcat)
    r2train.depend('r2gal', r2gal)
    r2train.depend('r2star', r2star)

    return r2train

def add_grid(ax):
    ax.xaxis.grid(True, which='major')
    ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    ax.yaxis.grid(True, which='minor')

def test5(ax, filters, bins):
    r2train = fisk()

    for use_star in [False, True]:
        r2train.task.config.update({'use_stars': use_star,
                                    'filters': filters})

        delta_r2 = r2train['r2train'][0]['delta_r2']

        delta_r2L = [x['delta_r2'] for x in r2train['r2train']]
        biasL = map(np.average, delta_r2L)
        print('Bias', np.average(biasL), np.std(biasL))

#        ipdb.set_trace()
        H, edges = np.histogram(delta_r2, bins=bins)
        xm = 0.5*(edges[:-1] + edges[1:])

        label = 'Stars' if use_star else 'Galaxies'
        intg = sum(H)*(np.max(bins) - np.min(bins)) / len(bins)
        ax.plot(xm, H/float(intg), 'o-', label=label)

#        ipdb.set_trace()
        assert not np.isnan(delta_r2).any(), 'Something went wrong.'

def plot2():
    L =  [('All', ['vis', 'r', 'i', 'z', 'Y', 'J', 'H']),
          ('All-vis', ['r', 'i', 'z', 'Y', 'J', 'H'])]
#          ('r,i,z', ['r', 'i', 'z'])]


    fig, A = plt.subplots(len(L), 1, sharex='col', sharey=True)

    #ipdb.set_trace()
    bins = np.linspace(-0.005, 0.005, 40)
    for (name,fs), ax in zip(L,A):
        test5(ax, fs, bins)
        add_grid(ax)
        ax.text(-0.004, 400, name, size=16)
        ax.legend()

    plt.subplots_adjust(wspace=0., hspace=0.)
    plt.savefig('../run/r18_plot2.pdf')

def test():
    ab_dir = '/data2/photoz/run/data/'
    filter_dir = 'des_euclid'

    r2vz = Job('R2vz')
    abgal = Job('AB')
    abstar = Job('AB')
    abgal.task.config.update({'data_dir': ab_dir,
                              'filter_dir': filter_dir,
                              'sed_dir': 'seds'}) 

    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'sed_dir': 'seds_stars'}) 

    starcatN = Job('StarCat')
    starcatN.depend('ab', abstar)
    starcatN.task.config['filters'] = ['g', 'r', 'i']
#    starcat()

    # Stars
#    r2starN = Job('R2Star')
#    r2starN.depend('r2vz', r2vz)
#    r2starN.depend('starcat', starcat)


    magnoise = Job('MagNoise')
    magnoise.depend('magcat', starcatN)

    offset = Job('Offset')
    offset.depend('magcat', magnoise)

    def test_offset():
        for i_off in [0., 1., 2.]:
            offset.task.config['offset']['i'] = i_off
            magcat = offset['magcat']

            H, edges = np.histogram(magcat['mag_i'])
            xm = 0.5*(edges[:-1] + edges[1:])

            plt.plot(xm, H, 'o-', label=str(i_off)) 


        plt.savefig('../run/r19_offset.pdf')
        plt.show()

    def test_sn():
        for SN in [5, 10, 50,100]:
            magnoise.task.config['sn'] = SN
            magcat = offset['magcat']

            H, edges = np.histogram(magcat['mag_i'])
            xm = 0.5*(edges[:-1] + edges[1:])

            plt.plot(xm, H, 'o-', label='SN: '+str(SN))

        plt.legend()
        plt.savefig('../run/r19_sn.pdf')

    def test_corr():
        for same_cat in [True, False]:
            magcat1 = offset['magcat']
            if not same_cat:
                magnoise.task.config['ind'] = 1

            magcat2 = offset['magcat']

            mag1 = magcat1['mag_i']
            mag2 = magcat2['mag_i']
            delta = (mag2 - mag1) / mag1

            print(same_cat, delta)
            ipdb.set_trace()
#        plt.show()

    test_corr()
#    ipdb.set_trace()
#    r2star()

plot2()

#test()
