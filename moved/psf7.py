#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pipel as gp

from xdolphin import Job

class psf7:
    def run(self):
        r2comb_fields = Job('r2comb_fields')
        r2comb = Job('r2comb')
        r2comb.depend('fields', r2comb_fields)

        r2comb()
        ipdb.set_trace() 
