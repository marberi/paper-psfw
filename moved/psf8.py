#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pipel as psf_pipel
from scipy.stats import gaussian_kde
from matplotlib import pyplot as plt

pipel = psf_pipel.pipel()
pipel, r2cal = psf_pipel.modify_pipel(pipel, 8)

r2cal.task.config['col_weigth'] = True
#r2cal()

D = r2cal.task.job.kids
nfields = sum(['magcat' in x for x in D.keys()])

cats = []
for i in range(nfields):
    magcatJ = D['magcat'+str(i)]
    store = magcatJ.kids['magcat']['magcat']
    cats.append(store['/cat'])

#ipdb.set_trace()

def prepare_kdes(filters):
    kdes = {}
    for f1 in filters:
        for f2 in filters:
            if f1 == f2: continue

            all_col = []
            for i,cat in enumerate(cats):
                col = np.array(cat['mag', f1] - cat['mag', f2])
                all_col.append(col)
                kdes[i,f1,f2] = gaussian_kde(col, 0.2) #factor=0.2)

            kdes['avg', f1, f2] = gaussian_kde(np.hstack(all_col))

    return kdes


f1 = 'g'
f2 = 'r'

filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']

kdes = prepare_kdes(filters)

nf = len(filters)
fig, A = plt.subplots(nf, nf)

x = np.linspace(-1., 1., 200)
for i,ax_row in enumerate(A):
    for j,ax in enumerate(ax_row):
        if i <= j: continue

        for l in range(nfields):
            f1 = filters[i]
            f2 = filters[j]

#            if f1 == f2: continue

            y = kdes[l,f1,f2](x)
            ax.plot(x, y, '--')

#        ipdb.set_trace()

plt.show()

#ipdb.set_trace()
#magcat
