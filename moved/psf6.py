#!/usr/bin/env python
# encoding: UTF8

import copy
import ipdb
import numpy as np
from matplotlib import pyplot as plt

#import sys
#sys.path.append('/home/marberi/drv/work/papers/psfw/plots')
import pipel as gp

class psf6:
    def run(self):
        pipel = gp.pipel()

        r2train = pipel['r2train']
        offset = pipel['starcat']


#        sconf = copy.deepcopy(starcat.task.config)

        filters = ['r', 'i', 'z']
        conf = {'use_stars': True, 'filters': filters, 'Ntrain': 2000}

        r2train.task.config.update(conf)

#r2train.task.config['filters'] = filters

        filter_name = 'z'
#        offset.task.config['offset']['i'] = 0.02
#        offset.task.config['offset']['z'] = -0.02
        orig_config = copy.deepcopy(offset.task.config)
        xL = [0.0,0.0001, 0.001, 0.005, 0.01, 0.015, 0.02, 0.03, 0.04, 0.1]
#            xL = [0.05, 0.1]
        xL = [0.00001, 0.000015, 0.00002, 0.000025, 0.00003, 0.00004, 0.00005, 0.0005, 0.001, 0.002, 0.003, 0.004, 0.005]
        y = []
        yerr = []
        for x in xL:
            pipel['r2vz_star'].task.config['r2_exp'] = 0.55+x

            
#            ipdb.set_trace()


#           pipel['r2star']
#           off_conf = copy.deepcopy(orig_config)
#           off_conf['offset'][fname] = x
#
#           offset.task.config = off_conf #['offset'][fname] = x
            store = r2train['r2train']
            cat = store['/r2eff']
            store.close()
#
            y.append(cat.mean().mean())
##                y.append(cat.mean().min())
#                yerr.append(cat.mean().std())
#
        plt.plot(xL, np.abs(y), 'o-') #, label=fname)

        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel('R^2(wavelength) exponent offset')
        plt.ylabel('R^2 bias')
#        plt.yscale('log')
        plt.legend()
        plt.savefig('/home/marberi/drv/work/papers/psfw/run/r27_rexpbias.pdf')
        plt.show()
#        ipdb.set_trace()
