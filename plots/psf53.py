#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

# Weave does not support Python3.x
try:
    from scipy import weave
    from scipy.weave import converters
except ImportError:
    pass

code_chi2_sparse = """

using namespace std;
double tmp, prob;
int indj;
int s,t;

int ngal = Nmag[0];
int ndim = Nmag[1];
//int nneigh = Nchi2[1];

s = 0;
for (int i=0; i<ngal; i++)
{
  for (int j=0; j<nneigh; j++)
  {
    indj = ind(i,j);

    tmp = 0;

    for (int k=0; k<ndim; k++)
    {
      tmp += pow(mag(i,k) - mag(indj,k), 2) / pow(err(i,k), 2);
    }

    xind(s) = i;
    yind(s) = indj;
    chi2_sp(s) = tmp;
    prob = exp(-0.5*tmp);
    prob_sp(s) = prob;
    W_sp(s) = Wextra_sp(i,j)*prob;    

    s += 1;
  }
}

// Normalization of rows. The counters s and t
// is used to directly modify the sparse matrix.

// I think it should be normalized on the columns instead.

for (int j=0; j<nneigh; j++)
{
  tmp = 0;
  for (int i=0; i<ngal; i++)
  {
    s = i*nneigh + j;
    tmp += W_sp(s);
  }

  
  for (int i=0; i<ngal; i++)
  {
    s = i*nneigh + j;
    W_sp(s) /= tmp;
  }

}
"""



def chi2_python(mag, chi2, err):
    ngal, ndim = mag.shape
    for i in range(ngal):
        for j in range(ngal):
            tmp = 0
            for k in range(ndim):
                tmp += (mag[i,k] - mag[j,k])**2 / err[i,k]**2

            chi2[i,j] = tmp

    return chi2

class psf53:
    name = 'psf53'
    config = {}

    def d1(self, B):
        # Test the chi2 calculation.

        ngal = 2000
        nk = 3
        mag = np.random.random((ngal, nk))
        chi2 = np.zeros((ngal,ngal))
        args = ['mag','chi2','err'],

        err = 0.002*np.random.random((ngal, nk))

        t1 = time.time()
        weave.inline(code_add, *args, type_converters=converters.blitz)
        dt1 = time.time() - t1

        print('time c++', dt1)
        t2 = time.time()
        xchi2 = chi2_python(mag, chi2, err)
        dt2 = time.time() - t2

        print('time Python', dt2)

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def test_dist(self, mag_gal, dist, ind):
        ipdb.set_trace()

    def d2(self, B):
        key = 'noisy'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
        reg = svm.NuSVR()
        reg.fit(Astar[inds], r2star[inds,0])

        # The unweighted predictions.
        r2pred = reg.predict(col_gal)


        if False:
            # Testing performance.
            for nneigh in [2, 10, 200]:
                reg_dens = neighbors.NearestNeighbors(n_neighbors=nneigh, metric='l2')
                t1 = time.time()
                reg_dens.fit(mag_gal)
                t2 = time.time()
                dist, ind = reg_dens.kneighbors(mag_gal)
                t3 = time.time()

                n = nneigh
                print('time fit ({0})'.format(n), t2-t1)
                print('time predict ({0})'.format(n), t3-t2)

            import sys
            sys.exit()


        nneigh = 40
        reg_dens = neighbors.NearestNeighbors(n_neighbors=nneigh, metric='l2')
        reg_dens.fit(mag_gal)
        dist, ind = reg_dens.kneighbors(mag_gal)

#        self.test_dist(mag_gal, dist, ind)

        # Weight based on redshift. New version.
        # - Currently there is uniform weight!
        ngal = len(mag_gal)


        z_diff = (cat.zs.values[ind].T - cat.zs.values).T
        z_inds = (np.abs(z_diff) < 0.01)
        Wextra_sp = (1*z_inds).astype(np.float)

#        Wextra_sp = np.ones((ngal, nneigh)) # Without weighting..

#        ipdb.set_trace()

        # Weight based on the observed magnitude probabilities.
        ### Testing a sparse version.
#        chi2 = np.zeros((ngal,nneigh))
        args = ['mag','chi2','err', 'ind'],

        Nentries = ngal*nneigh
        chi2_sp = np.zeros(Nentries)
        prob_sp = np.zeros_like(chi2_sp)
        W_sp = np.zeros_like(chi2_sp)
        row_sum = np.zeros(ngal)
        xind = np.zeros(Nentries, dtype=np.int)
        yind = np.zeros(Nentries, dtype=np.int)

        mag = mag_gal
        err = err_gal

        args = ['mag', 'chi2_sp', 'prob_sp', 'Wextra_sp', 'W_sp', 'err', 'ind', 'xind', 'yind', 'nneigh'],
        weave.inline(code_chi2_sparse, *args, type_converters=converters.blitz)

        import scipy.sparse
        import scipy.sparse.linalg

#        prob_sp = np.exp(-0.5*chi2_sp)
        W = scipy.sparse.csc_matrix((W_sp, (xind, yind)), shape=(ngal, ngal))

        # Trying to estimate a "R^2_{\\rm{PSF}}" pdf of each galaxy.

#        t1 = time.time()
#        r2pred_w = scipy.sparse.linalg.spsolve(W, r2pred)
#        t2 = time.time()

        # Another try.
        r2pred_w = W.dot(r2pred) #true)
#        r2pred_w = W.dot(r2pred_w) #true)
#        r2pred_w = W.dot(r2pred_w) #true)


#        ipdb.set_trace()

#        print('time sparse solving', t2-t1)


        # Temporary commenting out the redshift correction....
        if False:
            z_diff = (cat.zs.values[ind].T - cat.zs.values).T
            z_inds = (np.abs(z_diff) < 0.1) #& (1.5 < cat.zs)
            W = z_inds*prob

        # Debugging why the redshift weighting does not work..
        if False:
            S = pd.Series(z_inds.sum(axis=1))
            S.hist(bins=100)
            plt.show()
            ipdb.set_trace()


#        r2pred_w = (r2pred[ind]*W).sum(axis=1) / W.sum(axis=1)
        r2eff_w = (r2pred_w - r2true) / r2true

        cat['r2pred_w'] = r2pred_w
        cat['r2eff_w'] = r2eff_w

        cat['r2pred'] = r2pred
        cat['r2true'] = r2true
        cat['r2eff'] = (r2pred - r2true) / r2true

        store = pd.HDFStore('/data2/marberi/tmp/vol_free.h5', 'w')
        store['cat'] = cat
        store.close()

        import sys
        sys.exit(1)

        ipdb.set_trace()


    def run(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 50000


            B[lbl] = A


        self.d2(B)

if __name__ == '__main__':
    Job('psf53').run()
