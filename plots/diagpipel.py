#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import time 
from xdolphin import Job

def pipel(config):
    assert NotImplementedError('Move to function arguments.')
    ab_dir = '/data2/photoz/run/data/'

    data_dir = '/data2/photoz/run/data/'
    filter_dir = '/data2/photoz/run/data/des_euclid'

    abstar = Job('ab')
    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abkids = Job('ab')
    abkids.task.config.update({'data_dir': ab_dir,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    # Stars
    X = 100 



    pipel = Job('empty')
    toadd = []

    fieldnr = config['field_nr'] if config['star_fit'] else [0]
    star_catsN = []
    star_cats = []

    if config['star_fit']:
        for i,nr in enumerate(fieldnr):
            starcatN = Job('starcat')
            starfit = Job('starfit')
            starfit.config['fieldnr'] = nr

            starfit.depend['ab'] = abkids
            starcatN.depend['ab'] = abstar
            starcatN.depend['sed_prob'] = starfit


            star_noise = Job('magnoise')
            star_noise.depend['magcat'] = starcatN
            star_noise.config['sn_val'] = X
#            star_noise.config['sn_cap'] = X

            starcat = Job('offset')
            starcat.depend['magcat'] = star_noise

            star_catsN.append(starcatN)
            star_cats.append(starcat)
    else:
        starcatN = Job('starcat')
        starcatN.depend['ab'] = abstar

        star_noise = Job('magnoise')
        star_noise.depend['magcat'] = starcatN

        star_noise.config['sn_val'] = X
#        star_noise.config['sn_cap'] = X

        starcat = Job('offset')
        starcat.depend['magcat'] = star_noise

        star_catsN = [starcatN]
        star_cats = [starcat]


    for i,exp in enumerate(config['exp_vals']):
        r2vz_star = Job('r2vz')
        r2vz_star.config.update({\
          'data_dir': data_dir,
          'filter_dir': filter_dir,
          'r2_exp': exp})

        for j,nr in enumerate(fieldnr):
            starcatN = star_catsN[j]
            starcat = star_cats[j]

            r2star = Job('r2star')
            r2star.depend['r2vz'] = r2vz_star
            r2star.depend['mabs'] = starcatN
            r2star.depend['magcat'] = starcat

            r2diag = Job('r2diag')
            r2diag.depend['train_cat'] = starcatN
            r2diag.depend['train_r2'] = r2star

            r2diag.config['Nboot'] = 1
            r2diag.config['Ntrain'] = 2000
            r2diag.config['filters'] = config['filters'] #['i', 'z'] #, 'z']

            pipel.depend[i,j] = r2diag


    return pipel


