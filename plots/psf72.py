#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def add_task(self, pipel):

        r2train = pipel.r2train

        nstar = xd.Job('nstar')
        nstar.depend['galcat'] = r2train.depend['galcat']
        nstar.depend['starcat'] = r2train.depend['train_star_cat']

        pipel.depend['nstar'] = nstar

        return pipel


    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()
        pipel = self.add_task(pipel)

        pipel.config.update({
          'r2train.Ntrain': 400,
          'r2train.use_stars': True,
          'r2train.filters': ['r','i','z']})

        return pipel

    def prepare(self, pipel):
        gc = pipel.r2train.galcat.result
        gc = gc.join(pipel.r2train.r2gal.result)
        gc = gc.join(pipel.nstar.result) 
        store = pipel.r2train.get_store()
        r2eff = store['r2eff']
        store.close()
        gc['r2bias'] = r2eff[0] # Bit of a hack.


        field = 'coldiff_r_i'
        medges = np.linspace(-0.5, 0.5, 10)
        zedges = np.linspace(0.1, 1.8, 7)

        gr1 = pd.cut(gc[field], medges)
        gr2 = pd.cut(gc.zs, zedges)

        prepare = pd.DataFrame()
        for key, val in gc.groupby([gr1, gr2]):
            S = pd.Series()
            S['rbin'] = key[0]
            S['zbin'] = key[1]
            S['r'] = val.r2eff.mean()
            S['r2bias'] = val.r2bias.mean()
            S['coldiff_r_i'] = val['coldiff_r_i'].mean()
            S['coldiff_i_z'] = val['coldiff_i_z'].mean()

            prepare = prepare.append(S, ignore_index=True)

        return prepare, gc

    def plot(self, prepare, gc):

        field = 'coldiff_r_i'
        medges = np.linspace(-0.5, 0.5, 10)
        mean_all = gc.groupby(pd.cut(gc[field], medges)).mean()
        for key, val in prepare.groupby('zbin'):
            if key == 'all':
                continue

            plt.plot(val[field], val.r2bias, 'o-', label=key)

        plt.plot(mean_all[field], mean_all.r2bias, 'o-', lw=2., label='All')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('Galaxy-star col distance (r-i)', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend()
        plt.show()

    def run(self):
        pipel = self.topnode()
        X = self.prepare(pipel)
        self.plot(*X)

if __name__ == '__main__':
    Plot().run()
