#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time

import numpy as np
from scipy import weave
from scipy.weave import converters

code_chi2 = """
  using namespace std;
  cout << "Hello";

  int ngal = Nchi2[0];
  int nsed = Nchi2[1];
  int nf = Ncol[1];
  float nobs;

  float chi_f = 0;
  int x,y,i;

  cout << "ngal " << ngal << endl;
  cout << "nsed " << nsed << endl;

  for (x=0; x < ngal; x++)
  {
    for (y=0; y < nsed; y++)
    {
      // Calculating the reduced chi2.
      nobs = 0.;
      chi_f = 0.;
      for (i=0; i < nf; i++)
      {
        chi_f = chi_f + H(x,i)*pow(col(x, i)-col_model(y,i), 2);
        nobs = nobs + mask(x, i);
      }

      chi2(x,y) = chi_f / nobs;
    }
  }
"""
from numba import cuda,jit

np.seterr(divide='ignore')

#@cuda.jit
@jit
def calc_python(chi2, col_model, mask, col, H):
    ngal,nsed  = chi2.shape
    nf = col.shape[1]

    for x in range(ngal):
        for y in range(nsed):
            nobs = 0.
            chi_f = 0.

            for i in range(nf):
                if not mask[x,i]:
                    continue

                chi_f = chi_f + H[x,i]*pow(col[x, i]-col_model[y,i], 2) 
                nobs = nobs + 1.


            if not nobs:
                continue

            chi2[x,y] = chi_f / nobs

    return chi2

def test(col_model, col, col_var):

    ngal = col.shape[0]
    nsed = col_model.shape[0]

    chi2 = np.zeros((ngal, nsed))
    mask = 1.*np.logical_not(np.isnan(col))
    H = 1./col_var


    t1 = time.time()
    chi2_python = calc_python(chi2, col_model, mask, col, H)
    t2 = time.time()

    print('time', t2-t1)

#    import sys
#    sys.exit(1)


    args = [['chi2', 'col_model', 'mask', 'col', 'H']]

    print('test')
    t1 = time.time()
    weave.inline(code_chi2, *args, type_converters=converters.blitz)
    t2 = time.time()
    print('time', t2-t1)

    ipdb.set_trace()
    print('finished')
