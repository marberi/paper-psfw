#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import copy
import sys
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

class Plot:
    """Study how the PSF recovery and photo-z reacts to the noise."""

    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()

        pipel.config.update({\
          'r2train.Ntrain': 400,
          'r2train.Nboot': 400,
          'r2train.filters': ['r','i','z']})

        topnode = xd.Topnode(pipel)

#        R = [0.5, 1, 1.2, 1.5, 1.7, 2, 3, 4, 5, 6, 7, 8, 9]
        R = [0.5, 1, 1.2, 1.5,  2, 4, 8]
#        R = [1,9]

        layer1 = [{'r2train.galcat.r': x, \
                   'r2train.train_star_cat.magcat.r': x,
                   'pzcat.galcat.r': x} for x in R]

        layer2 = [{'r2train.use_stars': True, 'r2train.Ntrain': 400}, \
                  {'r2train.use_stars': False, 'r2train.Ntrain': 1500}]


        topnode.add_layer(layer1)
        topnode.add_layer(layer2) 

        return topnode

    def prepare(self, pipel):
        cat = pipel.r2train.galcat.result[['mag_vis']]
        cat = cat.join(pipel.r2train.get_store()['r2eff'])
        cat = cat.join(pipel.pzcat.result)
        cat['dx'] = (cat.zb - cat.zs) / (1+cat.zs)

        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2train.config['Nboot']
        S = pd.Series()
#        S['r2bias'] = cat[range(Nboot)].mean().abs().describe(percentiles=[0.68])['68%']
        S['r2bias'] = cat[range(Nboot)].mean().mean()

#        S['r2bias'] = cat[range(Nboot)].mean().abs().describe(percentiles=[0.68])['68%']
#        ipdb.set_trace()

        S['sig68'] = cat.dx.abs().describe(percentiles=[0.68])['68%']

        return S

    def run(self):
        topnode = self.topnode()

        M = {'r': 'r2train.galcat.r', 'use_stars': 'r2train.use_stars'}
        prepare = topnode.get_prepare(self.prepare)
        prepare = prepare.join(topnode.toS(M))

#        prepare = prepare.sort('r')

        for key,line in prepare.groupby('use_stars'):
            line = line.sort('r')

            lbl = 'Stars' if line.use_stars.irow(0) else 'SED train'
            plt.plot(line.r2bias, line.sig68, 'o-', label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        ax.axvline(-3e-4, ls='--', lw=2, color='k', alpha=0.5)
        ax.axvline(3e-4, ls='--', lw=2, color='k', alpha=0.5)
        ax.set_xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        ax.set_ylabel('$\sigma_{68}$', size=16)

        plt.xlim(-0.001, 0.001)
        plt.legend()
        plt.show()

    def fast(self, topnode):
        topnode.update_config({\
          'r2train.filters': ['r', 'i', 'z'], 
          'r2train.Ntrain': 4,
          'r2train.Nboot': 4})

        topnode.update_config({
          'pzcat.galcat.magcat.mabs.Ngal': 100
        })

        return topnode

if __name__ == '__main__':
    Plot().run()
