#!/usr/bin/env python
# encoding: UTF8

import ipdb
import xdolphin as xd

def build(nodes, dep, root):

    # Create the jobs
    jobD = {}
    stack = [root]
    while True:
        jobid = stack.pop()
        job = xd.Job(nodes.ix[jobid, 'name'])
        job.config.update(nodes.ix[jobid, 'config'])

        jobD[jobid] = job
        stack.extend(dep.to[dep['from'] == jobid].tolist())
 
        if not len(stack): break

    # Add the dependencies.
    for i, row in dep.iterrows(): 
        if not row['from'] in jobD:
            continue

        job1 = jobD[row['from']]
        job2 = jobD[row['to']]

        job1.depend[row['dep_name']] = job2

    return jobD[root]
