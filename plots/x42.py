#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job


class Plot:
    """psf13: Varying the training sample for different noise configurations."""

    def topnode(self):
        import trainpipel

        topnode = xd.Topnode()
        topnode['noisy',] = trainpipel.pipel(add_noise=True)
        
        topnode.update_config({\
          'r2x.Nboot': 100,
          'r2x.all_test': True,
          'r2x.filters': ['r', 'i', 'z']})

        #topnode.add_layer_key('r2train.Ntrain', [100, 500, 700, 1000, 2000, 3000])
        #topnode.add_layer_key('r2train.Ntrain', [100, 200, 300, 400, 500])
        topnode.add_layer_key('r2train.Ntrain', [150, 250, 350, 450, 550])

        #Ntrain = [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 700, 1000, 2000, 3000]
        Ntrain = [200, 250, 300, 350, 400, 450, 500, 550, 700, 1000, 2000]

#        Ntrain = [200, 500, 1000, 2000]
#        Ntrain = [200]
#        Ntrain = [400, 450, 500, 550, 700, 1000, 2000]
        topnode.add_layer_key('r2x.Ntrain', Ntrain)

        topnode.add_layer_key('r2x.use_stars', [True])

        return topnode

    def prepare(self, pipel):
        cat = pipel.r2x.test_cat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2x.config['Nboot']

        S = pd.Series()
        S['r2bias'] = cat[range(Nboot)].mean().mean()

#        ipdb.set_trace()        

        return S

    def make_hist(self, pipel):
        cat = pipel.r2x.galcat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        ipdb.set_trace()

    def run(self):
        topnode = self.topnode()


        M = {'Ntrain': 'r2x.Ntrain',
             'use_stars': 'r2x.use_stars'}

        S = topnode.toS(M)
        prepare = topnode.get_prepare(self.prepare)
        T = S.join(prepare)

        for key,line in T.groupby('ind0'):
            line = line.sort('Ntrain')
            lbl_star = 'Stars' if line.irow(0).use_stars else 'Galaxies'
#            lbl = '{0}, {1}'.format(lbl_star, line.ind2.irow(0)) #.capitalize())

            plt.plot(line.Ntrain, line.r2bias, 'o-', label=lbl_star)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('Number of training objects', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend()
        plt.show()


if __name__ == '__main__':
    Plot().run()
