#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
import time
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

def rem_pz_dep(job):
    # One actually have to the for use_cal in this way.
    right_task = job.name in ['r2train', 'r2x', 'r2next', 'r2t']
    use_cal = ('use_cal' in job.task.config) and job.config['use_cal']

    if not (right_task and not use_cal):
        return 

    print('deleting pz tasks')
    for key in ['test_pz', 'cal_pz']:
        if key in job.depend:
            del job.depend[key]


class Plot:
    """Main table with the PSF bias when training on galaxies and stars
       for different filter configurations. Previously in psf10 and psf12.
    """


    all_filters = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

    def _rows(self):
        I1 = [['r', 'i', 'z'],
              ['r', 'i'],
              ['i', 'z']]


        I2 = [['vis', 'r', 'i', 'z'],
              ['vis', 'r', 'i'],
              ['vis', 'i', 'z']]

#        I2 = [['vis', 'i', 'z'],
#              ['vis', 'r', 'i', 'z'],
#              ['vis', 'r', 'i']
#             ]

        I3 = [['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i', 'z',        'Y', 'J', 'H'],
              ['g',      'i', 'z', 'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i',      'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i',             'Y', 'J', 'H']
             ]

        I4 = [['g', 'r', 'i'],
              ['g', 'r', 'i', 'z']]

        I4 = [['r', 'i', 'z', 'Y'],
              ['r', 'i', 'z', 'J'],
              ['r', 'i', 'z', 'H']]

        I = I1 + I2 + I3

#        I = [I[0]] # For testing.
#        I = I3
        I = I1 + I2 + I3
#        I = I3

        # For quick testing..
        I = [I[0]]

        return I

    def topnode(self):

        import trainpipel
        pipel = trainpipel.pipel()

        # Running with higher resolution.
        pipel.r2x.test_cat.magcat.mabs.config['Ngal'] = int(1e7)


        pipel.config.update({\
          'r2x.use_cal': False,
          'r2x.Ntrain': 1000,
          'r2x.Nboot': 100,
          'r2x.all_test': True,
          'r2x.test_cat.const_sn': True,
          'r2x.test_cat.sn': 1000000,
          'r2x.train_star_cat.magcat.const_sn': True,
          'r2x.train_star_cat.magcat.sn': 1000000})


        layers = xd.Layers()
        layers.add_pipeline(pipel)

#        top = xd.Topnode(pipel)


        filters = self._rows()
        labels = range(len(filters))
        layers.add_layer_key('filters', 'r2x.filters', filters, labels=labels)

        layer = [{'r2x.use_stars': False, 'r2x.Ntrain': 5000}, \
                 {'r2x.use_stars': True, 'r2x.Ntrain': 400}]

        layers.add_layer('stars', layer, labels=['gal', 'star'])

        noise = {
          'r2x.test_cat.const_sn': False,
          'r2x.train_star_cat.magcat.const_sn': False}
#          'r2x.train_star_cat.magcat.magcat.norm_mag': 10}
#        ipdb.set_trace()

        layers.add_layer('noise', [{}, noise], labels=['noise', 'nn'])

        top = layers.to_topnode()

        top.rec_apply(rem_pz_dep)

#        ipdb.set_trace()

        return top

    def prepare_f(self, pipel):
        print('Staring prepare')
        tbl_data = pd.DataFrame()
        levels = ['filters', 'stars']
        perc = [.68, .95]



        mag_cat = pipel.r2x.test_cat.result[['mag_vis', 'zs']]
        r2eff = pipel.r2x.get_store()['r2eff'] 

        N = r2eff.shape[1]
        cat = r2eff.join(mag_cat)
        cat = cat[cat.mag_vis < 24.5]

        X = cat[range(N)].mean().abs()
        S = X.describe(percentiles=perc)

        # Should be moved.
        S['use_stars'] = pipel.r2x.config['use_stars']
        S['filters'] = pipel.r2x.config['filters']
        S['has_noise'] = not pipel.config['r2x.test_cat.const_sn']

        print('Finish prepare')
        return S


    def row_name(self, filters):
        """Because one need special configuration for the filters."""

        fspl = filters
        if len(fspl) <= 4:
            return ','.join(filters)

        diff = list(set(self.all_filters) - set(fspl))
        if diff:
            return 'All - {0}'.format(','.join(diff))
        else:
            return 'All'

    def create_tbl(self, prepare):
        prepare['filters'] = map(self.row_name, prepare['filters'])
        prepare['filter_ind'] = [x[-1] for x in prepare.index]

        tbl = pd.DataFrame()

        prepare = prepare.sort('filter_ind')
        for key, sub in prepare.groupby('filter_ind'): 
            row = pd.Series()
            row['Filters'] = sub.irow(0).filters
            for lbl, noise, stars in [('Gal', False, False), ('Star', False, True),
                                      ('Gal-noisy', True, False), ('Star-noisy', True, True)]:

                A = sub[(sub.has_noise == noise) & (sub['use_stars'] == stars)]

                # This is actually not used anywhere.           
                assert len(A) == 1
                row[lbl] = A.irow(0)['mean']

            row.name = key 
            tbl = tbl.append(row)

        return tbl


    def run(self):

        topnode = self.topnode()
        prepare = topnode.reduce(self.prepare_f)
        tbl = self.create_tbl(prepare)

        latex = tbl.to_latex(float_format='{0:.1e}'.format, index=False)

        print(latex)

if __name__ == '__main__':
    Plot().run()
