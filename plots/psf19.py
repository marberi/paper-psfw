#!/usr/bin/env python
# encoding: UTF8

import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from xdolphin import Job

import diagpipel

class Plot:
    """First PSF linear fit plot."""

    field_nr = ['129', '130', '132', '133', '136', '137', '139', '140', '141', '172', '175', '176', '177']

    field_nr = ['129']
    expL = 0.55*(1+np.arange(-0.01, 0.01, 0.002))

    # Only for testing..
    expL = expL[::4]
    expL = np.array([ 0.5445,  0.5489,  0.55, 0.5533])
#    expL = [0.55, 0.551]
    def get_part(self, filters):
        topol = {'exp_vals': self.expL, 
                 'field_nr': self.field_nr,
                 'star_fit': True,
                 'filters': filters}

        pipel = diagpipel.pipel(topol)

#        ipdb.set_trace9
        df = pd.DataFrame()
        for i,exp in enumerate(self.expL):
            for j,nr in enumerate(topol['field_nr']):
                df_part = pipel.depend[(i,j)].result

                df_part['exp'] = exp
                df_part['nr'] = nr
                df = df.append(df_part)

        return df

    def get_data(self):
        field_path = '/data2/data/kids/kidscat/field_list'
        field_nr = map(str.strip, open(field_path).readlines())

        self.field_nr = field_nr[:100]
#        ipdb.set_trace()


        fL = [['r', 'i'], ['z', 'r'], ['i','z']]

        df = pd.DataFrame()
        for filters in fL:
            df_filter = self.get_part(filters)
            df_filter['filters'] = ','.join(filters)
            df = df.append(df_filter)

        return df



    def _gamma_deriv(self, df):
        # Test using more filters
        bmean_cf = df[df.exp==0.55].b.mean()
        bmean_c = df[df.exp == 0.55].groupby('nr').mean().b

        # Needing a different definition.
        dfc = df[df.exp==0.55]
        dfc['div'] = dfc.b / dfc.a0

        xmean_cf = (dfc.b / dfc.a0).mean()
        xmean_c = dfc.groupby('nr').div.mean()

        S = pd.DataFrame()
        for (nr,fname), val in df.groupby(['nr', 'filters']):
            x0 = 0.55
            x1 = 0.551

            p0 = val[val.exp == x0].mean()
            p1 = val[val.exp == x1].mean()

            der = (p1 - p0)/ (x1 - x0)
            der['nr'] = nr
            der['filters'] = fname

            der['a_val'] = p0['a0']
            der['b_val'] = p0['b']
            der['xmean'] = xmean_c[nr]
#            ipdb.set_trace()

            S = S.append(der, ignore_index=True)

        return S

    def _measure_gamma(self, S):

        gamma = np.linspace(-0.1, 0.1,200)
        gamma = np.linspace(-0.02, 0.02, 1000) #200)
        filters = ['i,z', 'z,r', 'r,i']

        dg = pd.DataFrame()
        for nr, val in S.groupby('nr'):
            F = 0
            for fname in filters:
                X = val[val.filters == fname].irow(0)
#                ipdb.set_trace()
#                F += (X.b_val[0] + gamma*X.b[0]) / (X.a_val[0] + gamma*X.a0[0])
                F += (X.b_val + gamma*X.b) / (X.a_val + gamma*X.a0)


            y = X.xmean - F
            imin = np.abs(y).argmin()
            dg = dg.append(pd.Series({'nr': nr, 'dg': gamma[imin]}), ignore_index=True)

        return dg 

    def run(self):

        df = self.get_data()


        # Choosed to make the fitting for one color at the time.
        ncol = 1
        keys = ['b'] + ['a'+str(i) for i in range(ncol)]
        keys2 = keys+['exp']


        fL = [['r', 'i'], ['z', 'r'], ['i','z']]
        fig,axL = plt.subplots(1,len(fL), sharey=True)
        for i,(filters,ax) in enumerate(zip(fL, axL)):
            fname = ','.join(filters)
            val = df[df.filters == fname]
            val[keys2] /= val[val.exp==0.55][keys2].mean()
        
            mean = val.groupby('exp').mean()
            std = val.groupby('exp').std()

            for j,key in enumerate(keys):
                dx = 0.001 if key == 'b' else 0
                dx = j*0.0002
                ax.errorbar(dx+self.expL, mean[key], yerr=std[key], fmt='o-', label=key)

            ax.set_xlabel('$\gamma$ / 0.55')
            if not i:
                ax.set_ylabel('Coefficient / Coefficient($\gamma$ = 0.55)')
            else:
                ax.yaxis.set_visible(False)

            ax.text(0.545, 1.05, fname, size=16)
#        ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

        plt.subplots_adjust(wspace = 0, hspace=0.)
        plt.suptitle('Linear PSF/color fit coefficients.')
        plt.legend()
        plt.show()


if __name__ == '__main__':
    Plot().run()
