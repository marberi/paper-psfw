#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sympy as sp

# A bit clumsy...
K,gamma = sp.symbols(['K', 'gamma'])
b1,S1,a1,T1 = sp.symbols(['b1', 'S1', 'a1', 'S1'])
b2,S2,a2,T2 = sp.symbols(['b2', 'S2', 'a2', 'S2'])
b3,S3,a3,T3 = sp.symbols(['b3', 'S3', 'a3', 'S3'])

f = K
f -= (b1 + gamma*S1) / (a1 + gamma*T1)
f -= (b2 + gamma*S2) / (a2 + gamma*T2)
f -= (b3 + gamma*S3) / (a3 + gamma*T3)

S = sp.solvers.solve(f, gamma)

ipdb.set_trace()


