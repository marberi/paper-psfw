#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd

class Plot:

    def run(self):
        import euclid_setup

        conf = euclid_setup.config()
        filters = ['vis', 'g', 'r', 'i', 'z', 'Y', 'J', 'H']

        tbl = pd.DataFrame()
        for lbl, key in [('Galaxies', 'lim_ext'), ('Stars', 'lim_point')]:
            D = conf[key]
            tbl[lbl] = pd.Series(dict([(f, D[f]) for f in filters]))

        tbl = tbl.T
        latex = tbl.to_latex(float_format='{0:.1f}'.format, index=True, columns=filters)
        print(latex)


if __name__ == '__main__':
    Plot().run()
