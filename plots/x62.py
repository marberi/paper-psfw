#!/usr/bin/env python
# encoding: UTF8

import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import psf9
inst = psf9.Plot()
pipel = inst.topnode()

def bootstrap(S, gr):
    """Straight forward fuction for estimating the bootstrap error
       of the mean.
    """

    # Unfortunately this fuctionality was not directly implemented
    # in Pandas.
    Nboot = 100

    mean = S.groupby(gr).mean()
    std = pd.Series()
    for key, val in S.groupby(gr):
        if not len(val):
            std[key] = np.nan
            continue

        # This part is not exactly elegant, but should be straight
        # forward.
        r = np.random.randint(0, len(val), size=(Nboot, len(val)))
        tmp = pd.Series()
        for i,row in enumerate(r):
            mean_sample = val.ix[val.index.values[row]].mean()
            tmp[str(i)] = mean_sample

        std[key] = np.sqrt(tmp.var())

    return mean, std


#cat = pipel.r2pz.r2gal.result
cat = pipel.r2pz.result
cat = cat.join(pipel.r2pz.pzcat.galcat.result)

z = np.linspace(0.1, 2.0, 30)
gr = pd.cut(cat.zs, z)

# Testing the functionality.
mean, std = bootstrap(cat.r2peak, gr)
zs = cat.zs.groupby(gr).mean()
plt.errorbar(zs, mean, std)
plt.savefig('test.pdf')
#ipdb.set_trace()
