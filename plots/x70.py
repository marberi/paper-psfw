#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

import tosave

class Plot:
    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()
        pipel.config.update({
          'r2x.use_cal': False,
          'r2x.filters': ['r', 'i', 'z'],
        #  'r2x.filters': ['r', 'i'],
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
          'r2x.Nboot': 10,
          'r2x.use_stars': False})

#        ipdb.set_trace()

        return pipel

    def plot(self, pipel):
        # Test:
#        pipel.r2pz.pzcat.config['filters'] = ['r', 'i', 'z']
#        pipel.r2pz.pzcat.config['use_priors'] = False 
        pipel.config.update({
          'r2x.Ntrain': 1000,
          'r2x.filters': ['g', 'r', 'i', 'z', 'Y', 'J', 'H']})

        pipel.config.update({
          'r2pz.pzcat.out_pdf': True,
          'r2pz.pzcat.out_pdftype': True})


        r2pz_cat = pipel.r2pz.pzcat.galcat.result
        r2pz_cat = r2pz_cat.join(pipel.r2pz.result)
        r2pz_cat = r2pz_cat[r2pz_cat.mag_vis < 24.5]

        r2x_cat = pipel.r2x.get_store()['r2eff']
        r2x_cat = r2x_cat.join(pipel.r2x.test_cat.result)
        r2x_cat = r2x_cat[r2x_cat.mag_vis < 24.5]


        vpz = r2pz_cat.r2peak.mean()
        vtrain = r2x_cat[0].mean()
        plt.axvline(vpz, label='pz', lw=0.2)
        plt.axvline(vtrain, color='black', label='train', lw=0.2)

        print('pz', vpz)
        print('train', vtrain)

        bins = np.arange(-0.02, 0.02, 0.001)
        bins = np.linspace(-0.02, 0.02, 101) #0.001)
        K = {'histtype': 'step', 'normed': True, 'bins': bins} #100}
        r2x_cat[0].hist(label='Gal train',  ls='solid', lw=1.5, **K)
        r2pz_cat.r2peak.hist(label='SED fit', ls='dashed', lw=2., **K)

        ipdb.set_trace()

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16)
        plt.legend()

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='k', alpha=0.1)

        xlim = 0.01
        plt.xlim((-xlim, xlim))
#        plt.savefig(tosave.path('test.px700psf80'))
        plt.savefig('test.pdf')

#        ipdb.set_trace()

    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
