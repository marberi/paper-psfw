#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job, Topnode

import trainpipel

def swap_ab_job(pipel, L):
    for x in L:
        node = pipel.depend[x]
        old_ab = node.depend['ab']
        new_ab = Job('ab_nb')

        new_ab.depend['ab'] = old_ab
        node.depend['ab'] = new_ab

        for f in ['data_dir', 'sed_dir', 'filter_dir']:
            new_ab.config[f] = old_ab.config[f]

def N_layers(L):
    layers = []
    for N in [0, 1,2,3, 4, 5, 10, 20]:
        layer = {}
        layer[L[0]+'.ab.N'] = N
        layer[L[1]+'.ab.N'] = N

        riz = ['r', 'i', 'z'] 
        nb = map(str, range(N))
        layer['r2train.filters'] = riz + nb

        for x in ['galcatN', 'starcatN']:
            layer[x+'.filters'] = riz + ['vis'] + nb
        layers.append(layer)

    return layers

def topnode():
    L = ['galcat.magcat', 'starcatN']
    pipel = trainpipel.pipel()
    swap_ab_job(pipel, L)

    pipel.config.update({'galcat.const_sn': True, 
                         'starnoise.const_sn': True})

    pipel.config['r2train.all_test'] = True
    pipel.depend['r2train'].config.update({'Ntrain': 1000, 'use_stars':  True})

    pipel.config['galcatN.mabs.Ngal'] = 1e6

    xtop = Topnode(pipel)
    xtop.add_layer(N_layers(L))

    return xtop

xtop = topnode()

def d1(self):
    for key,node in xtop.nodes.iteritems():
        N = node.config['starcatN.ab.N']

        if not N in [0, 3, 5, 10, 20]: continue
        print('Running:', N)

    #    ipdb.set_trace()
        r2eff = node.depend['r2train']['r2train']['r2eff']
        r2eff[0].hist(bins=60, histtype='step', normed=True, label=str(N)) #, label='N = {0}'.format(N))


    plt.legend()
    plt.show()

def d2():
    xtop = topnode()

    z = np.linspace(0.,2., 5)
    for key, pipel in xtop.nodes.iteritems():
        N = pipel.config['starcatN.ab.N']

#        if not N in [0, 3, 5, 10, 20]: continue


        gc = pipel.depend['galcat']['magcat']['cat']
    #    gc = gc.join(pipel.depend['r2gal']['r2eff']['r2eff'])
        gc = gc.join(pipel.depend['r2train']['r2train']['r2eff'])
        gc = gc.join(pipel.depend['galabs']['mabs']['cat'][['sed']])

    #    gc = gc[gc.mag_vis < 24.5]
        plot_z = False
        field = 1
        if plot_z:
            mean = gc.groupby(pd.cut(gc.zs, z)).mean()
            plt.plot(mean.zs, mean[field], 'o-', label=str(N))

        else:
            mean = gc.groupby(pd.cut(gc.sed, range(0,66,2))).mean()
            plt.plot(mean.sed, mean[field], 'o-', label=str(N))
        

    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
    plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)
    #plt.axhline(3e-4)

    plt.xlabel('z', size=16)
    plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
    plt.ylabel

    plt.legend()
    plt.show()

d2()
