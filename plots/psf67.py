#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    """Histogram of the "R^2_{\\rm{PSF}}" bias values."""

    def topnode(self):
        import trainpipel

        top = xd.Topnode(trainpipel.pipel())

        R = [1, 2, 3, 5, 8, 10]
        f_riz = ['r', 'i', 'z']
        top.add_layer_key('r2sed.test_cat.r', R)
        top.add_layer_key('r2sed.filters', [f_riz])

        return top        

    def get_cat(self, topnode):

        D = pd.DataFrame()
        gc = topnode.values()[0].r2sed.test_cat.result
        for key,pipel in topnode.iteritems():
            col = '.'.join(map(str,key)) if isinstance(key, tuple) else key

            # Ignoreing the index gives the wrong results..
            gc[col] = pipel.r2sed.result.r2eff #.values

            # The filter labels is used from other plots.
            S = pd.Series()
            S['galnoise.r'] = pipel.config['r2sed.test_cat.r']
            S['nf'] = len(pipel.config['r2sed.filters'])
            S['filters'] = pipel.config['r2sed.filters']

            S['col'] = col

            D = D.append(S, ignore_index=True)

        gc = gc[gc.mag_vis < 24.5]
        D = D.set_index('col')

        return D, gc

    def show(self, D, gc):
        K = {'histtype': 'step', 'normed': True, 'bins': 40}
        D = D.sort('galnoise.r')
        for key, row in D.iterrows():
            lbl = 'R: {0}'.format(row['galnoise.r'])

            gc[key].hist(label=lbl, **K)

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16)
        plt.xlim((-0.02, 0.02))

        plt.legend()

        plt.show()

    def run(self):
        topnode = self.topnode()
        self.show(*self.get_cat(topnode))

if __name__ == '__main__':
    Plot().run()
