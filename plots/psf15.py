#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import sys
import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

import tosave

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    """Offset in gamma."""

    def topnode(self):
        import trainpipel
        topnode = xd.Topnode(trainpipel.pipel())
        topnode.update_config({\
#          'r2x.use_stars': True, #False, #True,
          'r2x.use_stars': False, #True, #False, #True,
          'r2x.filters': ['r','i','z'],
          'r2x.all_test': True,
          'r2x.Ntrain': 4000})
#          'r2x.Ntrain': 400})


        # Its simpler thinking in term of percentages.
#        per = np.linspace(-2., 2, 11)
#        gamma = 0.55*(1+per/100.)

        # Its simpler thinking in term of percentages.
        per = np.array([-2.,-1., 0, 1.,2.])
        gamma = 0.55*(1+per/100.)

        topnode.add_layer_key('r2x.test_r2.r2vz.r2_exp', gamma)

        return topnode

    def prepare(self, pipel):
        cat = pipel.r2x.test_cat.result[['zs', 'mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]
        N = pipel.r2x.config['Nboot']


        df = pd.DataFrame()
        S = pd.Series()
        S['zbin'] = 'all'
        S['r2bias'] = cat[range(N)].mean().mean() #abs().describe(percentiles=[0.68])['68%']
        df = df.append(S, ignore_index=True)

        z = np.linspace(0.1, 1.8, 7)
        z = np.arange(0.0, 2.2, 0.4)
        z = np.linspace(0.1, 1.8, 6)

        for i,(key,sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
            S = pd.Series()
            S['zbin'] = i
#            S['r2bias'] = sub[range(N)].mean().abs().describe(percentiles=[0.68])['68%']
#            S['r2bias'] = np.abs(sub[range(N)].mean().mean())
            S['r2bias'] = sub[range(N)].mean().mean()
#            ipdb.set_trace()

            S['zlbl'] = key
            df = df.append(S, ignore_index=True)

        df['r2_exp'] = pipel.r2x.test_r2.r2vz.config['r2_exp']

        return df

    def per(self, r2_exp):
        return 100.*(-1 + r2_exp/0.55)

    def run(self):
        topnode = self.topnode()

        prepare = pd.DataFrame()
        for key, pipel in topnode.iteritems():
            part = self.prepare(pipel)
            part['key'] = len(part)*[key] # Required hack.
            prepare = prepare.append(part, ignore_index=True)

        styles = gen_styles()
        for key, sub in prepare.groupby('zbin'):
            if key == 'all':
                continue

            sub = sub.sort('r2_exp')
            lbl = 'z = {0}'.format(sub.irow(0).zlbl)
            ls = next(styles)
            lw = 1.5 if ls == '-' else 2.

            r2_rel = sub.r2_exp / 0.55
            plt.plot(r2_rel, sub.r2bias, lw=lw, ls=ls, label=lbl)

        sub = prepare[prepare.zbin == 'all']
        sub = sub.sort('r2_exp')

        r2_rel = sub.r2_exp / 0.55
        plt.plot(r2_rel, sub.r2bias, ls=next(styles), lw=2., label='All', color='k')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        plt.axvline(0., ls='--', color='black', lw=2., alpha=0.5)

        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')


#        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2), useOffset=False)

        import matplotlib.ticker as mtick
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))


        plt.xlabel('$\delta \gamma\ /\ 0.55$', size=17)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=17)
        plt.xlim((0.975, 1.025))
        plt.ylim((-0.001, 0.001))
        plt.subplots_adjust(left=0.15)

        plt.legend(loc=3)
        plt.savefig(tosave.path('psf15'))

#        plt.show()

    def fast(self, topnode):
        topnode.update_config({\
          'r2x.filters': ['r','i','z'],
          'r2x.Ntrain': 4,
          'r2x.Nboot': 4})

        return topnode

if __name__ == '__main__':
    Plot().run()
