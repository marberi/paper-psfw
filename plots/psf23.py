#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
from matplotlib import pyplot as plt
import numpy as np
import matplotlib as mpl
from matplotlib.colors import ColorConverter

import xdolphin as xd
from xdolphin import Job

class Plot:
    """Color versus R^2 for stars and galaxies."""

    name = 'psf23'

    def pipel(self):
        import trainpipel
        pipel = trainpipel.pipel() 

        return pipel

    def plot4(self, galcat, starcat, x1, y1, ax=False, vis=True):
        col = lambda cat: cat['mag_'+x1] - cat['mag_'+y1]

        col_gal = col(galcat)
        col_star = col(starcat)

        ax.plot(col_gal, galcat.r2eff, '.', color='orange', ms=2.,label='Galaxy', visible=vis)
        ax.plot(col_star, starcat.r2eff, 'k.', ms=2., label='Star', visible=vis)

        return

    def load(self, pipel):
        N = 200

        ell_01 = pipel.r2x.test_r2.r2vz.result['/vis/Ell_01'][0]
        galmag = pipel.r2x.test_cat.magcat.result
        r2gal = pipel.r2x.test_r2.result
        galcat = galmag.join(r2gal)
        galcat.r2eff /= ell_01

        starmag = pipel.r2x.train_star_cat.result
        r2star = pipel.r2x.train_star_r2.result
        starcat = starmag.join(r2star)
        starcat.r2eff /= ell_01

        # Only use a subset. 
        r_gal = np.random.randint(0, len(galcat), N)
        r_star = np.random.randint(0, len(starcat), N)

        galcat = galcat.iloc[r_gal]
        starcat = starcat.iloc[r_star]

        return galcat, starcat

    def plot(self, X):
        galcat, starcat = X

        F = ['g', 'r', 'i', 'z', 'vis']
        F += ['Y', 'J', 'H']

        N = len(F)
        fig, A = plt.subplots(N-1,N-1, sharex='col', sharey=True)
        conv = ColorConverter()
        col = conv.to_rgb('#b0e0e6') ##87ceeb') #6495ed')

        cmarg = {'g': (-7,3), 'r': (-7, 3), 'i': (-4, 3), 'z': (-2, 2), 'vis': (-4, 3), 'Y': (-2,2), 'J': (-1,1)}


        over_vis = ['r', 'i', 'z', 'vis']
        for k1,(f,B) in enumerate(zip(F[1:], A)):
            for k2, (g,ax) in enumerate(zip(F[:-1], B)):
                if k2 < k1+1:
                    self.plot4(galcat, starcat, g, f, ax)
                else:
                    self.plot4(galcat, starcat, g, f, ax, False)
                    ax.axis('off')

                if f in over_vis and g in over_vis:
#                    ax.set_axis_bgcolor(col)
                    ax.set_facecolor(col)

                ax.tick_params(width=0.)

                ax.xaxis.grid(True, which='both')
                ax.yaxis.grid(True, which='both')
#                ax.set_yticks([])

                ax.locator_params(axis='y', nbins=5)
                ax.locator_params(axis='x', nbins=5)
                plt.setp(ax.get_xticklabels(), fontsize=7, rotation='vertical')
                plt.setp(ax.get_yticklabels(), fontsize=7) #, rotation='vertical')

                if k2 <= k1:
                    ax.set_title('{0}-{1}'.format(g, f), fontsize=9, y=0.92) #1.0)

#                ipdb.set_trace()
                ax.set_xlim(-np.array(cmarg[g])[::-1])
#                ax.set_xlim(cmarg[g])

        # y-label
        fig.text(0.02, 0.5, '$R^2_{\\rm{PSF}}$ / $R^2_{\\rm{PSF}}\,$(Ell_01, z=0)', ha='left', va='center', rotation='vertical', size=9)

        # Add legend
        lines = ax.lines
        labels = [x.get_label() for x in lines]
        plt.figlegend(ax.lines, labels, loc=(0.65, 0.6), markerscale=3., prop={'size': 11})

        plt.tight_layout()
        plt.subplots_adjust(left=0.1,wspace=0.2, hspace=0.5)

        # Tweaking other settings.
        mpl.rcParams.update() #{'ytick.labelsize': 6})

#        path = tosave.path('psf23')
#        plt.savefig(path)
#        plt.show()



#    def run(self):
#        pipel = self.topnode()
#        galcat, starcat = self.catalogs(pipel, 200)
#
#      self.plot(galcat, starcat)

if __name__ == '__main__':
#    Plot().run()
    xd.plot(Plot).save()
