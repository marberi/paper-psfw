#!/usr/bin/env python
# encoding: UTF8
 
from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

def old_pipel():
    config = {'star_fit': False, 'smag_distr': True}
    ab_dir = '/data2/photoz/run/data/'
    data_dir = '/data2/photoz/run/data/'
    filter_dir = '/data2/photoz/run/data/des_euclid2'
    
    abstar = xd.Job('ab')
    abkids = xd.Job('ab')
    
    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})
    
    
    abkids.task.config.update({'data_dir': ab_dir,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})
    
    # Stars
    starcatN = xd.Job('starcat')
    starcatN.depend['ab'] = abstar
    
    if config['smag_distr']:
        smag_prob = xd.Job('stardistr')
        starcatN.depend['mag_prob'] = smag_prob
    
    pipel = xd.Job()
    pipel.depend['starcat'] = starcatN

    return pipel

def new_pipel():
    photoz_data = '/data2/photoz/run/data/'
    filter_dir = '/data2/photoz/run/data/des_euclid2'

    abkids = xd.Job('ab')
    abstar = xd.Job('ab')
    abkids.task.config.update({'data_dir': photoz_data,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abstar.task.config.update({'data_dir': photoz_data,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})

    j1 = xd.Job('kidscat')


    j2 = xd.Job('starsel')
    j2.depend['catalog'] = j1

    j3 = xd.Job('starfit')
    j3.depend['catalog'] = j2
    j3.depend['ab'] = abkids

    j4 = xd.Job('starabs')
    j4.depend['starcat'] = j2
    j4.depend['starfit'] = j3


    j5 = xd.Job('starobs')
    j5.depend['starcat'] = j4
    j5.depend['ab'] = abstar

    pipel = xd.Job()
    pipel.depend['starcat'] = j5

    return pipel

def find_star_seds():
    """Find the list of the SEDs in the Pickles library."""

    # This support function is needed to map from the star_sed_ind to
    # the SED name.
    _star_path = '/data2/photoz/run/data/seds_stars/STAR_PICKLES.list'
    sedL = []
    for line in open(_star_path).readlines():
        # I forget regexp between each time I need them..
        sed = line.replace('PICKLES/','').split('.sed')[0]
        sedL.append(sed)

    sedS = pd.Series(sedL)

    return sedS

def check_magdistr():
    p1 = old_pipel()
    p2 = new_pipel()
    cat1 = p1.starcat.result
    cat2 = p2.starcat.result

    bins = np.linspace(12, 22, 200)
    K = {'bins': bins, 'histtype': 'step', 'normed': True}
    cat1.mag_i.hist(label='Old pipeline', **K)
    cat2.mag_i.hist(label='New pipeline', **K)

    plt.grid(True)
    plt.xlabel('i-band magnitude', size=16)
    plt.ylabel('Probability', size=16)

    plt.legend(loc=2)
    plt.savefig(tosave.path('x79_magdistr'))

def check_seddistr():
    # Compare the SED distributions.
    p1 = old_pipel()
    p2 = new_pipel()
    cat1 = p1.starcat.result
    cat2 = p2.starcat.result

    sedS = find_star_seds()
    cat1['sed'] = sedS.ix[cat1.star_sed_ind].values

    sed1 = cat1.sed.value_counts()
    sed2 = cat2.sed.value_counts()
    sed1.name='old'
    sed2.name='new'
    sed = pd.concat([sed1, sed2], axis=1)

    sed = sed.divide(sed.sum())
    sed.plot(kind='bar')
    plt.savefig(tosave.path('x79_seddistr'))

if __name__ == '__main__':
    check_magdistr()
    check_seddistr()
