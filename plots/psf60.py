#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import splrep, splev
from scipy.integrate import simps

star_dir = '/data2/photoz/run/data/seds_stars'

lmb = np.linspace(3000, 12000, 1000)

def load_splines():
    spls = {}
    spls_cut = {}
    for fname in os.listdir(star_dir):
        if not fname.endswith('sed'):
            continue

        sed = fname.replace('.sed', '')
        path = os.path.join(star_dir, fname)

        x,y = np.loadtxt(path).T

        # Replacing the Halpha region with a linear
        # interpolation. Only removing the points gives
        # numerical artifacts.
        to_rem = np.logical_and(6500< x, x < 6620)  
        i_min, i_max = to_rem.nonzero()[0][[0,-1]]
        xmin,xmax = x[i_min], x[i_max]
        ymin,ymax = y[i_min], y[i_max]
        der = (ymax - ymin) / (xmax - xmin)
        const = ymin - der*xmin
        x_cut = x 
        y_cut = y.copy()
        y_cut[to_rem] = const+der*x_cut[to_rem]

        spls_cut[sed] = splrep(x_cut, y_cut)
        spls[sed] = splrep(x, y)

    return spls, spls_cut


def calc_r2(spls):
    print('start')
    r2 = {}
    lmb_vis = np.linspace(5500, 9200, 1000)
    lmb_r2 = lmb_vis**0.55
    for sed, spl in spls.iteritems():
        y = splev(lmb_vis, spl)

        r2[sed] = simps(y*lmb_r2, lmb_vis) / simps(y, lmb_vis)

    return r2
#        ipdb.set_trace()        

def test1():
    spls, spls_cut = load_splines()

    r2D = calc_r2(spls)
    r2_cutD = calc_r2(spls_cut)

    seds = r2D.keys()
    r2 = np.array([r2D[x] for x in seds])
    r2_cut = np.array([r2_cutD[x] for x in seds])

    R = np.abs((r2_cut - r2) / r2)

    plt.plot(range(len(r2)), R)
    plt.xlabel('SED (random)', size=16)
    plt.ylabel('(r2_cut - r2)/r2', size=16)
    plt.show()


def calc_flux(spls):
    lmb_r = np.linspace(5667, 7156)
    fD = {}
    for sed, spl in spls.iteritems():
        flux = simps(lmb_r*splev(lmb_r, spl), lmb_r)

        fD[sed] = flux

    return fD

# Estimating the magnitudes.
spls, spls_cut = load_splines()
fD = calc_flux(spls)
f_cutD = calc_flux(spls_cut)

seds = spls.keys()

f = np.array([fD[x] for x in seds])
f_cut = np.array([f_cutD[x] for x in seds])
delta_m = pd.Series(2.5*np.log10(f/f_cut))

delta_m.hist(bins=40)
plt.show()

#calc_mag(spls)
#ipdb.set_trace()

