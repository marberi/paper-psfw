#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

import psf10
import psf12
import xdolphin as xd
from xdolphin import Job

import tosave

class Plot:
    """Histogram of fields for different configurations."""

    def topnode(self):
        import trainpipel

        top = xd.Topnode()
        top['noiseless',] = trainpipel.pipel(add_noise=False)        
        top['noisy',] = trainpipel.pipel(add_noise=True)
        top.update_config({'r2train.Ntrain': 400,
                           'r2train.Ntest': 55000,
                           'r2train.Nboot': 400})

#        ipdb.set_trace()

        filtersL = [x.split(',') for x in ['r,i,z', 'vis,r,i,z', 'r,i']]
        top.add_layer_key('r2train.filters', filtersL)
        top.add_layer_key('r2train.use_stars', [True, False])
#        ipdb.set_trace()        

        return top

    def toS(self, topnode):
        df = pd.DataFrame()
        for key,pipel in topnode.iteritems():
            S = pd.Series()
           
            filters = pipel.config['r2train.filters'] 
            S['filters'] = filters
            S['lbl_filters'] = ','.join(filters)
            S['lbl_stars'] = 'Stars' if pipel.config['r2train.use_stars'] else \
                             'Galaxies'
            S['key'] = key

            for i,n in enumerate(key):
                S['ind'+str(i)] = n
    
            df = df.append(S, ignore_index=True )

        return df


    def run(self):
        topnode = self.topnode()

        for key,pipel in topnode.iteritems():
            pipel.r2train.run()

        S = self.toS(topnode)
        N = topnode.values()[0].r2train.config['Nboot']

        # Numerical indexing is *very* confusing. Temporary solution.
        S['ind_stars'] = S.ind0
        S['ind_filters'] = S.ind1
        S['ind_noise'] = S.ind2

        nrows = S.ind_noise.unique().size
        ncols = S.ind_filters.unique().size

        fig, A = plt.subplots(nrows, ncols, sharex='col', sharey='row')

        K = {'histtype': 'step', 'normed': True}
        # Rows
        for i,(k1,v1) in enumerate(S.groupby('ind_noise')):
            # Columns
            for j,(k2,v2) in enumerate(v1.groupby('ind_filters')):
                ax = A[i,j]
                # Lines.

                for _, row in v2.sort('lbl_stars').iterrows():
                    pipel = topnode[row.key]

                    cat = pipel.r2train.galcat.result[['mag_vis']]
                    cat = cat.join(pipel.r2train.get_store()['r2eff']) 

                    cat = cat[cat.mag_vis < 24.5] 
                    cat[range(N)].mean().hist(ax=ax, label=row.lbl_stars, **K)


                plt.setp(ax.get_xticklabels(), rotation=70) 

                # The Euclid requirement.
                A[i,j].axvline(-3e-4, ls='--', color='k', lw=2, alpha=0.5)
                A[i,j].axvline(3e-4, ls='--', color='k', lw=2, alpha=0.5)

                A[0,j].set_title(v2.irow(0).lbl_filters, size=16)

            # They y-label.
            A[i,0].set_ylabel(k1.capitalize(), size=16)


        # Common legend.
        A[-1,-1].legend()

        # Tweaked values for the x-range.
        xranges = [0.0015, 0.0015, 0.004]
        for i,xlim in enumerate(xranges):
            A[0,i].set_xlim((-xlim, xlim))
            
#            ipdb.set_trace()

        plt.show()

if __name__ == '__main__':
    Plot().run()
