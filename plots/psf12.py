#!/usr/bin/env python
# encoding: UTF8

import ipdb
import xdolphin as xd

import psf10

# Here we need newstyle classes.
class Plot(psf10.Plot, object):
    """Main table including noise."""

    # I am not fully sure the configuration is correctly set. The old
    # version worked with the psf10 config attribute in a weird way. The
    # old case was 

#        self.config['myconf'].update({'galnoise': {'const_sn': False},
##                             'starnoise': {'const_sn': True, 'sn': 100.},
#                                      'starnoise': {'const_sn': False},
#                                      'starcatN': {'norm_mag': 10}})

    # with the message:
    # - A bit weird, but I needed to override *all* options set in 
    # - psf10 to get the right results.
    def topnode(self):
        top = super(Plot, self).topnode()
        top.update_config({\
          'galnoise.const_sn': False,
          'starnoise.const_sn': False,
          'starcatN.norm_mag': 10})


        return top

if __name__ == '__main__':
    Plot().run()
