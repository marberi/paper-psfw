#!/usr/bin/env python

import ipdb
import json
import os
import sys

import xdolphin as xd

# Later these parts need to be improved.
sys.path.append('/home/marberi/xdolphin/xdolphin')
import config
conf = config.config()


def toa(key):
    return str(key) if isinstance(key, unicode) else key

def toascii(X):
    if isinstance(X, dict):
        res = {}
        for key, val in X.iteritems():
            res[toa(key)] = toascii(val)

#        ipdb.set_trace()
        return res
    elif isinstance(X, list):
        res = [toascii(Xi) for Xi in X]
        return res

    elif isinstance(X, unicode):
        return str(X)

    elif isinstance(X, float) or isinstance(X, int):
        return X
    else:
        print('Need to also fix for this type.')

        ipdb.set_trace()

def load_pipel(path):
    pack = json.load(open(path))
    pack = toascii(pack)

#    ipdb.set_trace() 

    # First create jobs without dependencies. Ignore version number.
    res = {}
    for jobid, info in pack.iteritems():
        job = xd.Job(info['name'])
        job.config.update(info['config'])

        res[int(jobid)] = job

    for jobid, info in pack.iteritems():
        job = res[int(jobid)]

        for dep_name, dep_id in info['dep']:
            job.depend[dep_name] = res[dep_id]

    # Find the topnode... (not very elegant).
    topid = None
    for jobid, info in pack.iteritems():
        if info['name'] == 'empty':
            topid = int(jobid)

    assert topid, 'Did not find the top of the pipeline.'

    return res[topid]

def pretty(pipel, level=0):
    if not level:
        print(pipel.name)

    for key, dep_job in pipel.depend.iteritems():
        print(' '*(level+1)+'->'+key)

        pretty(dep_job, level+1)

def difference(pipel1, pipel2, root=()):

    # Try testing the difference between two pipelines, Looping
    ipdb.set_trace()

def comp(p1, p2, root):
    print(root)
    print('p1', p1.depend[root].taskid())
    print('p2', p2.depend[root].taskid())

def test():
    import trainpipel
    pipel1 = trainpipel.pipel()
    if True: #False:
        pipel1.save()
        pipel2 = load_pipel(path)

    #pretty(pipel1)

    #difference(pipel1, pipel2)
    p1 = pipel1
    p2 = pipel2
    for root in p1.depend.keys():
        comp(p1, p2, root)

def comp2(p1, p2, root=('pipel',)):
    t1 = p1.taskid()
    t2 = p2.taskid()

    if t1 != t2:
        print('.'.join(root))

    # Need to 
    for key, val in p1.depend.iteritems():
        comp2(val, p2.depend[key], root+(key,))

#        ipdb.set_trace()


d = '/data2/marberi/results/pipel/'
path1 = os.path.join(d, 'f46849eeb06b29cf81b6c5f8307990b7')

n2 = '29cdead622ff16c95db7097ea1728570'
n2 = 'f7309d96eccc1ee51b33fc250df408f4'
n2 = 'ca6f951b1e437e1ccb28366d1be58c75'
n2 = '20309db199c5d850ecd1e2cded689139'
path2 = os.path.join(d, n2) #'ca9a3047f85b5d119e6ec76c859c7b63')
#e312c62e3ac881a8ed5fde8fb96ba3b0'

p1 = load_pipel(path1)
p2 = load_pipel(path2)

#for root in p1.depend.keys():

comp2(p1, p2)

ipdb.set_trace()


