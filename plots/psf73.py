#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

import trainpipel

class Plot:
    def topnode(self):
        pipel = trainpipel.pipel()
        pipel.config.update({
          'r2x.use_cal': True,
          'r2x.Ntrain': 400,
#          'r2x.noise_cal': False,
          'r2x.use_stars': False, #True, #False,
          'r2x.filters': ['r', 'i']})
#          'r2x.filters': ['r', 'i', 'z']})

        top = xd.Topnode(pipel)
        top.add_layer_key('r2x.noise_cal', [False, True])
        top.add_layer_key('r2x.use_cal', [False, True])
#        top.add_layer_key('r2x.use_stars', [False, True])
        return top

    def get_cat(self, pipel):

        r2bias = pipel.r2x.get_store()['r2eff']
        cat = r2bias.join(pipel.r2x.test_cat.result)

        return cat

    def run(self):
        top = self.topnode()
        z = np.linspace(0.1, 1.8, 10)
        for key, pipel in top.iteritems():
            cat = self.get_cat(pipel)
            cat = cat[cat.mag_vis < 24.5]
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()
            r2bias = mean[range(2)].mean(axis=1)

            lbl_cal = 'Use cal: {0}'.format(pipel.r2x.config['use_cal'])
 #           lbl_stars = 'Use stars: {0}'.format(pipel.r2x.config['use_stars'])
            lbl_noise = 'Noise mocks: {0}'.format(pipel.r2x.config['noise_cal'])
            lbl = '{0}, {1}'.format(lbl_cal, lbl_noise)
# stars, lbl_cal)

            plt.plot(mean.zs, r2bias, 'o-', label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.xlabel('z', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
#        plt.ylim((-0.002, 0.002))
        plt.ylim((-0.005, 0.005))

        plt.legend()
        plt.show()

if __name__ == '__main__':
    Plot().run()
