#!/usr/bin/env python
# encoding: UTF8

# Some quick tests to see how many objects we need...
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

Nr = 10000
scale = 0.02
S = np.random.normal(scale=scale, size=Nr)
#S = pd.Series(S)

#nobj = 2000
nboot = 2000

df = pd.DataFrame()
for nobj in [100, 200, 500, 700, 1000, 1500, 2000, 3000, 4000, 5000]:
    r = np.random.randint(0, Nr, size=(nobj, nboot))
    X = S[r].mean(axis=0)

    df = df.append(pd.Series({'nobj': nobj, 'mean': np.abs(X).mean(), 'std': np.sqrt(X.mean())}),
                   ignore_index=True)

#ipdb.set_trace()
plt.plot(df.nobj, scale/np.sqrt(df.nobj), 'o-', lw=2)
plt.plot(df.nobj, df['mean'])
plt.yscale('log')
plt.show()

#print(S.mean())
#ipdb.set_trace()
