#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd
import trainpipel

import xdolphin as xd

def add_r2next(pipel):
    """Replace r2x with r2next."""

    r2next = xd.Job('r2next')
    # Copying the configuration items.
    for key, val in pipel.r2x.config.task_config.iteritems():
        r2next.config[key] = val

    # Add the dependencies
    for name, j in pipel.r2x.depend.iteritems():
        r2next.depend[name] = j

    pipel.depend['r2x'] = r2next

def new_pipel():
    photoz_data = '/data2/photoz/run/data/'
    filter_dir = '/data2/photoz/run/data/des_euclid2'

    abkids = xd.Job('ab')
    abstar = xd.Job('ab')
    abkids.task.config.update({'data_dir': photoz_data,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abstar.task.config.update({'data_dir': photoz_data,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})

    j1 = xd.Job('kidscat')


    j2 = xd.Job('starsel')
    j2.depend['catalog'] = j1

    j3 = xd.Job('starfit')
    j3.depend['catalog'] = j2
    j3.depend['ab'] = abkids

    j4 = xd.Job('starabs')
    j4.depend['starcat'] = j2
    j4.depend['starfit'] = j3


    j5 = xd.Job('starobs')
    j5.depend['starcat'] = j4
    j5.depend['ab'] = abstar

    pipel = xd.Job()
    pipel.depend['starcat'] = j5

    return pipel


pipel = trainpipel.pipel()
r2next = add_r2next(pipel)

ipdb.set_trace()
