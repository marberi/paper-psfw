#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import xdolphin as xd
from matplotlib import pyplot as plt

import trainpipel

pipel = trainpipel.pipel()
pipel.config.update({
  'r2x.reg': 'kneigh',
  'r2x.use_cal': False,
  'r2x.filters': ['r', 'i', 'z'],
#  'r2x.filters': ['r', 'i'],
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.use_stars': True})

noiseless = {
          'galnoise.const_sn': True,
          'galnoise.sn': 1000000,
          'starnoise.const_sn': True,
          'starnoise.sn': 1000000}

#pipel.config.update(noiseless)

def plot1():
    global pipel
    top = xd.Topnode(pipel)
    top.add_layer_key('r2x.filters', [['r','i'], ['r', 'i', 'z'], ['vis', 'r', 'i']])
    #top.add_layer_key('r2x.use_stars', [False, True])

    top.add_layer([{}, noiseless])

    K = {'histtype': 'step', 'bins': 150}
    order = [(0,0), (0,1), (0,2), (1,0), (1,1), (1,2)]
    for i,j in order:
#    for key, pipel in top.iteritems():
        pipel = top[i,j]
        cat = pipel.r2x.get_store()['r2eff']
        cat = cat.join(pipel.r2x.test_cat.result)
        cat = cat[cat.mag_vis < 24.5]

        #cat = cat[(0.5 < cat.zs) & (cat.zs < 0.6)]
#        cat = cat[(0.7 < cat.zs) & (cat.zs < 0.9)]
        lbl_f = ','.join(pipel.r2x.config['filters'])
        nl = pipel.galnoise.config['const_sn'] 
        lbl_nl = 'Noiseless' if nl else 'Noisy'
        lw = 1.0 if nl else 1.5

        lbl = '{0}, {1}'.format(lbl_f, lbl_nl)
        cat[0].hist(label=lbl, lw=lw, **K)

#        ipdb.set_trace()
        print(lbl, cat[0].mean())

    plt.xlim((-0.02, 0.02))
    plt.legend()
    plt.savefig('test.pdf')
    #ipdb.set_trace()

#plot1()

def plot2():
    pipel.config.update(noiseless)
    pipel.config['r2x.filters'] = ['r', 'i'] #, 'z']

    cat = pipel.r2x.get_store()['r2eff']
    cat = cat.join(pipel.r2x.test_cat.result)
    cat = cat[cat.mag_vis < 24.5]

    cat[0].hist(color='black', bins=50, alpha=0.1, normed=True)

    z = np.linspace(0.1,2.0, 7)
    z = np.arange(0., 2.0, 0.4)
    K = {'histtype': 'step', 'bins': 50, 'normed': True}
    for key,sub in cat.groupby(pd.cut(cat.zs, z)):
        sub[0].hist(label=key, **K)


    xlim = 0.005
    plt.xlim((-xlim, xlim))
    plt.legend()
    plt.savefig('test.pdf')

def plot3():
    pipel.config['r2x.filters'] = ['r', 'i', 'z'] #, 'z']

    cat = pipel.r2x.get_store()['r2eff']
    cat = cat.join(pipel.r2x.test_cat.result)
    cat = cat[cat.mag_vis < 24.5]
    cat = cat.join(pipel.r2x.test_pz.result, rsuffix='A')

    xa = 0.4
    xb = xa+0.1

    sub1 = cat[(xa < cat.zs) & (cat.zs < xb)]
    sub2 = cat[(xa < cat.zb) & (cat.zb < xb)]

    K = {'histtype': 'step', 'normed': True, 'bins': 50}
    c1 = 'black'
    c2 = 'blue'
    ls1 = '-'
    ls2 = '--'
    sub1[0].hist(label='Spec-z', color=c1, ls='solid', **K)
    sub2[0].hist(label='Photo-z', color=c2, ls='dashed', **K)

    plt.axvline(sub1[0].mean(), color=c1, ls=ls1)
    plt.axvline(sub2[0].mean(), color=c2, ls=ls2)

    plt.title('Redshift: [{0:.1f}, {1:.1f}]'.format(xa, xb))
    plt.legend()

    xlim = 0.03
    plt.xlim((-xlim, xlim))
    plt.yscale('log')
    plt.savefig('test.pdf')

import psf78
dsl = psf78.Plot()
rows = dsl._rows()

pipel.config.update({'r2x.use_cal': True})
#  r2x.use_cal': False,
#import trainpipel
top = xd.Topnode(pipel)
top.add_layer_key('r2x.filters', rows)
#top.add_layer_key('r2x.use_stars', [False, True])

df = pd.DataFrame()
for i in range(len(rows)):
    pipel = top[(i,)]

    cat = pipel.r2x.get_store()['r2eff']
    cat = cat.join(pipel.r2x.test_cat.result)
    cat = cat[cat.mag_vis < 24.5]
    cat = cat.join(pipel.r2x.test_pz.result, rsuffix='A')

    lbl_f = ','.join(pipel.r2x.config['filters'])
    err = np.sqrt(cat[0].var())
#    use_stars = pipel.r2
    S = pd.Series({'i': i, 'lbl_f': lbl_f, 'err': err})

    df = df.append(S, ignore_index=True)

ipdb.set_trace()
