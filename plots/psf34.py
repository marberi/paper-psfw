#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import svm
from sklearn.preprocessing import normalize

from xdolphin import Job

class psf34:
    name = 'psf34'
    config = {'norm_mag': False} #True} #True}

    def col(self, cat, filters):
        fmt = 'mag_{0}'

        A = np.array(cat[map(fmt.format, filters)])

        col = A[:,:-1] - A[:,1:]
        return col #pd.DataFrame(col)
#        return A

    def train_test(self, cstar, cgal, r2star, r2gal):
        Nstar = 400
        Ngal = 20000

        xstar = np.random.permutation(np.arange(len(cstar)))[:Nstar]
        xgal = np.random.permutation(np.arange(len(cgal)))[:Ngal]

        A_train = cstar[xstar]
        A_test = cgal[xgal]

        B_train = r2star[xstar]
        B_test = r2gal[xgal]

        if self.config['norm_mag']:
            v = {'axis': 1, 'norm': 'l2'}
            A_train = normalize(A_train, **v)
            A_test = normalize(A_test, **v)
#            ipdb.set_trace()


#        ipdb.set_trace()
        return A_train, A_test, B_train, B_test


    def change_corr(self, cstar, cgal, A_test):

        cov_star = np.cov(cstar.T)
        cov_gal = np.cov(cgal.T)

        var_star = np.diag(cov_star)
        xvar_star = 1. / np.diag(np.linalg.inv(cov_star))

        var_gal = np.diag(cov_gal)
        xvar_gal = 1. / np.diag(np.linalg.inv(cov_gal))

        a = np.sqrt(var_star / var_gal)
        ax = np.sqrt(xvar_star / xvar_gal)

        b = cstar.mean(axis=0) - a*cgal.mean(axis=0)
        bx = cstar.mean(axis=0) - ax*cgal.mean(axis=0)

        X = ax*A_test + bx

        return X

    def apply_shift(self, cstar, cgal, A_test):
        dmean = cstar.mean(axis=0) - cgal.mean(axis=0)


        a = cstar.std(axis=0) / cgal.std(axis=0)
        b = cstar.mean(axis=0) - a*cgal.mean(axis=0)
        b = cstar.mean(axis=0) - cgal.mean(axis=0)

        k1 = 1.0
        k2 = 1.0
        X = k1*a*A_test + k2*b

#        ipdb.set_trace()

        return X
       

    def next_shift(self, cstar, cgal, A_test):
        cov_star = np.cov(cstar.T)
        cov_gal = np.cov(cgal.T)

#        to_dec = np.dot(cov_star, np.linalg.inv(cov_gal))
#        R1 = cov_star
#        R2 = np.linalg.pinv(cov_gal)
#        R3 = np.dot(R1, R2)

        xc = np.linalg.cholesky(cov_gal)
        B = np.linalg.cholesky(cov_star)
        A = np.dot(B, np.linalg.inv(xc))

        # Ugly, but I want results. Ok, not that bad..
        Bt = cstar.mean(axis=0) - np.mean(np.dot(cgal,A.T), axis=0) 
#        Bt = cstar.mean(axis=0) - np.mean(cgal, axis=0) 

        At = np.dot(A_test, A.T)

        k1 = 1.0
        k2 = 1.0
        X = k1*At  + k2*Bt

        cov_X = np.cov(X.T)
        print(cov_star)
        print(cov_X)

        print('star_mean', cstar.mean(axis=0))
        print('new_mean', X.mean(axis=0))
#        ipdb.set_trace()
        return X
#        ipdb.set_trace()

#    def unit_shift(self, cstar, cgal, A_test):


    def run(self):
        import trainpipel
        pipel = trainpipel.pipel()

        gal = pipel.kids['galcat']['magcat']['cat']
        star = pipel.kids['starnoise']['magcat']['cat']

        r2star = pipel.kids['r2star']['r2eff']['r2eff']['r2eff']
        r2gal = pipel.kids['r2gal']['r2eff']['r2eff']['r2eff']
        r2star = np.array(r2star)
        r2gal = np.array(r2gal)


        filters = ['vis', 'r', 'i', 'z', 'H', 'J']
#        filters = ['r', 'i']
        cstar = self.col(star, filters)
        cgal = self.col(gal, filters)

        # Trying to understand the effect of the noise...
        galN = pipel.kids['galcatN']['magcat']['magcat']
        cgalN = self.col(galN, filters)

        starN = pipel.kids['starcatN']['magcat']['magcat']
        cstarN = self.col(starN, filters)

        # Testing..
#        cstar = cstarN
#        cgal = cgalN


#        cstarN = 
#        ipdb.set_trace()


        A_train, A_test, B_train, B_test = self.train_test(cstar, cgal, r2star, r2gal)

#        self.fix(cgal, cstar)
        reg = svm.NuSVR()
        reg.fit(A_train, B_train)

        if True: #False:
            L = [('Normal', A_test)]
            L.append(('Shift 1', self.apply_shift(A_train, A_test, A_test)))
            L.append(('xShift 1', self.next_shift(A_train, A_test, A_test)))

        if False: #True: #False:
            L = [('Normal', A_test)]
            L.append(('Shift 1', self.apply_shift(A_train, A_test, A_test)))
            L.append(('Shift 2', self.apply_shift(cstar, cgal, A_test)))
            L.append(('Shift 3', self.apply_shift(cstar, cgalN, A_test)))
            L.append(('Shift 4', self.apply_shift(cstarN, cgal, A_test)))
            L.append(('Shift 5', self.apply_shift(cstarN, cgalN, A_test)))

        if False: #True: #False:
            L = [('Normal', A_test)]
            L.append(('Shift 1', self.apply_shift(A_train, A_test, A_test)))
    #        L.append(('Shift x2', self.change_corr(cstarN, cgalN, A_test)))

            L.append(('xShift 1', self.next_shift(A_train, A_test, A_test)))
            L.append(('xShift 2', self.next_shift(cstar, cgal, A_test)))
            L.append(('xShift 3', self.next_shift(cstar, cgalN, A_test)))
            L.append(('xShift 4', self.next_shift(cstarN, cgal, A_test)))
            L.append(('xShift 5', self.next_shift(cstarN, cgalN, A_test)))


        for lbl, A in L:
            B_rec = reg.predict(A)
            delta = pd.Series((B_rec - B_test)/B_test)

            rec = pd.Series(B_rec) 
            delta.hist(bins=200, histtype='step', normed=True, label=lbl)

            print(lbl, delta.mean(), rec.mean())

        plt.legend()
        plt.show()
#        ipdb.set_trace()
        
#        ipdb.set_trace()

if __name__ == '__main__':
    Job('psf34')()
