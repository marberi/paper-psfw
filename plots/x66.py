#!/usr/bin/env python
# encoding: UTF8

import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import psf9
import tosave

class Plot:
    """Showing the redshift distribution of the different
       galaxy types.
    """

    def topnode(self):
        inst = psf9.Plot()
        pipel = inst.topnode()
        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 10000

        return pipel

    def plot(self, pipel):
        cat = pipel.r2pz.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result)
        cat = cat.join(pipel.r2pz.pzcat.galcat.magcat.mabs.result, rsuffix='ignore_')

#        cat = cat[cat.mag_vis < 24.5]
        #gr = pd.cut(cat.zs, z)

        ipdb.set_trace()

        cat = cat[1.9 < cat.zs]

        sedl = [0,17,55,65]
        S = pd.cut(cat.sed, sedl, labels=['ell', 'sp', 'irr'])

        mbins = np.linspace(16, 28)
        z = np.linspace(0.1, 2.0, 30)
        for key, val in cat.groupby(S):
            val.mag_vis.hist(bins=mbins, label=key)

#            ipdb.set_trace()
#        bins = np.linspace(0., 3, 40)
#        for key, val in cat.groupby(S):
#            val.zs.hist(label=key, bins=bins, histtype='step')

        plt.legend()
        plt.xlabel('Redshift', size=16)

        plt.savefig('test.pdf')
#        plt.savefig(tosave.path('x63'))

    def plot2(self, pipel):
        """Looking at the absolute magnitude distribution."""

        cat = pipel.r2pz.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result)
        cat = cat.join(pipel.r2pz.pzcat.galcat.magcat.mabs.result, rsuffix='ignore_')

        K = {'histtype': 'step'}
        Mbins = np.linspace(-23, -16)

        cat = cat[cat.zs < .4] # Cutting

        cat.M.hist(bins=Mbins, label='Full', **K)
        for mlim in [18, 20, 21, 22., 24.5]:
            sub = cat[cat.mag_vis < mlim]
            sub.M.hist(bins=Mbins, label='vis < {0}'.format(mlim), **K)

        plt.legend(loc=2)
        plt.savefig('test.pdf')


    def plot3(self, pipel):
        # Testing the completeness for different redshifts.

        cat = pipel.r2pz.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result)
        cat = cat.join(pipel.r2pz.pzcat.galcat.magcat.mabs.result, rsuffix='ignore_')

        cat = cat[cat.zs < 0.2]
        zbins = np.linspace(16, 28)
        cat.mag_vis.hist(bins=zbins, histtype='step')
        plt.axvline(24.5, color='black', lw=1.5, alpha=0.5)
        plt.savefig('test.pdf')

#        ipdb.set_trace()

    def run(self):
        pipel = self.topnode()
        self.plot3(pipel)

if __name__ == '__main__':
    Plot().run()
