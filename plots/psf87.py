#!/usr/bin/env python
# encoding: UTF8

import ipdb
import copy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from xdolphin import Job
import xdolphin as xd

import psf4
import tosave

class Plot(psf4.Plot):
    """Gaussian bias."""

    # Test the bias expression at a different redshift.
    def _load(self, var, vals, N = 50000):

        # Should later be in the topnode function
        r2gauss = xd.Job('r2gauss')

        z0 = 0.8
        A = []
        for x in vals:
            r2gauss.task.config[var] = x
            r2gauss.task.config['N'] = N
            r2gauss.task.config['z0'] = z0
            r2gauss.task.config['zmax'] = z0 + 0.01

            df = r2gauss.result
            df[var] = x
            A.append(df)

        df = pd.concat(A)

        return df

    # To not destroy the original plots.
    def run(self):
        biasL = np.logspace(-3., -1.)
        df = self._load('bias', biasL)

        seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14']
        mean = df.groupby(['bias']).mean()
        var = df.groupby(['bias']).var()

        pz = np.array(mean.index) # Not the best...
        colors = ['b', 'r', 'k', 'm', 'g', 'm', 'purple']
        styles = ['-',':','-.','-', '--']

        for i,sed in enumerate(seds):
            plt.errorbar(pz, np.abs(mean[sed]), label=sed, ls=styles[i], lw=2., color=colors[i])

        self.pretty()

        pconf = tosave.config()['plot']
        plt.xlabel('photo-z bias', size=18)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=18)
        plt.ylim(3e-5, 1e-2)

        xline = np.linspace(*plt.xlim())
        y_lower = 0*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, 0, 0.002, color='b', alpha=0.1, hatch='/')
#        plt.axvline(0.002)

        path = tosave.path('psf87_z0p8')
        plt.savefig(path)
#k        plt.show()

if __name__ == '__main__':
    Plot().run()
