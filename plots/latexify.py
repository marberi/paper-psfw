import matplotlib
import math

# This code is adapted from:
#
# http://nipunbatra.github.io/2014/08/latexify/

# Sensible values to add to the matplotlibrc file:

"""
backend: ps
xtick.labelsize: 8
ytick.labelsize: 8
text.fontsize: 8
font.family: 'serif'
mathtext.fontset: stix
axes.labelsize: 8
"""

def figsize(fig_height=None, columns=1):

    fig_width = 3.39 if columns==1 else 6.9 # width in inches

    if fig_height is None:
        golden_mean = (math.sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    params = {'figure.figsize': [fig_width,fig_height]}

#    import ipdb; ipdb.set_trace()
    matplotlib.rcParams.update(params)
