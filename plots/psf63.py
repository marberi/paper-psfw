#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

def storeme(path, df):
    store = pd.HDFStore(path, 'w')
    store['cache'] = df
    store.close()

class psf63:
    name = 'psf63'
    config = {}

    S = [['r', 'i', 'z'], ['r', 'vis'], ['vis', 'r', 'i'], ['i', 'vis'], ['z', 'vis']]

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def d1(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')
        E = B['noisy']['galnoise'].depend['magcat']['magcat']['magcat']
        cat = cat.join(E, rsuffix='_x')

        fL = ['r', 'i', 'z']

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        Ntrain = 2000 #800
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]


        S = self.S
        for i,fL in enumerate(S):
            print('Fitting', ','.join(fL))

            Astar, _ = self.get_col(star_cat, fL)

            col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)

            from sklearn import svm, neighbors, tree
            reg = svm.NuSVR()
#            reg = tree.ExtraTreeRegressor()

            reg.fit(Astar[inds], r2star[inds,0])


            r2pred = reg.predict(col_gal)
            cat['r2pred'+str(i)] = r2pred 
            cat['r2eff'+str(i)] = (r2pred - r2true) / r2pred

        cat['r2predx'] = 0.5*(cat.r2pred0 + cat.r2pred1)
        cat['r2effx'] = (cat.r2predx - r2true) / r2true
        cat['r2true'] = r2true

#        cat = cat[cat.mag_vis < 24.5]

        z = np.linspace(0., 2., 20)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        for i in range(len(S)):
            plt.plot(mean.zs, mean['r2eff'+str(i)], 'o-', label=','.join(S[i]))

        plt.plot(mean.zs, mean.r2effx, 'o-', label='Combined')

#        ipdb.set_trace()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        if False:
            plt.legend()
            plt.show()

        return cat

#        storeme('/data2/marberi/tmp/psf63.h5', cat)

#        ipdb.set_trace()

    def h1(self):
        store = pd.HDFStore(self.in_file)
        cat = store['cache']
        store.close()

        if False:
            K = {'bins':100, 'histtype': 'step', 'normed': True}

            cat.r2eff0.hist(label='r,i,z', **K)
            cat.r2eff1.hist(label='r,vis', **K)

            plt.legend()
            plt.show()


        K = {'bins': 100, 'histtype': 'step', 'normed': True}

        z = np.linspace(0.1, 2., 6) #10)
        for key, val in cat.groupby(pd.cut(cat.zs, z)):
            z_lbl = ':z {0:.2f}'.format(val.zs.mean())
            
            #val.r2eff0.hist(label='r,i,z'+z_lbl, **K)
            val.r2eff1.hist(label='r,vis'+z_lbl, **K)


#        cat.r2eff1.hist(label='r,vis', **K)

        plt.legend()
        plt.show()




    def h2(self):
        store = pd.HDFStore(self.in_file)
        cat = store['cache']
        store.close()

        zL = []
        skewL = []
        z = np.linspace(0.1, 2., 20) #6) #10)
        for key, val in cat.groupby(pd.cut(cat.zs, z)):
            zL.append(val.zs.mean())
            skewL.append(val.r2eff1.skew())

        plt.plot(zL, skewL, 'o-')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('z', size=16)
        plt.ylabel('Skewness', size=16)

        plt.show()

    def h3(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('r - vis', size=16)
        plt.ylabel('"R^2_{\\rm{PSF}}" bias', size=16)

        plt.plot(cat.mag_r - cat.mag_vis, cat.r2eff1, '.')

        plt.show()


    def h4(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        cat['c1'] = cat.mag_r - cat.mag_vis

        c = np.linspace(-0.8, 2.5, 20)
        mean = cat.groupby(pd.cut(cat.c1, c)).mean()

        plt.plot(mean.c1, mean.r2pred1, 'o-', label='Predicted')
        plt.plot(mean.c1, mean.r2true, 'o-', label='True')

        plt.xlabel('r - vis', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$', size=16)
        plt.ylim((120, 150))

        plt.legend(loc=0)
        plt.show()

    def h5(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        cat['c1'] = cat.mag_r - cat.mag_vis
        cat.c1.hist(bins=100)

        plt.show()


    def h6(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        cat['c1'] = cat.mag_r - cat.mag_vis
#        cat.c1.hist(bins=100)

        z = np.linspace(0.1, 2.0, 10)
        for i,X in enumerate([(0., 1.), (-0.5, 1.)]):
            sub = cat[(X[0] < cat.c1) & (cat.c1 < X[1])]

            mean = sub.groupby(pd.cut(sub.zs, z)).mean()
            plt.plot(mean.zs, mean.r2eff1, label=str(i))

        plt.legend()
        plt.show()


    def h7(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        cat['c1'] = cat.mag_r - cat.mag_vis
        cat['c1_x'] = cat.mag_r_x - cat.mag_vis_x

        K = {'bins':100, 'histtype': 'step', 'normed': True}
        cat.c1.hist(label='Noisy', **K)
        cat.c1_x.hist(label='Noiseless', **K)

        plt.xlabel('r - vis', size=16)
        plt.legend()
        plt.show()


    def h8(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        cat['c1'] = cat.mag_r - cat.mag_vis
        cat['c1_x'] = cat.mag_r_x - cat.mag_vis_x

#        cat = cat[cat.mag_vis < 24.5]
        cat = cat[cat.mag_vis_x < 24.5]

        c = np.linspace(-0.8, 2.5, 60)
        mean = cat.groupby(pd.cut(cat.c1, c)).mean()
        mean_x = cat.groupby(pd.cut(cat.c1_x, c)).mean()

        if False:
            plt.plot(mean.c1, mean.r2pred1, 'o-', label='Predicted')
            plt.plot(mean.c1, mean.r2true, 'o-', label='True')
            plt.plot(mean.c1_x, mean.r2pred1, 'o-', label='Predicted - X')
            plt.plot(mean.c1_x, mean.r2true, 'o-', label='True - X')

        plt.plot(mean.c1, mean.r2eff1, 'o-')


        plt.xlabel('r - vis', size=16)
#        plt.ylabel('$"R^2_{\\rm{PSF}}"$', size=16)
#        plt.ylim((120, 150))

        plt.legend(loc=0)
        plt.show()

#        ipdb.set_trace()

    def h9(self, cat):
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        cat['c1'] = cat.mag_r - cat.mag_vis
        cat['x1_rvis'] = cat.mag_r_x - cat.mag_vis_x

        c = np.linspace(-0.8, 2.5, 60)
        mean = cat.groupby(pd.cut(cat.c1, c)).mean()

        for i in range(5):
            plt.plot(mean.x1_rvis, mean['r2eff'+str(i)], 'o-', label=','.join(self.S[i]))

        plt.plot(mean.x1_rvis, mean.r2eff1, label='r - vis')


        plt.legend()
        plt.show()

        ipdb.set_trace()


    def get_B(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 500000


            B[lbl] = A

        return B

    def h10(self, cat, B):
        # Comparting the r-vis and "R^2_{\\rm{PSF}}" value for various redshifts
#        ax = plt.gca()
#        ax.xaxis.grid(True, which='both')
#        ax.yaxis.grid(True, which='both')

        # Normalize to Ell_01(z=0)
        norm = B['noiseless']['r2train'].depend['r2gal'].depend['r2vz']['r2vz']['r2vz']['/vis/Ell_01'][0]
        cat.r2true /= norm

        cat['c1'] = cat.mag_r - cat.mag_vis
        cat['x1_rvis'] = cat.mag_r_x - cat.mag_vis_x

        cat = cat[cat.x1_rvis < 24.5]

        z = np.linspace(0.1, 2., 8)
        gr_z = pd.cut(cat.zs, z)

        fig,A = plt.subplots(1,3, sharey=True)
        fL = [('r', 'vis'), ('i', 'vis'), ('z', 'vis')]


        # Adding some galaxy points.
        r = np.random.randint(0, len(cat), 10000)
        sub = cat.ix[r]        
        for i,(f1,f2) in enumerate(fL):
            col = sub['mag_'+f1] - sub['mag_'+f2]
            A[i].plot(col, sub.r2true, '.', color='orange', alpha=0.3, label='Galaxies')


        for i,(f1,f2) in enumerate(fL):
            c = np.linspace(-0.8, 2.5, 60)

            for key,sub in cat.groupby(gr_z):
                sub['xcol'] = sub['mag_'+f1] - sub['mag_'+f2]
                mean = sub.groupby(pd.cut(sub.xcol, c)).mean()

                z_lbl = 'z = {0:.2f}'.format(sub.zs.mean())
                A[i].plot(mean.xcol, mean.r2true, '-', lw=1.4, label=z_lbl)


        # Add the stars.
        scat = B['noiseless']['r2train'].depend['starcat'].depend['magcat']['magcat']['cat']
        scat = scat.join(B['noiseless']['r2train'].depend['r2star']['r2eff']['r2eff'])
        scat.r2eff /= norm
        r = np.random.randint(0, len(scat), 400)
        subS = scat.ix[r]

        for i,ax in enumerate(A):
            F = fL[i]
            col = subS['mag_'+F[0]] - subS['mag_'+F[1]]

            ax.plot(col, subS.r2eff, 'k.', ms=2, label='Stars') #/norm, '.'

        # Gris and ylabel.
        for i,ax in enumerate(A):
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

        A[0].set_ylabel('$"R^2_{\\rm{PSF}}"/"R^2_{\\rm{PSF}}"$(Ell_01)', size=16)

        # Legend and x-label
        for i,ax in enumerate(A):
            ax.legend(loc=0)

            F = fL[i]
            ax.set_xlabel('{0} - {1}'.format(F[0], F[1]), size=16)



        plt.show()


    def h11(self, cat, B):
        field = 'r2eff1'
 
        fig,A = plt.subplots(1,3, sharey=True)
        fL = [('r', 'vis'), ('i', 'vis'), ('z', 'vis')]

        cat['x1_rvis'] = cat.mag_r_x - cat.mag_vis_x
        cat = cat[cat.x1_rvis < 24.5]

        z = np.linspace(0.1, 2., 8)
        for i, (f1, f2) in enumerate(fL):
            c = np.linspace(-1.8, 2.5, 60)

            cat['xcol'] = cat['mag_'+f1] - cat['mag_'+f2]

            for key, sub in cat.groupby(pd.cut(cat.zs, z)):
                mean = sub.groupby(pd.cut(sub.xcol, c)).mean()
                z_lbl = 'z = {0:.2f}'.format(sub.zs.mean())

                A[i].plot(mean.xcol, mean[field], '-', lw=1.4, label=z_lbl)

#            ipdb.set_trace()
            X = cat.groupby(pd.cut(cat.xcol, c)).mean()
            A[i].plot(X.xcol, X[field], '--', lw=1.4, label='All') #z_lbl)


        for i, (f1, f2) in enumerate(fL):
            ax = A[i]
            ax.set_xlabel('{0} - {1}'.format(f1, f2), size=16)

            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            # Euclid requirement.
            ax.axhline(-3e-4, color='black')
            ax.axhline(3e-4, color='black')

        A[0].set_ylabel('$"R^2_{\\rm{PSF}}"$', size=16)
        plt.ylim(-0.005, 0.005)

        plt.legend()
        plt.show()


    def run(self):
#        if Ture: #False: #True: #eFalse:
#        if False:
        self.in_file = '/data2/marberi/tmp/psf63_nl.h5'

        if os.path.exists(self.in_file):
            store = pd.HDFStore(self.in_file)
            cat = store['cache']
            store.close()

            B = self.get_B()
            self.h11(cat, B)

            return

        print('egei')
        cat = self.d1(B)

        storeme(self.in_file, cat)

if __name__ == '__main__':
    Job('psf63').run()
