#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    """Gausian photo-z bias."""

    name = 'psf4'

    def topnode(self):
        r2gauss = xd.Job('r2gauss')
        pipel = xd.Job('empty')
        pipel.depend['r2gauss'] = r2gauss
        pzL = np.logspace(-2.3, -1.)

        pipel.config.update({'r2gauss.N': 50000})

        layers = xd.Layers()
        layers.add_pipeline(pipel)
        layers.add_layer_key('pz', 'r2gauss.pz', pzL)


        top = layers.to_topnode()

        return top


    def f_reduce(self, pipel):
        cat = pipel.r2gauss.result
        part = pd.concat({'mean': cat.mean(), 'var': cat.var()}, axis=1)

        part.index.name = 'sed'
        part = part.reset_index()

#        ipdb.set_trace()

        return part

    def pretty(self):
        plt.legend(loc=4)
        plt.xscale('log')
        plt.yscale('log')

        plt.ylim(1e-6, 1e-2)
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

    def plot(self, prepare):

        labels = {'pz': 'r2gauss.pz'}
        seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14']
        colors = ['b', 'r', 'k', 'm', 'g', 'm', 'purple']
        styles = ['-',':','-.','--', '-']


        #for sed in seds:
        for i,sed in enumerate(seds):
            sub = prepare[prepare.sed == sed].sort_values('pz')

            plt.plot(sub['pz'], np.abs(sub['mean']), label=sed, ls=styles[i], color=colors[i])


        self.pretty()
        plt.xlabel('$\sigma_z$ / (1+z)', size=14)
        plt.ylabel('$|\delta R^2_{\\rm{PSF}}|\, /\, R^2_{\\rm{PSF}}$', size=14)
       
        xline = np.linspace(*plt.xlim())
        y_lower = 0*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, 0, 0.05, color='b', alpha=0.1, hatch='/')

        plt.xlim(prepare.pz.min(), prepare.pz.max())
        plt.ylim((1e-5, 5e-3))

if __name__ == '__main__':
    xd.plot(Plot).save()
