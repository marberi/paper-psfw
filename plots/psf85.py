#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

import trainpipel
import tosave

class Plot:
    def pipel(self):
        pipel = trainpipel.pipel()
        pipel.r2x.config['use_cal'] = False
        del pipel.r2x.depend['cal_pz']
        del pipel.r2x.depend['test_pz']


        pipel.config.update({
        'r2x.all_test': True,
        'r2x.Nboot': 10,
        'r2x.filters': ['r','i','z']})

        return pipel

    def plot(self, pipel):
        NL = [100, 400, 1000, 2000, 3000, 5000]

        fig, A = plt.subplots(ncols=2, sharey=True)
        fL = [['r','i','z'], ['r','i']]

        for i,filters in enumerate(fL):
            ax = A[i]
            pipel.r2x.config['filters'] =  filters #['r','i']

            # Trying again...
            Nboot = pipel.r2x.config['Nboot']
            prepare = pd.DataFrame()
            for N in NL:
                pipel.r2x.config['Ntrain'] = N

                cat = pipel.r2x.get_store()['r2eff']
                cat = cat.join(pipel.r2x.test_cat.result)

                cat = cat[cat.mag_vis < 24.5]
                S = cat.mean()
                S['N'] = N
                S['r2bias_all'] = cat[range(Nboot)].mean().mean()

                prepare = prepare.append(S, ignore_index=True)

            for i in range(Nboot):
                ax.plot(prepare.N, prepare[i])

            ax.plot(prepare.N, prepare.r2bias_all, 'o-', color='black', lw=1.5)
            ax.set_title('Gal: {0}'.format(','.join(filters)))

            ax.grid(True)
            ax.set_xlabel('# training objects')

        plt.tight_layout()
        plt.savefig(tosave.path('psf85'))

    def run(self):
        pipel = self.pipel()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
