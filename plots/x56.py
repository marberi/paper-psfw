#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import scipy
import scipy.stats
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

import tosave

class Plot:
    """Requirement for the photo-z redshift resolution."""

    def topnode(self):

        import trainpipel
        pipel = trainpipel.pipel()

        interp = 2 
        #interp = 0
        pzconf = {'use_priors': False, 'filters': ['r', 'i', 'z']} #, 'zmax': 4.5}
        #pzconf = {'use_priors': False, 'filters': ['r', 'i']}
        pzconf['interp'] = interp
        #pzconf['zmax'] = 3.
        pipel.pzcat.config.update(pzconf)
        #pipel.depend['mabs'].config['Ngal'] = 10007
        pipel.pzcat.config['out_pdf'] = False
        pipel.galcat.config['sn'] = 100
        pipel.galcat.config['sn'] = 1e7

        dzL = [0.02, 0.01, 0.007]
        dzL = [.315, 0.2, 0.15, 0.1, 0.05, 0.02, 0.015, 0.01, 0.007]
        dzL = [0.0005, 0.001, 0.003, 0.005, 0.01]
#        dzL += [0.007, 0.01, 0.015, 0.02, 0.03, 0.035, 0.04, 0.045, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]#, 0.12, 0.13,0.14, 0.15, 0.2]

#        dzL = [0.1] # FAST


        pipel.config.update({\
          'r2pz.pzcat.galcat.magcat.mabs.limit_seds': True,
          'r2pz.pzcat.galcat.const_sn': True})

        topnode = xd.Topnode(pipel)
        topnode.add_layer_key('r2pz.pzcat.dz', dzL)

        return topnode

    def prepare(self, pipel):

        cat = pipel.r2pz.result
        cat = cat.join(pipel.galcat.result)
        cat = cat.join(pipel.r2gal.result)
        cat = cat.join(pipel.pzcat.result, rsuffix='x')
        cat = cat[cat.mag_vis < 24.5]
        
        cat = cat[['r2peak']] 
        cat.index = range(len(cat))

#        nL = [1000, 10000, 54000, 100000]
        df = pd.DataFrame()

        Nboot = 10
        ngal = 55000
        y = pd.DataFrame()

        rind = cat.index.copy()
        for i in range(Nboot):
            rind = np.random.permutation(rind)
            sub = cat.ix[rind[:ngal]]

#            ipdb.set_trace()

            S = pd.Series({'r2peak': sub.r2peak.mean(),
                           'r2peak_abs': sub.r2peak.abs().mean()})
            y = y.append(S, ignore_index=True)


        # Combining these into a single series.
        y_mean = y.mean()
        y_err = np.sqrt(y.abs().var()).rename(lambda x: x+'_err')

        S = y_mean.append(y_err)

#        ipdb.set_trace()

        # Labels.
        S['dz'] = pipel.r2pz.pzcat.config['dz']

        return S

    def show(self, prepare):
        ax1 = plt.gca()
        ax2 = ax1.twinx()

        ax1.xaxis.grid(True, which='both')
        ax1.yaxis.grid(True, which='both')

        val = prepare.sort('dz')
        line1 = ax1.errorbar(val.dz, val.r2peak_abs, val.r2peak_abs_err, fmt='o-', color='b', label='<|$"R^2_{\\rm{PSF}}"$|>')
        line2 = ax2.errorbar(val.dz, val.r2peak.abs(), val.r2peak_err, fmt='o-', color='k', label='|<$"R^2_{\\rm{PSF}}"$>|')

        ax1.set_ylabel('<|$"R^2_{\\rm{PSF}}"$|>', size=16)
        ax2.set_ylabel('|<$"R^2_{\\rm{PSF}}"$>|', size=16)

        xlabel = '$\Delta$ z - SED fit'
        ax1.set_xlabel(xlabel, size=16)
        ax2.set_xlabel(xlabel, size=16)

        ax1.set_yscale('log')
        ax2.set_yscale('log')

        plt.xscale('log')
        ax1.set_xlabel('dz - SED fit redshift resolution', size=16)

        ax1.set_xscale('log')
        ax2.set_xscale('log')

        xlim = (val.dz.min(), val.dz.max())
        plt.xlim(xlim)
        plt.xlim(0.0004, 0.01)
        ax1.set_ylim(5e-6, 1e-4)

        # Labels with two y-axis is more tricky.
        lines = [line1, line2]
        lbls = [l.get_label() for l in [line1, line2]]
        plt.legend(lines, lbls, loc=0)

        plt.savefig(tosave.path('psf41'))

    def plot1(self, topnode):
#        pipel = topnode[(0,)]

        N = len(topnode._nodes)
        for i in range(N):
            pipel = topnode[(i,)]
            cat = pipel.r2pz.result
            cat = cat.join(pipel.galcat.result)
            cat = cat.join(pipel.r2gal.result)
            cat = cat.join(pipel.pzcat.result, rsuffix='x')
            cat = cat[cat.mag_vis < 24.5]

            dz = pipel.config['r2pz.pzcat.dz']
            label = 'dz: {0:.1e}'.format(dz)
            cat.r2peak.hist(histtype='step', bins=100, normed=True, label=label)

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16) 
        plt.ylim((0., 40000.))

        xlim = 4e-4
        plt.xlim((-xlim, xlim))
        plt.legend()
        plt.savefig(tosave.path('x56_plot1'))

    def plot2(self, topnode):
#        pipel = topnode[(0,)]

        f_gauss = scipy.stats.norm()
        frac = 2.0*(np.array(map(f_gauss.cdf, [1,2,3])) - 0.5)

        N = len(topnode._nodes)
        df = pd.DataFrame()
        for i in range(N):
            pipel = topnode[(i,)]
            cat = pipel.r2pz.result
            cat = cat.join(pipel.galcat.result)
            cat = cat.join(pipel.r2gal.result)
            cat = cat.join(pipel.pzcat.result, rsuffix='x')
            cat = cat[cat.mag_vis < 24.5]

            S = cat.r2peak.abs().describe(percentiles=frac)
            S['dz'] = pipel.config['r2pz.pzcat.dz']

            df = df.append(S)

#        ignore = ['count', 'dz', 'min']
#        touse = ['25%', '50%', '75%']
#        for col in df.columns:
#            if col in ignore: continue
        touse = ['68.3%', '95.4%', '99.7%']
 
        for col in touse:
            plt.plot(df.dz, df[col], 'o-', label=col)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xscale('log')
        plt.yscale('log')

        xlim = (0.0005, 0.01)
        xdiff = np.log10(xlim[1] / xlim[0])
        fac = 10**(0.05*xdiff)

        plt.xlim((xlim[0]/fac, fac*xlim[1]))
        plt.ylim((5e-6, 5e-4))
        plt.xlabel('dz - SED fit redshift resolution', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$', size=16)

        plt.legend(loc=2)
        plt.savefig(tosave.path('x56_plot2'))

    def run(self):
        topnode = self.topnode()
#        self.debug(topnode)

        self.plot2(topnode)

#        prepare = topnode.get_prepare(self.prepare)
#        self.show(prepare)

if __name__ == '__main__':
    Plot().run()
