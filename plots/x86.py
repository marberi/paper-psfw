#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import trainpipel
import xdolphin as xd

def rem_pz_dep(job):
    # One actually have to the for use_cal in this way.
    right_task = job.name in ['r2train', 'r2x', 'r2next', 'r2t']
    use_cal = ('use_cal' in job.task.config) and job.config['use_cal']

    if not (right_task and not use_cal):
        return 

    for key in ['test_pz', 'cal_pz']:
        if key in job.depend:
            del job.depend[key]

#    ipdb.set_trace()


#pipel = trainpipel.pipel(star_r2train_task='r2t')

# While working
#pipel.r2x.config['use_cal'] = False

top = xd.Topnode(trainpipel.pipel())
top2 = xd.Topnode(trainpipel.pipel(star_r2train_task='r2t'))
top.join(top2)

top.update_config({\
  'r2x.use_cal': True,
  'r2x.use_stars': True,
  'r2x.Nboot': 100,
  'r2x.Ntrain': 400, #104,
})

ipdb.set_trace()

z = np.linspace(0.1, 1.8, 10)
for key,pipel in top.iteritems():
    name = 'New' if pipel.r2x.name == 'r2t' else 'Old'
    use_cal = 'Cal' if pipel.r2x.config['use_cal'] else 'No cal'

    label = '{0} {1}'.format(name, use_cal)
    print('name', name)

    cat = pipel.r2x.get_store()['r2eff']
    cat = cat.join(pipel.r2x.test_cat.result)
    mean = cat.groupby(pd.cut(cat.zs, z)).mean()

    Nboot = pipel.r2x.config['Nboot']
    r2bias = mean[range(Nboot)].mean(axis=1)
#    ipdb.set_trace()

    plt.plot(mean.zs, r2bias, label=label)

plt.legend()
plt.show()
#ipdb.set_trace()
