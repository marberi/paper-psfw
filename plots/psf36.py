#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
from sklearn.preprocessing import normalize

from xdolphin import Job

class psf36:
    name = 'psf36'
    config = {}

    def col(self, cat, filters):
        fmt = 'mag_{0}'

        A = np.array(cat[map(fmt.format, filters)])

        col = A[:,:-1] - A[:,1:]
        return col #pd.DataFrame(col)
#        return A

    def train_test(self, cstar, cgal, r2star, r2gal):
        Nstar = 400
        Ngal = 50000

        xstar = np.random.permutation(np.arange(len(cstar)))[:Nstar]
        xgal = np.random.permutation(np.arange(len(cgal)))[:Ngal]

        A_train = cstar[xstar]
        A_test = cgal[xgal]

        B_train = r2star[xstar]
        B_test = r2gal[xgal]

        return A_train, A_test, B_train, B_test


    def old_tests(self):
        cat = r2gal.join(mabs)
        for i in [0,1,2]:
            val = cat[cat.sed==i]
            val.zs.hist(bins=200, histtype= 'step', normed= True, label=str(i))
            val[(139 < val.r2eff) & (val.r2eff < 141)].zs.hist(bins=20, histtype= 'step', 
                                         normed= True, label='Lim '+str(i))

#            ipdb.set_trace() 
            
        plt.legend()
        plt.show()

        ipdb.set_trace()

        from scipy.interpolate import splev, splrep
        for sed in ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']:
            r2vis = r2vz['/vis/{0}'.format(sed)]
            zspl = np.arange(len(r2vis))
            spl = splrep(zspl, r2vis)

            zint = np.linspace(0., len(zspl), 10*len(zspl))
            r2ev = splev(zint, spl)
            plt.plot(zint, r2ev, '-', label=sed)

        plt.title('Spline interplation, 10x presicion', size=16)
        plt.legend()
        plt.show()

        ipdb.set_trace()
        
        ipdb.set_trace()
        H, edges = np.histogram(r2gal, bins=2000)
        diff = H[1:] - H[:-1]
       
        plt.plot(edges[:-2], diff)
        plt.show()

        return 
#        plt.plot
#        ipdb.set_trace()

        for Nint in [200, 400, 1000]:
#        for dz_ab in [0.01, 0.001]:
#            pipel.kids['r2vz_gal'].config['dz_ab'] = dz_ab
            pipel.kids['r2vz_gal'].config['Nint'] = Nint #dz_ab
            r2gal = pipel.kids['r2gal']['r2eff']['r2eff']['r2eff']

            r2gal.hist(bins=2000, histtype= 'step', normed= True, label=str(Nint))

        plt.legend()
        plt.show()

        return


    def run(self):
        import trainpipel
        pipel = trainpipel.pipel()

        gal = pipel.kids['galcat']['magcat']['cat']
        star = pipel.kids['starnoise']['magcat']['cat']

        mabs = pipel.kids['galmabs']['mabs']['cat']
        r2gal = pipel.kids['r2gal']['r2eff']['r2eff']['r2eff']
#        r2gal = pipel.kids['r2gal']['r2eff']['r2eff']

        pipel.kids['r2vz_star'].config['Nint'] = 1000
        pipel.kids['r2vz_gal'].config['Nint'] = 1000
        pipel.kids['starnoise'].config.update({'const_sn': True, 'sn': 10000})
        pipel.kids['galnoise'].config.update({'const_sn': True, 'sn': 10000})
        r2star = pipel.kids['r2star']['r2eff']['r2eff']['r2eff']
        r2gal = pipel.kids['r2gal']['r2eff']['r2eff']['r2eff']
        r2star = np.array(r2star)
        r2gal = np.array(r2gal)

#        dz_ab

#        filters = ['vis', 'r', 'i', 'z', 'H', 'J']
        filters = ['r', 'i', 'z']
#        filters = ['r', 'i']
        cstar = self.col(star, filters)
        cgal = self.col(gal, filters)

        # Trying to understand the effect of the noise...
        galN = pipel.kids['galcatN']['magcat']['magcat']
        cgalN = self.col(galN, filters)

        starN = pipel.kids['starcatN']['magcat']['magcat']
        cstarN = self.col(starN, filters)

        if True: #False: #True: #True: #False: #True:
            ctrain = cstar
            r2train = r2star
        else:
            ctrain = cgal
            r2train = r2gal

        if False: #True: #False: #True:
            ctest = cstar
            r2test = r2star
        else:
            ctest = cgal
            r2test = r2gal
        
#            A_train, A_test, B_train, B_test = self.train_test(cstar, cgal, r2star, r2gal)
#        else:
#            A_train, A_test, B_train, B_test = self.train_test(cgal, cgal, r2gal, r2gal)

        A_train, A_test, B_train, B_test = self.train_test(ctrain, ctest, r2train, r2test)

        if False:
            import os
            d = '/data2/photoz/run/data/des_euclid'
            path = os.path.join(d, 'vis.res')
            x,y = np.loadtxt(path).T

            ipdb.set_trace()

        r2 = pd.Series(r2star)

        from sklearn.neighbors import KNeighborsRegressor
        Ntrain = 800
        Nboot = 10
        clL = [('svr', svm.SVR), ('neigh', KNeighborsRegressor)]

        # Redoing the training selection here instead of modifying
        # the working code.
        R = np.arange(len(ctrain))

        df = pd.DataFrame()
        for i in range(Nboot):
            print('i', i)
            R = np.random.permutation(R)

            A_train = ctrain[R[:Ntrain]]
            B_train = r2train[R[:Ntrain]]

            for lbl, cl in clL:
                if lbl == 'neigh':
                    reg = cl(n_neighbors=10, weights='distance')
                else:
                    reg = cl()


                reg.fit(A_train, B_train)
                B_rec = reg.predict(A_test)
                delta = pd.Series((B_rec - B_test)/B_test)


                H, edges = np.histogram(delta, bins=2000, range=(-0.02,0.02))
                xm = 0.5*(edges[:-1] + edges[1:])
                peak = xm[H.argmax()]
       
                S = pd.Series({'i': i, 'lbl': lbl, 'mean': delta.mean(), 'peak': peak})
                df = df.append(S, ignore_index=True)

        print(df.groupby('lbl').mean())

        ipdb.set_trace()
 
#        reg = KNeighborsRegressor()
        reg = svm.SVR() #tol=1e-12)
#        reg = svm.SVR() #tol=1e-8)
        reg.fit(A_train, B_train)
        B_rec = reg.predict(A_test)
        delta = pd.Series((B_rec - B_test)/B_test)


        ipdb.set_trace()

        delta.hist(bins=800, histtype= 'step', normed=True)
        plt.xlim(-0.005, 0.005)
        plt.show()

        return

        df = pd.DataFrame({'delta': delta, 'r2true': B_test, 'r2rec': B_rec})

#        df.delta.hist(bins=200, histtype= 'step', normed= True, label='All')
#        df[139<df.r2true].delta.hist(bins=200, histtype= 'step', normed= True, label='High R2')
#        df[(139<df.r2true) & (df.r2true < 141)].delta.hist(bins=200, histtype= 'step', 
#          normed= True, label='Medium R2')

        df.delta.hist(bins=1000, histtype= 'step', normed= True, label='All')
#        df[132 < df.r2rec].delta.hist(bins=200, histtype= 'step', normed= True, label='Cut low')
#        df[df.r2rec < 140].delta.hist(bins=200, histtype= 'step', normed= True, label='Cut high')
#        df[(132 < df.r2rec) & (df.r2rec < 140)].delta.hist(bins=200, histtype= 'step', normed= True, label='Cut both')

        plt.legend()
        plt.xlim(-0.02, 0.02)
        plt.show()
#        df[df.true]
        #df[df.r2true < 141].delta.hist(bins=200, histtype= 'step', normed= True, label='Low R2')


#        plt.show()

        return
        ipdb.set_trace()

#        reg = svm.NuSVR(tol=x)
#        reg.fit(A_train, B_train)
#        B_rec = reg.predict(A_test)
#        delta = pd.Series((B_rec - B_test)/B_test)
        pd.Series(B_test).hist(bins=2000)

        plt.show()

        tol = [1e-3, 1e-4, 1e-5]
        y = []
        for x in tol:
            reg = svm.NuSVR(tol=x)
            reg.fit(A_train, B_train)
            B_rec = reg.predict(A_test)
            delta = pd.Series((B_rec - B_test)/B_test)

            y.append(delta.mean())
        ipdb.set_trace()

        plt.plot(tol, y)
        plt.xscale('log')
        plt.yscale('log')
        plt.show()
#        ipdb.set_trace()

#        delta.hist(bins=200)
#        plt.show()


if __name__ == '__main__':
    Job('psf36')()
