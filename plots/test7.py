#!/usr/bin/env python
# encoding: UTF8

# Test if the plot use the new structure.

from __future__ import print_function
import imp
import ipdb

import squid

inuse = squid.find_inuse()

def test_naming():
    print('================')
    for pl in inuse:
        mod = imp.load_module(pl, *imp.find_module(pl))
        test = hasattr(mod, 'Plot')

        print(pl, test)

def test_doc():
    print('================')
    for pl in inuse:
        mod = imp.load_module(pl, *imp.find_module(pl))
        inst = mod.Plot()

        # Should we allow empty strings?
        test = inst.__doc__ != None

        print(pl, test)


print('================')
# Where having a config dict makes sense.
ex = ['psf1', 'psf2']
for pl in inuse:
    if pl in ex:
        continue

    mod = imp.load_module(pl, *imp.find_module(pl))
    inst = mod.Plot()

    # Should we allow empty strings?
    test = hasattr(inst, 'config')

    print(pl, test)
