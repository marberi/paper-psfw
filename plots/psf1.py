#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
from matplotlib import pyplot as plt
#from scipy.interpolate import splev
from scipy.integrate import simps

import xdolphin as xd

import matplotlib as mpl
from cycler import cycler
mpl.rcParams['axes.prop_cycle'] = cycler(color='bgrcmyk')

class Plot:
    """Filter response curves."""

    name = 'psf1'

    config = {'filters': ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H'], 
              'mark_vis': True}

    def pipel(self):
        import trainpipel
        pipel = trainpipel.pipel()

        return pipel

    def load(self, pipel):
        # This is really ugly... The PSF paper was written without
        # having a separate interface for the filter curves.
        fcurves = {}
        filter_dir = '/nfs/pic.es/user/e/eriksen/data/psfw/des_euclid2'
#        filter_dir = self.config['filter_dir']
        for f in self.config['filters']:
            file_path = os.path.join(filter_dir, '{0}.res'.format(f))
            x,y = np.loadtxt(file_path).T
            
            fcurves[f] = x,y

        return fcurves

    def plot(self, fcurves):

        mean = {}
        fM = {}
        for fname, (x,y) in fcurves.iteritems():
            mean[fname] = simps(x*y, x) / simps(y, x)
            fM[fname] = max(y)

#        for fname, val in mean.iteritems():
#            plt.text(val, fM[fname]+0.05, fname)
#            ipdb.set_trace()
        hshift = {'g': -450., 'r': 0., 'i':150., 'z':600., 'Y': -0., 'J': -100., 'H': 500.}
        vshift = {'g': -0.03, 'r': 0.03, 'i': 0.02, 'z': -0.02, 'Y': -0.015, 'J': 0.01, 'H': -0.04}

        if self.config['mark_vis']:
            x,y = fcurves['vis']
            plt.fill(x,y, alpha=0.3, label='vis', color='b', hatch='/')

        for f in self.config['filters']:
            if f == 'vis' and self.config['mark_vis']:
                continue

            x,y = fcurves[f]
            label = f if f == 'vis' else None
            line = plt.plot(x, y, lw=2., label=label)

            plt.text(mean[f]+hshift[f], fM[f]+vshift[f], f, color=line[0].get_color(), 
                     size=18, horizontalalignment='center')
#            ipdb.set_trace()

#        plt.rc('text', usetex=True)
        plt.xlabel('Wavelength [$\mathrm{\AA}$]', size=14)
        plt.ylabel('Response', size=14)

        plt.xlim(3000,21000)
        plt.ylim(0., 0.7)
        plt.legend()

        ax = plt.gca()
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(True, which='minor')
        ax.yaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='minor')

if __name__ == '__main__':
    xd.plot(Plot).save()
