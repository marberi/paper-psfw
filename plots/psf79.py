#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import itertools as it
from matplotlib import pyplot as plt
import xdolphin as xd

import trainpipel
import psf22

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    styles = ['solid', 'dashed'] #, 'dashdot']

    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    name = 'psf79'

    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()

        pipel.config.update({\
          'r2x.Ntrain': 400,
          'r2x.Nboot': 10,
#          'r2x.filters': ['r','i','z']})
          'r2x.z_cal': 'zb',
          'r2x.z_test': 'zb'})

#          'r2x.filters': ['r','i']})

        # only needed for the z-band function.
        dsl = psf22.Plot()

        layers = xd.Layers()
        layers.add_pipeline(pipel)

        #topnode.add_layer_key('r2x.filters', [['r','i','z'], ['r','z']])
        layers.add_layer('zband', map(dsl.zband, [True, False]))
        layers.add_layer_key('stars', 'r2x.use_stars', [True, False])

        topnode = layers.to_topnode()

        return topnode

    def prepare(self, pipel):
        cat = pipel.r2x.test_cat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]


        X = np.histogram(cat[0], bins=100)
        ipdb.set_trace()

    def prepare(self, pipel):
        return pd.Series({'fisk': 1})


    def load(self, pipel):
        cat = pipel.r2x.test_cat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        return cat

    def plot(self, X):
        labels, data = X

        fL = ['r,i,z', 'r,i']
        has_zL = [True, False]
        use_starsL = [True, False]

        styles = gen_styles()
        for use_stars, has_z in it.product(use_starsL, has_zL):
            sub = labels[(labels.stars == use_stars) & (labels.zband == has_z)]
            assert len(sub) == 1.

            cat = data[sub.index[0]]

            lbl_star = 'Star train' if use_stars else 'Gal train'
            lbl_f = 'r,i,z' if has_z else 'r,i'

            lbl = '{0}: {1}'.format(lbl_f, lbl_star)

            ls = 'solid' if use_stars else 'dashed'
            cat[0].hist(histtype='step', bins=100, ls=ls, label=lbl)

        plt.xlabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)
        plt.ylabel('Probability', size=14)

        xlim = 0.025
        plt.xlim((-xlim, xlim))

        yline = np.linspace(0, 15000)
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='black', alpha=0.1)
        plt.ylim((0, 11000))

        plt.legend()

if __name__ == '__main__':
    xd.plot(Plot).save()
