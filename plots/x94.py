#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import SmoothBivariateSpline, splrep, splev

import psf83
import tosave

class Plot(psf83.Plot):
    def rshift(self, df, rL, dz):
        """Determine the optimal r-band magnitude shift."""

        print('Determine the optimal shift')
        #zsplit = np.arange(0.0, 2.2, 0.05)
        zsplit = np.arange(0.0, 2.1, dz)

        zkey = 'zb' 
        points = pd.DataFrame()
        for key, sub in df.groupby(pd.cut(df[zkey], zsplit)):
            r2bias = sub[range(len(rL))].mean(axis=0)
#            print('key', key)
#            print(len(sub))
#            print('r2bias', r2bias, len(r2bias))
#            print('rL', rL, len(rL))
            
            spl = splrep(r2bias, rL)

            y = float(splev(0., spl))
            part = pd.Series({'z': sub[zkey].mean(), 'y': y})
            points = points.append(part, ignore_index=True)


        return points

    def plot(self, pipel):
        rL = np.linspace(-0.1, 0.1, 100) #20)
        df = self.get_df(pipel, rL)

        points1 = self.rshift(df, rL, 0.05)
        points2 = self.rshift(df, rL, 0.2)

        plt.plot(points1.z, points1.y, 'x')
#        plt.plot(points2.z, points2.y, 'o')

        spl = splrep(points1.z, points1.y)
        zev = np.linspace(min(points1.z), max(points1.z), 200)
        yev = splev(zev, spl)

        plt.plot(zev, yev, '--')

        plt.plot(points2.z, points2.y, 'o')
#        ipdb.set_trace()

        plt.xlim((0., 2.1))
        plt.ylim((-0.1, 0.1))

        plt.grid(True)
        plt.xlabel('Redshift [$z_p$]', size=16)
        plt.ylabel('r-band offset')

        plt.savefig(tosave.path('x94'))

#        plt.show()
#
#        import sys
#        sys.exit(1)
#
#
##        ipdb.set_trace()
#
#
##        zev, r2shift, points = self.rshift(df, rL)
#
#        # Adjust the figure size.
#        fig = plt.gcf()
#        size = fig.get_size_inches()
#        size[1] *= 0.35
#        fig.set_size_inches(*size)
#        
#        plt.grid(True)
#        plt.xlabel('Redshift [$z_p$]', size=16)
#        plt.ylabel('r-band offset')
#
#        ipdb.set_trace()
#
#        plt.plot(zev, r2shift, lw=1.3, color='black')
#
#        plt.plot(points.z, points.y, 'x')
#
#        plt.xlim((0., 2.1))
##        plt.ylim((-0.1, 0.03))
#        plt.ylim((-0.1, 0.1))
#        plt.subplots_adjust(bottom = 0.28, left = 0.15, top=.98)
#
#        plt.savefig(tosave.path('x94'))
#
##        ipdb.set_trace()
    

    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
