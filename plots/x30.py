#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

def add_r2cal(pipel):
    r2cal = xd.Job('r2cal')
    r2cal.depend['r2train'] = pipel.r2train

    cal = pipel.r2train.clone()
    cal.depend['galcat'] = cal.train_gal_cat
    cal.depend['r2gal'] = cal.train_gal_r2

    cal.config.update({\
      'all_test': True,
      'use_stars': True, #False,
      'Ntrain': 400,
      'filters': ['r','i','z']})


    r2cal.depend['cal'] = cal

    pipel.depend['r2cal'] = r2cal

    return pipel

pipel = trainpipel.pipel()
pipel.config.update({
  'r2train.filters': ['r', 'i', 'z'],
#  'r2train.filters': ['r', 'i'],
  'r2train.all_test': True,
  'r2train.Nboot': 10})

per = np.array([-2,-1,0,1,2])
per = np.array([-10, 0., 10.])
per = np.array([-2., -1., 0., 1., 2.])
gamma = 0.55*(1. + per/100.) 

# Deeper observations of the stars.
pipel.r2train.train_star_cat.magcat.config['r'] = 10.

pipel = add_r2cal(pipel)

top = xd.Topnode(pipel)

#top.add_layer_key('r2train.r2gal.r2vz.r2_exp', gamma)

layer2 = [{'r2train.use_stars': True, 'r2train.Ntrain': 400}, \
          {'r2train.use_stars': False, 'r2train.Ntrain': 1500}]


#top.add_layer_key('r2train.use_stars', [False,True]) #r2gal.r2vz.r2_exp', gamma)
top.add_layer(layer2)


# Shifts \gamma in the observations, but not in the mocks.
layer = [{'r2train.r2gal.r2vz.r2_exp': x,
          'r2train.train_star_r2.r2vz.r2_exp': x} for x in gamma]


top.add_layer(layer)


def f_prepare(pipel):
    A = pipel.r2train.get_store()['r2pred']
    gc = pipel.r2train.galcat.result[['zs', 'mag_vis']]
    gc = gc.join(A)
    gc = gc[gc.mag_vis < 24.5]

    Nboot = pipel.r2train.config['Nboot']

    S = pd.Series()
    S['r2pred'] = gc[range(Nboot)].mean().mean()
    S['r2_exp'] = pipel.r2train.r2gal.r2vz.config['r2_exp']
    S['use_stars'] = pipel.r2train.config['use_stars']

    return S

def plot1():
    prepare = top.get_prepare(f_prepare)


    for key,sub in prepare.groupby('use_stars'):
        sub = sub.sort('r2_exp')

        lbl = 'Stars' if key else 'Galaxies'
        plt.plot(sub.r2_exp, sub.r2pred / 135., 'o-', label=lbl)

    plt.yscale('log')
    plt.legend()
    plt.show()

def plot2():
    # Ugly way of enforcing 
    for field in [True, False]:
        for key,pipel in top.iteritems():
            use_stars = pipel.r2train.config['use_stars']
            if not use_stars == field:
                continue

            r2train = pipel.r2train
            print('galaxies', r2train.r2gal.r2vz.config['r2_exp'])
            print('stars', r2train.train_star_r2.r2vz.config['r2_exp'])
            print('mock', pipel.r2train.train_gal_r2.r2vz.config['r2_exp'])

    #        ipdb.set_trace()

            store = pipel.r2train.get_store()
            gc = pipel.r2train.galcat.result[['zs', 'mag_vis']]
            gc = gc.join(store['r2pred'])
            gc = gc.join(store['r2true'], rsuffix='_true')

            gc = gc[gc.mag_vis < 24.0]
    #        gc = gc[gc[0] < 140]
            gc = gc[gc.zs < 1.0]
            store.close()
    #        ipdb.set_trace()

            lbl_star = 'Stars' if use_stars else 'Galaxies'
            gamma = pipel.r2train.r2gal.r2vz.config['r2_exp']
            lbl = '{0}, $\gamma$ = {1}'.format(lbl_star, gamma)

            K = {'histtype': 'step', 'bins': 150, 'normed': True}
    #        gc['0'].hist(label=lbl, **K)

            if use_stars:
                gc['0_true'].hist(label='True', **K)

    plt.legend()
    plt.show()

def get_cat(pipel):
    
    store = pipel.r2cal.get_store()
#    ipdb.set_trace()

#    store = pipel.r2train.get_store()
    gc = pipel.r2train.galcat.result #[['zs', 'mag_vis']]
#    gc = gc.join(store['r2eff'])
    gc = gc.join(store['/default']) # BAD HACK.
#    gc = gc.join(store['r2pred'])
#    gc = gc.join(store['r2true'], rsuffix='_true')
    store.close()

    gc = gc.join(pipel.r2train.galcat.magcat.mabs.result, rsuffix='_x')

    # Tryint to cut in color.
    gc['col_ri'] = gc.mag_r - gc.mag_i

#    gc = gc[(0.3 < gc.zs) & (gc.zs < 0.7)]

#    gc = gc[gc.col_ri > 0.5] #1.1]

#    gc = gc[gc.mag_vis < 23.0]
#    gc = gc[(0.3 < gc.zs) & (gc.zs < 0.7)]
#    gc = gc[(0.45 < gc.zs) & (gc.zs < 0.55)]
    gc = gc[(10 < gc.sed) & (gc.sed < 17)]
#    gc = gc[(20 < gc.sed) & (gc.sed < 30)]
#    gc = gc[(30 < gc.sed) & (gc.sed < 40)]


    return gc

# A bit ad hoc.
for f in range(len(per)):
    p0 = top[f,0]
    p1 = top[f,1]
    gamma0 = p0.r2train.r2gal.r2vz.config['r2_exp']
    gamma1 = p1.r2train.r2gal.r2vz.config['r2_exp']

    gc0 = get_cat(p0)
    gc1 = get_cat(p1)

#    ipdb.set_trace()

    K = {'histtype': 'step', 'bins': 450, 'normed': True}
    lbl = str(gamma0)

    field = 0
    X = (gc1[field] - gc0[field])

    print(gamma0, X.skew())
#    ipdb.set_trace()
    X.hist(label=lbl, **K)


plt.legend()
plt.show()
