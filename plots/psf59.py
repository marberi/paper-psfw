#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import splrep, splev

sed_dir = '/data2/photoz/run/data/seds'
gal_seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']

filter_dir = '/data2/photoz/run/data/des_euclid'
filters = ['r', 'i', 'z', 'vis']

lmb = np.linspace(3000., 12000., 10000)

def load_gal():
    spls = {}
    for sed in gal_seds:
        path = os.path.join(sed_dir, '{0}.sed'.format(sed))

        val = np.loadtxt(path).T

        spls[sed] = splrep(val[0], val[1])
   
    return spls 


def plot_gal(spls, seds):
    z = 1.5
    for sed in seds:
        y = splev((1+z)*lmb, spls[sed])
        norm = splev((1+z)*8000, spls[sed])
        y *= 0.2/norm

        plt.plot(lmb, y, lw=2,label=sed)

def plot_filters():

    for fname in filters:
        path = os.path.join(filter_dir, '{0}.res'.format(fname))

        x,y = np.loadtxt(path).T

        plt.plot(x, y, lw=2., label=fname)


def load_stars():
    star_dir = '/data2/photoz/run/data/seds_stars'

    spls = {}
    for fname in os.listdir(star_dir):
        if not fname.endswith('sed'):
            continue

        path = os.path.join(star_dir, fname)
        x,y = np.loadtxt(path).T

        sed = fname.replace('.sed', '')
        spls[sed] = splrep(x, y)

    return spls

def plot_stars(spls):
    seds = ['a7v']
    seds = list(spls.keys())[:5]
    seds = ['a7v', 'a0iii', 'o9v']
    for sed in seds: #spls.iteritems():
        spl = spls[sed]
        y = splev(lmb, spl)
        norm = splev(8000, spl)
        y *= 0.2 / norm

        plt.plot(lmb, y, label=sed)


plot_filters()
plot_gal(load_gal(), ['Ell_01', 'Irr_01'])

plt.ylim(0., 0.75)
plt.legend()
plt.show()

#    ipdb.set_trace()
