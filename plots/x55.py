#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import xdolphin as xd

import tosave

import trainpipel

def zband(has_z):
    pz_filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']
    pz_train = ['r','i','z']
    if not has_z:
        pz_filters = filter(lambda x: x!='z', pz_filters)
        pz_train = filter(lambda x: x!='z', pz_train)

    D = {'r2x.cal_pz.filters': pz_filters,
         'r2x.test_pz.filters': pz_filters,
         'r2x.filters': pz_train}

    return D



pipel = trainpipel.pipel()
pipel.config.update({\
  'r2x.test_pz.use_priors': False, # Test!
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.z_cal': 'zb',
  'r2x.z_test': 'zb'})
top = xd.Topnode(pipel)
#top.add_layer_key('r2x.filters', [['r','i'], ['r','i','z']])
top.add_layer(map(zband, [True, False]))


# The prepare function.
z = np.linspace(0.1, 2., 10)
df = pd.DataFrame()
for key,pipel in top.iteritems():
    cat = pipel.r2x.test_cat.result[['mag_vis']]
#    cat = cat.join(pipel.r2x.get_store()['r2eff'])
    cat = cat.join(pipel.r2x.test_pz.result)
    cat = cat[cat.mag_vis < 24.5]
    cat = cat[cat.mag_vis < 24.0]

    cat['dx'] = (cat.zb - cat.zs) / (1+cat.zs)
    cat['outl'] = 0.15 < cat.dx

    f_lbl = ','.join(pipel.config['r2x.filters'])
    
    for i,(k2,sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
        sig_68 = sub.dx.abs().describe(percentiles=[0.68])['68%']
        outl = sub.outl.mean()

        S = pd.Series({'sig_68': sig_68, 'outl': outl, 'z': sub.zs.mean(), \
                       'zbin': i, 'f_lbl': f_lbl})

        df = df.append(S, ignore_index=True)

# Two photo-z plots.
fig,A = plt.subplots(2, 1, sharex='col')
ax = A[0]

for key, sub in df.groupby('f_lbl'):
    sub = sub.sort('zbin')

    f_lbl = sub.irow(0).f_lbl
    lbl = 'All bands' if 'z' in f_lbl else 'No z-band'
    A[0].plot(sub.z, sub.sig_68, label=lbl)
    A[1].plot(sub.z, sub.outl, label=lbl)

for i,ax in enumerate(A):
    ax.grid(True)
    ax.legend(loc=0)

A[0].set_ylabel('${\sigma}_{68}$ / (1+z)', size=16)
A[1].set_ylabel('Outlier fraction', size=14)

A[1].set_xlabel('Redshift [zs]', size=14)

plt.savefig(tosave.path('x55'))
