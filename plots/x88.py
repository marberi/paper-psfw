#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

import xdolphin as xd
import trainpipel

import tosave
def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    def _add_offset(self, pipel):
        """Add a task for the offset."""

        offset = xd.Job('offset')
        offset.depend['magcat'] = pipel.r2x.test_cat
        pipel.r2x.depend['test_cat'] = offset

    def limit_seds_cal(self, x):
        D = {'r2x.cal_cat.magcat.mabs.limit_seds': x,
             'r2x.train_gal_cat.mabs.limit_seds': x}

        return D

    def limit_seds_data(self, x):
        D = {'r2x.test_cat.magcat.magcat.mabs.limit_seds': x}

        return D


    def _rem_type(self, i):
        def lim_list(A, x):
            return A[:x]+A[x+1:]

        only_seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
        sed_types =  ['ell', 'sp', 'sp', 'irr', 'irr', 'irr']

        D = {'only_seds': lim_list(only_seds, i),
             'sed_types': lim_list(sed_types, i)}


        return D        

    def rem_type(self, i):
#        D = {'r2x.cal_cat.magcat.mabs.limit_seds': x,
#             'r2x.train_gal_cat.mabs.limit_seds': x}

        L = ['r2x.cal_cat.magcat.mabs.{0}', 'r2x.train_gal_cat.mabs.{0}']
        D_in = self._rem_type(i)
        D = {}
        for x in L:
            for k,v in D_in.iteritems():
                key = x.format(k)
                D[key] = v

            D[x.format('limit_seds')] = True

        x = 'r2x.test_cat.magcat.magcat.mabs.{0}'
        D[x.format('limit_seds')] = True

        return D

    def topnode(self):
        pipel = trainpipel.pipel()

        # Deleting the photo-z dependencies. I need to run
        # with some large simulations..
        del pipel.r2x.depend['cal_pz']
        del pipel.r2x.depend['test_pz']

        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000
        pipel.r2x.test_cat.magcat.mabs.config['Ngal'] = 500000
#        ipdb.set_trace()

        pipel.config.update({\
          'r2x.use_cal': True,
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
#          'r2x.Nboot': 100,
          'r2x.Nboot': 2,
#          'r2x.use_stars': False,
          'r2x.filters': ['r','i','z']})

        self._add_offset(pipel)

        top = xd.Topnode(pipel)
        layer = [{}, self.limit_seds_cal(True),
                 self.limit_seds_data(True)] 

        X = self.limit_seds_cal(True)
        X.update(self.limit_seds_data(True))

        X = self.rem_type(5)

#        ipdb.set_trace()

        layer = [{}]
        lblL = ['Fiducial'] #, 'Test']
        seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
        for i,sed in enumerate(seds):
            if sed == 'Ell_01': continue

            layer.append(self.rem_type(i))
            lblL.append(sed)

        self.lblL = lblL


        top.add_layer(layer)



        return top

    def prepare(self, topnode):
        # For having the same Ell_01 "R^2_{\\rm{PSF}}" value for all the result.
        pipel = topnode.values()[0]
        ell_01 = pipel.r2x.train_gal_r2.r2vz.result['/vis/Ell_01'][0]

        field = 'r2pred'
        df = pd.DataFrame()
        for key, pipel in topnode.iteritems():
            # Not exactly pretty way of configuring the pipelines.
            pipel.config['r2x.use_stars'] = False
            cat = pipel.r2x.get_store()[field]

            pipel.config['r2x.use_stars'] = True
            cat = cat.join(pipel.r2x.get_store()[field], rsuffix='_stars')


            Nboot = pipel.r2x.config['Nboot']
#            cat[range(Nboot)] /= ell_01

            col_gal = range(Nboot)
            col_stars = map('{0}_stars'.format, range(Nboot))
            sub_stars = cat[col_stars]
            sub_stars.columns = col_gal
            sub_gal = cat[col_gal]
            sub_gal.columns = col_gal

            diff = (sub_stars - sub_gal) / ell_01
            galcat = pipel.r2x.test_cat.result

            # First here we have the actual catalog.
            cat = galcat.join(diff)
            cat = cat[cat.mag_vis < 24.5]

            z = np.linspace(0.1, 1.8, 10)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            part = pd.DataFrame()
            part['r2pred'] = mean[col_gal].mean(axis=1)
            part['r2pred_std'] = np.sqrt(mean[col_gal].var(axis=1))
            part['zs'] = mean.zs
            part['zbin'] = range(len(part))

            part['pipel'] = key[0]

            df = df.append(part, ignore_index=True)

        return df

    def plot(self, prepare):

#        lblL = ['Fiducial', 'Limit SEDs (Cal)', 'Limit SEDs (Data)']
#, 'r-band offset(0.01)', 'i-band offset(0.01)', 'z-band offset(0.01)',
#                '$\gamma$ offset(1%)']

        # Cutting away some of the noisy values.
        prepare = prepare[prepare.r2pred_std < 0.001]

        prepare = prepare.sort('pipel')
        styles = gen_styles()
        for key,sub in prepare.groupby('pipel'):
            ls = next(styles)

            sub = sub.sort('zbin')
            plt.errorbar(sub.zs, sub.r2pred, sub.r2pred_std,
                         lw=2., ls=ls, label=self.lblL[key])

        plt.grid(True)
        plt.ylim((-0.002, 0.002))

        plt.xlabel('Redshift [zs]', size=14)
        plt.ylabel('[<$"R^2_{\\rm{PSF}}"$(Train stars) - <$"R^2_{\\rm{PSF}}"$(Train gal)]/ $"R^2_{\\rm{PSF}}"$(Ell_01)', size=14)
#        plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,2), useOffset=False)
        ax = plt.gca()

        handles, labels = ax.get_legend_handles_labels()
        handles = [x[0] for x in handles]

        ax.legend(handles, labels, loc=2, prop={'size': 12})
#        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.1e'))

        plt.ylim((-0.0005, 0.0012))
        plt.subplots_adjust(left=0.15)
        plt.savefig(tosave.path('x88'))

        plt.ylim((-0.001, 0.001))
#        plt.show()


    def run(self):
        topnode = self.topnode()
        prepare = self.prepare(topnode)
        self.plot(prepare)

if __name__ == '__main__':
    Plot().run()
