#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import imp
import os

# yes.. this should be improved.
d = '/home/marberi/drv/work/papers/psfw/'
path = os.path.join(d, 'mylinks')

def find_inuse():
    """Parse the mylinks file to find out which plots
       that is in use.
    """

    inuse = []
    for line in open(path).readlines():
        if line.startswith('#'):
            continue

        spl = line.strip().split(' ')
        if not len(spl) == 3:
            continue

        print('found:', spl[-1])
        inuse.append(spl[-1])

    return inuse

def get_class(fig_name):
    pl = fig_name
    mod = imp.load_module(pl, *imp.find_module(pl))
    cls = mod.Plot

    return cls

def find_topnodes(inuse):
    topD = {}
    for pl in inuse:
        print('checking:', pl)

        mod = imp.load_module(pl, *imp.find_module(pl))
        inst = mod.Plot()
        if not hasattr(inst, 'topnode'):
            continue

        topD[pl] = inst.topnode()

#    ipdb.set_trace()
    return topD
