#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

import trainpipel 

pipel = trainpipel.pipel()


gc = pipel.depend['galcat']['magcat']['cat']
gc = gc.join(pipel.depend['r2gal']['r2eff']['r2eff'])

sc = pipel.depend['starnoise']['magcat']['cat']
sc = sc.join(pipel.depend['r2star']['r2eff']['r2eff'])

A = sc.r2eff.unique()
A.sort()

N = np.digitize(gc.r2eff.values, A)
for i in [-2, -1, 0, 1]:
    B = (A[N+i] - gc.r2eff) / gc.r2eff
    B.hist(bins=100, histtype='step', normed=True, label=str(i))

plt.legend()
plt.show()

#ipdb.set_trace()

