#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import statsmodels.api as sm
from sklearn import linear_model

import psf4
import tosave

class Plot(psf4.Plot):
    """Test the "R^2_{\\rm{PSF}}" expansion in the paper."""

    def run(self):
        #pzL = np.logspace(-2.3, -1.)
        N = int(5e5)
        pzL = np.logspace(-3.3, -1.3)
        df = self._load('pz', pzL, N)


        mean = df.groupby('pz').mean()
        var = df.groupby('pz').var()
        mean['r2bias'] = mean.Ell_01.abs()
        mean = mean.reset_index()



        low = mean[mean.pz < 0.02]
        high = mean[mean.pz > 0.02]
        p_quad = np.polyfit(low.pz, low.r2bias, 2)
        p_lin = np.polyfit(high.pz, high.r2bias, 1)
        mean['yfit_lin'] = np.polyval(p_lin, mean.pz)
        mean['yfit_quad'] = np.polyval(p_quad, mean.pz)

        low = mean[mean.pz < 0.02]
        high = mean[mean.pz > 0.02]

        plt.plot(mean.pz, mean.r2bias, label='Data')
        plt.plot(low.pz, low.yfit_quad, 'r-', label='Fit quad')
        plt.plot(low.pz, low.yfit_lin, 'g--', label='Fit lin')
        plt.plot(high.pz, high.yfit_quad, 'r-')
        plt.plot(high.pz, high.yfit_lin, 'g--')

        plt.axvline(0.02, -10, 10, color='black', lw=2, alpha=0.5)

        plt.grid(True)
        plt.xlabel('$\sigma_z$/(1+z)', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.legend()
#        plt.show()
        plt.savefig(tosave.path('psf86'))

if __name__ == '__main__':
    Plot().run()
