#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import copy
import sys
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

def gen_styles():
    i = 0
    styles = ['-','-.','--', ':']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    """Study how the PSF recovery and photo-z reacts to the noise."""

    name = 'psf22'

    def zband(self, has_z):
        pz_filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']
        pz_train = ['r','i','z']
        if not has_z:
            pz_filters = filter(lambda x: x!='z', pz_filters)
            pz_train = filter(lambda x: x!='z', pz_train)

        D = {'r2x.cal_pz.filters': pz_filters,
             'r2x.test_pz.filters': pz_filters,
             'r2x.filters': pz_train}


        return D

    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()

#        ipdb.set_trace()

        pipel.config.update({\
          'r2x.Ntrain': 400,
          'r2x.Nboot': 10,
#          'r2x.filters': ['r','i','z']})
          'r2x.z_cal': 'zb',
          'r2x.z_test': 'zb'})
#          'r2x.filters': ['r','i']})

#        # More testing
#        pipel.config['r2x.Nboot'] = 3
 
        layers = xd.Layers()
        layers.add_pipeline(pipel)

        R = [0.5, 1, 1.2, 1.5, 1.7, 2, 3, 4, 5, 6, 7, 8, 9]
        R = [0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 1, 1.2, 2, 10]
        R = [0.3, 0.4, 0.5, 0.7, 0.8, 1, 1.2, 2, 10]

        layer = [{'r2x.test_cat.r': x, \
                  'r2x.cal_cat.r': x, \
                  'r2x.train_star_cat.magcat.r': x}
                  for x in R]

        layers.add_layer('R', layer)
        layers.add_layer_key('use_stars', 'r2x.use_stars', [True, False])
        layers.add_layer('use_zband', map(self.zband, [True, False]))


        topnode = layers.to_topnode()

        return topnode

    def f_reduce(self, pipel):
        cat = pipel.r2x.test_cat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat.join(pipel.r2x.test_pz.result.unstack())
        pzcat = pipel.r2x.test_pz.galcat.magcat.mabs.result
        cat['zs'] = pzcat.zs

        cat['dx'] = (cat.zb - cat.zs) / (1+cat.zs)
        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2x.config['Nboot']
        S = pd.Series()
        S['r2bias'] = cat[range(Nboot)].mean().mean()
        S['sig68'] = cat.dx.abs().describe(percentiles=[0.68])['68%']

        # Ok, this could have been set through the labels (not tested)
        S['Rval'] = pipel.r2x.test_cat.config['r']
#        ipdb.set_trace()

        # Ok, this is only for testing...
        from matplotlib import pyplot as plt
        z = np.linspace(0., 2., 10)
        mean = cat.groupby(pd.cut(cat.zb, z))[range(Nboot)].mean()

        return S

    def plot(self, prepare):
        # Should be set earlier....
        prepare.index.set_names('stat', level=1, inplace=True)
        #print('time in prepare step', t2-t1)

        order = [('r,i,z', False), ('r,i', False),
                 ('r,i,z', True), ('r,i', True)]

        prepare['lbl_stars'] = np.where(prepare.use_stars, 'Star train', 'Gal train')

        labels = {}
        labels['use_stars'] = {True: 'Star train', False: 'Gal train'}
        for f_lbl, use_stars in order:
            use_zband = 'z' in f_lbl

            line = prepare[(prepare.use_zband == use_zband) & (prepare.use_stars == use_stars)]
            line = line.reset_index().sort_values('R')

            line = line.pivot('R', 'stat', 0)

            lbl_stars = labels['use_stars'][use_stars]
            lbl = '{0}: {1}'.format(f_lbl, lbl_stars)

            color = 'blue' if f_lbl=='r,i,z' else 'red'
            marker = 'o' #if f_lbl=='r,i,z' else '^'
            ls = '-' if not use_stars else '--'

            plt.plot(line.r2bias, line.sig68, marker+ls, color=color, lw=1.5, label=lbl)

        ax = plt.gca()

        for_text = prepare[(prepare.use_zband == 1.) & (prepare.use_stars == 1.)]
        for_text = for_text.reset_index().pivot('R', 'stat', 0)

        yline = for_text.loc[1.].sig68
        ax.axhline(yline, lw=3, color='b', alpha=0.2)

#        ipdb.set_trace()

        for key, line in for_text.iterrows():
            R = line.Rval
            if R < 0.3: continue

            plt.text(-7e-4, line.sig68, 'r = {0}'.format(R), fontsize=13,
                     verticalalignment='center', weight='bold')
            

        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlim(-0.001, 0.001)
        plt.ylim(0., 0.12)
        ax.set_xlabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)
        ax.set_ylabel('${\sigma}_{68}$', size=14)

        plt.legend(loc=4, prop={'size': 12})

        yline = np.linspace(0., 5.)
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='black', alpha=0.1)

if __name__ == '__main__':
    xd.plot(Plot).save()
