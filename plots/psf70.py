#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    """Histogram form r2sed and r2pz."""

    def topnode(self):
        import trainpipel
        top = xd.Topnode(trainpipel.pipel())

        return top

    def show(self, topnode):
        pipel = topnode.values()[0]

        # This might actually fail.. then I need to fix something
        # more fundamental.
        r1 = pipel.depend['r2pz.pzcat.galcat'].taskid()
        r2 = pipel.depend['r2sed.test_cat'].taskid()

        assert r1 == r2, 'Using two different galaxy catalogs in different pipelines.'

        gc = pipel.r2pz.pzcat.galcat.result
        gc = gc.join(pipel.r2pz.result)
        gc = gc.join(pipel.r2sed.result)

        gc = gc[gc.mag_vis < 24.5]

#        gc = gc[1.5 < gc.zs]

        K = {'bins': 100, 'histtype': 'step', 'normed': True}
        gc.r2peak.hist(label='SED fit', **K)
        gc.r2eff.hist(label='SED train', **K)

        x = 0.025
        plt.xlim((-x, x))

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16)
        plt.legend()
        plt.show()

    def run(self):
        topnode = self.topnode()
        self.show(topnode)

if __name__ == '__main__':
    Plot().run()
