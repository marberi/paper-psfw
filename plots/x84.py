#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
import time
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

class Plot:
    """Main table with the PSF bias when training on galaxies and stars
       for different filter configurations. Previously in psf10 and psf12.
    """


    all_filters = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

    def _rows(self):
        I1 = [['r', 'i', 'z'],
              ['r', 'i'],
              ['i', 'z']]


        I2 = [['vis', 'r', 'i', 'z'],
              ['vis', 'r', 'i'],
              ['vis', 'i', 'z']]


        I3 = [['r', 'i', 'zx'],
              ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']]

#              ['r', 'i'],
#              ['i', 'z']]
#        I2 = [['vis', 'i', 'z'],
#              ['vis', 'r', 'i', 'z'],
#              ['vis', 'r', 'i']
#             ]

#        I3 = [['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H'],
#              ['g', 'r', 'i', 'z',        'Y', 'J', 'H'],
#              ['g',      'i', 'z', 'vis', 'Y', 'J', 'H'],
#              ['g', 'r', 'i',      'vis', 'Y', 'J', 'H'],
#              ['g', 'r', 'i',             'Y', 'J', 'H']
#             ]
#
#        I = I1 + I2 + I3
#
#        I = [I[0]] # For testing.
#        I = I3
        I = I1 + I2 + I3
#        I = I3

        # For testing.
        I = [I[0]]


        return I

    def topnode(self):

        import trainpipel
        pipl = trainpipel.pipel()

        # move this.
        pipl.r2train.galcat.magcat.mabs.config['Ngal'] = int(1e6)
        top = xd.Topnode(pipl)
#        top.rec_apply(zx_mods)

#        ipdb.set_trace()
        
        top.update_config({\
          'r2train.Ntrain': 1000,
          'r2train.Nboot': 100,
#          'r2train.Ntest': 540000,
          'r2train.all_test': True,
          'galnoise.const_sn': True,
          'galnoise.sn': 1000000,
          'starnoise.const_sn': True,
          'starnoise.sn': 1000000})


        top.add_layer_key('r2train.filters', self._rows())

#        layer = [{'r2train.use_stars': False, 'r2train.Ntrain': 10000}, \
#                 {'r2train.use_stars': True, 'r2train.Ntrain': 1000}]

        layer = [{'r2train.use_stars': False, 'r2train.Ntrain': 5000}, \
                 {'r2train.use_stars': True, 'r2train.Ntrain': 400}]

#        layer = [layer[0]]
#        top.add_layer_key('r2train.use_stars', [False, True])
        top.add_layer(layer)
#        top.update_config(self.flatten_conf(config['myconf']))

        noise = {
          'galnoise.const_sn': False,
          'starnoise.const_sn': False,
          'starcatN.norm_mag': 10}
        top.add_layer([{}, noise])

        return top

    def prepare_f(self, pipel):
        print('Staring prepare')
        tbl_data = pd.DataFrame()
        levels = ['filters', 'stars']
        perc = [.68, .95]

        mag_cat = pipel.galnoise.result[['mag_vis', 'zs']]
        r2eff = pipel.r2train.get_store()['r2eff'] 

        N = r2eff.shape[1]
        cat = r2eff.join(mag_cat)
        cat = cat[cat.mag_vis < 24.5]

        X = cat[range(N)].mean().abs()
        
        S = X.describe(percentiles=perc)

        # Should be moved.
        S['r2train.use_stars'] = pipel.r2train.config['use_stars']
        S['r2train.filters'] = pipel.r2train.config['filters']
        S['has_noise'] = not pipel.config['galnoise.const_sn']

        print('Finish prepare')
        return S


    def row_name(self, filters):
        """Because one need special configuration for the filters."""

        fspl = filters
        if len(fspl) <= 4:
            return ','.join(filters)

        diff = list(set(self.all_filters) - set(fspl))
        if diff:
            return 'All - {0}'.format(','.join(diff))
        else:
            return 'All'

    def create_tbl(self, prepare):
        prepare['filters'] = map(self.row_name, prepare['r2train.filters'])
        prepare['filter_ind'] = [x[-1] for x in prepare.index]

        tbl = pd.DataFrame()

        prepare = prepare.sort('filter_ind')
        for key, sub in prepare.groupby('filter_ind'): 
            row = pd.Series()
            row['Filters'] = sub.irow(0).filters
            for lbl, noise, stars in [('Gal', False, False), ('Star', False, True),
                                      ('Gal-noisy', True, False), ('Star-noisy', True, True)]:

                A = sub[(sub.has_noise == noise) & (sub['r2train.use_stars'] == stars)]

                # This is actually not used anywhere.           
                assert len(A) == 1
                row[lbl] = A.irow(0)['mean']

            row.name = key 
            tbl = tbl.append(row)

        return tbl


    def run(self):

        topnode = self.topnode()
        prepare = topnode.get_prepare(self.prepare_f)
        tbl = self.create_tbl(prepare)

        latex = tbl.to_latex(float_format='{0:.1e}'.format, index=False)

        print(latex)

    def fast(self, top):
        top.update_config({\
          'r2train.Ntrain': 100,
          'r2train.Nboot': 4,
          'r2train.galcat.magcat.mabs.Ngal': 1000})

if __name__ == '__main__':
    Plot().run()
