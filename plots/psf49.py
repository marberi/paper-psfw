#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
from matplotlib import pyplot as plt

import psf10
import psf12

class psf49:
    name = 'psf49'
    config = {}

    def gauss1(self):
        x1 = 5.
        x2 = 7.

        N = 100000
        r1 = np.random.normal(scale=x1, size=N)
        r2 = np.random.normal(scale=x2, size=N)

        R = x1**2 + x2**2

        print('var', R)
        print('var[x1+x2]', np.var(r1+r2))
        print('var[x1-x2]', np.var(r1-r2))


#        ipdb.set_trace()

        import sys
        sys.exit(1)

    def gauss2(self):
        x1 = 5.
#        x2 = 7.

        N = 1000
        r1 = np.random.normal(scale=x1, size=N)
        r2 = x1*np.random.normal(size=N)

        s1 = pd.Series(r1)
        s2 = pd.Series(r2)


        ipdb.set_trace()


    def d1(self, B):
#        magcat = B['noisy']['galnoise']['magcat']['cat']

        from sklearn import svm
        reg = svm.NuSVR()

        dep = B['noisy']['r2train'].depend
        starcat = dep['starcat']['magcat']['cat']

        fL = ['r', 'i', 'z']
        cols = map('mag_{0}'.format, fL)

        # Doing the fitting....
        At_mag = starcat[cols].values
        At = At_mag[:,:-1] - At_mag[:,1:]
        r2star = dep['r2star']['r2eff']['r2eff']

        inds = np.arange(len(r2star))
        inds = np.random.permutation(inds)

        N = 400
        inds = inds[:N]

        reg.fit(At[inds], r2star.ix[inds,0])

        cat = B['noisy']['galnoise']['magcat']['cat']

        Ap_mag = cat[cols].values
        Ap = Ap_mag[:,:-1] - Ap_mag[:,1:]


        r2gal = dep['r2gal']['r2eff']['r2eff']
        err_cat = cat[['err_{0}'.format(x) for x in fL]]
        var_cat = (err_cat**2).values
        err_col = np.sqrt(var_cat[:,:-1] + var_cat[:,1:])

#        ans = reg.predict(Ap)
        k = 0.1
        Nrand = 10
#        Nrand = 2
        K = np.zeros((len(r2gal), Nrand))
    
        for i in range(Nrand):
            print('i', i)

            delta_col = err_col*np.random.normal(size=err_col.shape)
            K[:, i] = reg.predict(Ap + k*delta_col)


        r2true = r2gal['r2eff'].values
        r2eff = ((K.T - r2true) / r2true).T
        r2eff = pd.DataFrame(r2eff)

        r2pred_nn = reg.predict(Ap)
        r2eff_nn = (r2pred_nn - r2true) / r2true

#        L = K.var(axis=1) / K.mean(axis=1)
        df = pd.DataFrame()
        df['one'] = r2eff[0]
        df['corr'] = r2eff.mean(axis=1)
        df['orig'] = r2eff_nn

        df = df.join(cat)
        df = df[df.mag_vis < 24.5]

        zs = np.linspace(0.1, 1.5, 10)
        K = df.groupby(pd.cut(cat.zs, zs)).mean()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.plot(K.zs, K.orig, 'o-', label='Orig')
        plt.plot(K.zs, K.one, 'o-', label='One')
        plt.plot(K.zs, K['corr'], 'o-', label='Corr')

        plt.legend()
        plt.show()

        ipdb.set_trace()



        plt.show()

    def d2(self, B):
        cat = B['noisy']['galnoise']['magcat']['cat']
        cat = cat[cat.mag_vis < 24.5]

        from scipy.stats import gaussian_kde

        fL = ['r', 'i', 'z']
        col1 = map('mag_{0}'.format, fL)
        col2 = map('err_{0}'.format, fL)

        N = 1000
        inds = np.arange(len(cat))
        inds = np.random.permutation(inds)[:N]

        kde = gaussian_kde(cat.ix[cat.index[inds], col1].T)
        #kde = gaussian_kde(cat[col1].T)

        err = cat[col2]
        k = 0.2

        mag1 = cat[col1]
#        mag2 = mag1 + err*np.random.normal(size=err.shape)
        mag2 = mag1 + k*err.values*np.random.normal(size=err.shape)

#        ipdb.set_trace()
        import time
        print('starting kde')
        t1 = time.time()
        d1 = kde.evaluate(mag1[:10000].T)
        d2 = kde.evaluate(mag2[:10000].T)
        dt1 = time.time() - t1

        print('time', dt1)
        if False:
            R = pd.Series(d2 / d1)
            R = R[R < 10]
    #        ipdb.set_trace() 
            R.hist(bins=100, histtype='step', normed=True)

            plt.axvline(1., lw=3, color='k', alpha=0.5)
            plt.xlabel('Weight [p(x)/p(y)]', size=16)
            plt.ylabel('Probability', size=16)
            plt.xlim(0., 5)
            plt.show()

        if False:
            pd.Series(d1).hist(bins=100, label='Orig', histtype='step', normed=True)
            pd.Series(d2).hist(bins=100, label='Add noise', histtype='step', normed=True)
            plt.xlabel('Density', size=16)
            plt.ylabel('Probability', size=16)

            plt.legend()
            plt.show()

        plt.plot(d1, d2, '.')
        plt.show()

        ipdb.set_trace()


    def d3(self, B):
#        magcat = B['noisy']['galnoise']['magcat']['cat']

        from sklearn import svm
        reg = svm.NuSVR()

        dep = B['noisy']['r2train'].depend
        starcat = dep['starcat']['magcat']['cat']

        fL = ['r', 'i', 'z']
        cols = map('mag_{0}'.format, fL)

        # Doing the fitting....
        At_mag = starcat[cols].values
        At = At_mag[:,:-1] - At_mag[:,1:]
        r2star = dep['r2star']['r2eff']['r2eff']

        inds = np.arange(len(r2star))
        inds = np.random.permutation(inds)

        N = 400
        inds = inds[:N]

        reg.fit(At[inds], r2star.ix[inds,0])

        cat = B['noisy']['galnoise']['magcat']['cat']

        Ap_mag = cat[cols].values
        Ap = Ap_mag[:,:-1] - Ap_mag[:,1:]

        # Estimate the color errors.
        r2gal = dep['r2gal']['r2eff']['r2eff']
        err_cat = cat[['err_{0}'.format(x) for x in fL]]
        var_cat = (err_cat**2).values
        err_col = np.sqrt(var_cat[:,:-1] + var_cat[:,1:])

        # Use a density estimator to determine the relative
        # densities in color space.
        N = 1000
        inds = np.arange(len(Ap))
        inds = np.random.permutation(inds)[:N]

        from scipy.stats import gaussian_kde

        kde = gaussian_kde(Ap[inds].T)

#        ipdb.set_trace()
#        kde = gaussian_kde(cat.ix[Ap.index[inds], cols].T)

#        ans = reg.predict(Ap)
        k = 1.
        Nrand = 5

        # Currently the density ratio is stored separately..
        K = np.zeros((len(r2gal), Nrand))
        L = np.zeros((len(r2gal), Nrand))
    
        for i in range(Nrand):
            print('i', i)

            delta_col = err_col*np.random.normal(size=err_col.shape) + 0.02  # HACK
            col_real = Ap + k*delta_col

            L[:, i] = kde.evaluate(col_real.T)
            K[:, i] = reg.predict(col_real)


#        ipdb.set_trace()


        r2true = r2gal['r2eff'].values
        r2eff = ((K.T - r2true) / r2true).T
        r2eff = pd.DataFrame(r2eff)


        # Weighting based on the density.
        L_orig = kde.evaluate(Ap.T)
        weight = (L.T / L_orig).T

        # Weighting the outliers with zero..
        weight[3 < weight] = 0.

        r2pred_w = (K*weight).mean(axis=1) / weight.mean(axis=1)
        r2eff_w = (r2pred_w - r2true) / r2true

        r2pred_nn = reg.predict(Ap)
        r2eff_nn = (r2pred_nn - r2true) / r2true

#        ipdb.set_trace()

#        L = K.var(axis=1) / K.mean(axis=1)
        df = pd.DataFrame()
        df['one'] = r2eff[0]
        df['corr'] = r2eff.mean(axis=1)
        df['orig'] = r2eff_nn
        df['weight'] = r2eff_w


        ipdb.set_trace()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        df = df.join(cat)
        df = df[df.mag_vis < 24.5]

        zs = np.linspace(0.1, 1.5, 10)
        K = df.groupby(pd.cut(cat.zs, zs)).mean()


        plt.plot(K.zs, K.orig, 'o-', label='Orig')
        plt.plot(K.zs, K.one, 'o-', label='One')
        plt.plot(K.zs, K['corr'], 'o-', label='Corr')
        plt.plot(K.zs, K['weight'], 'o-', label='Weight')

        plt.legend()
        plt.show()


    def run(self):


        inst = psf10.psf10()
#        inst = psf12.psf12()
#        topnode = inst.create_topnode()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['use_stars'] = True
#            A['r2train'].config['Nboot'] = 200 #use_stars'] = True
            A['r2train'].config['Nboot'] = 30 #use_stars'] = True

#            A['galabs'].config['Ngal'] = 1000000 #2e6 #200007
#            A['r2train'].config['Ntest'] = 1000000
#            ipdb.set_trace()

#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['Ntest'] = 200000
            if lbl == 'noiseless':
                A['galabs'].config['Ngal'] = 200008

            B[lbl] = A


        self.d3(B)

if __name__ == '__main__':
    Job('psf49').run()

