#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import sys

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

from xdolphin import Job
import xdolphin as xd

import tosave

import matplotlib as mpl

mpl.rcParams['savefig.directory'] = '../run'
mpl.rcParams['savefig.format'] = 'pdf'
#ipdb.set_trace()

def find_seds():
    """Read in the list of all SEDs."""

    fjc_file = '/home/marberi/drv/work/papers/psfw/data/fjc_temp'
    seds = map(str.strip, open(fjc_file).readlines())

    return seds

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    def topnode(self):
        import pzpipel

        xtop_base = xd.Topnode(pzpipel.pipel())

        xtop_base.update_config({\
          'r2pz.pzcat.galcat.not_scale': ['Y', 'J', 'H'],
          'r2pz.pzcat.dz': 0.001,
          'r2pz.pzcat.out_pdf': False,
          'r2pz.pzcat.out_pdftype': False,
          'r2pz.pzcat.use_priors': False,
          'r2pz.pzcat.zmax': 5.,
          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 10002,
          'r2pz.use_ml': False
        })



        # The x-axis and panel split.

#        R = [0.5, 1, 2,3,4, 5,7, 10, 20, 50, 70, 100]
        R = [0.5, 0.6, 0.7, 0.75, 0.8, 0.9, 1, 2,3,4, 5,7, 10, 20, 50, 70, 100]
#        R = [1, 10] # FAST... To see if things still works.

        R = [1]

        xtop_base.add_layer_key('r2pz.pzcat.galcat.magcat.mabs.limit_seds', [True])
        xtop_base.add_layer_key('r2pz.pzcat.galcat.r', R)
#        xtop = xtop_base.clone()
        xtop2 = xtop_base.clone()

        # The normal lines.
        all_filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']
        #xtop.add_layer_key('r2pz.pzcat.filters', [['r', 'i', 'z'], all_filters])
#        xtop.add_layer_key('r2pz.pzcat.filters', [['r', 'i', 'z']])
#        xtop.add_layer_key('r2pz.pzcat.use_priors', [ False])

        # The 65 SED configurations.
        xtop2.add_config('r2pz.pzcat.seds', find_seds())
#        xtop2.add_layer_key('r2pz.pzcat.filters', [['r', 'i', 'z'], all_filters])
#        xtop2.add_config('r2pz.pzcat.use_priors', False)

        xtop2.add_layer_key('r2pz.pzcat.filters', [['r', 'i', 'z']])
        xtop2.add_layer_key('r2pz.pzcat.use_priors', [ False])


        return xtop2

    def plot(self, top):
        assert len(top.keys()) == 1, 'Something went wrong in the configuration'
        pipel = top.values()[0]

        ipdb.set_trace()


    def run(self):
        topnode_in = self.topnode()

        self.plot(topnode_in)



if __name__ == '__main__':
    Plot().run()
