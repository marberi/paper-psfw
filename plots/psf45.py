#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from xdolphin import Job

import psf10
import psf12

class psf45:
    name = 'psf45'
    config = {}

    def d1(self, B):

        norm = 137
        for lbl, A in B.iteritems():
            r2true = A['r2train']['r2train']['r2true']
            r2pred = A['r2train']['r2train']['r2pred']
            r2eff = A['r2train']['r2train']['r2eff']

            n = 1
#            for n in [0,1,2,4,5,6,7, 20,30,40,50]:
            for n in [0,1]: #,2,4,5,6,7, 20,30,40,50]:

                rvals = r2true[n]
                rvals = r2pred[n]
                rbins = np.linspace(np.min(rvals), np.max(rvals), 25)
                rmean = 0.5*(rbins[:-1] + rbins[1:])      

                E = r2true[n].groupby(pd.cut(rvals, rbins)).mean()

                plt.plot(rmean / norm, E/norm, 'o-')

        ipdb.set_trace()

        x0, x1 = 0.93, 1.06
        x = np.linspace(x0, x1)
    
        plt.plot(x, x, '-')
        plt.xlim(x0, x1)
        plt.ylim(x0, x1)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ Pred', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ True', size=16)

#        E.plot()
        plt.show() 
 
        ipdb.set_trace()

    def d3(self, B):
        for lbl, val in B.iteritems():
            magcat = val['galnoise']['magcat']['cat']
            cat = magcat.join(val['r2train']['r2train']['r2eff'])
            cat = cat[cat.mag_vis < 24.5]

            sub = cat[range(val['r2train'].config['Nboot'])]
            #ipdb.set_trace()

            nbins = 100
            sub.mean(axis=1).hist(label=lbl, histtype='step', bins=nbins, normed=True)
            sub[0].hist(label=lbl, histtype='step', bins=nbins, normed=True)

        plt.xlim(-0.03, 0.03)
        plt.legend()
        plt.show()

        import sys
        sys.exit()

        ipdb.set_trace()

    def d4(self, B):

        E1_t = B['noiseless']['r2train']['r2train']['r2true']
        E1 = B['noiseless']['r2train']['r2train']['r2pred']
        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2 = B['noisy']['r2train']['r2train']['r2pred']

#        ids = E2[0]
#        rvals = [135,135.1]

        for i,rx in enumerate([136, 137, 138]):
            ids = (rx < E1[0]) & (E1[0] < rx+0.1)
            E1_t[0][ids].hist(bins=20, label='Bin: {0}'.format(i), histtype='step')

            ids = (rx < E2[0]) & (E2[0] < rx+0.1)
            E2_t[0][ids].hist(bins=20, label='Noisy, Bin: {0}'.format(i), histtype='step')

        
        plt.legend()
        plt.show()

    def d5(self, B):
        from scipy.stats import gaussian_kde

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2 = B['noisy']['r2train']['r2train']['r2pred']

        inds = ~np.isnan(E2[0])
        data = np.array([E2[0][inds], E2_t[0][inds]])
        kde = gaussian_kde(data)

        rvals = np.linspace(132, 140)
        X = 137*np.ones(50)
        points = np.vstack([X, rvals])

        # The Gaussian KDE fit.
        HACK = 7
        Y = kde(points)
        plt.plot(rvals, HACK*Y)

        tosel = (137 < E2[0]) & (E2[0] < 137.1)
        E2_t[0][tosel].hist(histtype='step', normed=True)
  
        plt.xlabel('$"R^2_{\\rm{PSF}}"$ True', size=16)
        plt.ylabel('Probability', size=16)

        plt.axvline(137, lw=3, alpha=0.5) 
#        ipdb.set_trace()

        plt.show()

#        ipdb.set_trace()

    def d6(self, B):
        from scipy.stats import gaussian_kde

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2 = B['noisy']['r2train']['r2train']['r2pred']


        rvals = np.linspace(132, 140)
        rvals_sub = rvals[10:-10]

        plt.plot(rvals_sub, rvals_sub, '-')

        for n in [0,1,2,3]:
            inds = ~np.isnan(E2[n])
            data = np.array([E2[n][inds], E2_t[n][inds]])
            kde = gaussian_kde(data)

            # Implementation for simplicity over performance!
            corrL = []
            for rx in rvals_sub:
                print('rx', rx)
                X = rx*np.ones(50)
                points = np.vstack([X, rvals])
                y = kde(points)
                corrL.append((y*points[1]).sum() / y.sum())

            plt.plot(rvals_sub, corrL, 'o-')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.show()

    def d7(self, B):
        from scipy.stats import gaussian_kde

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2 = B['noisy']['r2train']['r2train']['r2pred']

        N = 200

        rvals = np.linspace(130, 145, N) #100)
        rvals_sub = rvals[10:-10]

#        plt.plot(rvals_sub, rvals_sub, '-')

        n = 0

        inds = ~np.isnan(E2[n])
        data = np.array([E2[n][inds], E2_t[n][inds]])
        kde = gaussian_kde(data)

        # Implementation for simplicity over performance!
        corrL = []
        for rx in rvals_sub:
            print('rx', rx)
            X = rx*np.ones(len(rvals))
            points = np.vstack([X, rvals])
            y = kde(points)
            corrL.append((y*points[1]).sum() / y.sum())


        from scipy.interpolate import splrep, splev

        spl = splrep(rvals_sub, corrL)

        A = splev(E2, spl)
        A = pd.DataFrame(A)

        r2eff = (E2 - E2_t) / E2_t
        R = (A - E2_t) / E2_t

        # First plot..
        if False:
            nbins = 30
            r2eff.mean().hist(label='Original', bins=nbins, histtype='step', normed=True)
            R.mean().hist(label='Corrected', bins=nbins, histtype='step', normed=True)

            plt.legend()
            plt.show()

        Nboot = r2eff.shape[1]
        magcat = B['noisy']['galnoise']['magcat']['cat']
        r2eff = r2eff.join(magcat)
        R = R.join(magcat)

        zbins = np.linspace(0.1, 1.5, 10)
        df = pd.DataFrame()
        for lbl,cat in [('Orig', r2eff), ('Corr', R)]:
            for key, val in cat.groupby(pd.cut(cat.zs, zbins)):
                zs = val.zs.mean()
                r2bias = val[range(Nboot)].mean().abs().describe(percentiles=[0.68])['68%']

                df = df.append(pd.Series({'lbl': lbl, 'zs': zs, 'r2bias': r2bias}), ignore_index=True)

        for key, val in df.groupby('lbl'):
            plt.plot(val.zs, val.r2bias, 'o-', label=val.irow(0).lbl)

        plt.legend()
        plt.show()

    def d8(self, B):
#        from scipy.stats import gaussian_kde

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2_e = B['noisy']['r2train']['r2train']['r2eff']
        E2_p = B['noisy']['r2train']['r2train']['r2pred']

        F = pd.DataFrame()
        F['r2eff'] = E2_e[0]
        F['r2true'] = E2_t[0]
        F['r2pred'] = E2_p[0]

        magcat = B['noisy']['galnoise']['magcat']['cat']
        F['zs'] = magcat['zs']

        zsL = np.linspace(0.1, 1.5, 5)
        rvals = np.linspace(132, 140, 25)

        df = pd.DataFrame()
        for key, val in F.groupby(pd.cut(F.zs, zsL)):
            zs = val.zs.mean()
            for k2,v2 in val.groupby(pd.cut(val.r2pred, rvals)):
                r2true = v2.r2true.mean()
                r2pred = v2.r2pred.mean()
                r2bias = v2.r2eff.mean()

                S = pd.Series({'zs': zs, 'r2true': r2true, 'r2pred': r2pred, 'r2bias': r2bias})
                df = df.append(S, ignore_index=True)

        for key, val in df.groupby('zs'):
            plt.plot(val.r2pred, val.r2bias, label='zs: {0:.2f}'.format(key))

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ pred', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend(loc=4)
        plt.show()
#            ipdb.set_trace()

 
    def d9(self, B):

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2_e = B['noisy']['r2train']['r2train']['r2eff']
        E2_p = B['noisy']['r2train']['r2train']['r2pred']

        zs = B['noisy']['galnoise']['magcat']['cat'][['zs']]

#        ipdb.set_trace()

#        n = 0
        from sklearn import neighbors
        reg1 = neighbors.KNeighborsRegressor()
        reg2 = neighbors.KNeighborsRegressor()

        # Fitting..
        inds = ~np.isnan(E2_p[0])
        X1 = np.array([E2_p[inds][0]]).T
        X2 = np.hstack([X1, zs[inds].values])

        Y1 = np.array([E2_t[inds][0]]).T

        reg1.fit(X1, Y1)
        reg2.fit(X2, Y1)

        # Predicting. To test..
        n = 2
        inds = ~np.isnan(E2_p[n])
        T1 = np.array([E2_p[inds][n]]).T
        T2 = np.hstack([T1, zs[inds].values])

        Y1 = reg1.predict(T1) #np.array([E2_p[inds][n]]).T)
        Y2 = reg2.predict(T2) #np.array([E2_p[inds][n]]).T)


        # Testing this correction....
        R1 = (Y1.flatten() - E2_t[inds][n]) / E2_t[inds][n]
        R2 = (Y2.flatten() - E2_t[inds][n]) / E2_t[inds][n]

        nbins = 100
        E2_e[n].hist(label='Orig', bins=nbins, histtype='step', normed=True)
        R1.hist(label='Corr(pred)', bins=nbins, histtype='step', normed=True)
        R2.hist(label='Corr(pred,z)', bins=nbins, histtype='step', normed=True)

        plt.legend()
        plt.show()


    def d12(self, B):

        E2_t = B['noisy']['r2train']['r2train']['r2true']
        E2_e = B['noisy']['r2train']['r2train']['r2eff']
        E2_p = B['noisy']['r2train']['r2train']['r2pred']
        zp2 = B['noisy']['pzcat']['pzcatdf']['pzcat'][['zb']]
        magcat2 = B['noisy']['galnoise']['magcat']['cat']
        

        # Old..
#        zp = B['noisy']['galnoise']['magcat']['cat'][['zs']]

        # Doing futher tests...
        E1_p = B['noiseless']['r2train']['r2train']['r2pred']
        E1_t = B['noiseless']['r2train']['r2train']['r2true']
        #zp1 = B['noiseless']['pzcat']['pzcatdf']['pzcat'][['zb']]
        zp1 = B['noiseless']['pzcat']['pzcatdf']['pzcat'][['zb']]
        zs1 = B['noiseless']['pzcat']['pzcatdf']['pzcat'][['zs']]

        magcat1 = B['noiseless']['galnoise']['magcat']['cat']

#        ipdb.set_trace()


#        zp = E.zb

#        n = 0
        from sklearn import neighbors, svm
#        reg1 = neighbors.KNeighborsRegressor()
#        reg2 = neighbors.KNeighborsRegressor()
#        reg3 = neighbors.KNeighborsRegressor()
#        reg4 = neighbors.KNeighborsRegressor()
#        reg4 = svm.NuSVR()
        reg2 = svm.NuSVR()
        reg3 = svm.NuSVR()
        reg4 = svm.NuSVR()
    
        # Fitting..
        inds = ~np.isnan(E1_p[0])
        X1 = np.array([E1_p[inds][0]]).T
        X2 = np.hstack([X1, zp1[inds].values])
        X3 = np.hstack([X1, zs1[inds].values])
        cols4 = ['mag_r', 'mag_i', 'mag_z', 'mag_vis'] #, 'zs']
#        cols4 = ['mag_g', 'mag_J']
#        cols4 = ['mag_vis']
        X4_1 = np.hstack([zp1[inds].values])
        X4 = magcat1[inds][cols4].values
#        X4 = np.hstack([X4_1, X4])

#        ipdb.set_trace()
        Y1 = np.array([E1_t[inds][0]]).T
        Y2 = Y1.T[0] # The SVR method is expecting a different dimension..

        Ntrain = 1000 #400
        tosel = np.arange(inds.sum())
        tosel = np.random.permutation(tosel)[:Ntrain]
#        reg1.fit(X1, Y1)
#        reg2.fit(X2, Y1)
        reg2.fit(X2[tosel], Y2[tosel])
        reg3.fit(X3[tosel], Y2[tosel])

        Y4 = Y1.T[0] # The SVR method is expecting a different dimension..
        reg4.fit(X4[tosel], Y2[tosel])

#        G1 = E2_p.copy()
        G2 = E2_p.copy()
        G3 = E2_p.copy()
        G4 = E2_p.copy()
#        G1[:] = np.nan
        G2[:] = np.nan
        G3[:] = np.nan
        G4[:] = np.nan

        Nboot = E2_p.shape[1]

        # Predicting. To test..
        nL = []
        for n in range(1, Nboot)[:2]:
            nL.append(n)
            print('n', n)

#        n = 2
            inds = ~np.isnan(E2_p[n])
            T1 = np.array([E2_p[inds][n]]).T
            T2 = np.hstack([T1, zp2[inds].values])
            T3 = T2
            T4_1 = np.hstack([zp2[inds].values])
            T4 = magcat2[inds][cols4].values
#            T4 = np.hstack([T4_1, T4])
#            ipdb.set_trace()

            Y2 = reg2.predict(T2)
            Y3 = reg3.predict(T3)
            Y4 = reg4.predict(T4)

            # Testing this correction....
#            R1 = (Y1.flatten() - E2_t[inds][n]) / E2_t[inds][n]
            R2 = (Y2.flatten() - E2_t[inds][n]) / E2_t[inds][n]
            R3 = (Y3.flatten() - E2_t[inds][n]) / E2_t[inds][n]
            R4 = (Y4.flatten() - E2_t[inds][n]) / E2_t[inds][n]

#            G1.ix[inds,n] = R1
            G2.ix[inds,n] = R2
            G3.ix[inds,n] = R3
            G4.ix[inds,n] = R4

        if False:
            nbins = 15
            E2_e.mean().hist(label='Orig', bins=nbins, histtype='step', normed=True)
#            G1.mean().hist(label='Corr', bins=nbins, histtype='step', normed=True)
            G2.mean().hist(label='Corr,zp', bins=nbins, histtype='step', normed=True)
            G3.mean().hist(label='Corr,zs', bins=nbins, histtype='step', normed=True)

            plt.legend()
            plt.show()

        zsL = np.linspace(0.1, 2.0, 12)
        magcat2 = B['noisy']['galnoise']['magcat']['cat']

        orig = E2_e.join(magcat2)
#        G1 = G1.join(magcat2)
        G2 = G2.join(magcat2)
        G3 = G3.join(magcat2)
        G4 = G4.join(magcat2)

        if True:
#            magcat = magcat[magcat
            orig = orig[orig.mag_vis < 24.5]
    #        G1 = G1[G1.mag_vis < 24.5]
            G2 = G2[G2.mag_vis < 24.5]
            G3 = G3[G3.mag_vis < 24.5]
            G4 = G4[G4.mag_vis < 24.5]

#        ipdb.set_trace()
        # Testing the result on a single field.
        if True: #False: #True:
            nbins = 50 #200
            n = 2

            for lbl,cat in [('Orig', orig), ('Corr,zp', G2), ('Corr,zs', G3), ('Corr (mag)', G4)]:
                cat[(0.9 < cat.zs) & (cat.zs < 1.1)][n].hist(label=lbl, bins=nbins, histtype='step', normed=True)
 

#            ipdb.set_trace()
#            orig[n].hist(label='Orig', bins=nbins, histtype='step', normed=True)
##            G1[n].hist(label='Corr', bins=nbins, histtype='step', normed=True)
#            G2[n].hist(label='Corr,zp', bins=nbins, histtype='step', normed=True)
#            G3[n].hist(label='Corr,zs', bins=nbins, histtype='step', normed=True)
#            G4[n].hist(label='Corr (mag)', bins=nbins, histtype='step', normed=True)

            plt.legend()
            plt.show()


#        ipdb.set_trace()

#('Corr', G1)
#  ('Corr,zp', G2), ('Corr,zs', G3),
        df = pd.DataFrame()
        for lbl,cat in [('Orig', orig), ('Corr,zp', G2), ('Corr,zs', G3), ('Corr (mag)', G4)]:
            for key,val in cat.groupby(pd.cut(cat.zs, zsL)):
                r2bias = val[nL].mean().abs().describe(percentiles=[0.68])['68%']
                r2bias *= np.sign(val[nL].mean().mean())

                S = pd.Series({'lbl': lbl, 'zs': val.zs.mean(), 'r2bias': r2bias})

#                               'r2': val[1].mean()})
                df = df.append(S, ignore_index=True)


        for key, val in df.groupby('lbl'):
            plt.plot(val.zs, val.r2bias, 'o-', label=key)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('zs', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
#        plt.yscale('log')
#        plt.ylim(5e-5, 5e-3)

        plt.axhline(-3e-4, lw=3, color='k', alpha=0.5)
        plt.axhline(3e-4, lw=3, color='k', alpha=0.5)
 
        plt.legend()
        plt.show()

    def d13(self, B):

        r2eff = B['noisy']['r2train']['r2train']['r2eff']
        magcat = B['noisy']['galnoise']['magcat']['cat']

        # Cutting first to get the weight right.
        inds = (magcat.mag_vis < 24.5) #& (1. < magcat.zs)
        r2eff = r2eff[inds]

        nbins = 20
        r2eff.mean().hist(label='Orig', bins=nbins, histtype='step', normed=True)

        for per in [0.001, 0.003, 0.005, 0.01]:
            W = np.exp(-r2eff.abs()/per)
            r2W = r2eff*W/W.mean()
            r2W.mean().hist(label='Cap: {0}'.format(str(per)), bins=nbins, histtype='step', normed=True) #False)

        plt.legend()
#        plt.yscale('log')
        plt.show()

        ipdb.set_trace()

    def d14(self, B):
        ipdb.set_trace()

    def run(self):
        inst = psf10.psf10()
#        inst = psf12.psf12()
#        topnode = inst.create_topnode()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf12.psf12()), 
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['use_stars'] = True
            A['r2train'].config['Nboot'] = 200 #use_stars'] = True
            A['r2train'].config['Nboot'] = 30 #use_stars'] = True

#            A['galabs'].config['Ngal'] = 1000000 #2e6 #200007
#            A['r2train'].config['Ntest'] = 1000000
#            ipdb.set_trace()

#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['Ntest'] = 200000
            if lbl == 'noiseless':
                A['galabs'].config['Ngal'] = 200008

            B[lbl] = A

#        for key, val in B.iteritems():
#            val['pzcat'].run()

        
        self.d12(B)
        import sys
        sys.exit(1)




        r2eff = A['r2train']['r2train']['r2eff']
        magcat = A['galnoise']['magcat']['cat']

        N = A['r2train'].config['Nboot']
        cat = r2eff.join(magcat)

        cat = cat[cat.mag_vis < 24.5]


        # The ordinary plot....
        df = pd.DataFrame()
        z = np.linspace(0.1, 2.0, 15)

        df = pd.DataFrame()

        for key,val in cat.groupby(pd.cut(cat.zs, z)): #[-2:]):
#            r2bias = val[range(N)].mean().abs().mean()
            zmean = val.zs.mean()

            r2bias = val[range(N)].mean().abs().describe(percentiles=[0.68])['68%']
            r2bias *= np.sign(val[range(N)].mean().mean())
#            ipdb.set_trace()
#            if 1. < zmean:
#                ipdb.set_trace()

            df = df.append({'z': zmean, 'r2bias': r2bias}, ignore_index=True)

        plt.plot(df.z, df.r2bias, 'o-', label='Noiseless')
        plt.show()

#        pl
#            ipdb.set_trace()

if __name__ == '__main__':
    Job('psf45').run()
