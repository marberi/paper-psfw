#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
import time

import squid
import xdolphin as xd

import imp
inuse = squid.find_inuse()


# test first with one plot.
topD = squid.find_topnodes(inuse)

def _toempty(X):
    # Its an 'empty' job
    if isinstance(X, xd.Job):
        return X

    pipel = xd.Job('empty')
    for key, val in X.iteritems():
        xkey = '.'.join(map(str, key)) if isinstance(key, tuple) else key

        pipel.depend[xkey] = val

    return pipel

def toempty(D):


    res = {}
    for key, val in D.iteritems():
        res[key] = _toempty(val)

    return res

def find(root, job, res):
    res['.'.join(root)] = job

    for dep_name, dep_job in job.depend.iteritems():
        find(root+(dep_name,), dep_job, res)

def rem_empty(X):
    res = {}
    for key, job in X.iteritems():
        if job.name == 'empty': continue

        res[key] = job

    return res

newD = toempty(topD)

# This one will take time.
del newD['psf31']

# Collect together all jobs.
alljobs = {}
for pl, pipel in newD.iteritems():
    find((pl,), pipel, alljobs)

torun = rem_empty(alljobs)

print('start: taskid')
taskidD = {}
t1 = time.time()
for key, job in torun.iteritems():
    taskidD[key] = job.taskid()

t2 = time.time()
print('end: taskid')
print('time taskid:', t2-t1)

D = dict([(y,x) for x,y in taskidD.iteritems()])

finished = os.listdir('/data2/marberi/results/default')
for taskid,key in D.iteritems():
    # This part is a bit paranoid..
    if taskid in finished:
        continue

    print('running:', taskid, key)
    job = alljobs[key]
    job.run()

    finished = os.listdir('/data2/marberi/results/default')
