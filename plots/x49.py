#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from sklearn import svm, linear_model

import xdolphin as xd
import trainpipel

def add_pzcat(pipel):
    """Replace the current pzcat."""

    # This shouldn't actually make a difference.
    pzcat = pipel.pzcat.clone()
    pzcat.depend['galcat'] = pipel.r2x.test_cat
    pipel.depend['pzcat'] = pzcat


pipel = trainpipel.pipel()
pipel.config.update({
  'r2x.filters': ['r', 'i', 'z'],
#  'r2x.filters': ['r', 'i'],
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.use_stars': False})


pipel.r2x.run()
#cat = pipel.pzcat.result


ipdb.set_trace()
