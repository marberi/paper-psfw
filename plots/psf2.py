#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import splev, splrep

import xdolphin as xd
import trainpipel

class Plot:
    """"R^2 redshift dependence."""

    name = 'psf2'
    config = {'r2_filter': 'vis', 
              'norm_sed': 'Ell_01',
              'norm_z0': True,
              'seds': ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14']}


    def pipel(self):
        pipel = trainpipel.pipel(toplevel='r2train')
        pipel.depend['r2train'] = pipel.r2train.toempty()

        return pipel

    def pipel(self):
        pipel = trainpipel.pipel()

        return pipel

    def _norm_z0(self, r2vz):
        # Plot the ratios normalized to z=0.

        assert r2vz['z'][0] == 0., 'Assumes first entry is z=0 later.'

        f = self.config['r2_filter']
        fmt = '/{0}/{1}'

        #ipdb.set_trace()
        norm_val = r2vz[fmt.format(f, self.config['norm_sed'])][0]

#        norm_val = X['r2vz'][0]

        colors = ['b', 'r', 'k', 'm', 'g', 'm', 'purple']
        styles = ['-',':','-.','--', '-']
        for i,sed in enumerate(self.config['seds']):
            r2 = r2vz[fmt.format(f, sed)]
            plt.plot(r2vz['z'], r2 / norm_val, label=sed, ls=styles[i], color=colors[i])

    def _pretty(self):
        plt.rcParams['legend.fontsize'] = 12

        plt.xlim(0., 4.)
        plt.legend()
        ax = plt.gca()
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(True, which='minor')
        ax.yaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='minor')

        plt.axvline(0.4, lw=3, alpha=0.5, color='k')
        plt.axvline(2.3, lw=3, alpha=0.5, color='k')

        plt.xlim(0., 3.5)
<<<<<<< HEAD
        plt.xlabel('$z_{\mathrm{s}}$', size=14)
        plt.ylabel('$R_{\\rm{PSF}}^2\, /\, R_{\\rm{PSF}}^2\,$(Ell_01, z=0)', size=13)
=======
        plt.xlabel('$z_s$', size=14)
        plt.ylabel('$R_{\\rm{PSF}}^2\, /\, R_{\\rm{PSF}}^2\,$(Ell_01, z=0)')
        if self.config['title']:
            plt.suptitle('$R^2_{\\rm{PSF}}$ redshift dependence.')
>>>>>>> 0a69a82160a549d0a1ad619323851fad7563238a


    def load(self, pipel):
        return pipel.r2x.train_gal_r2.r2vz.result

    def plot(self, r2vz):
        if self.config['norm_z0']:
            self._norm_z0(r2vz)
        else:
            self._norm(r2vz)
        
        self._pretty()

if __name__ == '__main__':
    xd.plot(Plot).save()
