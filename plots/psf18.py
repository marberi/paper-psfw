#!/usr/bin/env python
# encoding: UTF8

import ipdb
import copy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from xdolphin import Job
import psf17

import tosave

class Plot(psf17.Plot):
    """Redshift dependence of the photo-z, "R^2_{\\rm{PSF}}" correaltions."""

    def prepare_part(self, pipel):
        # Not usable from topnonode.get_prepare .. but quite close to
        # how these functions normally work.

        cat = pipel.r2train.galcat.result[['mag_vis']]
        cat = cat.join(pipel.r2train.get_store()['r2eff'])
        cat = cat.join(pipel.pzcat.result)

        cat['zx'] = (cat.zb - cat.zs) / (1 + cat.zs)
        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2train.config['Nboot']

        z = np.linspace(0.1, 2.0, 10)
        df = pd.DataFrame()
        for ztype in ['zb', 'zs']:
            for i,(key, sub) in enumerate(cat.groupby(pd.cut(cat[ztype], z))):

                S = pd.Series()
                S['r2bias'] = sub[range(Nboot)].mean().mean()
                S['zb'] = sub.zb.mean()
                S['zs'] = sub.zs.mean()
                S['z'] = sub[ztype].mean()
                S['ztype'] = ztype
                S['zind'] = i
                S['isindep'] = pipel.pzcat.galcat.config['isindep'] 
                S['use_stars'] = pipel.r2train.config['use_stars']

                df = df.append(S, ignore_index=True)

        return df
  
    def prepare(self, topnode):
        prepare = pd.DataFrame()
        for key, pipel in topnode.iteritems():
            part = self.prepare_part(pipel)
            part['key'] = len(part)*[key]

            prepare = prepare.append(part, ignore_index=True)

        return prepare


    def plot(self, prepare):

        lbl_ind = {True: 'Indepedent', False: 'Equal'}
        for key, val in prepare.groupby(['ztype', 'use_stars']):
            sub_dep = val[val.isindep==0.]
            sub_in = val[val.isindep==1.]

            A = sub_in.set_index('zind').join(sub_dep.set_index('zind'), rsuffix='_dep')
            A = A.sort('zs')

            lbl_star = 'Stars' if key[1] else 'Galaxies'
            lbl_z = {'zb': 'photo-z', 'zs': 'true-z'}[key[0]]
            lbl = '{0}, {1}'.format(lbl_star, lbl_z)

            plt.plot(A.z, A.r2bias - A.r2bias_dep, 'o-', label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

#        plt.yscale('log')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.xlabel('Redshift', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias(Indep) - $"R^2_{\\rm{PSF}}"$ bias(Dep)', size=16)

        plt.xlim(0.1, 1.9)
#        plt.ylim(5e-5, 4e-3)
        plt.legend(loc=0)
#        plt.savefig(tosave.path(self.name))
        plt.show()
#        ipdb.set_trace()




    def run(self):
        topnode = self.topnode()
        prepare = self.prepare(topnode)
        self.plot(prepare)

if __name__ == '__main__':
    Plot().run()
