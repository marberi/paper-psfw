#!/usr/bin/env python
# encoding: UTF8

import ipdb
import x84

class Plot(x84.Plot):
    def run(self):
        top = self.topnode()

        for key, pipel in top.iteritems():
            job = pipel.r2train.galcat

            print('running for')
            job.run()

#            ipdb.set_trace()

if __name__ == '__main__':
    Plot().run()
