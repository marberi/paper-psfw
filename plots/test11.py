#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import squid
import xdolphin as xd



def get_fig(fig):
    print('Running for:', fig)
    cls = squid.get_class(fig)
    inst = cls()

    # A few figures does not have a topnode
    # method yet..
    if not hasattr(inst, 'topnode'):
        return pd.DataFrame(), pd.DataFrame()

    # For the figures where the topnode
    # method returns a pipeline.
    top = inst.topnode()
    if not isinstance(top, xd.Topnode):
        top = xd.Topnode(top)

    t1 = time.time()
    nodesx, depx = top.gecko()
    t2 = time.time()

    nodesx['fig'] = fig
    print('time creating', t2-t1)

    return nodesx, depx

def get_many(figs):
    nodes = pd.DataFrame()
    dep = pd.DataFrame()
    for fig in figs:
        nodesx, depx = get_fig(fig)

        nodes = nodes.append(nodesx)
        dep = dep.append(depx, ignore_index=True)

    return nodes, dep

def prepare(figs):
    nodes, dep = get_many(figs)

    # We should not need a topnode for this.
    cls = squid.get_class('psf22')
    inst = cls()
    top = inst.topnode()
    cached = top.gecko_cached()

    nodes['cached'] = nodes.taskid.isin(cached)
    nodes['torun'] = ~nodes.cached

    return nodes, dep

def get(figs):
    d = '/data2/marberi/results/scratch'
    name = 'v6.h5'

    path = os.path.join(d, name)
    if os.path.exists(path):
        with pd.HDFStore(path) as store:
            nodes = store['nodes']
            dep = store['dep']
    else:
        nodes, dep = prepare(figs)
        with pd.HDFStore(path, 'w') as store:
            store['nodes'] = nodes
            store['dep'] = dep

    return nodes, dep

if False:
    nodes, dep = prepare(['psf84'])
    nodes['torun'] = 1*nodes['torun']
    nodes = nodes[nodes.name != 'empty']

# When testing all.
if False: #True:
    nodes, dep = get(inuse)
    nodes['torun'] = 1*nodes['torun']
    nodes = nodes[nodes.name != 'empty']
    A = nodes.groupby('fig').torun.sum()

    print(A)

if False:
    nodes.name.value_counts().hist()
    plt.show()

if False:
    nodes['r2train'] = 1*(nodes.name == 'r2train')
    print(nodes.groupby('fig').r2train.sum())

if False:
    d = '/home/marberi/drv/work/papers/psfw/links'
    lfiles = os.listdir(d)
    links = [x.replace('prod_', '').split('.')[0] for x in lfiles]

    inuse = squid.find_inuse()
    print('# links', len(links))
    print('# inuse', len(inuse))

#ipdb.set_trace()
