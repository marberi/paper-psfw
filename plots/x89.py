#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

import xdolphin as xd
import trainpipel

import tosave
def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    binning = 'linspace'

    def _add_offset(self, pipel):
        """Add a task for the offset."""

        offset = xd.Job('offset')
        offset.depend['magcat'] = pipel.r2x.test_cat
        pipel.r2x.depend['test_cat'] = offset

    def limit_seds_cal(self, x):
        D = {'r2x.cal_cat.magcat.mabs.limit_seds': x,
             'r2x.train_gal_cat.mabs.limit_seds': x}

        return D

    def limit_seds_data(self, x):
        D = {'r2x.test_cat.magcat.magcat.mabs.limit_seds': x}

        return D


    def _rem_type(self, i):
        def lim_list(A, x):
            return A[:x]+A[x+1:]

        only_seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']
        sed_types =  ['ell', 'sp', 'sp', 'irr', 'irr', 'irr']

        D = {'only_seds': lim_list(only_seds, i),
             'sed_types': lim_list(sed_types, i)}


        return D        

    def rem_type(self, i):
#        D = {'r2x.cal_cat.magcat.mabs.limit_seds': x,
#             'r2x.train_gal_cat.mabs.limit_seds': x}

        L = ['r2x.cal_cat.magcat.mabs.{0}', 'r2x.train_gal_cat.mabs.{0}']
        D_in = self._rem_type(i)
        D = {}
        for x in L:
            for k,v in D_in.iteritems():
                key = x.format(k)
                D[key] = v

            D[x.format('limit_seds')] = True

        x = 'r2x.test_cat.magcat.magcat.mabs.{0}'
        D[x.format('limit_seds')] = True

        return D

    def replace_seds(self):
        key = 'r2x.test_cat.magcat.magcat.mabs.limit_seds.{0}'



        ipdb.set_trace()

    def print_jobs(self, job, root=tuple()):
#        ipdb.set_trace()
        if job.name == 'galabs':
            print('root', '.'.join(root))

        for dep_key,dep_job in job.depend.iteritems():
            new_root = root+(dep_key,)

            self.print_jobs(dep_job, new_root)
#
#            ipdb.set_trace()

    def replace_job(self, old_job):
        new_job = xd.Job('rmsed')
        new_job.depend['default'] = old_job

        return new_job
#        ipdb.set_trace()

    def _add_sedrm(self, pipel):
        """Add task which remove one of the SEDs."""

#        self.print_jobs(pipel)

#        ipdb.set_trace()

        # One can automatically detect these, but it does not
        # improve anything.
#        toreplace = ['r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs',
#                     'r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs']


        j_train = self.replace_job(pipel.depend['r2x.train_gal_cat.mabs'])
        j_cal = self.replace_job(pipel.depend['r2x.cal_cat.magcat.mabs'])


        # I should actually only do this for the calibration sample.
        tochange = [(j_train, ('r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs')), \
                    (j_cal,   ('r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs'))]

#        tochange = [tochange[1]]

        for j, X in tochange:
            for x in X:
                print(x)
                print(pipel.depend[x].name)

                assert pipel.depend[x].name == 'galabs'

                F = x.replace('.mabs', '')
                pipel.depend[F].depend['mabs'] = j
                print(pipel.depend[x].name)

        return pipel


    def topnode(self):
        pipel = trainpipel.pipel()

        # Deleting the photo-z dependencies. I need to run
        # with some large simulations..
        del pipel.r2x.depend['cal_pz']
        del pipel.r2x.depend['test_pz']

        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000
        pipel.r2x.test_cat.magcat.mabs.config['Ngal'] = 500000
#        ipdb.set_trace()

        pipel.config.update({\
          'r2x.use_cal': True,
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
#          'r2x.Nboot': 100,
#          'r2x.Nboot': 2,
          'r2x.Nboot': 10,
#          'r2x.use_stars': False,
          'r2x.filters': ['r','i','z']})

#        self._add_offset(pipel)
        pipel = self._add_sedrm(pipel)

        top = xd.Topnode(pipel)

#        L = [-10, 0, 5,  10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
        L = [-10, 0, 10, 20, 30, 40, 50]
        L = [-10, 0, 10, 20, 30, 40, 50]

        L = [-10, 0, 10, 20, 30, 40, 50]

        def xval(torem):
            D = {}
            for key in ['r2x.cal_r2.mabs.{0}', 'r2x.train_gal_r2.mabs.{0}']:
                D[key.format('torem')] = torem
                D[key.format('dsed')] = 10.

            return D
#            return {'r2x.cal_r2.mabs.torem': torem,
#                    'r2x.train_gal_r2.mabs.torem': torem}

        top.add_layer(map(xval, L)) #_key(key, L)
        self.lblL = map('sed: {0}'.format, L) #i['Test']

#        ipdb.set_trace()

        return top

    def prepare(self, topnode):
        # For having the same Ell_01 "R^2_{\\rm{PSF}}" value for all the result.
        pipel = topnode.values()[0]
        ell_01 = pipel.r2x.train_gal_r2.r2vz.result['/vis/Ell_01'][0]

        field = 'r2pred'
        df = pd.DataFrame()
        for key, pipel in topnode.iteritems():
            # Not exactly pretty way of configuring the pipelines.
            pipel.config['r2x.use_stars'] = False
            cat = pipel.r2x.get_store()[field]

            pipel.config['r2x.use_stars'] = True
            cat = cat.join(pipel.r2x.get_store()[field], rsuffix='_stars')


            Nboot = pipel.r2x.config['Nboot']
#            cat[range(Nboot)] /= ell_01

            col_gal = range(Nboot)
            col_stars = map('{0}_stars'.format, range(Nboot))
            sub_stars = cat[col_stars]
            sub_stars.columns = col_gal
            sub_gal = cat[col_gal]
            sub_gal.columns = col_gal

            diff = (sub_stars - sub_gal) / ell_01
            galcat = pipel.r2x.test_cat.result

            # First here we have the actual catalog.
            cat = galcat.join(diff)
            cat = cat[cat.mag_vis < 24.5]

            # More bins than I probably need..
            if self.binning == 'arange':
                z = np.arange(0., 1.5, 0.2)
            else:
                z = np.linspace(0.1, 1.8, 10)

            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            part = pd.DataFrame()
            part['r2diff'] = mean[col_gal].mean(axis=1)
            part['r2diff_std'] = np.sqrt(mean[col_gal].var(axis=1))
            part['zs'] = mean.zs
            part['zbin'] = range(len(part))
            part['za'] = z[:-1]
            part['zb'] = z[1:]


            r2diff_all = cat[col_gal].mean()
            S = pd.Series({'zbin': 'all',
                           'r2diff': r2diff_all.mean(),
                           'r2diff_std': r2diff_all.var()})

            part = part.append(S, ignore_index=True)

            part['pipel'] = key[0]
            part['torem'] = pipel.config['r2x.cal_r2.mabs.torem']

            df = df.append(part, ignore_index=True)

        return df

    def test1(self, topnode):
        for key,pipel in topnode.iteritems():
            torem = pipel.r2x.cal_r2.mabs.config['torem']
            T = pipel.r2x.cal_r2.mabs.result

            T.sed.hist(bins=200, histtype='step', label='Removed: {0}'.format(torem))

        plt.xlabel('SED')
        plt.ylabel('# Gal')

        plt.legend()
        plt.savefig(tosave.path('x89_test1'))



    def test2(self, topnode):
        # For having the same Ell_01 "R^2_{\\rm{PSF}}" value for all the result.
        pipel = topnode.values()[0]
        ell_01 = pipel.r2x.train_gal_r2.r2vz.result['/vis/Ell_01'][0]

        for key,pipel in topnode.iteritems():

            field = 'r2pred'
            pipel.config['r2x.use_stars'] = False
            cat = pipel.r2x.get_store()[field]

            pipel.config['r2x.use_stars'] = True
            cat = cat.join(pipel.r2x.get_store()[field], rsuffix='_stars')

            Nboot = pipel.r2x.config['Nboot']

            col_gal = range(Nboot)
            col_stars = map('{0}_stars'.format, range(Nboot))
            sub_stars = cat[col_stars]
            sub_stars.columns = col_gal
            sub_gal = cat[col_gal]
            sub_gal.columns = col_gal

            diff = (sub_stars - sub_gal) / ell_01
            galcat = pipel.r2x.test_cat.result

            cat =  galcat.join(diff)
            mabs = pipel.r2x.test_cat.magcat.mabs.result
            cat = cat.join(mabs, rsuffix='_Y')

            torem = pipel.r2x.cal_r2.mabs.config['torem']

            sed = np.arange(55)
            mean = cat.groupby(pd.cut(cat.sed, sed)).mean()

            mean['r2diff'] = mean[range(Nboot)].mean(axis=1)

#            ipdb.set_trace()
#            y = (mean['0_stars'] - mean['0']) / ell_01
#            plt.plot(mean.sed, mean['0'], '.')
#            plt.plot(mean.sed, mean['0_stars'], '.')
            plt.plot(mean.sed, mean.r2diff, '.', label=torem)

#            plt.show()
#            ipdb.set_trace()

#            X = (cat['1'] - cat['1_stars']) / ell_01
#            X.hist(bins=300, label=str(key), histtype='step')

        plt.grid(True)
        plt.legend()
        plt.show()

        ipdb.set_trace()

    def test3(self, prepare):

        prepare = prepare[prepare.zbin != 'all']
        T = prepare[(prepare.torem == -10)]
        for key,sub in prepare.groupby('torem'):
            ratio = sub.r2diff.values / T.r2diff.values
            prepare.ix[sub.index, 'ratio'] = ratio #sub.r2diff / T.r2diff_std

        for key,sub in prepare.groupby('zbin'):
            zbin = sub.iloc[0].zbin
            plt.plot(sub.torem, sub.ratio, 'o', label=str(zbin))

        plt.legend()
        plt.show()

    def test4(self, prepare):

        prepare = prepare[prepare.zbin != 'all']
        T = prepare[(prepare.torem == -10)]


        ipdb.set_trace()

    def plot(self, prepare):
#        prepare = prepare.sort('torem')


        # A region to mark what we expect when not dropping a
        # SED.
        sub = prepare[(prepare.torem == -10) & (prepare.zbin == 'all')]
        row = sub.iloc[0]

        toincl = [1, 3]
        toincl = [1, 2, 3]
        T = prepare[(prepare.torem == -10)].set_index('zbin')

        colorL = ['blue', 'green', 'red']

        bin_order = T.ix[toincl].sort('r2diff_std', ascending=False).index
        for i,zbin in enumerate(bin_order): #toincl):
            yval = T.ix[zbin].r2diff_std
            xline = np.linspace(-100, 100)
            
            y_lower = -yval*np.ones_like(xline)
            y_upper = yval*np.ones_like(xline)

            plt.fill_between(xline, y_lower, y_upper, color=colorL[i], alpha=0.8, label='fill')

        # Dropping the fiducial line.
        torem = (prepare.torem == -10) | (prepare.zbin == 'all')
        prepare = prepare[~torem]


        print(T)
        for zbin in bin_order:
            sub = prepare[prepare.zbin == zbin].sort('torem')

            row = sub.iloc[0]

            label = '{0:.1f} < z < {1:.1f}'.format(row.za, row.zb)
            plt.errorbar(sub.torem, sub.r2diff, sub.r2diff_std.values, label=label)

        plt.grid(True)
        plt.legend()

        plt.xlim((-5, 60))
#        plt.ylim((-0.0001, 0.0001))

        ylim = 0.00005
        plt.ylim((-ylim, ylim))

        plt.xlabel('SED', size=16)
        plt.ylabel('[<$"R^2_{\\rm{PSF}}"$(Train stars) - <$"R^2_{\\rm{PSF}}"$(Train gal)]/ $"R^2_{\\rm{PSF}}"$(Ell_01)', size=14)

        plt.subplots_adjust(left=0.15, right = 0.90)

        ax = plt.gca()
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))

#        ipdb.set_trace()


        print('Here...')
#        plt.savefig(tosave.path('x89_{0}'.format(self.binning)))
        plt.savefig(tosave.path('x89'))
        plt.show()

#        plt.clf()
#        import sys
#        sys.exit(1)




    def run(self):
        topnode = self.topnode()

        prepare = self.prepare(topnode)
#        self.test4(prepare)

        self.plot(prepare)

if __name__ == '__main__':
    Plot().run()
