#!/usr/bin/env python
# encoding: UTF8

import os
import ipdb
import trainpipel
import numpy as np
import pandas as pd
import xdolphin as xd
from matplotlib import pyplot as plt

def add_r2pz(pipel):
    r2sed = xd.Job('r2sed')

    _train = pipel.r2train.clone()
    _test = pipel.r2train.clone()

    _train.galcat.magcat.mabs.config['Ngal'] = 10000

    r2sed.depend['train_r2gal'] = _train.depend['r2gal']
    r2sed.depend['train_cat'] = _train.depend['galcat.magcat']

    r2sed.depend['test_r2gal'] = _test.depend['r2gal']
    r2sed.depend['test_cat'] = _test.depend['galcat']

    pipel.depend['r2sed'] = r2sed



pipel = trainpipel.pipel()
add_r2pz(pipel)

f_all = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']
pipel.r2sed.config.update({
  'filters': ['r', 'i', 'z'],
  'Ntrain': 1000
})

pipel.r2train.galcat.magcat.mabs.config['Ngal'] = 50000
pipel.r2sed.test_cat.magcat.mabs.config['Ngal'] = 200000

pipel.r2sed.test_cat.config['r'] = 1

pipel.save()

K = {'bins': 100, 'histtype': 'step', 'normed': True}

cat = pipel.r2sed.test_cat.result
cat['r2eff_all'] = pipel.r2sed.result['r2eff']

#pipel.r2sed.train_cat.mabs.config['limit_seds'] = True
cat['r2eff_lim'] = pipel.r2sed.result['r2eff']

cat = cat[cat.mag_vis < 24.5]

# First plot.
if False:
    cat.r2eff_all.hist(label='All SEDs', **K)
    cat.r2eff_lim.hist(label='Limit SEDs', **K)
    plt.legend()
    plt.show()

if False:
    z = np.linspace(0.1, 2., 10)
    mean = cat.groupby(pd.cut(cat.zs, z)).mean()

    all_lbl = 'All SEDs'
    lim_lbl = 'Limit SEDs'
    plt.plot(mean.zs, mean.r2eff_all, 'o-', label=all_lbl)
    plt.plot(mean.zs, mean.r2eff_lim, 'o-', label=lim_lbl)

    plt.legend()
    plt.show()

if True:
    # Testing using the x14 prepare structure.
    import x14
    dsl = x14.x14()

    #D, gc = dsl.prepare(xd.Topnode(pipel))
    #gc = gc[gc.mag_vis < 24.5]
    #z = np.linspace(0.1, 2.0, 10)
    #mean = gc.groupby(pd.cut(gc.zs,z)).mean()

    d = '/data2/marberi/results/pipel/'
    path1 = os.path.join(d, 'f46849eeb06b29cf81b6c5f8307990b7')
    p14 = xd.load_pipel(path1)
    top14 = xd.Topnode(p14)

    top21 = xd.Topnode(pipel)

    D14, gc14 = dsl.prepare(top14)
    D21, gc21 = dsl.prepare(top21)

    mean14 = dsl.simple(gc14)
    mean21 = dsl.simple(gc21)

    z = np.linspace(0.1, 2.0, 10)
    meanx = cat.groupby(pd.cut(cat.zs, z, )).mean()

    ipdb.set_trace()

# Testing the plotting again...
z = np.linspace(0.1, 2.0, 10)
mean = cat.groupby(pd.cut(cat.zs,z)).mean()


ipdb.set_trace()
