#!/usr/bin/env python
# encoding: UTF8

import ipdb
from xdolphin import Job
from matplotlib import pyplot as plt
class x4:
    name = 'x4'
    config = {}

    def plot1(self, pipel):
        cat_star = pipel.kids['starcatN']['magcat']['magcat']
        r2_star = pipel.kids['r2star']['r2eff']['r2eff']
        cat_star = cat_star.join(r2_star)

        cat_gal = pipel.kids['galcatN']['magcat']['magcat']
        r2_gal = pipel.kids['r2gal']['r2eff']['r2eff']
        cat_gal = cat_gal.join(r2_gal)


        for lbl, cat_orig in [('Stars', cat_star), ('Galaxies', cat_gal)]:
            cat = cat_orig[cat_orig.mag_vis < 22.5]
            cat = cat[cat.zs < 1.5]
            col = cat.mag_r - cat.mag_i 
            plt.plot(col, cat.r2eff, '.', label=lbl)

        plt.legend()
        plt.show()

    def has(self, pipel, task):
        for key, j in pipel.kids.iteritems():
            if task in j.config.task_config:
                yield key
            
#            ipdb.set_trace()

    def plot2(self, pipel):
#        task = 'galcatN'
#        task = 'r2gal'
        pipel.kids[task].config.update({'HACK': 23})
        pipel.kids[task].run()

#        self.has(pipel)
        for name in ['abgal','r2vz_star', 'r2vz_gal']:
            pipel.kids[name].config['dz_ab'] = 0.2


#        ipdb.set_trace()

        j = pipel.kids['r2train']
        j.config.update({'Ntrain': 400, 'Nboot': 100, 'use_stars': True})

        r2train = j['r2train']

        if False:
            cat = r2train['r2eff'].join(pipel.kids['galmabs']['mabs']['cat'])
            cat = cat.join(r2train['r2true'], rsuffix='_true')
            A = cat.groupby('sed')[range(100)].mean().abs().mean(axis=1)

            k = map('{0}_true'.format, range(100))
            B = cat.groupby('sed')[k].mean()
            B.plot()
            plt.show()

        ipdb.set_trace()

    def plot3(self, pipel):
#        task = 'galcatN'
        task = 'r2gal'
        pipel.kids[task].config.update({'HACK': 23})
        pipel.kids['r2vz_gal'].config.update({'HACK': 25})
        pipel.kids['r2vz_gal'].config.update({'HACK': 26})
        pipel.kids['r2vz_star'].config.update({'HACK': 26})
        pipel.kids['abgal'].config.update({'HACK': 25})
        pipel.kids['galcatN'].config.update({'HACK': 26})

        pipel.kids['r2train'].config.update({'HACK': 26})

        pipel.kids['galcatN'].run()
#        pipel.kids[task].run()

#        pipel.kids['galcatN'].config['kcorr'] = True
#        ipdb.set_trace()

        j = pipel.kids['r2train']
        j.config.update({'Ntrain': 400, 'Nboot': 200, 'use_stars': True,
                         'filters': ['r', 'i', 'z']})


#        self.has(pipel)
        for name in ['abgal','r2vz_star', 'r2vz_gal']:
            pipel.kids[name].config['dz_ab'] = 0.01

#        for name in self.has(pipel, 'Nint'):
#            pipel.kids[name].config['Nint'] = 400

#        pipel.kids['galnoise'].config['r'] = 0.01
        pipel.kids['starcatN'].config['star_zs'] = 0.0003
#r2vz_gal


        galcat = pipel.kids['galcat']['magcat']['cat']
        for reg in ['nusvr']:
            print('reg', reg)
            j.config['reg'] = reg
            r2eff = pipel.kids['r2train']['r2train']['r2eff']

            cat = galcat.join(r2eff)
            cat = cat[cat.mag_vis < 24.3]

            cat[range(100)].mean().hist(bins=25)

#            ipdb.set_trace()

#            r2eff.mean().hist(bins=25, label=reg)

        plt.legend()
        plt.show()
#        ipdb.set_trace()

    def run(self):
        import trainpipel
        pipel = trainpipel.pipel()
        pipel.kids['galmabs'].config.update({'HACK': 21})


        self.plot3(pipel)

        import sys
        sys.exit()

        A = pipel.kids['r2vz_gal']['r2vz']['r2vz']

#        plt.plot(A.z, A['/vis/Ell_01'])

        seds = [x.split('/')[2] for x in filter(lambda x: 'vis' in x, A.columns)]

        fmt = '/vis/{0}'
        B = A[fmt.format('Ell_01')][0]
#        ipdb.set_trace()
        for sed in seds:
            plt.plot(A.z, A[fmt.format(sed)] /B, label=sed)

        plt.show()
#        ipdb.set_trace()




if __name__ == '__main__':
    Job('x4').run()
