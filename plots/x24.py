#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    """Redshift dependence of the r2sed results."""

    def topnode(self):
        import trainpipel
        top = xd.Topnode(trainpipel.pipel())

        f_riz = ['r', 'i', 'z']
        f_all = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

        eps = 1e-6
        r = 1+eps*np.arange(10)
        top.add_layer_key('r2sed.test_cat.r', r.tolist())
        top.add_layer_key('r2sed.filters', [f_riz]) #, f_ri])

        top.update_config({\
          'r2sed.test_cat.magcat.mabs.Ngal': 1000001
        })

        return top


    def get_cat(self, topnode):
        import psf67
        dsl = psf67.Plot()

        return dsl.get_cat(topnode)

    def show(self, D, gc):
        z = np.linspace(0.1, 2., 10)
        mean = gc.groupby(pd.cut(gc.zs, z)).mean()


        D['lbl_filters'] = map(','.join, D.filters)

        for key, val in D.iterrows():
            R = val['galnoise.r']
            lbl_f = val['lbl_filters']
            lbl = 'R: {0}, {1}'.format(R, lbl_f)
            plt.plot(mean.zs, mean[key], 'o-', label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.xlabel('z', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylim(-0.002, 0.002)

#        plt.legend()
        plt.show()
        
    def run(self):
        topnode = self.topnode()

        D, gc = self.get_cat(topnode)
        self.show(D, gc)


if __name__ == '__main__':
    Plot().run()
