#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

import tosave

def find_seds():
    """Read in the list of all SEDs."""

    fjc_file = '/home/marberi/drv/work/papers/psfw/data/fjc_temp'
    seds = map(str.strip, open(fjc_file).readlines())

    return seds

class Plot:
    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()
        pipel.config.update({
          'r2x.use_cal': False,
          'r2x.filters': ['r', 'i', 'z'],
        #  'r2x.filters': ['r', 'i'],
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
          'r2x.Nboot': 10,
          'r2x.use_stars': False})

#        ipdb.set_trace()

        return pipel

    def plot(self, pipel):
        # Testing this plot with the 65 templates.
        pipel.config['r2pz.pzcat.seds'] = find_seds()
        pipel.config['r2pz.pzcat.use_priors'] = False

        r2pz_cat = pipel.r2pz.pzcat.galcat.result
        r2pz_cat = r2pz_cat.join(pipel.r2pz.result)

        r2x_cat = pipel.r2x.get_store()['r2eff']
        r2x_cat = r2x_cat.join(pipel.r2x.test_cat.result)

        K = {'histtype': 'step', 'normed': True, 'bins': 100}
        r2x_cat[0].hist(label='Gal train',  ls='solid', lw=1.5, **K)
        r2pz_cat.r2peak.hist(label='SED fit', ls='dashed', lw=2., **K)

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16)
        plt.legend()

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='k', alpha=0.1)

        xlim = 0.02
        plt.xlim((-xlim, xlim))
        plt.savefig(tosave.path('x60'))

#        ipdb.set_trace()

    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
