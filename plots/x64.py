#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave

#def find
d = '/home/marberi/drv/work/papers/psfw/tasks'
sys.path.append(d)
import galabs

inst = galabs.galabs()
lum_conf = inst.config['lum']

def f(z, a,b,c):
    return a*np.exp(-(1+z)**b) + c

fig, A = plt.subplots(ncols=3)

z = np.linspace(0., 2.)
for key, D in lum_conf.iteritems():
    # phi
    philog = f(z, *D['phi'])
    phi = 10**philog
    A[0].plot(z, phi, label=key)

    # mstar
    Ms = f(z, *D['Ms'])
    A[1].plot(z, Ms, label=key)

    # alpha
    alpha = f(z, *D['alpha'])
    A[2].plot(z, alpha, label=key)

titles = ['phi', 'Mstar', 'alpha']
for i,ax in enumerate(A):
    ax.grid(True)
    ax.set_title(titles[i])
    ax.legend()
#    ipdb.set_trace()

plt.tight_layout()
plt.savefig(tosave.path('x64')) #'test.pdf')
