#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import trainpipel

# Plotting the r,i,z and r,i against each other.
pipel = trainpipel.pipel()
pipel.config.update({\
  'r2x.use_cal': False,
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.use_stars': False, 
  'r2x.filters': ['r','i','z']})


cat = pipel.r2x.test_cat.result
cat = cat.join(pipel.r2x.get_store()['r2eff'])

pipel.config['r2x.filters'] = ['r','i']
cat = cat.join(pipel.r2x.get_store()['r2eff'], rsuffix='_ri')

cat.plot(kind='hexbin', x='0', y='0_ri')

ax = plt.gca()
ax.xaxis.grid(True, which='both')
ax.yaxis.grid(True, which='both')

plt.xlabel('r,i,z', size=16)
plt.ylabel('r,i', size=16)

lim = 0.01

line = np.linspace(-lim, lim)
plt.xlim((-lim, lim))
plt.ylim((-lim, lim))

plt.plot(line, line, ls='--', color='black')

plt.show()
#ipdb.set_trace()
