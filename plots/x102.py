#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import time
import sys
import xdolphin as xd


torun = ['psf17', 'psf2', 'psf22', 'psf4', 'psf5', 'psf6',
         'psf66', 'psf7', 'psf74', 'psf76', 'psf79', 'psf80',
         'psf81', 'psf82', 'psf83', 'psf84', 'psf9', 'psf92',
         'psf93', 'psf94', 'psf95']

#torun = ['psf17', 'psf2', 'psf22']
#torun = ['psf17', 'psf22']
#torun = ['psf22']

t0 = time.time()
for name in torun:
    print('STARTING TO RUN', name)
    t1 = time.time()
    #xd.plot(name).save()
    os.system('./{0}.py'.format(name))
#    print(name, time.time() - t1)

print('time total', time.time() - t0)
