#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

photoz_data = '/data2/photoz/run/data/'
abkids = xd.Job('ab')
abkids.task.config.update({'data_dir': photoz_data,
                           'filter_dir': 'kids_filters',
                           'zmax': 1.,
                           'sed_dir': 'seds_stars'})


j1 = xd.Job('kidscat')


j2 = xd.Job('starsel')
j2.depend['catalog'] = j1

j3 = xd.Job('starfit')
j3.depend['catalog'] = j2
j3.depend['ab'] = abkids

j4 = xd.Job('starabs')
j4.depend['starcat'] = j2
j4.depend['starfit'] = j3


def plot1():
    """Plotting the magnitude distribution for different fields."""

    scat = j4.result
    bins = np.linspace(15., 21., 40)
    #K = {'bins': bins, 'histtype': 'stepfilled', 'normed': True}

    for nr in range(147):
        sub = scat[scat.fieldnr == nr]
        #sub.imag.hist(label='field', **K)
        y,H = np.histogram(sub['imag'], bins=bins, normed=True)
        x = 0.5*(H[:-1] + H[1:])

        plt.plot(x, y, '-', lw=0.5, alpha=0.5)

    # Average distribution for all the fiels.
    y,H = np.histogram(scat['imag'], bins=bins, normed=True)
    x = 0.5*(H[:-1] + H[1:])
    plt.plot(x, y, '-', lw=2.0, color='black')

     
    plt.ylim((0., 0.42))
    plt.xlabel('i-band magnitude', size=16)
    plt.ylabel('Probability', size=16)
    plt.grid(True)

    plt.savefig(tosave.path('x76_mag'))

def get_cat():
    # Experimenting more with the best SED fits.
    cat = j2.result
    sedf = j3.result

    # Only look at the objects with 3 colors.
    sedf = sedf[sedf.nobs == 3.]
    nobs = sedf.nobs
    sedf = sedf.multiply(nobs, axis='rows')
    del sedf['nobs']

    #ipdb.set_trace()
    #xmin = np.argmin(sedf.values, axis=1)

    best_fit = sedf.idxmin(axis=1)
    best_fit.name = 'sed'

    cat = cat.join(best_fit)
    cat['chi2'] = pd.Series(sedf.lookup(best_fit.index, best_fit), 
                            index=best_fit.index)

    return cat

def plot2():
    """The chi2 for the best fit as a function of the i-band
       magnitude.
    """

    cat = get_cat()

    sub = cat.sample(n=10000)
    plt.plot(sub.MAG_AUTO_I, sub.chi2, '.')

    plt.grid(True)
    plt.yscale('log')
    plt.xlabel('i-band magnitude', size=16)
    plt.ylabel('$\chi^2$', size=16)

    plt.savefig(tosave.path('x76_chi2'))

def plot3():
    """Simple 2D histogram of the SEDs."""

    cat = get_cat()
    cat = cat[~np.isnan(cat.chi2)]

    counts = cat.groupby('fieldnr').sed.value_counts()


    inst = xd.Job('starabs')
    sedL = inst.task.find_star_seds()

    #sed_map = pd.Series(sedL.ravel())
    sed_map = pd.Series(range(len(sedL)), index=sedL.ravel())

    nfields = len(cat.fieldnr.unique())
    nsed = len(cat.sed.unique())

    l0, l1 = zip(*counts.index)
    l0 = np.array(l0)
    l1 = sed_map.ix[np.array(l1)].values

    H = np.zeros((nfields, nsed))
    H[l0, l1] = counts.values

    H[137, :] = 0.

    plt.imshow(H)
    plt.xlabel('SED nr', size=16)
    plt.ylabel('Field', size=16)

    plt.savefig(tosave.path('x76_sed_2dhist'))
