#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import psf22

inst = psf22.Plot()

t1 = time.time()
top = inst.topnode()
print('time', time.time()-t1)

top.reduce(inst.f_reduce)

ipdb.set_trace()
