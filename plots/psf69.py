#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    """r2pz versus r2sed scatter plot."""

    def topnode(self):
        import trainpipel
        top = xd.Topnode(trainpipel.pipel())

        return top

    def show(self, topnode):
        pipel = topnode.values()[0]

        r1 = pipel.depend['r2pz.pzcat.galcat'].taskid()
        r2 = pipel.depend['r2sed.test_cat'].taskid()
        assert r1 == r2, 'Using two different galaxy catalogs in different pipelines.'

        gc = pipel.r2sed.test_cat.result
        gc = gc.join(pipel.r2pz.result)
        gc = gc.join(pipel.r2sed.result)

        gc = gc[gc.mag_vis < 24.5]

        Ngal = 3000
        inds = np.random.permutation(gc.index)[:Ngal]
        sub = gc.ix[inds]

        plt.plot(sub.r2peak, sub.r2eff, '.', alpha=0.7)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('SED fit [$"R^2_{\\rm{PSF}}"$ bias]', size=16)
        plt.ylabel('SED train [$"R^2_{\\rm{PSF}}"$ bias]', size=16)

        x = 0.025
        plt.xlim((-x, x))
        plt.ylim((-x, x))

        line = np.linspace(-x, x)
        plt.plot(line, line, '--', lw=2, color='black') #, alpha=0.7)

        plt.show()

    def run(self):
        topnode = self.topnode()
        self.show(topnode)


if __name__ == '__main__':
    Plot().run()
