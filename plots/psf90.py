#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
import time
from matplotlib import pyplot as plt

import xdolphin as xd

import tosave
import psf78

def zx_mods(X):
    raise NotImplemented('This is moved to psf78 or x84')

    # This code
    if X.name == 'magnoise':
        X.config['sn_lims']['zx'] = X.config['sn_lims']['z']

    elif X.name in ['magobs', 'starcat']:
        if not 'zx' in X.config['filters']:
            X.config['filters'] += ['zx']


class Plot(psf78.Plot):

    def _rows(self):

        I = [['r', 'i'],
             ['r', 'i', 'z'],
             ['r', 'i', 'zx']]

        return I 

    def labels(self, X):
        # when testing I want to move this functionality
        # in here.

        lbls = {
          'filters': ','.join(X.r2train.config['filters']),
          'use_stars': 1*X.r2train.config['use_stars'],
          'has_noise': 1*(not X.config['r2train.galcat.const_sn'])
        }

        return lbls

    def add_lines(self, top, ax, sub):
        K = {'histtype': 'step', 'bins': 100}
        sub = sub.sort('filters')
        for l, row in sub.iterrows():
            pipel = top[l]

            cat = pipel.r2train.get_store()['r2eff']
            cat = cat.join(pipel.r2train.galcat.result)

            # Plot the histograms of multiple pointings.
            lbl = row['filters'] 
            Nboot = pipel.r2train.config['Nboot']
            cat[range(Nboot)].unstack().hist(ax=ax, label=lbl, **K)

        ax.legend(prop={'size': 12})

    # This has to be handled in a better way.
    set_lim = True

    def after_plot(self, A):
        path = tosave.path('psf90')
        plt.savefig(path)

    def run(self):
        top = self.topnode()
        top.update_config({'r2train.Nboot': 100})
        top.rec_apply(zx_mods)

        labels = top.labels(self.labels)

        fig, A = plt.subplots(nrows=2, ncols=2, sharex='col', sharey='row')
        fig.set_size_inches(15, 15)
        for i,use_stars in enumerate([0, 1]):
            for j,has_noise in enumerate([0, 1]):
                sub = labels[(labels.use_stars == use_stars) & (labels.has_noise==has_noise)]
                ax = A[i,j]

                self.add_lines(top, ax, sub)

                if i == 1:
                    A[i,j].set_xlabel('Noisy' if has_noise else 'Noiseless')

            A[i,0].set_ylabel('Stars' if use_stars else 'Galaxies')

        if self.set_lim:
            A[0,0].set_xlim((-0.01, 0.01))
            A[0,1].set_xlim((-0.03, 0.03))

        self.after_plot(A)

if __name__ == '__main__':
    Plot().run()
