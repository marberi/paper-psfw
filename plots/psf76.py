#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

def gen_styles():
    i = 0
    styles = ['-','-.','--', ':']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    name = 'psf76'

    def pipel(self):
        import trainpipel
        pipel = trainpipel.pipel()

        pipel.config.update({\
            'r2x.filters': ['r','i', 'z'],
        #    'r2x.filters': ['r','i'],
            'r2x.all_test': True,
            'r2x.use_stars': True,
            'r2x.Nboot': 100,
            'r2x.Ntrain': 400})

        return pipel

    def topnode(self):
        pipel = self.pipel()

        layers = xd.Layers()
        layers.add_pipeline(pipel)
        layers.add_layer_key('z_cal', 'r2x.z_cal', ['zs', 'zb'])
        layers.add_layer_key('z_test', 'r2x.z_test', ['zb'])

        top = layers.to_topnode()

        return top

    def f_reduce(self, pipel):

        zfield = 'zb'
        z = np.linspace(0.1, 1.8, 10)

        cat = pipel.r2x.test_cat.result
        cat = cat.join(pipel.r2x.test_pz.result.unstack(), rsuffix='_ignore')
        cat = cat.join(pipel.r2x.get_store()['r2eff'])


        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2x.config['Nboot']
        mean = cat.groupby(pd.cut(cat[zfield], z)).mean()
    
        part = pd.DataFrame()
        part['r2bias'] = mean[range(Nboot)].mean(axis=1)
        part['r2bias_std'] = np.sqrt(mean[range(Nboot)].var(axis=1))
        part['z'] = mean.zb.values
#        part['pipel'] = len(part)*[key]
        part['zbin'] = range(len(part))

        # It complains when trying to cache the index.
        F = part.reset_index()
        del F['zb']

        # .
        return F

    def plot(self, prepare):
        styles = gen_styles()
        colors = ['k', 'b', 'g', 'r']
        order = [('zb', 'zb'),
                 ('zs', 'zb')]

        for i,(z_cal,z_test) in enumerate(order):
            sub = prepare[(prepare.z_cal == z_cal) & (prepare.z_test == z_test)]
            sub = sub.sort_values('z')

            lbl_z = 'z_p' if z_cal == 'zb' else 'z_s'
            lbl = 'Calibration z: ${0}$'.format(lbl_z) #z_cal)

            ls = next(styles)

            plt.errorbar(sub.z, sub.r2bias, sub.r2bias_std, ls=ls, \
                         color=colors[i], label=lbl)

        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

        ax = plt.gca()

        plt.xlabel('$z_{\mathrm{p}}$', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)

        ylim = 0.0015
        plt.xlim((0.2, 1.8))
        plt.ylim((-ylim, ylim))
        plt.grid(True)
        plt.legend()
        plt.subplots_adjust(left=0.155)

if __name__ == '__main__':
    xd.plot(Plot).save()
