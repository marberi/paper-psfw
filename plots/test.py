#!/usr/bin/env python
# encoding: UTF8

# Testing some of the new galaxy mocks.
import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job

from matplotlib import pyplot as plt

import trainpipel
pipel1 = trainpipel.pipel()
pipel2 = trainpipel.pipel(new_galcat=True)

I = [('Normal', pipel1), ('New', pipel2)]

def plot1():
    for lbl,P in I:
        sed = P.kids['galabs']['mabs']['cat'].sed
        ipdb.set_trace()
        sed.hist(bins=100, histtype='step', normed=True, label=lbl)

    plt.legend()
    plt.show()

def plot2():
    for lbl,P in I:
        mag = P.kids['galcatN']['magcat']['cat']
        r2eff = P.kids['r2gal']['r2eff']['r2eff']

        cat = mag.join(r2eff)
        (cat.mag_r - cat.mag_i).hist(bins=30, histtype='step', normed=True, label=lbl)

    plt.legend()
    plt.show()


def plot3():
    for lbl,P in I[1:]:
        mag = P.kids['galcatN']['magcat']['cat']
        r2eff = P.kids['r2gal']['r2eff']['r2eff']
        cat = mag.join(r2eff)
        col = cat.mag_r - cat.mag_i

        plt.plot(col, r2eff, '.', label=lbl, alpha=0.4)

    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    plt.xlabel('r - i', size=16)
    plt.ylabel('$"R^2_{\\rm{PSF}}"$ value', size=16)

    plt.legend(loc=2)
    plt.show()

def plot4():
    for lbl,P in I: #[1:]:
        mag = P.kids['galcatN']['magcat']['cat']
        r2eff = P.kids['r2gal']['r2eff']['r2eff']
        mabs = P.kids['galabs']['mabs']['cat']

        cat = mag.join(r2eff)
        cat = cat.join(mabs, rsuffix='x')
        cat['col'] = cat.mag_r - cat.mag_i

        za = 0.1
        dz = 1.9
        cat = cat[(za < cat.zs) & (cat.zs < za+dz)]
        val = cat[(0.5 < cat.sed) & (cat.sed < 1.5)] 
        plt.plot(val.col, val.r2eff, '.', label=lbl, alpha=0.4)

    plt.legend()
    plt.show()

def plot5():
    for lbl,P in I: #[1:]:
        mag = P.kids['galcatN']['magcat']['cat']
        r2eff = P.kids['r2gal']['r2eff']['r2eff']
        mabs = P.kids['galabs']['mabs']['cat']

        cat = mag.join(r2eff)
        cat = cat.join(mabs, rsuffix='x')

        val = cat[(0.5 < cat.sed) & (cat.sed < 1.5)]

        val.zs.hist(bins=20, histtype='step', normed=True, label=lbl)

    plt.legend()
    plt.show()

def plot6():
    for lbl,P in I: #[1:]:
        mag = P.kids['galcatN']['magcat']['cat']
        r2eff = P.kids['r2gal']['r2eff']['r2eff']
        mabs = P.kids['galabs']['mabs']['cat']

        mag.mag_vis.hist(bins=30, histtype='step', normed=False, label=lbl, lw=3)

    plt.axvline(24.5, color='black', lw=4)
    plt.legend()
    plt.show()

plot4()

#lbl,P = I[1]
#P.kids['r2train']()

#ipdb.set_trace()
