#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

class Plot:
    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()
        pipel.config.update({
          'r2x.use_cal': False,
          'r2x.filters': ['r', 'i', 'z'],
        #  'r2x.filters': ['r', 'i'],
          'r2x.all_test': True,
#          'r2x.Ntrain': 400,
          'r2x.Ntrain': 400,
          'r2x.Nboot': 2,
          'r2x.use_stars': False})

        pipel.config['r2x.Ntrain'] = 5000

        del pipel.r2x.depend['cal_pz']
        del pipel.r2x.depend['test_pz']
        top = xd.Topnode(pipel)
        top.add_layer_key('r2x.Ntrain', [400, 5000])
        top.add_layer_key('r2x.test_cat.r', [1.0, 10, 100])

        return top

    def plot(self, top):
        fig, A = plt.subplots(ncols=2)
        for i in range(2):
            for key,pipel in top.iteritems():
                if not key[1] == i: continue

                r2x_cat = pipel.r2x.get_store()['r2eff']
                r2x_cat = r2x_cat.join(pipel.r2x.test_cat.result)

                rval = pipel.r2x.test_cat.config['r']
                lbl = 'r: {0}'.format(rval)
                r2x_cat[0].hist(ax=A[i], label=lbl, bins=100, histtype='step')


            A[i].legend()

        plt.savefig('test.pdf')

#        K = {'histtype': 'step', 'normed': True, 'bins': 100}
#        r2x_cat[0].hist(label='Gal train',  ls='solid', lw=1.5, **K)
#        r2pz_cat.r2peak.hist(label='SED fit', ls='dashed', lw=2., **K)
#
#        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
#        plt.ylabel('Probability', size=16)
#        plt.legend()
#
#        yline = np.linspace(*plt.ylim())
#        plt.fill_betweenx(yline, -3e-4, 3e-4, color='k', alpha=0.1)
#
#        xlim = 0.02
#        plt.xlim((-xlim, xlim))
#        plt.savefig(tosave.path('x68'))


    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
