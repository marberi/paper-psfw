#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
import cosmolopy as cpy

from matplotlib import pyplot as plt

import tosave

#def find
d = '/home/marberi/drv/work/papers/psfw/tasks'
sys.path.append(d)
import galabs

inst = galabs.galabs()
lum = inst.config['lum']

def f(z, a,b,c):
    return a*np.exp(-(1+z)**b) + c


z = np.linspace(0.01, 4.0, 300)
Dl = cpy.distance.luminosity_distance(z, **cpy.fidcosmo)
Mconv = 5*np.log10(Dl) + 25

# Then trying again to estimate the luminousity function..
tD = {}
for gal_type, Ki in lum.iteritems():
    phi = f(z, *Ki['phi'])
    Ms = f(z, *Ki['Ms'])
    alpha = f(z, *Ki['alpha'])

    # Here we are using the cumulative luminousity function.
    M = np.linspace(-24, -18, 200)
    M = np.linspace(-24, -17, 200)

    x = 10**(-0.4*np.subtract.outer(M, Ms))
    Ni = (10**phi)*(x**alpha)*np.exp(-x)

    # Better version:
    mag = np.add.outer(M, Mconv)
#    touse = np.logical_and(17. < mag, mag < 28)
#    Ni[np.logical_not(touse)] = 0.

    tD[gal_type] = Ni

from scipy.interpolate import RectBivariateSpline

# Then looking at these distributions.
weightD = {}
#for gal_type in ['ell']:
for gal_type, Ni in tD.iteritems():
    Ni = tD[gal_type]

    spl = RectBivariateSpline(M, z, Ni)
    weightD[gal_type] = spl.integral(-30, 0, 2., 10.)

print('Weights')
print(weightD)

#ipdb.set_trace()
