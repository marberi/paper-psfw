#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

import trainpipel
import bootstrap

class Plot:
    """The photo-z R^2 estimation when splitting on zp-zs."""

    name = 'psf7'

    def pipel(self):
        pipel = trainpipel.pipel(toplevel='r2pz')

        J = pipel.r2pz
        J.pzcat.galcat.magcat.mabs.config['Ngal'] = 30000 
        J.pzcat.galcat.magcat.mabs.config['Ngal'] = 3000 # TEST...
        J.pzcat.config.update({'out_pdf': True, 'out_pdftype': True})
#
        return pipel

    def f_reduce(self, pipel):
        ipdb.set_trace()

        J = pipel.r2pz
        r2eff = J.r2gal.result
        r2pz = J.result
        galcat = J.pzcat.galcat.result
        mabs = J.pzcat.galcat.magcat.mabs.result
        pzcat = J.pzcat.result.unstack()

        # I should be consider a proper join, but this should work
        # by now..
        df = pd.concat([r2eff,r2pz,galcat,mabs,pzcat], axis=1)

        return df

    def plot(self, prepare):
        df = prepare.ix[0]

        zs = df['zs'].iloc[:,0]
        df['px'] = (df.zb - zs) / (1+zs)

        # Cut in magnitude.
        df = df[df.mag_vis < 24.5]

        sedl = [0,17,55,65]
        S = pd.cut(df.sed, sedl, labels=['ell', 'sp', 'irr'])
        x = np.linspace(-0.4, 0.4, 20)

        lblD = {'ell': 'Elliptical', 'sp': 'Spiral', 'irr': 'Irregular'}
        lsD = {'ell': 'solid', 'irr': 'dashed', 'sp': 'dashdot'}
        for key, val in df.groupby(S):
            gr = pd.cut(val.px, x)
            px_mean = val.px.groupby(gr).mean()

            r2key = 'r2peak'
            bias, bias_std = bootstrap.bootstrap(val[r2key], gr)

            plt.errorbar(px_mean, bias, bias_std, 
                         ls=lsD[key], label=lblD[key])


        plt.xlabel('$z_{\mathrm{p}}$ - $z_{\mathrm{s}}$', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)

        plt.subplots_adjust(left=0.14)

        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')
        plt.xlim(-0.1, 0.1)
        plt.ylim(-0.005, 0.005)

        # NOTE: This is not working in the next matplotlib version.
        # Tweaking the label markers. 
        handles, labels = ax.get_legend_handles_labels()
#        handles = [x[0] for x in handles]
        ax.legend(handles, labels, loc=4) #, prop={'size': 14})

if __name__ == '__main__':
    xd.plot(Plot).save()
