#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
from matplotlib import pyplot as plt

import psf10
import psf12

class psf50:
    name = 'psf50'
    config = {}

    def train(self, B):

        from sklearn import svm
        
        lbl = 'noiseless'
        lbl = 'noisy'

        dep = B[lbl]['r2train'].depend
        starcat = dep['starcat']['magcat']['cat']

        fL = ['r', 'i', 'z']
#        fL = ['r', 'i'] #, 'z']
        cols = map('mag_{0}'.format, fL)

        # Doing the fitting....
        At_mag = starcat[cols].values
        At = At_mag[:,:-1] - At_mag[:,1:]
        r2star = dep['r2star']['r2eff']['r2eff']

        inds = np.arange(len(r2star))
        inds = np.random.permutation(inds)

        N = 4000
        inds = inds[:N]

        reg1 = svm.NuSVR()
        reg2 = svm.NuSVR()
        reg1.fit(At[inds,:1], r2star.ix[inds,0])
        reg2.fit(At[inds], r2star.ix[inds,0])

        r2true = dep['r2gal']['r2eff']['r2eff']['r2eff']

        # Selecting a subset of the galaxies for the
        # second correction.
        inds2 = np.arange(len(r2true))
        inds2 = np.random.permutation(inds)
        inds2 = inds2[:N]


        cat = B[lbl]['galnoise']['magcat']['cat']
        cat_nn = dep['galcat'].depend['magcat']['magcat']['magcat']

        Ap_mag = cat[cols].values
        Ap = Ap_mag[:,:-1] - Ap_mag[:,1:]

        Apnn_mag = cat_nn[cols].values
        Apnn = Apnn_mag[:,:-1] - Apnn_mag[:,1:]


        cal_ri = reg1.predict(Ap[inds2,:1])
        cal_all = reg2.predict(Ap[inds2])
        cal_nn = reg2.predict(Apnn[inds2])

#        r2eff_cal_ri = (cal_ri - r2true) / r2true
#        r2eff_cal_all = (cal_all - r2true) / r2true
        from sklearn.neighbors import  KNeighborsRegressor

        filters_corr = ['r', 'i', 'z']
#        filters_corr = ['r', 'i']
        col_corr = map('mag_{0}'.format, filters_corr)
        col_corr += ['zs']

        reg3 = KNeighborsRegressor()
        reg4 = KNeighborsRegressor()
        reg5 = KNeighborsRegressor()

        f1 = cal_all / cal_ri
        f2 = cal_nn / cal_all
#        f2 = cal_all / cal_nn # Testing the inverse...

#        reg3.fit(Ap[inds2,:1], f)
        reg3.fit(cat[col_corr].values[inds2], f1)
        reg4.fit(Ap[inds2], f2)
        reg5.fit(cat[col_corr].values[inds2], f2)

#        ipdb.set_trace()

        r2pred_ri = reg1.predict(Ap[:,:1])
        r2pred_all = reg2.predict(Ap)
        pred_f1 = reg3.predict(cat[col_corr].values) #Ap[:,:1])
        pred_f2 = reg4.predict(Ap)
        pred_f3 = reg5.predict(cat[col_corr].values) #Ap[:,:1])

        r2pred_c1 = pred_f1 * r2pred_ri
        r2pred_c2 = pred_f2 * r2pred_all
        r2pred_c3 = pred_f3 * r2pred_all

#        ipdb.set_trace()

        cat['r2eff_c1'] = (r2pred_c1 - r2true) / r2true
        cat['r2eff_c2'] = (r2pred_c2 - r2true) / r2true
        cat['r2eff_c3'] = (r2pred_c3 - r2true) / r2true
        cat['r2eff_all'] = (r2pred_all - r2true) / r2true
        cat['r2eff_ri'] = (r2pred_ri - r2true) / r2true

        cat = cat[cat.mag_vis < 24.5]

        if False:
            fields = ['c1', 'all', 'ri']
            for f in fields:
                cat['r2eff_'+f].hist(bins=100, label=f, histtype='step', normed=True)

            plt.legend()
            plt.show()

        zs = np.linspace(0.1, 1.5, 10)
        K = cat.groupby(pd.cut(cat.zs, zs)).mean()

        # Then testing the predictions..

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')
        
        fields = ['c1', 'c2', 'c3', 'all', 'ri']
        fields = ['all', 'c3']

        for f in fields:
            plt.plot(K.zs, K['r2eff_'+f], 'o-', label=f)

        plt.legend()
        plt.show()

        ipdb.set_trace()


        plt.plot(r2eff_cal_ri, r2eff_cal_all, '.')
        plt.show()

#        ipdb.set_trace()



        r2pred = reg.predict(Ap)
        r2eff = (r2pred - r2true) / r2true
        cat['r2eff'] = r2eff

        return cat

    def d1(self, B):

        cat = self.train(B)

        if False:
            r2true = dep['r2gal']['r2eff']['r2eff']['r2eff']

            r2pred = reg.predict(Ap)
            r2eff = (r2pred - r2true) / r2true

            x = np.linspace(-0.005, 0.005)
            y = [(r2eff-xi).mean() for xi in x]
            y = np.abs(np.array(y))

            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            plt.plot(x, y, 'o-')
            plt.show()

        if True: #False:
            cat = cat[cat.mag_vis < 24.5]
            zs = np.linspace(0.1, 2.0, 5)

            nbins = 200
            for key, val in cat.groupby(pd.cut(cat.zs, zs)):
                val.r2eff.hist(bins=nbins, label=key, histtype='step', normed=True)

            cat[0.5 < cat.zs].r2eff.hist(bins=nbins, label='Total', lw=2, color='k', histtype='step', normed=True)
            plt.legend()
            plt.show()


        if False:
            r2true = dep['r2gal']['r2eff']['r2eff']['r2eff']
            r2pred = reg.predict(Ap)
            r2eff = (r2pred - r2true) / r2true
            cat['r2eff'] = r2eff

            cat_nn = dep['galcat'].depend['magcat']['magcat']['magcat']

            mag_nn = cat_nn[cols].values
            col_nn = mag_nn[:,:-1] - mag_nn[:,1:]

            delta_col = Ap - col_nn
            cat['delta_ri'] = delta_col[:,0]
    #        cat['delta_iz'] = delta_col[:,1]

            x = np.linspace(-0.5, 0.5, 10)

    #        for key, val in cat.groupby(pd.cut(cat.delta_ri, x)):
    #            ipdb.set_trace()


            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            field = 'delta_ri' 
    #        field = 'delta_iz' 
            zs = np.linspace(0.1, 2.0, 5)
            for key, val in cat.groupby(pd.cut(cat.zs, zs)):

                K = val.groupby(pd.cut(val[field], x)).mean()
                plt.plot(K[field], K.r2eff, 'o--', label=key)


            K = cat.groupby(pd.cut(cat[field], x)).mean()
            plt.plot(K[field], K.r2eff, 'o-', color='k', lw=2, label='Total')

            plt.xlabel(field, size=16)
            plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

            plt.legend(loc=0)

            plt.show()

#        ipdb.set_trace()

    def run(self):
        inst = psf10.psf10()
#        inst = psf12.psf12()
#        topnode = inst.create_topnode()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
#            A['r2train'].config['use_stars'] = False
#            A['r2train'].config['use_stars'] = True
#            A['r2train'].config['Nboot'] = 200 #use_stars'] = True
            A['r2train'].config['Nboot'] = 30 #use_stars'] = True
            A['r2train'].config['filters'] = ['r', 'i']

#            ipdb.set_trace()
#            A['galabs'].config['Ngal'] = 1000000 #2e6 #200007
#            A['r2train'].config['Ntest'] = 1000000
#            ipdb.set_trace()

#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['Ntest'] = 200000
            if lbl == 'noiseless':
                A['galabs'].config['Ngal'] = 200008

            B[lbl] = A


        self.d1(B)

if __name__ == '__main__':
    Job('psf50').run()
