#!/usr/bin/env python
# encoding: UTF8

import copy
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel


import tosave

class Plot:    

    def change(self, Aorig, gtype):
        A = copy.deepcopy(Aorig)
        B = A[gtype]['Ms']

        #B[-1] = B[-1]*1.02
        B[-1] = B[-1] + 0.02

        return A

#        ipdb.set_trace()

    def topnode(self):
        pipel = trainpipel.pipel(new_star_pipel=True,
                                 star_r2train_task='r2x')
#        replace_r2x(pipel)

        pipel.config.update({\
            'r2x.filters': ['r','i', 'z'],
            'r2x.all_test': True,
            'r2x.use_stars': True, #True,
            'r2x.Nboot': 100,
#            'r2x.Nboot': 2,
            'r2x.bin_by': 'zbin',
            'r2x.Ntrain': 400})

        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000

        A = pipel.r2x.cal_cat.magcat.mabs.config['lum']
#        A = copy.deepcopy(Aorig)
#        A['ell']['Ms']

        B = self.change(A, 'ell')
        

        top = xd.Topnode(pipel)
#        var = 'r2x.train_star_cat.magcat.magcat.starcat.resample'
        X = 'r2x.train_star_cat.magcat.magcat.starcat'

        star_conf = {'r2x.bin_by': 'zbin', X+'.resample': True, 
                     X+'.sed_distr': True}

#        top.add_layer([{'r2x.use_cal': False}, {'r2x.bin_by': 'zbin'},
#                       {'r2x.bin_by': 'c0bin'}, star_conf])

#        top.add_layer([{'r2x.bin_by': 'zbin'}]) #, star_conf])
        layer_key = []
        for x in ['ell', 'sp', 'irr']:
            layer_key.append(self.change(A, x))
#        layer = map(['ell', 'sp', 'irr'], self.
        top.add_layer_key('r2x.cal_cat.magcat.mabs.lum', layer_key)
        top.add_layer(trainpipel.star_layer())


#        ipdb.set_trace()


#{'r2x.bin_by': 'zbin', var: True}])

        return top



    def prepare(self, topnode):

        z = np.linspace(0.1, 1.8, 7)
        df = pd.DataFrame()
        for key, pipel in topnode.iteritems():
            cat = pipel.r2x.test_cat.result
            cat = cat.join(pipel.r2x.get_store()['r2eff'])
            cat = cat[cat.mag_vis < 24.5]

            cols = range(pipel.r2x.config['Nboot'])
            use_stars = pipel.r2x.config['use_stars']

            orig_vals = [-21.9, -21.0, -20.0]
            lum = pipel.r2x.cal_cat.magcat.mabs.config['lum']
            gL = ['ell', 'sp', 'irr']

            new_vals = [lum[gtype]['Ms'][-1] for gtype in gL]
            changed = np.array(gL)[np.array(new_vals) != np.array(orig_vals)][0]


#            mean = cat.groupby(pd.cut(cat.zs, z)).mean()
            for zi,(k2, sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
                mean = sub.mean()

                real = pd.Series([mean[x] for x in cols]) # yes, this is weird
                S = pd.Series({'zbin': zi, 'z': sub.zs.mean(), 
                               'pipel': key,
                               'use_stars': use_stars,
                               'gtype': changed,
                               'r2bias': real.mean(),
                               'r2bias_std': np.sqrt(real.var())})

                df = df.append(S, ignore_index=True)

        return df

    def plot(self, prepare):
        lbls = ['Uncalibrated', 'By redshift', 'By r-i color', 'By redshift, KiDS stars']
        lbls = ['By r-i color']

        lsL = ['--', '-', '-.', ':']
        lsL = ['-.', '--', ':', '-']
        colL = ['b', 'g', 'r', 'k']
        for i,(key, sub) in enumerate(prepare.groupby('pipel')):

#            ipdb.set_trace()
#            ls = lsL[i]
#            lw = 1.5 if ls == '-' else 2.0
            row = sub.iloc[0]
            lbl = 'Stars' if row.use_stars else 'Gal'
            lbl = '{0}: {1}'.format(lbl, row.gtype)

            ls = '-' if row.use_stars else '--'
#            ipdb.set_trace()

            plt.errorbar(sub.z, sub.r2bias, sub.r2bias_std, label=lbl, ls=ls)
#                         lw=lw) #, fmt=ls, color=colL[i], label=lbls[key[0]])

        plt.xlabel('Redshift [zs]', size=16)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=17)

        plt.xlim((0.1, 1.9))
        plt.ylim((-0.0015, 0.002))

        # Shade the requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        # Just looked ugly without
#        ylim = 0.0025
        plt.ylim((-0.002, 0.0027))
        plt.subplots_adjust(left=0.14)
        plt.grid(True)
        #plt.legend(loc=9)
        plt.legend(loc=(0.20,0.75), prop={'size': 12})
        plt.savefig(tosave.path('x95'))
   
    def run(self):
        topnode = self.topnode()
        prepare = self.prepare(topnode)
        self.plot(prepare)

if __name__ == '__main__':
    Plot().run()
