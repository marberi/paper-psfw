#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

if False:
    d = '/data2/marberi/data/'
    fname = 'cfhtlens_W3_with_unique_numeric_id.csv.bz2'

    path = os.path.join(d, fname)
    cat = pd.io.api.read_csv(path)


j = xd.Job('csv_bz2')
j.config['file_name'] = 'cfhtlens_W3_with_unique_numeric_id.csv.bz2'
#j.run()

cat = j.result
#cat.columns.rename('ALPHA_J2000', 'ra')
#cat.columns.rename('DELTA_J2000', 'dec')
cat['ra'] = cat['ALPHA_J2000']
cat['dec'] = cat['DELTA_J2000']

cat = cat[cat.MAG_i.abs() != 99]
isstar = 0.8 < cat.CLASS_STAR
gal = cat[~isstar]
stars = cat[isstar]

def plot1():
    """Show the magnitude distribution for starts and galaxies
       in CFHTls.
    """

    K = {'bins': 100, 'histtype': 'step', 'normed': True}
    gal.MAG_i.hist(label='Galaxies', **K)
    stars.MAG_i.hist(label='Stars', **K)

    plt.xlabel('mag i-band', size=16)
    plt.ylabel('Probability', size=16)

    plt.legend()
    plt.show()


def plot2():
    """Show where the different objects are located."""

    plt.plot(cat.ra, cat.dec, '.')
    plt.show()


#ra_min, ra_max = cat.ra.min(), cat.ra.max()
ra_min = cat.ra.min()
ra_max = cat.ra.max()
dec_min = cat.dec.min()
dec_max = cat.dec.max()

dra = ra_max - ra_min
ddec = dec_max - dec_min

ra_edges = np.arange(ra_min, ra_max, 0.735)
dec_edges = np.arange(dec_min, dec_max, 0.735)

for i,rai in enumerate(ra_edges):
    plt.axvline(rai, ls='--', lw=2.)

for i,deci in enumerate(dec_edges):
    plt.axhline(deci, ls='--', lw=2.)

plt.show()
