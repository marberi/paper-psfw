#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import xdolphin as xd
from xdolphin import Job

import euclid_setup
#from euclid_setup import lim_point, lim_ext

data_dir = '/nfs/pic.es/user/e/eriksen/data/psfw/'

def f_add_noise(pipel, add_noise):
    """Add noise or not to the magnitudes."""
    # These values should later be moved into the Euclid setup. By
    # now they are doing fine here.

    if add_noise:
        # Not very good to avoid the configuration API..
        euclid_conf = euclid_setup.config()
        pipel.galnoise.config.task_config['sn_lims'].update(euclid_conf['lim_ext'])
        pipel.starnoise.config.task_config['sn_lims'].update(euclid_conf['lim_point'])
        pipel.starnoise.config['const_sn'] = False
    else:
        pipel.galnoise.config.update({'const_sn': True, 'sn': 1000000})
        pipel.starnoise.config.update({'const_sn': True, 'sn': 1000000})

    return pipel

def f_add_r2pz(pipel):
    """Using a photo-z code to estimate "R^2_{\\rm{PSF}}"."""

    riz_pzcat = xd.Job('pzcat')
    riz_pzcat.depend['galcat'] = pipel.depend['r2train.galcat']
    riz_pzcat.depend['ab'] = pipel.depend['pzcat.ab']

    pzcat = pipel.depend['pzcat']
    riz_pzcat.config['sky_spec'] = os.path.join(data_dir, 'sky_spectrum.txt')
    for x in ['filter_dir', 'sed_dir']:
        riz_pzcat.config[x] = pzcat.config[x]


    r2pz = xd.Job('r2pz')
    r2pz.depend['pzcat'] = riz_pzcat
    r2pz.depend['r2vz'] = pipel.depend['r2train.r2gal.r2vz']
    r2pz.depend['r2gal'] = pipel.depend['r2train.r2gal']

    # These should probably be set elsewhere.
    r2pz.config.update({\
#      'pzcat.filters': ['r', 'i', 'z'],
#      'pzcat.dz': 0.001,
      'pzcat.out_pdf': False,
      'pzcat.out_pdftype': False,
      'pzcat.zmax': 5.})
#      'pzcat.use_priors': False})

    pipel.depend['r2pz'] = r2pz

def f_add_galtrain(pipel):

    # Noiseless version for training.
    train_gal = pipel.r2train.galcat.magcat.clone()
    train_gal.mabs.config['Ngal'] = 85000 # Just so.

    train_gal_r2 = pipel.r2train.r2gal.clone()
    train_gal_r2.depend['mabs'] = train_gal.mabs

    pipel.r2train.depend['train_gal_cat'] = train_gal
    pipel.r2train.depend['train_gal_r2'] = train_gal_r2


def f_add_r2sed(pipel):
    r2sed = xd.Job('r2sed')

    train = pipel.galnoise.magcat.clone()


    _train = pipel.r2train.clone()
    _test = pipel.r2train.clone()

    _train.galcat.magcat.mabs.config['Ngal'] = 10000

    r2sed.depend['train_r2gal'] = _train.depend['r2gal']
    r2sed.depend['train_cat'] = _train.depend['galcat.magcat']

    r2sed.depend['test_r2gal'] = _test.depend['r2gal']
    r2sed.depend['test_cat'] = _test.depend['galcat']

    pipel.depend['r2sed'] = r2sed

    return pipel

def f_add_r2x(pipel):
    """Add a task for training with a calibration sample."""

    # Copy over the entried from r2train.
    r2x = xd.Job('r2x')
    name_map = {'galcat': 'test_cat', 'r2gal': 'test_r2'}
    for dep_name, dep_job in pipel.r2train.depend.iteritems():
        new_name = name_map[dep_name] if dep_name in name_map \
                   else dep_name

        r2x.depend[new_name] = dep_job

    tmp = xd.Job('empty')
    tmp.depend['cal_cat'] = pipel.r2train.galcat
    tmp.depend['cal_r2'] = pipel.r2train.r2gal

    # Create a special calibration sample.
    assert tmp.cal_r2.mabs.jobid == tmp.cal_cat.magcat.mabs.jobid

    tmp = tmp.clone()
    mabs = tmp.cal_r2.mabs
    mabs.config['Ngal'] = 50014
#    ipdb.set_trace()

    for dep_name, dep_job in tmp.depend.iteritems():
        r2x.depend[dep_name] = dep_job

    # Setting up the photo-z catalogs.

#    test_p
#    ipdb.set_trace()

    pipel.depend['r2x'] = r2x

#    ipdb.set_trace()

def OLDf_add_r2x_pz(pipel, config):
    """Include photo-z catalogs for the "R^2_{\\rm{PSF}}" calibration."""

    r2x = pipel.r2x

    test_pz = pipel.pzcat.clone()
    test_pz.depend['galcat'] = r2x.test_cat
    cal_pz = pipel.pzcat.clone()
    cal_pz.depend['galcat'] = r2x.cal_cat

    r2x.depend['test_pz'] = test_pz
    r2x.depend['cal_pz'] = cal_pz



def f_add_r2x_pz(pipel, config):
    # Copied from the main function. Should be deleted there if
    # this works.


    # Not sure if these are actually needed.
    pz_conf = {'filter_dir': config['filter_dir'],
               'sed_dir': os.path.join(data_dir, 'seds'),
               'sky_spec': os.path.join(data_dir, 'sky_spectrum.txt'),
               'add_noise': False,
               'out_pdf': False}


    r2x = pipel.r2x
    # First doing this almost manually...on the 'test_cat'
    for name in ['test', 'cal']:
        galcat = r2x.depend['{0}_cat'.format(name)]

        euclid_conf = euclid_setup.config() # Not exactly right...
        if config['ind_noise_{0}'.format(name)]:
            galcat_pz = Job('magnoise')
            galcat_pz.config.update({'const_sn': False, 'isindep': True})
            galcat_pz.config.task_config['sn_lims'].update(euclid_conf['lim_ext'])
#            galcat_pz.depend['magcat
            galcat_pz.depend['magcat'] = galcat.magcat
#            ipdb.set_trace()
        else:
            galcat_pz = galcat

        pzcat = Job('pzcat')
        pzcat.depend['galcat'] = galcat_pz
        pzcat.depend['ab'] = galcat.magcat.ab
        pzcat.config.update(pz_conf)

        r2x.depend['{0}_pz'.format(name)] = pzcat

def zx_mods(X):
    if X.name == 'magnoise':
        X.config['sn_lims']['zx'] = X.config['sn_lims']['z']

    elif X.name in ['magobs', 'starcat']:
        if not 'zx' in X.config['filters']:
            X.config['filters'] += ['zx']


def pipel(star_fit=False, add_pz=True, 
          ind_noise_cal=False, ind_noise_test=False,
          new_galcat=False, smag_distr=True,
          add_noise=True,
          new_star_pipel=True,
          use_zx=False,
          toplevel=['r2x'],
          star_r2train_task='r2x',
          filter_dir = '/data2/photoz/run/data/des_euclid'):

    # Trying to simplify the interface.
    restrictive_add = True


    # Setting the new filter_dir here is the simplest.
    filter_dir = os.path.join(data_dir, 'des_euclid2')
    if use_zx:
        filter_dir = '/data2/photoz/run/data/des_euclid3'

    new_galcat = True

#    new_galcat = False

    # Testing that I did not just pass the dictionary.
    assert not isinstance(star_fit, dict)

    # This might be more suitable for a decorator, but
    # keeping things simple by now...
    config = {'star_fit': star_fit, 
              'add_pz': add_pz, 
              'ind_noise_cal': ind_noise_cal,
              'ind_noise_test': ind_noise_test,
              'smag_distr': smag_distr,
              'filter_dir': filter_dir}

    ab_dir = '/data2/photoz/run/data/'
#    filter_dir = '/data2/photoz/run/data/des_euclid'
#    data_dir = '/data2/photoz/run/data/'

    ab_dir = data_dir
    abgal = Job('ab')
    abstar = Job('ab')

    abkids = Job('ab')
    abgal.task.config.update({'data_dir': ab_dir,
                              'filter_dir': filter_dir,
                              'sed_dir': 'seds'})

    abstar.task.config.update({'data_dir': ab_dir,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abkids.task.config.update({'data_dir': ab_dir,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})

    # Stars
    starcatN = Job('starcat')
    starcatN.depend['ab'] = abstar

    if config['star_fit']:
        starfit = Job('starfit')
        starfit.depend['ab'] =  abkids

        starcatN.depend['sed_prob'] = starfit

    if config['smag_distr']:
        smag_prob = Job('stardistr')
        starcatN.depend['mag_prob'] = smag_prob

    star_noise = Job('magnoise')
    star_noise.depend['magcat'] = starcatN

    starcat = Job('offset')
    starcat.depend['magcat'] = star_noise

    # Galaxies.
    if new_galcat:
        mabs = Job('galabs')
    else:
        mabs = Job('mabs')
        mabs.config['Ngal'] = 'all'

    #mabs.config['Ngal'] = 54000
#    mabs.config['Ngal'] = 100000
    galcatN = Job('magobs') #GalCat')
    galcatN.depend['mabs'] = mabs
    galcatN.depend['ab'] = abgal

    galcat_noise = Job('magnoise')
    galcat_noise.depend['magcat'] = galcatN
    galcat_noise.config['const_sn'] = False


    galcat = Job('magcut')
    galcat.depend['magcat'] = galcat_noise

    # HACK. ... testing the combined pipeline
    galcat = galcat_noise

    # R2 galaxies.
    r2vz_gal = Job('r2vz')
    r2vz_gal.config['filter_dir'] = filter_dir
    r2vz_gal.config['data_dir'] = data_dir
#    r2vz_gal.config['Nint'] = 300

    r2gal = Job('r2gal')
    r2gal.depend['r2vz'] = r2vz_gal
    r2gal.depend['mabs'] = mabs
#    r2gal.depend('magcat', galcat)

    # R2 stars
    r2vz_star = Job('r2vz')
    r2vz_star.config['filter_dir'] = filter_dir
    r2vz_star.config['data_dir'] = data_dir
    r2vz_star.config['obj_type'] = 'star'

    r2star = Job('r2star')
    r2star.depend['r2vz'] = r2vz_star
    r2star.depend['mabs'] = starcatN
    r2star.depend['magcat'] = starcatN

    # Training.
    r2train = Job('r2train')
    r2train.depend['galcat'] = galcat
    r2train.depend['r2gal'] = r2gal
    r2train.depend['train_star_cat'] = starcat
    r2train.depend['train_star_r2'] = r2star

    euclid_conf = euclid_setup.config()
    if config['add_pz']:
        if False: #config['ind_noise']:
            galcat_pz = Job('magnoise')
#            galcat_pz.config['isindep'] = True
            galcat_pz.config.update({'const_sn': False, 'isindep': True})
            galcat_pz.config.task_config['sn_lims'].update(euclid_conf['lim_ext'])
            galcat_pz.depend['magcat'] = galcatN
        else:
            galcat_pz = galcat

        pzcat = Job('pzcat')
        pzcat.depend['galcat'] = galcat_pz
        pzcat.depend['ab'] = abgal

        # Normally I should not need to set these...
        pz_conf = {'filter_dir': filter_dir,
                   'sed_dir': os.path.join(data_dir, 'seds'),
                   'sky_spec': os.path.join(data_dir, 'sky_spectrum.txt'),
                   'add_noise': False,
                   'out_pdf': False}

        pzcat.task.config.update(pz_conf)

    Xtoadd = [('r2train', r2train), ('galcatN', galcatN), ('galnoise', galcat_noise), ('starnoise', star_noise),
             ('starcatN', starcatN), ('galabs', mabs), ('galcat', galcat),
             ('galmabs', mabs), ('abgal', abgal),
             ('r2gal', r2gal), ('r2star', r2star), ('r2vz_gal', r2vz_gal), ('r2vz_star', r2vz_star), ('star_offset', starcat)]

    if config['star_fit']:
        Xtoadd.append(('starfit', starfit))

    if config['add_pz']:
        Xtoadd.append(('pzcat', pzcat))
        
    pipel = Job('empty')
    for name, J in Xtoadd:
        pipel.depend[name] =  J

    # Instead of constructing the pipeline in one step, these extend with various
    # parts.
    f_add_noise(pipel, add_noise) 
    f_add_r2pz(pipel)
    f_add_galtrain(pipel)
    f_add_r2sed(pipel)
    f_add_r2x(pipel)
    f_add_r2x_pz(pipel, config)

    if new_star_pipel:
        import star_pipel
        star_pipel.add_pipel(pipel, star_r2train_task, data_dir, filter_dir)

    # This is the simplest way to apply the configuration.
    if use_zx:
        _top = xd.Topnode(pipel)
        _top.rec_apply(zx_mods)
        pipel = _top._nodes['']

    # The simplification has to come at the end. Several functions
    # modifying the pipeline rely on this interface.
    if restrictive_add:
        if isinstance(toplevel, str):
            toplevel = [toplevel]

        npipel = xd.Job()
        for x in toplevel:
            npipel.depend[x] = pipel.depend[x]

    assert restrictive_add

    return npipel

# Other parts which should maybe be stored elsewhere.
ylabel = 'R_{\\rm{PSF}}^2 / R_{\\rm{PSF}}^2'
def star_layer():

    layer = [{'r2x.use_stars': False, 'r2x.Ntrain': 4000}, \
             {'r2x.use_stars': True, 'r2x.Ntrain': 400}]

    return layer
