#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
#from astropy.table import Table

columns = ['id', 'x', 'y', 'ra', 'dec', 'r2', 'flag', 'u', 'g', 'r', 'i', 'z', \
           'uerr', 'gerr', 'rerr', 'ierr', 'zerr', 'e(b-v)', 'u(SExflag)',
           'g(SExflag)', 'r(SExflag)', 'i(SExflag)', 'z(SExflag)', 'dk',
           'u_IQ20', 'g_IQ20', 'r_IQ20', 'i_IQ20', 'z_IQ20']

# #       id       x          y        ra        dec         r2   flag    u        g        r        i         z       uerr     gerr     rerr     ierr         zerr   e(b-v) u(SExflag) g(SExflag) r(SExflag) i(SExflag)   z(SExflag)     dk

import shutil
import gzip
import tempfile

def unzip(path_in, path_out):
    """Uncompress a .gz file."""

    f_in = gzip.open(path_in)
    f_out = open(path_out, 'w')

    shutil.copyfileobj(f_in, f_out)

    f_in.close()
    f_out.close()

def get_columns(path):
    fb = open(path)
    while True:
        line = fb.readline()
        if 'follows' in line:
            continue

    ipdb.set_trace()

d = '/data2/marberi/data/cfhtls' #/renamed'
for fname in os.listdir(d):
    if not 'ugriz' in fname:
        continue

    gz_path = os.path.join(d, fname)
    tmp = tempfile.mktemp()

    unzip(gz_path, tmp)


    cat_raw = np.loadtxt(tmp)

    ipdb.set_trace()


#    with gzip.open(path) as gz_in, open(tmp_file, 'w') as file_out:
#        ipdb.set_trace()
#    shutil.copy(path, tmp_gz)
#    os.system
    
#    cat+
    cat = pd.DataFrame(np.loadtxt(path),columns=columns)

    ipdb.set_trace()
