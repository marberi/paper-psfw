#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

def get_r2tweak(pipel):
    """Copy the configuration from r2x over onto r2tweak."""

    r2tweak = xd.Job('r2tweak')
    # Copying the configuration items.
    for key, val in pipel.r2x.config.task_config.iteritems():
        r2tweak.config[key] = val

    # Add the dependencies
    for name, j in pipel.r2x.depend.iteritems():
        r2tweak.depend[name] = j

    return r2tweak


pipel = trainpipel.pipel()
pipel.config.update({\
  'r2x.use_cal': True,
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 2,
  'r2x.filters': ['r','i','z']})


z = np.linspace(0.1, 1.8, 10)
#z = np.linspace(0.1, 1.8, 9) #21)
r2tweak = get_r2tweak(pipel)

r2tweak.config['Nsteps'] = 10 #4 # 10 #4

ell_01 = pipel.r2train.train_gal_r2.r2vz.result['/vis/Ell_01'][0]

del r2tweak.depend['cal_pz']
del r2tweak.depend['test_pz']
#if False:
r2tweak.cal_cat.magcat.mabs.config['Ngal'] = 100000
r2tweak.test_cat.magcat.mabs.config['Ngal'] = 500000
#ipdb.set_trace()

for to_min in ['r2bias', 'r2diff']:
#for to_min in ['r2diff']:
    r2tweak.config['to_minimize'] = to_min

    cat = r2tweak.test_cat.result

    field = 'r2pred'
#    field = 'r2true' #pred'
    r2tweak.config['use_stars'] = False
    cat = cat.join(r2tweak.get_store()[field])

#    store = r2tweak.get_store()
#    ipdb.set_trace()

    r2tweak.config['use_stars'] = True
    cat = cat.join(r2tweak.get_store()[field], rsuffix='_stars')
    cat = cat[cat.mag_vis < 24.5]

#    ipdb.set_trace()

    gr = pd.cut(cat.zs, z)
    diff = (cat['0_stars'] - cat['0']) / ell_01
    #diff = cat['1_stars'] - cat['1']

    zs_mean = cat.zs.groupby(gr).mean()
    diff_mean = diff.groupby(gr).mean()

#    diff.hist(bins=300)
#    ipdb.set_trace()
    plt.plot(zs_mean, diff_mean, 'o-', label=to_min)

plt.grid(True)
#plt.xlabel('Redshift', size=16)
plt.legend()
ylim = 0.0004
plt.ylim((-ylim, ylim))
plt.savefig('test.pdf')
