#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

job = xd.Job('kidscat')

store = job.get_store()

filters = ['u', 'g', 'r', 'i']
def limit_cols():

    def tolist(pre, L):
        return [pre+x.upper() for x in filters]

    mag_cols = tolist('MAG_AUTO_', filters)
    err_cols = tolist('MAGERR_AUTO_', filters)
    other = ['CLASS_STAR']
    cols = mag_cols + err_cols + other

#    return cols
    return mag_cols, err_cols

def make_model(job):

    ab = job.result
    row = ab.irow(0)

    t1 = time.time()
    sedL = []
    for key in row.keys():
        if not key.startswith('/u'):
            continue

        sedL.append(key.split('/')[2])
    t2 = time.time()
    print('time', t2-t1)

    # Use an array for storing the mode at z=0.
    t3 = time.time()
    assert row.z == 0.
    model = np.zeros((len(filters), len(sedL)))
    for i,fname in enumerate(filters):
        for j,sed in enumerate(sedL):
            key = '/{0}/{1}'.format(fname, sed)
            model[i,j] = row[key]

    t4 = time.time()
    print('time', t4-t3)

    return model, sedL


def plot_star_distr(model, cat):
#    cat.CLASS_STAR.hist(bins=1000)

    inds = cat.CLASS_STAR < 0.8
    gal = cat[inds]
    star = cat[~inds]

    K = {'histtype': 'step', 'bins': 100}
    gal.MAG_AUTO_I.hist(label='Galaxies', **K)
    star.MAG_AUTO_I.hist(label='Stars', **K)

    plt.axvline(21., lw=1.5, color='black')
    plt.legend()
    plt.savefig('test.pdf')

from numba import jit

@jit
def calc_python(chi2, col_model, mask, col, H):
    ngal,nsed  = chi2.shape
    nf = col.shape[1]

    for x in range(ngal):
        for y in range(nsed):
            nobs = 0.
            chi_f = 0.

            for i in range(nf):
                if not mask[x,i]:
                    continue

                chi_f = chi_f + H[x,i]*pow(col[x, i]-col_model[y,i], 2) 
                nobs = nobs + 1.


            if not nobs:
                continue

            chi2[x,y] = chi_f / nobs

    return chi2


def fit(model, cat, sedL):
    # Converting to magnitudes instead.
    model = model.T
    model = -2.5*np.log10(model)

    mag_cols, err_cols = limit_cols()

    mag = cat[mag_cols].values
    err = cat[err_cols].values
    _var = err**2

    # Note: Var(X-Y) = Var(X)+Var(Y)
    col = mag[:, :-1]-mag[:, 1:]
    col_var = _var[:, :-1] + _var[:, 1:]

    col_model = model[:, :-1]-model[:, 1:]

    # Remove some of the observations. Inversting the variance
    # makes handling errors easier.
    np.seterr(divide='ignore')
    mask = 1.*np.logical_not(np.isnan(col))
    H = 1./col_var
    np.seterr(divide='raise')

    ngal = col.shape[0]
    nsed = col_model.shape[0]
    chi2 = np.zeros((ngal, nsed))

    t1 = time.time()
    chi2 = calc_python(chi2, col_model, mask, col, H)
    t2 = time.time()

    chi2_df = pd.DataFrame(chi2, columns=sedL, index=cat.index)

    print('time fit', t2-t1)
    return chi2_df

    ipdb.set_trace()


import trainpipel
pipel = trainpipel.pipel()
photoz_data = '/data2/photoz/run/data/'
abkids = xd.Job('ab')
abkids.task.config.update({'data_dir': photoz_data,
                           'filter_dir': 'kids_filters',
                           'zmax': 1.,
                           'sed_dir': 'seds_stars'})


model, sedL = make_model(abkids)

j1 = xd.Job('kidscat')
j2 = xd.Job('starsel')
j2.depend['catalog'] = j1

cat = j2.result

chi2_df = fit(model, cat, sedL)

ipdb.set_trace()
