#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

class Plot:    
    name = 'psf74'

    def pipel(self):
        pipel = trainpipel.pipel(new_star_pipel=True,
                                 star_r2train_task='r2x')

        pipel.config.update({\
            'r2x.filters': ['r','i', 'z'],
            'r2x.all_test': True,
            'r2x.use_stars': True, #True,
            'r2x.Nboot': 100,
#            'r2x.Nboot': 2,
            'r2x.bin_by': 'c0bin',
            'r2x.Ntrain': 400})

        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000

        return pipel

    def topnode(self):
        pipel = self.pipel()

        layers = xd.Layers()
        layers.add_pipeline(pipel)

        X = 'r2x.train_star_cat.magcat.magcat.starcat'
        star_conf = {'r2x.bin_by': 'zbin', X+'.resample': True, 
                     X+'.sed_distr': True}
        layer = [{'r2x.use_cal': False}, {'r2x.bin_by': 'zbin'},
                 {'r2x.bin_by': 'c0bin'}, star_conf]
        layers.add_layer('cal', layer) 

        top = layers.to_topnode()

        return top

    def f_reduce(self, pipel):
        z = np.linspace(0.1, 1.8, 7)
        df = pd.DataFrame()

        cat = pipel.r2x.test_cat.result
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        cols = range(pipel.r2x.config['Nboot'])

        for zi,(k2, sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
            mean = sub.mean()

            real = pd.Series([mean[x] for x in cols]) # yes, this is weird
            S = pd.Series({'zbin': zi, 'z': sub.zs.mean(), 
                           'r2bias': real.mean(),
                           'r2bias_std': np.sqrt(real.var())})

            df = df.append(S, ignore_index=True)

        return df

#    def plot(self, prepare):
#        lbls = ['Uncalibrated', 'By redshift', 'By r-i color', 'By redshift, KiDS stars']
#
#        lsL = ['--', '-', '-.', ':']
#        lsL = ['-.', '--', ':', '-']
#        colL = ['b', 'g', 'r', 'k']
#        for i,(key, sub) in enumerate(prepare.groupby('pipel')):
#            ls = lsL[i]
#            lw = 1.5 if ls == '-' else 2.0
#            plt.errorbar(sub.z, sub.r2bias, sub.r2bias_std, 
#                         lw=lw, fmt=ls, color=colL[i], label=lbls[key[0]])
#
#        plt.xlabel('Redshift [zs]', size=16)
#        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=17)
#
#        plt.xlim((0.1, 1.9))
#        plt.ylim((-0.0015, 0.002))
#
#        # Shade the requirement region.
#        xline = np.linspace(*plt.xlim())
#        y_lower = -3e-4*np.ones_like(xline)
#        y_upper = 3e-4*np.ones_like(xline)
#        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')
#
#        # Just looked ugly without
##        ylim = 0.0025
#        plt.ylim((-0.002, 0.0027))
#        plt.subplots_adjust(left=0.14)
#        plt.grid(True)
#        #plt.legend(loc=9)
#        plt.legend(loc=(0.20,0.75), prop={'size': 12})
#        plt.savefig(tosave.path('psf74'))
   
    def plot(self, prepare):
        lbls = ['Uncalibrated', 'By redshift', 'By r-i color', 'By redshift, KiDS stars']

        lsL = ['--', '-', '-.', ':']
        lsL = ['-.', '--', ':', '-']
        colL = ['b', 'g', 'r', 'k']
        for key, sub in prepare.groupby('cal'):
            i = int(key)
            ls = lsL[i]
            plt.errorbar(sub.z, sub.r2bias, sub.r2bias_std, 
                         fmt=ls, color=colL[i], label=lbls[i])


<<<<<<< HEAD
        plt.xlabel('$z_{\mathrm{s}}}}$', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)
=======
        plt.xlabel('Redshift [$z_s$]')
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$')
>>>>>>> 0a69a82160a549d0a1ad619323851fad7563238a

        plt.xlim((0.1, 1.9))
        plt.ylim((-0.0015, 0.002))

        # Shade the requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
#        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

        # Just looked ugly without
#        ylim = 0.0025
        plt.ylim((-0.002, 0.0027))
        plt.subplots_adjust(left=0.14)
        plt.grid(True)


#        plt.legend(loc=(0.20,0.75))
#        plt.legend(loc=(0.17,0.75))
        plt.legend(loc=(0.17,0.7))



if __name__ == '__main__':
    xd.plot(Plot).save()
