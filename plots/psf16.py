#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import matplotlib.ticker as mtick

import xdolphin as xd
from xdolphin import Job

import tosave

def gen_styles():
    i = 0
    styles = ['-.','-','--', ':']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    """Magnitude offsets."""

    def _add_offset(self, pipel):
        """Add a task for the offset."""

        offset = Job('offset')
        offset.depend['magcat'] = pipel.r2x.test_cat

        pipel.r2x.depend['test_cat'] = offset

        return pipel

    def _extend(self, topnode, new_key, toadd):
        for key, val in toadd.iteritems():
            topnode[(new_key,)+key] = val

    def topnode(self):
        import trainpipel

        _pipel = self._add_offset(trainpipel.pipel())
        topnode_in = xd.Topnode(_pipel)
        
        fL = ['r','i','z']
        mag_list = [-0.01, -0.005, -0.002, -0.001, 0., 0.001, 0.002, 0.005, 0.01]

        mag_list = [-0.07, -0.05, -0.03, -0.02, 0.01, 0., 0.01, 0.02, 0.03, 0.05, 0.07]

        mag_list = [-0.05, -0.03, -0.02, 0.01, 0., 0.01, 0.02, 0.03, 0.05]
        mag_list = [-0.02, -0.01, 0., 0.01, 0.02]
        mag_list = [-0.02, -0.015, -0.01, -0.005, 0., 0.005, 0.01, 0.015, 0.02]

        topnode = xd.Topnode()
        for f in fL:
            to_add = topnode_in.clone()
            to_add.add_layer_key('r2x.test_cat.{0}'.format(f),
                              mag_list)

            self._extend(topnode, f, to_add)


        topnode.update_config({\
          'r2x.filters': ['r', 'i', 'z'], 
#          'r2x.Ntrain': 400,
#          'r2x.use_stars': True, 
          'r2x.Ntrain': 4000,
          'r2x.use_stars': False, 
          #'r2x.use_stars': True, #False,
          'r2x.all_test': True
        })


        return topnode

    def prepare(self, pipel):
        cat = pipel.r2x.test_cat.result[['zs', 'mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        N = pipel.r2x.config['Nboot']
       
        df = pd.DataFrame()
        S = pd.Series()
        S['zbin'] = 'all'
#        S['r2bias'] = cat[range(N)].mean().abs().describe(percentiles=[0.68])['68%']
        S['r2bias'] = cat[range(N)].mean().mean()
        df = df.append(S, ignore_index=True)

        z = np.linspace(0.1, 1.8, 6)
#        z = np.arange(0.0, 2.2, 0.4)

        for i,(key,sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
            S = pd.Series()
            S['zbin'] = i
#            S['r2bias'] = sub[range(N)].mean().abs().describe(percentiles=[0.68])['68%']
            S['r2bias'] = sub[range(N)].mean().mean()
#            ipdb.set_trace()
            S['zlbl'] = 'z = {0}'.format(key)
            df = df.append(S, ignore_index=True)

        return df


    def lbl_offval(self, pipel):
        """Offset label."""

        offD = pipel.r2x.test_cat.config
        res = [f for f,v in dict(offD).iteritems() if v]

        assert len(res) <= 1, 'Offsets in multiple filters: {0}'.format(','.join(res))

        off_val = offD[res[0]] if len(res) else 0.

        return off_val

    def run(self):
        topnode = self.topnode()

        labels = {'off_val': self.lbl_offval}
        prepare = topnode.prep_cache(self.prepare, '2', labels)

        ind_filter, ind_off = zip(*prepare.index)
        prepare['ind_filter'] = ind_filter
        prepare['ind_off'] = ind_off

        ipdb.set_trace()

        fL = ['r', 'i', 'z']

        # Then start the plotting..
        fig, A = plt.subplots(len(fL), sharex=True)
        fig.set_size_inches([8, 15.])
        A[0].set_xlim((-0.022, 0.022))
        ylim = 7e-4

        for i,f in enumerate(fL):
            ax = A[i]

            sub = prepare[prepare.ind_filter == f]
            styles = gen_styles()
            for zbin, sub2 in sub.groupby(sub.zbin):
                if zbin == 'all':
                    continue

                sub2 = sub2.sort('off_val')
                zlbl = sub2.irow(0).zlbl
                ls = next(styles)
                lw = 1.5 if ls == '-' else 2.0
                ax.plot(sub2['off_val'], sub2.r2bias, ls=ls, lw=lw, label=zlbl)


            sub2 = sub[sub.zbin == 'all']
            sub2 = sub2.sort('off_val')
            ax.plot(sub2['off_val'], sub2.r2bias, '-', lw=2, color='k', label='All')

            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            ax.yaxis.set_major_locator(MaxNLocator(prune='both', nbins=5))

#            ax.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
#            ax.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)
            ax.axvline(0, ls='--', color='black', lw=2., alpha=0.5)

            # Requirement band
            xline = np.linspace(*plt.xlim())
            y_lower = -3e-4*np.ones_like(xline)
            y_upper = 3e-4*np.ones_like(xline)
            ax.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

            ax.set_ylim((-ylim, ylim))
#            ax.set_xlim((-0.03, 0.07))
#            ax.legend()

            ax.tick_params(axis='y', which='major', labelsize=14)
            ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))

        ax.xaxis.set_major_locator(MaxNLocator(prune='both', nbins=5))
        plt.tick_params(axis='both', which='major', labelsize=14)

        plt.subplots_adjust(hspace=0.0, top=0.95, left=0.14, right=0.95)
        A[1].set_ylabel('$\delta "R^2_{\\rm{PSF}}" /\, "R^2_{\\rm{PSF}}"$', size=18)
        A[-1].set_xlabel('Magnitude offset', size=18)

        # Mark the subplots with the band.
        btextD = {'r':-5e-4, 'i': 5e-4, 'z': 5e-4}
        for i,f in enumerate(fL):
            A[i].text(0.01, btextD[f], 'Band: {0}'.format(f), fontsize=16,
                      horizontalalignment='center', weight='bold')

        # Because an unwanted line showed up.
        lines = []
        labels = []
        for xline in A[1].get_lines():
            xlbl = xline.get_label()
            if xlbl.startswith('_'):
                continue

            lines.append(xline)
            labels.append(xlbl)


#        fig = plt.gca()
        fig.legend(lines, labels, loc=(0.17,.4), ncol=3, prop={'size': 12}) #, columnspacing=0.1)
#        fig.legend(lines, labels, bbox_to_anchor=(0.15,.4, 0.9, 0.5), ncol=3, prop={'size': 13}) #, columnspacing=0.1)
#        fig.legend(lines, labels, loc=(0.15,.4, 0.9, 0.5), ncol=3, prop={'size': 13}) #, columnspacing=0.1)

        plt.savefig(tosave.path('psf16'))

    def fast(self, topnode):
        topnode.update_config({\
          'r2x.filters': ['r', 'i', 'z'], 
          'r2x.Ntrain': 4,
          'r2x.Nboot': 4})

        return topnode

if __name__ == '__main__':
    Plot().run()
