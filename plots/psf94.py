#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

import xdolphin as xd
import trainpipel

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    name = 'psf94'

    # Some config.
    binning = 'linspace'

    def print_jobs(self, job, root=tuple()):
        """Prints the location of the mabs tasks. This is not
           strictly needed.
        """

        if job.name == 'galabs':
            print('root', '.'.join(root))

        for dep_key,dep_job in job.depend.iteritems():
            new_root = root+(dep_key,)

            self.print_jobs(dep_job, new_root)

    def replace_job(self, old_job):
        new_job = xd.Job('rmsed')
        new_job.depend['default'] = old_job

        ngal = old_job.config['Ngal']
        old_job.config['Ngal'] = 10*ngal
        new_job.config['Nobj'] = ngal

        return new_job

    def _add_sedrm(self, pipel):
        """Add task which remove one of the SEDs."""

        j_train = self.replace_job(pipel.depend['r2x.train_gal_cat.mabs'])
        j_cal = self.replace_job(pipel.depend['r2x.cal_cat.magcat.mabs'])


        # I should actually only do this for the calibration sample.
        tochange = [(j_train, ('r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs')), \
                    (j_cal,   ('r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs'))]


        for j, X in tochange:
            for x in X:
                print(x)
                print(pipel.depend[x].name)

                assert pipel.depend[x].name == 'galabs'

                F = x.replace('.mabs', '')
                pipel.depend[F].depend['mabs'] = j
                print(pipel.depend[x].name)

        return pipel


    def topnode(self):
        pipel = trainpipel.pipel()

        # Deleting the photo-z dependencies. I need to run
        # with some large simulations..
        del pipel.r2x.depend['cal_pz']
        del pipel.r2x.depend['test_pz']

        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000
        pipel.r2x.test_cat.magcat.mabs.config['Ngal'] = 500000
#        ipdb.set_trace()

        pipel.config.update({\
          'r2x.use_cal': True,
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
#          'r2x.Nboot': 100,
#          'r2x.Nboot': 2,
          'r2x.Nboot': 10,
          'r2x.use_stars': True,
          'r2x.filters': ['r','i','z']})

#        self._add_offset(pipel)
        pipel = self._add_sedrm(pipel)

#        top = xd.Topnode(pipel)
        layers = xd.Layers()
        layers.add_pipeline(pipel)

        def xval(lrem):
            D = {}
            for key in ['r2x.cal_r2.mabs.{0}', 'r2x.train_gal_r2.mabs.{0}']:
#                D[key.format('torem')] = torem
#                D[key.format('dsed')] = 10.

                D[key.format('lkeep')] = 1.
                D[key.format('lrem')] = lrem

            return D


#        top.add_layer(map(xval, [0., 2.3, 5.0, 8.]))
#        top.add_layer(trainpipel.star_layer())

        layers.add_layer('lrem', map(xval, [0., 2.3, 5.0, 8.]))
        layers.add_layer('use_stars', trainpipel.star_layer())

        top = layers.to_topnode()

        return top

    def f_reduce(self, pipel):
        prep = pd.DataFrame()

        with pipel.r2x.get_store() as store:
            cat = store['r2eff']

        cat = cat.join(pipel.r2x.test_cat.result)
        cat = cat[cat.mag_vis < 24.5]


        z = np.linspace(0.1, 1.8, 10)
        gr = pd.cut(cat.zs, z)

        nboot = pipel.r2x.config['Nboot']
        mean = cat[range(nboot)].groupby(gr).mean()
        r2bias = mean.mean(axis=1)
        r2bias_std = np.sqrt(mean.var(axis=1))

        zs = cat.zs.groupby(gr).mean()

        prep['zs'] = zs
        prep['zbin'] = range(len(zs))
        prep['r2bias'] = r2bias
        prep['r2bias_std'] = r2bias_std

        prep.index.name = 'fisk'
        F = prep.reset_index()
        del F['fisk']

        return F

    def plot(self, prepare):
        topnode = self.topnode()

        prepare = prepare.sort_values(['lrem', 'use_stars', 'zs'])

        colorL = ['black', 'red', 'blue', 'green']
        for i,(key,sub) in enumerate(prepare.groupby('lrem')):
            color = colorL[i]

            for k2,sub2 in sub.groupby('use_stars'):
                use_stars = sub2.iloc[0].use_stars
                lrem = sub2.iloc[2].lrem

                # Assumes lkeep == 1.
                lkeep = 1. #sub2.iloc[2].lkeep
                ls = '-' if use_stars else '--'
                frac = lkeep / (lkeep +lrem) 
                lbl_stars = 'Star' if use_stars else 'Gal'

                label = '{0} - {1:.1f}%'.format(lbl_stars, 100*frac)

                plt.errorbar(sub2.zs, sub2.r2bias, sub2.r2bias_std,
                             color=color, ls=ls, label=label)


        plt.xlabel('$z_s$', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}} \, /\, R^2_{\\rm{PSF}}$', size=13)

        plt.grid(True)
        plt.xlim((0.1, 1.9))
        plt.ylim((-0.001, 0.002))
        plt.legend(loc=2, ncol=2)


        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        ax = plt.gca()
        ax.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))

        plt.subplots_adjust(left=0.14)

if __name__ == '__main__':
    xd.plot(Plot).save()
