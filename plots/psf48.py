#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
from scipy.interpolate import splev, splrep
from matplotlib import pyplot as plt

def test1():
    d = '/data2/photoz/run/data/des_euclid'

    for file_name in os.listdir(d):
        fname = file_name.replace('.res', '')


        file_path = os.path.join(d, file_name)
        x,y = np.loadtxt(file_path).T

        spl = splrep(x, y)
        x2 = np.linspace(x[0], x[-1], 10000)
        y2 = splev(x2, spl)

        plt.plot(x, y, label='Orig: '+fname)
        plt.plot(x2, y2, label='Intrp: '+fname)

        plt.legend()
        plt.show()

# Looking at training out some bias..

A
