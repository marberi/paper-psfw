#!/usr/bin/env python
# encoding:

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

import xdolphin as xd

#import tosave

class Plot:
    name = 'psf95'

    def topnode(self):
        import x91
        inst = x91.Plot()
        topnode = inst.topnode()

        import xdolphin as xd

        # ok, this is not quite normal. It is needed
        # since the prepare compare the results when
        # training on stars or galaxies.

        new_top = xd.Topnode()
        nval = len(topnode._nodes) / 2
        for i in range(nval):
            A = xd.Job()
            for j in [0,1]:
                pipel = topnode[j,i]
                ind = 'star' if pipel.config['r2x.use_stars'] else 'gal'
                A.depend[ind] = pipel

            new_top[(i,)] = A


        return new_top

    def replace_job(self, old_job):
        new_job = xd.Job('rmsed')
        new_job.depend['default'] = old_job

        return new_job

    def _add_sedrm(self, pipel):
        """Add task which remove one of the SEDs."""

#        self.print_jobs(pipel)

#        ipdb.set_trace()

        # One can automatically detect these, but it does not
        # improve anything.
#        toreplace = ['r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs',
#                     'r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs']


        j_train = self.replace_job(pipel.depend['r2x.train_gal_cat.mabs'])
        j_cal = self.replace_job(pipel.depend['r2x.cal_cat.magcat.mabs'])


        # I should actually only do this for the calibration sample.
        tochange = [(j_train, ('r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs')), \
                    (j_cal,   ('r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs'))]

#        tochange = [tochange[1]]

        for j, X in tochange:
            for x in X:
                print(x)
                print(pipel.depend[x].name)

                assert pipel.depend[x].name == 'galabs'

                F = x.replace('.mabs', '')
                pipel.depend[F].depend['mabs'] = j
                print(pipel.depend[x].name)

        return pipel

    def pipel(self):
        import trainpipel

        pipel = trainpipel.pipel()
        self._add_sedrm(pipel)

        pipel_gal = pipel
        pipel_star = pipel_gal.clone() 
        pipel_star.config['r2x.use_stars'] = True

        npipel = xd.Job()
        npipel.depend['gal'] = pipel_gal
        npipel.depend['star'] = pipel_star

        npipel.config.update(
          {'star.r2x.Ntrain': 400, 
           'gal.r2x.Ntrain': 4000})

        return npipel

    def xval(self, val):
        D = {}
        tochange = ['r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs', \
                    'r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs']

        for field in ['gal', 'star']:
            for x in tochange:
                D['{0}.{1}.lrem'.format(field, x)] = val
                D['{0}.{1}.lkeep'.format(field, x)] = 1.

#                D[key] = val
#                D[key.format('lkeep')] = 1.
#                D[key.format('lrem')] = lrem

        return D

#                L.append('{0}.{1}'.format(field, x))

    def topnode(self):
        pipel = self.pipel()


        layers = xd.Layers()
        layers.add_pipeline(pipel)
#        layers.add_layer
#        layers.add_layer('lrem', map(self.xval, [0., 2.3, 5.0, 8.]))
        layers.add_layer('lrem', map(self.xval, [0., 2.3, 5.0, 8.0]))

#[0., 2.3, 5., 8.]


        top = layers.to_topnode()

        return top

#        tochange = ['r2x.train_gal_cat.mabs', 'r2x.train_gal_r2.mabs', \
#                    'r2x.cal_cat.magcat.mabs', 'r2x.cal_r2.mabs']
#
#
#
#        L = []
#        for field in ['gal', 'star']:
#            for x in tochange:
#                L.append('{0}.{1}'.format(field, x))
#
#
#        ipdb.set_trace()
#
#
#        import trainpipel
#
#        pipel_gal = trainpipel.pipel()
#        pipel_stars = pipel_gal.clone() 
#
#        npipel = xd.Job()
#        npipel.depend['gal'] = pipel_gal
#        npipel.depend['star'] = pipel_star
#    
#        ipdb.set_trace()



    def f_reduce(self, pipel): 
        df = pd.DataFrame()

        ell_01 = pipel.star.r2x.train_gal_r2.r2vz.result['/vis/Ell_01'][0]

        field = 'r2pred'

        cat = pipel.gal.r2x.get_store()[field]
        cat = cat.join(pipel.star.r2x.get_store()[field], rsuffix='_stars')

        Nboot = pipel.star.r2x.config['Nboot']

        col_gal = range(Nboot)
        col_stars = map('{0}_stars'.format, range(Nboot))
        sub_stars = cat[col_stars]
        sub_stars.columns = col_gal
        sub_gal = cat[col_gal]
        sub_gal.columns = col_gal

        diff = (sub_stars - sub_gal) / ell_01
        galcat = pipel.star.r2x.test_cat.result

        # First here we have the actual catalog.
        cat = galcat.join(diff)
        cat = cat[cat.mag_vis < 24.5]

        z = np.linspace(0.1, 1.8, 10)

        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        part = pd.DataFrame()
        part['r2diff'] = mean[col_gal].mean(axis=1)
        part['r2diff_std'] = np.sqrt(mean[col_gal].var(axis=1))
        part['zs'] = mean.zs
        part['zbin'] = range(len(part))
        part['za'] = z[:-1]
        part['zb'] = z[1:]

        part['lkeep'] = pipel.star.r2x.train_gal_cat.mabs.config['lkeep']
        part['rem'] = pipel.star.r2x.train_gal_cat.mabs.config['lrem']

#        ipdb.set_trace()

        r2diff_all = cat[col_gal].mean()
        S = pd.Series({'zbin': -1, #'all',
                       'r2diff': r2diff_all.mean(),
                       'r2diff_std': r2diff_all.var()})

        part = part.append(S, ignore_index=True)

        return part

#    def labels(self):
#        labels = {'lkeep': 'star.r2x.cal_r2.mabs.lkeep',
#                  'lrem': 'star.r2x.cal_r2.mabs.lrem'}
#
#        return labels

    def plot(self, prepare):
        # Here we have clipped on redshift since the changes
        # is most visible there.

        prepare = prepare[prepare.zbin != -1]
        prepare = prepare[prepare.zs < 1.2]
        prepare = prepare.sort_values(['lrem', 'zs'])

        colorL = ['black', 'blue', 'red', 'green']
        lsL = ['-', '-.', ':', '--']
        for i,(key,sub) in enumerate(prepare.groupby('lrem')):
            row = sub.iloc[0]
            frac = row.lkeep / (row.lkeep + row.lrem)
            label = 'frac: {0:.1f}%'.format(100*frac)
        
            plt.errorbar(sub.zs, sub.r2diff, sub.r2diff_std,
                         color=colorL[i], ls=lsL[i], label=label)


        plt.xlabel('$z_s$', size=14)
        plt.ylabel('<$R^2_{\\rm{PSF}}$(Train stars) - $R^2_{\\rm{PSF}}$(Train gal)>/ $R^2_{\\rm{PSF}}$(Ell_01)', size=13)

        ax = plt.gca()
#        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax.yaxis.major.formatter._useMathText = True
 
        plt.grid(True)
        zmin = prepare.zs.min()
        zmax = prepare.zs.max()
        dz = 0.05*(zmax-zmin)

        plt.xlim((zmin-dz, zmax+dz)) #0.1, 1.2))
        plt.ylim((-0.00015, 0.0005))

        plt.subplots_adjust(left=0.14)

        ax = plt.gca()

        plt.axhline(0., ls='--', color='black', lw=2., alpha=0.5)
        plt.legend(loc=(0.045, 0.71))


if __name__ == '__main__':
    #Plot().run()
    xd.plot(Plot).save()
