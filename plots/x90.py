#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

f = '/home/marberi/drv/work/papers/psfw/data/fjc_temp'

G = open(f).readlines()
G = [x.strip() for x in G]

only_seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']


X = ['CWW_Ell', 'CWW_Sbc', 'CWW_Scd', 'CWW_Irr', 'I99_05Gy']

ipdb.set_trace()

fig, A = plt.subplots(2)

sed_dir = '/data2/photoz/run/data/seds'
sedD = {}
for sed in G:
    path = os.path.join(sed_dir, '{0}.sed'.format(sed))

    x,y = np.loadtxt(path).T

    if sed in only_seds:
        ax = A[0]
    elif sed in X:
        ax = A[1]
    else:
        continue

    ax.plot(x,y, label=sed)

for ax in A:
    ax.legend()

    ax.set_xlim((1e3, 1e5))
    ax.set_ylim((1e-2, 1e3))

    ax.set_xscale('log')
    ax.set_yscale('log')

plt.show()
#    ipdb.set_trace()
