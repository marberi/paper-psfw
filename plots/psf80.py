#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

class Plot:
    name = 'psf80'

    def pipel(self):
        import trainpipel
        pipel = trainpipel.pipel(toplevel=['r2pz', 'r2x'])
        pipel.config.update({
          'r2x.use_cal': False,
          'r2x.filters': ['r', 'i', 'z'],
        #  'r2x.filters': ['r', 'i'],
          'r2x.all_test': True,
          'r2x.Ntrain': 4000,
          'r2x.Nboot': 10,
          'r2x.use_stars': False})

#        ipdb.set_trace()

        return pipel

    def load(self, pipel):
        data = {}
        r2pz_cat = pipel.r2pz.pzcat.galcat.result
        r2pz_cat = r2pz_cat.join(pipel.r2pz.result)
        data['r2pz_cat'] = r2pz_cat[r2pz_cat.mag_vis < 24.5]

        r2x_cat = pipel.r2x.get_store()['r2eff']
        r2x_cat = r2x_cat.join(pipel.r2x.test_cat.result)
        data['r2x_cat'] = r2x_cat[r2x_cat.mag_vis < 24.5]

        return data

    def plot(self, data):
        # This will end up giving some subtle problems when 
        # creating the prepare object (hirarchical columns).
        r2pz_cat = data['r2pz_cat']
        r2x_cat = data['r2x_cat']

        K = {'histtype': 'step', 'normed': True, 'bins': 100}
        r2x_cat[0].hist(label='Gal train',  ls='solid', **K)
        r2pz_cat.r2peak.hist(label='SED fit', ls='dashed', **K)

        plt.xlabel('$\delta R^2_{\\rm{PSF}} \, /\, R^2_{\\rm{PSF}}$', size=13)
        plt.ylabel('Probability', size=14)
        plt.legend()

        yline = np.linspace(0, 300)
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='k', alpha=0.1)
        xlim = 0.01
        plt.xlim((-xlim, xlim))
        plt.ylim((0, 280))
        plt.subplots_adjust(bottom = 0.12)
#        plt.rcParams['xtick.labelsize'] = 'small'

if __name__ == '__main__':
    xd.plot(Plot).save()
