#!/usr/bin/env python
# encoding: UTF8

#def config():
#    lim_point = {
#    'vis': 26.7,
#    'g': 25.35,
#    'r': 24.85,
#    'i': 25.05,
#    'z': 24.65,
#    'Y': 24.0,
#    'J': 24.0,
#    'H': 24.0}
#
#    lim_ext = dict([(fname,lim-0.7) for fname,lim in lim_point.iteritems()])
#
#    conf = {'lim_point': lim_point,
#            'lim_ext': lim_ext}
#
#    return conf

def config():
    ext_to_point = 0.7
    ns5_to_10 = 0.75257

    # These numbers are for the 10 sigma point source.
    lim_ext = {
    'vis': 24.5,
    'g': 24.4,
    'r': 24.1,
    'i': 24.1,
    'z': 23.7,
    'Y': 24.0 - ns5_to_10,
    'J': 24.0 - ns5_to_10,
    'H': 24.0 - ns5_to_10}


    lim_point = dict([(fname,lim+0.7) for fname,lim in lim_ext.iteritems()])


    conf = {'lim_point': lim_point,
            'lim_ext': lim_ext}

    return conf
