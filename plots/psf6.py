#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

import trainpipel

class Plot:
    """Gal type histogram."""

    name = 'psf6'

    def pipel(self):
        pipel = trainpipel.pipel(toplevel='r2pz')
        pipel.config.update({'r2pz.use_ml': False})

        return pipel

    def f_reduce(self, pipel):
        J = pipel.r2pz
        r2eff = J.r2gal.result
        r2pz = J.result
        galcat = J.pzcat.galcat.result
        mabs = J.pzcat.galcat.magcat.mabs.result

        cat = pd.concat([r2eff,r2pz,galcat,mabs], axis=1)
        cat = cat[cat.mag_vis < 24.5]

        return cat

    def plot(self, prepare):
        cat = prepare.ix[0]

        sedl = [0,17,55,65]
        S = pd.cut(cat.sed, sedl, labels=['ell', 'sp', 'irr'])

        K = {'histtype': 'step', 'normed': True}

        nbins = 100
        r2type = 'r2peak'
        labels = {'ell': 'Elliptical', 'sp': 'Spiral', 'irr': 'Irregular'}
        lsD = {'ell': 'solid', 'irr': 'dashed', 'sp': 'dashdot'}
        for key, val in cat.groupby(S):
            val[r2type].hist(bins=nbins, stacked=True, linestyle=lsD[key], label=labels[key], **K)

        plt.legend()
        plt.xlabel('$\delta R^2_{\\rm{PSF}} \, /\, R^2_{\\rm{PSF}}$', size=13)
        plt.ylabel('Probability', size=14)

        yline = np.linspace(0, 300)
#*plt.ylim())
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='black', alpha=0.1, label='fill')

        plt.xlim(-0.01, 0.01)
        plt.rcParams['xtick.labelsize'] = 'small'
#        plt.axvline(-3e-4)
#        plt.axvline(3e-4)
        plt.ylim((0, 300))
        plt.subplots_adjust(bottom = 0.12)
if __name__ == '__main__':
    xd.plot(Plot).save()
