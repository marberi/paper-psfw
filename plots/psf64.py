#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import os
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

class psf64:
    name = 'psf64'
    config = {}

    def get_B(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 500000


            B[lbl] = A

        return B

    def d1(self, B):
        scat = B['noiseless']['r2train'].depend['starcat'].depend['magcat']['magcat']['cat']
        scat = scat.join(B['noiseless']['r2train'].depend['r2star']['r2eff']['r2eff'])
        scat['col'] = scat.mag_r - scat.mag_vis

        def toflux(x):
            return 10**(-0.4*x)

        def tomag(x):
            return -2.5*np.log10(x)

        assert tomag(toflux(20)) == 20

        f_r = toflux(scat.mag_r.values)
        f_vis = toflux(scat.mag_vis.values)

        ind_i = np.random.randint(0, len(scat), 2000)
        ind_j = np.random.randint(0, len(scat), 2000)

        x_r = 0.5*(f_r[ind_i] + f_r[ind_j])
        x_vis = 0.5*(f_vis[ind_i] + f_vis[ind_j])
        m_r = tomag(x_r)
        m_vis = tomag(x_vis)

        col = m_r - m_vis
        r2arr = scat.r2eff.values
        r2comb = 0.5*(r2arr[ind_i] + r2arr[ind_j])

        # Only using a subset.
        r1 = np.random.randint(0, len(scat), 400)
        r2 = np.random.randint(0, len(r2comb), 400)

        A = scat.ix[r1] #scat.index[r1]]

        plt.plot(A.col, A.r2eff, '.', label='Original stars')
        plt.plot(col[r2], r2comb[r2], '.', label='Linear combinations')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('r - vis', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$', size=16)

        plt.legend(loc=0)
        plt.show()



        ipdb.set_trace()

        #plt.plot(scat[
        


    def run(self):
        B = self.get_B()

        self.d1(B)


if __name__ == '__main__':
    Job('psf64').run()
