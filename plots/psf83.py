#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd

import trainpipel

def get_col(cat, filtersL, errors=False):
    mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
    col = mag[:,:-1] - mag[:,1:]

    if errors:
        err = cat[['err_{0}'.format(x) for x in filtersL]].values
        return col, mag, err
    else:
        return col, mag

def gen_styles():
    i = 0
    styles = [':', '-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1


class Plot:
    name = 'psf83'

    rL = np.linspace(-0.1, 0.1, 10)

    def pipel(self):
        pipel = trainpipel.pipel()

        return pipel

    def topnode(self):
        # This step should not be needed.
        pipel = self.pipel()
        layers = xd.Layers()
        layers.add_pipeline(pipel)
        top = layers.to_topnode()

        return top

    def get_df(self, pipel, rL):
        # Then looking 
        star_cat = pipel.r2x.train_star_cat.result
        star_cat = star_cat.join(pipel.r2x.train_star_r2.result)

        gc = pipel.r2x.test_cat.result
        gc = gc.join(pipel.r2x.test_r2.result)
        gc['r2true'] = gc.r2eff
#        gc = gc.join(pipel.r2x.test_pz.result, rsuffix='_ignore')

        Ntrain = 400
        fL = ['r', 'i', 'z']

        Astar, _ = get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = get_col(gc, fL, True)

        inds = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds]
        Btrain = star_cat.r2eff.values[inds]
        from sklearn import svm


        #if False:
        cal_cat = pipel.r2x.cal_cat.result
        cal_cat = cal_cat.join(pipel.r2x.cal_r2.result)
        cal_cat['r2true'] = cal_cat.r2eff

        cal_cat = cal_cat.join(pipel.r2x.cal_pz.result.unstack(), rsuffix='_ignore')

        cal_cat = cal_cat[cal_cat.mag_vis < 24.5]

        col_cal, mag_cal, err_cal = get_col(cal_cat, fL, True)

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        df = pd.DataFrame({'zs': cal_cat.zs, 'zb': cal_cat.zb})

        for i, ri in enumerate(rL):
            col_tmp = col_cal.copy()
            col_tmp[:,0] += ri
            r2pred = reg.predict(col_tmp)
            r2bias = (r2pred - cal_cat.r2true) / cal_cat.r2true

            df[i] = r2bias
       
        return df

    def f_reduce(self, pipel):
        """Reduction step."""

        df = self.get_df(pipel, self.rL)

        return df

    def plot(self, prepare):

        rL = self.rL
        df = prepare

        zkey = 'zb'
        z = np.arange(0.0, 2.2, 0.4)
        mean = df.groupby(pd.cut(df[zkey], z)).mean()

        all_line = [df[x].mean() for x in range(len(rL))]
        plt.plot(rL, all_line, ls='-', color='black', label='All')

        T = mean.T
        styles = gen_styles()
        for key,col in mean[range(len(rL))].T.iteritems():
            lbl ='z = {0}'.format(key)
            ls = next(styles)
            plt.plot(rL, col, ls=ls, label=lbl)

        # Shade the requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

        plt.grid(True)

        xlim = 0.1
        plt.xlim((-xlim,xlim))
        plt.xlabel('r-band offset', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)
        plt.legend(loc=2)
        plt.subplots_adjust(left=0.15)

if __name__ == '__main__':
    xd.plot(Plot).save()
