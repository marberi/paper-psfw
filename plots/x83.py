#!/usr/bin/env python
# encoding: UTF8

import ipdb
import psf78
import psf90

class Plot(psf78.Plot):
    def _rows(self):

        I = [['r', 'i'],
             ['r', 'i', 'z'],
             ['r', 'i', 'zx']]

        I = [['r', 'i','z'],
             ['r', 'i'],
             ['r', 'i', 'zx']]

#        I = [I[2]]

        return I

    def topnode(self):
        top = psf78.Plot.topnode(self)
#        top.update_config({'r2train.Nboot': 4})
        top.update_config({'r2train.Nboot': 100})
        top.rec_apply(psf90.zx_mods)

        return top


if __name__ == '__main__':
    Plot().run()
