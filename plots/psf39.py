#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
from matplotlib import pyplot as plt

import sys

class Plot:

    def __init__(self):
        import psf38
        self.dsl = psf38.Plot()

    def topnode(self):
        return self.dsl.topnode()

    def run(self):
        # Use the information directly from psf38.... 

        topnode = self.topnode()
        dsl = self.dsl

        Nboot = pipel.north.r2train.config['Nboot']
        res = pipel.result 
        catS = dsl.getcat(pipel.south)
        catSX = dsl.getcat(pipel.southX)
        catN = dsl.getcat(pipel.north)

        cat_corr = catN.join(res)

        L = [('South', catS, 'r2eff_{0}'),
             ('North', catN, 'r2eff_{0}'),
             ('North, corrected', cat_corr, 'r2eff_corr_{0}')]

        field = 'color'
        x = np.linspace(0.,1.5, 20)

        for lbl, cat, fmt in L:
            cat['color'] = cat['mag_r'] - cat['mag_i']

            keys = map(fmt.format, range(Nboot))
            sub = cat[cat.mag_vis < 24.5]

            gr = pd.cut(cat[field], x)
            xL = []
            yL = [] 
            for key, val in cat.groupby(gr):
                xL.append(val[field].mean())
                y = val[keys].mean(axis=0).abs().describe(percentiles=[0.68])['68%']
                yL.append(y)

            plt.plot(xL, yL, label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.xlabel('$r_{AB}$ - $i_{AB}$', size=16)
        plt.legend(loc=0)
        plt.show()

if __name__ == '__main__':
    Plot().run()
