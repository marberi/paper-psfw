#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave

import xdolphin as xd
import trainpipel
import star_pipel
pipel = trainpipel.pipel()
photoz_data = '/data2/photoz/run/data/'
filter_dir = '/data2/photoz/run/data/des_euclid2'

star_pipel.add_pipel(pipel, photoz_data, filter_dir)


# Some configuration.
pipel.config.update({
  'r2x.Ntrain': 400,
  'r2x.all_test': True,
  'r2x.use_stars': True}
)

pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 200000

root = 'r2x.train_star_cat.magcat.magcat.starcat'

#pipel.config.update({X+'.resample': True})
#pipel.config.update({X+'.sed_distr': 'point'})

layer =  [{root+'.resample': True}]
layer += [{root+'.sed_distr':x } for x in ['uni', 'mean', 'point']]

top = xd.Topnode(pipel)
top.add_layer(layer)

#ipdb.set_trace()
Nboot = pipel.r2x.config['Nboot']
for key, pipel in top.iteritems():
    if pipel.depend[root].config['resample']:
        lbl = 'Resample'
    else:
        lbl = 'Distr: {0}'.format(pipel.depend[root].config['sed_distr'])

    # Checking the results.
    cat = pipel.r2x.test_cat.result
    cat = cat.join(pipel.r2x.get_store()['r2eff'])
    cat = cat[cat.mag_vis < 24.5]

    z = np.linspace(0.1, 1.8, 10)
    mean = cat.groupby(pd.cut(cat.zs, z)).mean()

    r2bias = mean[range(Nboot)].mean(axis=1)
    r2bias_std = np.sqrt(mean[range(Nboot)].var(axis=1))

    plt.errorbar(mean.zs, r2bias, r2bias_std, label=lbl)

plt.savefig(tosave.path('x82_diff_star_distr'))
plt.grid(True)
plt.legend()
ylim = 0.001
plt.ylim((-ylim, ylim))

xline = np.linspace(*plt.xlim())
y_lower = -3e-4*np.ones_like(xline)
y_upper = 3e-4*np.ones_like(xline)
plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

plt.show()
