#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import trainpipel

# Plotting the r,i,z and r,i against each other.
pipel = trainpipel.pipel()
pipel.config.update({\
  'r2x.use_cal': False, #True, #False, #False, #True, #False, #True,
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.use_stars': False,
  'r2x.filters': ['r','i', 'z']})


cat = pipel.r2x.test_cat.result
cat = cat.join(pipel.r2x.get_store()['r2eff'])

pipel.config['r2x.use_stars'] = True
cat = cat.join(pipel.r2x.get_store()['r2eff'], rsuffix='_stars')

def plot1():
    cat.plot(kind='hexbin', x='0', y='0_stars')

    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    #plt.xlabel('r,i,z', size=16)
    #plt.ylabel('r,i', size=16)

    plt.xlabel('Galaxies', size=16)
    plt.ylabel('Stars', size=16)

    lim = 0.01

    line = np.linspace(-lim, lim)
    plt.xlim((-lim, lim))
    plt.ylim((-lim, lim))

    plt.plot(line, line, ls='--', color='black')

    plt.show()

#cat = cat[(0.8 < cat.zs) & (cat.zs < 1.0)]
cat = cat[cat.mag_vis < 24.5]

if False:
    A = cat['0'] - cat['0_stars']
    print(A.mean())
    A.hist(bins=200, histtype='step')
    plt.xlim((-0.01, 0.01))
    plt.show()

cat['c0'] = cat.mag_r - cat.mag_i
cat['c1'] = cat.mag_i - cat.mag_z
ri = np.linspace(0.0, 1.5, 10)

mean = cat.groupby(pd.cut(cat.c1, ri)).mean()

Nboot = pipel.r2x.config['Nboot']
for i in range(Nboot):
    plt.plot(mean.c1, mean[str(i)], 'o-')
    plt.plot(mean.c1, mean[str(i)+'_stars'], 'o-')

#    ipdb.set_trace()

ax = plt.gca()
ax.xaxis.grid(True, which='both')
ax.yaxis.grid(True, which='both')

plt.legend()
plt.show()
