#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.interpolate import SmoothBivariateSpline, splrep, splev

import xdolphin as xd

import psf83

class Plot(psf83.Plot):
    name = 'psf84'

    def rshift(self, df, rL, zsplit):
        """Determine the optimal r-band magnitude shift."""

#        ipdb.set_trace()
        zkey = 'zb' 
        tmp = pd.DataFrame()
        for key, sub in df.groupby(pd.cut(df[zkey], zsplit)):
            r2bias = sub[range(len(rL))].mean(axis=0)
            part = pd.DataFrame({'r2bias': r2bias, 'lum': rL, 'z': sub[zkey].mean()})
            tmp = tmp.append(part, ignore_index=True)

        # Evaluate the spline...
        tmp = tmp[tmp.z < 2.1]
        spl = SmoothBivariateSpline(tmp.z, tmp.r2bias, tmp.lum, s=0.2)
        zev = np.arange(0.1, 2.2, 0.02)
        v2 = np.zeros_like(zev)
        r2shift = spl.ev(zev, v2)

        # Find the points.
        points = pd.DataFrame()
        for key, sub in tmp.groupby('z'):
            spl = splrep(sub.r2bias, sub.lum)

            y = float(splev(0., spl))
            points = points.append(pd.Series({'z': key, 'y': y}), ignore_index=True)

        return zev, r2shift, points

    def f_reduce(self, pipel):
        rL = np.linspace(-0.1, 0.1, 10)
        df = self.get_df(pipel, rL)

        zsplit1 = np.arange(0.0, 2.2, 0.02)
        zsplit2 = np.linspace(0.1, 1.8, 10)
        _,_, points1 = self.rshift(df, rL, zsplit1)
        _,_, points2 = self.rshift(df, rL, zsplit2)

        res = pd.concat({'hres': points1, 'lres': points2}, axis=0)

        return res

    def plot(self, prepare):
        hres = prepare.ix[0,'hres']
        lres = prepare.ix[0,'lres']

        # Adjust the figure size.
        fig = plt.gcf()
        size = fig.get_size_inches()
        size[1] *= 0.55
        fig.set_size_inches(*size)
        
        plt.grid(True)
        plt.xlabel('$z_{\mathrm{p}}$', size=14)
        plt.ylabel('r-band offset', size=14)

        # Separation between the calibration bins.
        zedges_cal = np.linspace(0.1, 1.8, 10)
        ax = plt.gca()
        for zi in zedges_cal:
            ax.axvline(zi, color='red', ls='--', alpha=0.4)

        # The high resolution version...
        plt.plot(hres.z, hres.y, 'x')
        plt.plot(lres.z, lres.y, 'o', color='k', ms=8)

        plt.subplots_adjust(bottom=0.188)
        plt.xlim((0., 2.1))
        plt.ylim((-0.1, 0.1))

if __name__ == '__main__':
    xd.plot(Plot).save()
