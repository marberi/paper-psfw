#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd
import scipy

def find_inter(prepare, xval, by):
    """Find the intercept with the Euclid requirement."""

    inter = pd.DataFrame()
    euclid_rec = 3e-4
    for k2,sub in prepare.groupby(by):
        lbls = dict(zip(by, k2))
        if ('fname' in lbls) and (lbls['fname'] == 'none'):
            continue

        fit = scipy.stats.linregress(sub[xval].values, sub.r2bias.values)

        xa = (-euclid_rec - fit.intercept) / fit.slope
        xb = (euclid_rec - fit.intercept) / fit.slope

        S = pd.Series()
        S['xa'] = xa
        S['xb'] = xb

        for name,val in zip(by, k2):
            S[name] = val

        S['zlbl'] = sub.iloc[0].zlbl
        inter = inter.append(S, ignore_index=True)

    return inter
