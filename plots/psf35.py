#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import svm
from sklearn.covariance import EllipticEnvelope
from sklearn.preprocessing import normalize

from xdolphin import Job

class psf35:
    name = 'psf35'
    config = {}

    def col(self, cat, filters):
        fmt = 'mag_{0}'

        A = np.array(cat[map(fmt.format, filters)])

        col = A[:,:-1] - A[:,1:]
        return col #pd.DataFrame(col)
#        return A

    def train_test(self, cstar, cgal, r2star, r2gal):
        Nstar = 400
        Ngal = 20000

        xstar = np.random.permutation(np.arange(len(cstar)))[:Nstar]
        xgal = np.random.permutation(np.arange(len(cgal)))[:Ngal]

        A_train = cstar[xstar]
        A_test = cgal[xgal]

        B_train = r2star[xstar]
        B_test = r2gal[xgal]

        return A_train, A_test, B_train, B_test

    def run(self):
        import trainpipel
        pipel = trainpipel.pipel()

        gal = pipel.kids['galcat']['magcat']['cat']
        star = pipel.kids['starnoise']['magcat']['cat']

        r2star = pipel.kids['r2star']['r2eff']['r2eff']['r2eff']
        r2gal = pipel.kids['r2gal']['r2eff']['r2eff']['r2eff']
        r2star = np.array(r2star)
        r2gal = np.array(r2gal)


        filters = ['vis', 'r', 'i', 'z', 'H', 'J']
        filters = ['r', 'i', 'z']
#        filters = ['r', 'i']
        cstar = self.col(star, filters)
        cgal = self.col(gal, filters)

        # Trying to understand the effect of the noise...
        galN = pipel.kids['galcatN']['magcat']['magcat']
        cgalN = self.col(galN, filters)

        starN = pipel.kids['starcatN']['magcat']['magcat']
        cstarN = self.col(starN, filters)

        if False:
            A_train, A_test, B_train, B_test = self.train_test(cstar, cgal, r2star, r2gal)
        else:
            A_train, A_test, B_train, B_test = self.train_test(cgal, cgal, r2gal, r2gal)

        reg = svm.NuSVR()
        reg.fit(A_train, B_train)
        B_rec = reg.predict(A_test)
        delta = pd.Series((B_rec - B_test)/B_test)

#        routl = EllipticEnvelope()
#        reg = svm.NuSVR()


        delta.hist(bins=200, histtype='step', normed=True, label='Normal') #lbl)
        for cont in [0.005, 0.01, 0.1, 0.2, 0.4]:
            routl = svm.OneClassSVM(nu=cont)
#            routl = EllipticEnvelope(contamination=cont)
            routl.fit(A_train)

            D = routl.predict(A_test)

#            ipdb.set_trace()
            print('Frac', (D != 1).sum()/ float(len(D)))
            delta[D == 1].hist(bins=200, histtype='step', normed=True, label='SVM: {0}'.format(cont))

            print('Mean', delta[D==1].mean())

#        ipdb.set_trace()
#        rec = pd.Series(B_rec)
#        delta.hist(bins=200, histtype='step', normed=True, label='Normal') #lbl)
#        ipdb.set_trace()

        plt.legend()
        plt.show()




if __name__ == '__main__':
    Job('psf35')()
