#!/usr/bin/env python

import ipdb
import trainpipel


pipel = trainpipel.pipel()

jpz = pipel.r2x.test_pz

#config = {'out_pdf': True, 'out_pdftype': True}
jpz.config.update(\
  {'out_pdf': True, 'out_pdftype': True})

#ipdb> jpz.config['out_pdf']
#False
#ipdb> jpz.config['out_pdftype']
#False

jpz.run()

#ipdb.set_trace()
