#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import statsmodels.api as sm
from sklearn import linear_model
from scipy.interpolate import splrep, splev

import xdolphin as xd

import psf4
import tosave

class Plot(psf4.Plot):
    """Test the "R^2_{\\rm{PSF}}" expansion in the paper."""


    def plot(self):
        # This is not the usual approach...
        r2vz_job = xd.Job('r2vz')
        r2vz_job.config['dz_ab'] = 0.0001
        r2vz_job.config['data_dir'] = '/data2/photoz/run/data'
        r2vz_job.config['filter_dir'] = '/data2/photoz/run/data/des_euclid'
        r2vz = r2vz_job.result

        # Testing first with the Elliptical galaxy (Ell_01).
        z = r2vz['z']
        y = r2vz['/vis/Ell_01']
        
        assert z[0] == 0., 'Something went wrong'
        norm = y[0]

        spl = splrep(z, y, k=5)

#        ipdb.set_trace()
#        if False
        mzs = 0.5
        zinterp = np.linspace(mzs - 0.2, mzs + 0.2, 200)
        mder = np.zeros(5)
        for i in range(len(mder)):
            mder[i] = splev(mzs, spl, der=i)

        mdz = zinterp-mzs
        ana1 = mder[0] + mder[1]*mdz 

        ana2 = mder[0] + mder[1]*mdz + 0.5*mder[2]*mdz**2 

        ana3 = mder[0] + mder[1]*mdz + 0.5*mder[2]*mdz**2 + (1./6.)*mder[3]*mdz**3 

        ana4 = mder[0] + mder[1]*mdz + 0.5*mder[2]*mdz**2 + (1./6.)*mder[3]*mdz**3 + \
               (1./24.)*mder[4]*mdz**4 

        ydata = splev(zinterp, spl) 

        fig, A  = plt.subplots(nrows=2, sharex=True)
        # The absolute values.
        ax = A[0]
        ax.plot(zinterp, ydata/norm,'.', label='Input')
        ax.plot(zinterp, ana1/norm, '--', label='1st Order')
        ax.plot(zinterp, ana2/norm, '--', label='2nd Order')
        ax.plot(zinterp, ana3/norm, '--', label='3rd Order')
        ax.grid(True)
        ax.set_xlim((0.4, 0.6))
        ax.set_ylim((1.01, 1.03))
        ax.legend(loc=4)
        ax.set_ylabel('$"R^2_{\\rm{PSF}}"/"R^2_{\\rm{PSF}}"$(Ell_01)', size=16)
        ax.vlines(mzs, *ax.get_ylim(), color='black', lw=2, alpha=0.5)

        # Showing the ratios
        ax = A[1]
        ax.plot(zinterp, ana1/ydata, '--', label='1st Order')
        ax.plot(zinterp, ana2/ydata, '--', label='2nd Order')
        ax.plot(zinterp, ana3/ydata, '--', label='3rd Order')
#        plt.plot(zinterp, ana4, '--', label='4th Order')

        ax.grid(True)
        ax.set_xlim((0.4, 0.6))
        ax.set_ylim((0.994, 1.006))
        ax.hlines(1., *ax.get_xlim(), color='black', lw=2, alpha=0.5)
        ax.vlines(mzs, *A[1].get_ylim(), color='black', lw=2, alpha=0.5)
        ax.set_xlabel('Redshift', size=16)
        ax.set_ylabel('$"R^2_{\\rm{PSF}}"$(Expansion) / $"R^2_{\\rm{PSF}}"$(True)', size=16)
        ax.legend(loc=4)

        plt.savefig(tosave.path('psf88'))

    def run(self):
        self.plot()

if __name__ == '__main__':
    Plot().run()
