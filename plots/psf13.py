#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

import tosave

class Plot:
    """Varying the training sample for different noise configurations."""

    def topnode(self):
        import trainpipel

        topnode = xd.Topnode()
        topnode['noisy',] = trainpipel.pipel(add_noise=True)
        
        topnode.update_config({\
          'r2train.Nboot': 10,
          'r2train.all_test': True,
#          'r2train.Nboot': 4,
          'r2train.filters': ['r', 'i', 'z']})


        Ntrain = [400, 450, 500, 550, 700, 1000, 2000]
#        Ntrain = [400, 500, 700, 1000]
        topnode.add_layer_key('r2train.Ntrain', Ntrain)

        topnode.add_layer_key('r2train.use_stars', [False])

        return topnode

    def prepare(self, pipel):
        cat = pipel.r2train.galcat.result[['mag_vis']]
        cat = cat.join(pipel.r2train.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2train.config['Nboot']

        S = pd.Series()
        S['r2bias'] = cat[range(Nboot)].mean().mean()

#        ipdb.set_trace()        

        return S

    def make_hist(self, pipel):
        cat = pipel.r2train.galcat.result[['mag_vis']]
        cat = cat.join(pipel.r2train.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        ipdb.set_trace()

    def run(self):
        topnode = self.topnode()


        M = {'Ntrain': 'r2train.Ntrain',
             'use_stars': 'r2train.use_stars'}

        S = topnode.toS(M)
        prepare = topnode.get_prepare(self.prepare)
        T = S.join(prepare)

        for key,line in T.groupby('ind0'):
            line = line.sort('Ntrain')
            lbl_star = 'Stars' if line.irow(0).use_stars else 'Galaxies'
#            lbl = '{0}, {1}'.format(lbl_star, line.ind2.irow(0)) #.capitalize())

            plt.plot(line.Ntrain, line.r2bias, 'o-', label=lbl_star)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('Number of training objects', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend()
        plt.savefig(tosave.path('psf13'))
#        plt.show()


if __name__ == '__main__':
    Plot().run()
