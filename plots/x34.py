#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

path = '/data2/marberi/tmp/x33.h5'
store = pd.HDFStore(path)
gc = store['default']
store.close()

z = np.linspace(0., 2., 20)

mean = gc.groupby(pd.cut(gc.zs, z)).mean()

if False:
    plt.plot(mean.zs, mean[0], 'o-')
    plt.show()

xL = np.linspace(-0.2, 0.2, 51)
Nshift = 51

def plot1():
    A = mean[range(Nshift)].values

    for i,zs in enumerate(mean.zs):
        plt.plot(xL, A[i], label=str(zs))

    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    plt.xlabel('r-band shift', size=16)
    plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

    plt.show()

def plot2():
    A = mean[range(Nshift)].values

    i = 10
    plt.plot(xL, A[i], 'o-')

    plt.show()

def plot2():
    i0 = 0
    i1 = 50
    x0 = xL[i0]
    x1 = xL[i1]
    y0 = gc[i0].mean()
    y1 = gc[i1].mean()

    a = (y1 - y0) / (x1 - x0) #xL[i1] - xL[i0])
    b = y0 - a*x0

    z = -b/a

    # A quick test so we didn't do anything wrong..
    D = gc.mean()

    r2bias = gc[range(Nshift)].mean()
    plt.plot(xL, r2bias, 'o-')
    plt.axhline(0.0)
    plt.axvline(z)

    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    plt.show()

if False:
    x = np.linspace(0., 10.)
    y = (x-3.1)**2 + 4.

    plt.plot(x, y, 'o-')
    plt.show()

if False:
    def y(x0):
        x = x0[0]

        return (x-3.1)**2 + 4.

    import scipy.optimize
    A = scipy.optimize.minimize(y, 0)

z = np.linspace(0.1, 2.0, 10)
mean = gc.groupby(pd.cut(gc.zs, z)).mean()

fisk = mean[range(Nshift)]
fisk = fisk.set_index(mean.zs)
fisk.columns = xL

# This could probably be done better.
E = fisk.unstack()
Atrain = np.zeros((len(E), 2))
Btrain = np.zeros(len(E))
for i,(key, val) in enumerate(E.iteritems()):
    Atrain[i,:] = key
    Btrain[i] = val

from sklearn import svm
reg = svm.NuSVR()
reg.fit(Atrain, Btrain)

def f(mshift):
    ipdb.set_trace()


import scipy.optimize
#mshift = np.linspace(
ipdb.set_trace()
