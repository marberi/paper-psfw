#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator, AutoLocator
import matplotlib as mpl

from cycler import cycler
import xdolphin as xd
import trainpipel

def gen_styles():
    i = 0
#    styles = ['-','-.','--']
    styles = ['-', '--', ':', '-.','']
    while True:
        yield styles[i % len(styles)]
        i += 1


plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y', 'k']) +
                           cycler('linestyle', ['-', '--', ':', '-', '-.'])))


class Plot:
    """Detailed comparison of colors and R^2 for stars and galaxies."""

    name = 'psf66'

    def pipel(self):
        return trainpipel.pipel()

    def load(self, pipel):
        data = {}

        gc = pipel.r2x.test_cat.magcat.result
        gc = gc.join(pipel.r2x.test_r2.result)
        norm = pipel.r2x.test_r2.r2vz.result['/vis/Ell_01'][0]
        gc['r2true'] = gc.r2eff / norm

        # The stars.
        starcat = pipel.r2x.train_star_cat.magcat.magcat.result
        starcat = starcat.join(pipel.r2x.train_star_r2.result)
        starcat['r2true'] = starcat.r2eff / norm

        data['gc'] = gc
        data['starcat'] = starcat

        return data

    def plot(self, data):
        # Not the best names.
        gc = data['gc']
        starcat = data['starcat']

        # Euclid selection
        gc = gc[gc.mag_vis < 24.5]

        fL = [('r', 'vis'), ('i', 'vis'), ('z', 'vis')]
        fL = [('r', 'i'), ('i', 'z'), ('r', 'z')]
        z = np.linspace(0.1, 2., 8)
        z = np.arange(0.0, 2.2, 0.4)

        fig,A = plt.subplots(1,3, sharey=True)
        gr_z = pd.cut(gc.zs, z)

        # Lines marking the mean of the different galaxy samples.
        for i,(f1,f2) in enumerate(fL):

            gc['xcol'] = gc['mag_'+f1] - gc['mag_'+f2]
            gc.plot(ax=A[i], kind='hexbin', x='xcol', y='r2true', gridsize=20, colormap='binary',
                    colorbar=False,
                    label='Galaxies')

            c = np.linspace(-0.8, 2.5, 60)
            styles = gen_styles()
            for key,sub in gc.groupby(gr_z):
                mean = sub.groupby(pd.cut(sub.xcol, c)).mean()

                z_lbl = 'z = {0}'.format(key) #sub.irow(0).zlbl)
                ls = next(styles)
                A[i].plot(mean.xcol, mean.r2true, lw=1.4, label=z_lbl)

#        # Add the stars. Select a subset?
#        r = np.random.randint(0, starcat.size, 400)
#        subS = starcat.ix[r]
#        ipdb.set_trace()

        subS = starcat.sample(400)

        for i,(f1,f2) in enumerate(fL):
            col = subS['mag_'+f1] - subS['mag_'+f2]
            A[i].plot(col, subS.r2true, '.', color='orange', ms=4, label='Stars')

        # Legend
#        A[-1].legend()
#        leg = A[1].legend(bbox_to_anchor=(1.5, 1.05)) #, zorder=1000) #loc=4)
#        leg.set_zorder(100000000000000.)

        # Another try on the legend.
        lines = A[1].get_lines()
        labels = [x.get_label() for x in lines]

        fig = plt.gca()
#        fig.legend(lines, labels, loc=(-2.15,0.87), ncol=3, prop={'size': 14})
#        fig.legend(lines, labels, loc=(-2.15,0.87), ncol=3) #, prop={'size': 14})
        fig.legend(lines, labels, loc=(-2.05,0.87), ncol=3) #, prop={'size': 14})

        # Manually tweaking the limits
        for ax in A:
            ax.set_ylim((0.97, 1.075))

        # Removing some frames.
        A[0].spines['right'].set_visible(False)
        A[1].spines['left'].set_visible(False) 
        A[1].spines['right'].set_visible(False)
        A[2].spines['left'].set_visible(False) 
        A[2].spines['right'].set_visible(False) 
 
        A[0].spines['top'].set_visible(False)
        A[1].spines['top'].set_visible(False)
        A[2].spines['top'].set_visible(False)

        # Labels. Need to disable the y-label set by Pandas.
        for i in range(len(fL)):
            if i == 0:
                A[i].set_ylabel('$R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}\,$(Ell_01, z=0)', size=13)
            else:
                A[i].yaxis.label.set_visible(False)

        for i,(f1,f2) in enumerate(fL):
            A[i].set_xlabel('{0} - {1}'.format(f1,f2), size=14)
            A[i].xaxis.set_major_locator(MaxNLocator(prune='both', nbins=5))

        # And then the grid.
        for i in range(len(fL)):
            A[i].xaxis.grid(True, which='both')
            A[i].yaxis.grid(True, which='both')

        plt.subplots_adjust(left=0.11, wspace=0.1 )

if __name__ == '__main__':
    xd.plot(Plot).save()
