#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave
import xdolphin as xd
from xdolphin import Job
sys.path.append('/home/marberi/xdolphin/xdolphin/lang')

import bootstrap
import trainpipel

def find_seds():
    """Read in the list of all SEDs."""

    fjc_file = '/home/marberi/drv/work/papers/psfw/data/fjc_temp'
    seds = map(str.strip, open(fjc_file).readlines())

    return seds

class Plot:
    """Redshift dependence for different photo-z "R^2_{\\rm{PSF}}" estimations."""

    def topnode(self):
        pipel = trainpipel.pipel()
        pipel.config.update({\
#          'r2pz.use_ml': False,
          'r2pz.pzcat.use_priors': False,
          'r2pz.pzcat.out_pdf': True,
          'r2pz.pzcat.out_pdftype': True})

        # Mostly because of being tired of waiting for the
        # results.
#        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 35000
        pipel.config.update({\
#          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 10000,
#          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 1000,
          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 4000,
          'r2pz.pzcat.galcat.magcat.mabs.limit_seds': True})

        pipel.config['r2pz.pzcat.seds'] = find_seds()

        pipel.r2pz.pzcat.config['filters'] = ['r','i','z']

#        ipdb.set_trace()
#        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 30000
#        ipdb.set_trace()

        return pipel

    def plot1(self, pipel):

        J = pipel.r2pz
        r2eff = J.r2gal.result
        r2pz = J.result
        galcat = J.pzcat.galcat.result
        mabs = J.pzcat.galcat.magcat.mabs.result
        pzcat = J.pzcat.result

        df = pd.concat([r2eff,r2pz,galcat,mabs,pzcat], axis=1)
#        df = df[df.mag_vis < 24.5]
#        df = df[df.mag_vis < 22.5]

        colors = ['blue', 'green', 'red']
        L = ['r2peak', 'r2pdf', 'r2pdf_type']
        for i,key in enumerate(L):
            color = colors[i]

            df[key].hist(bins=100, normed=True, histtype='step', color=color, label=key)
            vline = df[key].mean()
            plt.axvline(vline, label='line: '+key, color=color)
            print('"R^2_{\\rm{PSF}}" bias', key, vline)

        xlim = 0.01
        plt.xlim((-xlim, xlim))
        plt.legend()
        plt.savefig('test.pdf')
        plt.show()

    def plot2(self, pipel):
        cat = pipel.r2pz.pzcat.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result, rsuffix='_I')
        cat = cat.join(pipel.r2pz.pzcat.galcat.magcat.mabs.result, rsuffix='I2_')
#        cat = cat[cat.mag_vis < 18.0]


        all_seds = find_seds()
        only_seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14', 'I99_05Gy']

        sed_nr = map(all_seds.index, only_seds)
        nr_map = dict(enumerate(sed_nr)) #only_seds))

        cat['input_sed'] = cat.sed.map(nr_map)

        if False:
            plt.plot(cat.input_sed, cat.t_b, '.')
            plt.savefig('test.pdf')

        for key, sub in cat.groupby(cat.input_sed):
            sub.t_b.hist(label=all_seds[key], normed=True, histtype='step', lw=1.5, bins=20)

        plt.legend()
        plt.savefig('test.pdf')

        # We need to replace 
        # ipdb.set_trace()

    def run(self):
        pipel = self.topnode()
        self.plot2(pipel)

if __name__ == '__main__':
    Plot().run()
