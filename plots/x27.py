#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

class Plot:
    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag


    def plot1(self):
        pipel = trainpipel.pipel()

        star_cat = pipel.r2train.train_star_cat.result
        star_cat = star_cat.join(pipel.r2train.train_star_r2.result)

        gc = pipel.r2train.galcat.result
        gc = gc.join(pipel.r2train.r2gal.result)
        norm = pipel.r2train.r2gal.r2vz.result['/vis/Ell_01'][0]

        Ntrain = 400
        fL = ['r', 'i', 'z']
#        fL = ['r', 'i'] #, 'z']

        Astar, _ = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(gc, fL, True)

        inds = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds]
        Btrain = star_cat.r2eff.values[inds]
        from sklearn import svm

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        r2pred = reg.predict(col_gal)

        gc['r2bias'] = (r2pred - gc.r2eff) / gc.r2eff

        redges = np.linspace(gc.r2eff.min(), gc.r2eff.max(), 10)

        zedges = np.linspace(0.1, 1.8, 7)
        df = pd.DataFrame()

        gr1 = pd.cut(gc.r2eff, redges)
        gr2 = pd.cut(gc.zs, zedges)

        df = pd.DataFrame()
        for key, val in gc.groupby([gr1, gr2]):
            S = pd.Series()
            S['rbin'] = key[0]
            S['zbin'] = key[1]
            S['r'] = val.r2eff.mean()
            S['r2bias'] = val.r2bias.mean()

            df = df.append(S, ignore_index=True)

        mean_all = gc.groupby(pd.cut(gc.r2eff, redges)).mean()

        # Then plotting.
        df = df.sort('zbin')
        for key, val in df.groupby(df.zbin):
            if key == 'all': continue

            plt.plot(val.r / norm, val.r2bias, 'o-', label=key)


        plt.plot(mean_all.r2eff / norm, mean_all.r2bias, 'o-', lw=2, color='k', label='All')


        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(0., ls='--', color='black', lw=2., alpha=0.5)
 
        plt.xlabel('$"R^2_{\\rm{PSF}}"$ / $"R^2_{\\rm{PSF}}"$[Ell_01(z=0)]', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.legend()
        plt.show()

    def plot2(self):
        pipel = trainpipel.pipel()

        star_cat = pipel.r2train.train_star_cat.result
        star_cat = star_cat.join(pipel.r2train.train_star_r2.result)

        gc = pipel.r2train.galcat.result
        gc = gc.join(pipel.r2train.r2gal.result)
#        norm = pipel.r2train.r2gal.r2vz.result['/vis/Ell_01'][0]

        Ntrain = 400
        fL = ['r', 'i', 'z']
#        fL = ['r', 'i'] #, 'z']

        Astar, _ = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(gc, fL, True)

        inds_train = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds_train]
        Btrain = star_cat.r2eff.values[inds_train]
        from sklearn import neighbors, svm

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        gc['r2pred'] = reg.predict(col_gal)
        gc['r2bias'] = (gc.r2pred - gc.r2eff) / gc.r2eff
 
        regn = neighbors.KNeighborsRegressor(n_neighbors=1)
        regn.fit(Atrain, Btrain)

        # Neded some index magic.
        inds = regn.kneighbors(col_gal)[1][:,0]
        Adiff = Atrain[inds] - col_gal

        gc['dc1'] = Adiff[:,0]
#        gc['dc2'] = Adiff[:,1]
        field = 'dc1'
        xlblD = {'dc1': 'r-i', 'dc2': 'i-z'}

        medges = np.linspace(-0.5, 0.5, 10)
        zedges = np.linspace(0.1, 1.8, 7)

        gr1 = pd.cut(gc[field], medges)
        gr2 = pd.cut(gc.zs, zedges)

        df = pd.DataFrame()
        for key, val in gc.groupby([gr1, gr2]):
            S = pd.Series()
            S['rbin'] = key[0]
            S['zbin'] = key[1]
            S['r'] = val.r2eff.mean()
            S['r2bias'] = val.r2bias.mean()
            S['cdiff'] = val[field].mean()

            df = df.append(S, ignore_index=True)

        mean_all = gc.groupby(pd.cut(gc[field], medges)).mean()

        for key, val in df.groupby('zbin'):
            if key == 'all':
                continue

            plt.plot(val.cdiff, val.r2bias, 'o-', label=key)

        plt.plot(mean_all[field], mean_all.r2bias, 'o-', lw=2., label='All')
#            ipdb.set_trace()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('Col difference [{0}]'.format(xlblD[field]), size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend()
        plt.show()


    def plot3(self):
        pipel = trainpipel.pipel()

        star_cat = pipel.r2train.train_star_cat.result
        star_cat = star_cat.join(pipel.r2train.train_star_r2.result)

        gc = pipel.r2train.galcat.result
        gc = gc.join(pipel.r2train.r2gal.result)
#        norm = pipel.r2train.r2gal.r2vz.result['/vis/Ell_01'][0]

        Ntrain = 400
        fL = ['r', 'i', 'z']
#        fL = ['r', 'i'] #, 'z']

        Astar, _ = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(gc, fL, True)

        inds_train = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds_train]
        Btrain = star_cat.r2eff.values[inds_train]
        from sklearn import neighbors, svm

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        gc['r2pred'] = reg.predict(col_gal)
        gc['r2bias'] = (gc.r2pred - gc.r2eff) / gc.r2eff
#        gc['r2diff'] = (gc.r2pred - gc.r2eff) / gc.r2eff
 
        regn = neighbors.KNeighborsRegressor(n_neighbors=1)
        regn.fit(Atrain, Btrain)

        # Neded some index magic.
        inds = regn.kneighbors(col_gal)[1][:,0]
        Adiff = Atrain[inds] - col_gal
        gc['dc1'] = Adiff[:,0]
        gc['dc2'] = Adiff[:,1]

        df = pd.DataFrame()
        medges = np.linspace(-0.5, 0.5, 10)
        zedges = np.linspace(0.1, 1.8, 7)

        gr1 = pd.cut(gc.zs, zedges)
        gr2 = pd.cut(gc.dc1, medges)
        for key, val in gc.groupby([gr1, gr2]):
            S = pd.Series()
            S['z'] = val.zs.mean()
            S['dc'] = val.dc1.mean()
            S['r2bias'] = val.r2bias.mean()

            df = df.append(S, ignore_index=True)

        ipdb.set_trace()

        if False:
            plt.plot(df.z, df.p1, 'o-', label='p1')
            plt.plot(df.z, df.p2, 'o-', label='p2')
            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            plt.legend()
            plt.show()

        reg2 = svm.NuSVR()
        reg2.fit(df[['dc', 'z']].values, df.r2bias)

        bpred = reg2.predict(gc[['dc1', 'zs']].values)
        gc['bpred'] = bpred

        gc['rup'] = gc.r2bias - gc.bpred

        z = np.linspace(0.1, 1.8, 10)
        mean = gc.groupby(pd.cut(gc.zs, z)).mean()

        plt.plot(mean.zs, mean.r2bias, 'o-', label='Original')
        plt.plot(mean.zs, mean.rup, 'o-', label='rup')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.legend()
        plt.show()


    def run(self):
        pipel = trainpipel.pipel()

        star_cat = pipel.r2train.train_star_cat.result
        star_cat = star_cat.join(pipel.r2train.train_star_r2.result)

        gc = pipel.r2train.galcat.result
        gc = gc.join(pipel.r2train.r2gal.result)
#        norm = pipel.r2train.r2gal.r2vz.result['/vis/Ell_01'][0]

        Ntrain = 400
        fL = ['r', 'i', 'z']
#        fL = ['r', 'i'] #, 'z']

        Astar, _ = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(gc, fL, True)

        inds_train = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds_train]
        Btrain = star_cat.r2eff.values[inds_train]
        from sklearn import neighbors, svm

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        gc['r2pred'] = reg.predict(col_gal)
        gc['r2bias'] = (gc.r2pred - gc.r2eff) / gc.r2eff
 
        regn = neighbors.KNeighborsRegressor(n_neighbors=1)
        regn.fit(Atrain, Btrain)

        # Neded some index magic.
        inds = regn.kneighbors(col_gal)[1][:,0]
        Adiff = Atrain[inds] - col_gal

        gc['dc1'] = Adiff[:,0]

        gc['c1'] = col_gal[:,0]
        gc['c2'] = col_gal[:,1]

#        gc['dc2'] = Adiff[:,1]
        #field = 'dc1'
        field = 'c1'
        xlblD = {'dc1': 'r-i', 'dc2': 'i-z'}

        #medges = np.linspace(-0.5, 0.5, 10)
        medges = np.linspace(-3, 1.0, 10)
        zedges = np.linspace(0.1, 1.8, 10)

        gr1 = pd.cut(gc[field], medges)
        gr2 = pd.cut(gc.zs, zedges)
        gr3 = pd.cut(gc[field], medges)

        df = pd.DataFrame()

        if False:
            gc.c1.hist(label='c1')
            gc.c2.hist(label='c2')
            plt.legend()
            plt.show()

#        ipdb.set_trace()        

        for key, val in gc.groupby([gr1, gr2, gr3]):
#        for key, val in gc.groupby([gr1, gr3]):
            S = pd.Series()
            S['rbin'] = key[0]
            S['zbin'] = key[1]
            S['r'] = val.r2eff.mean()
            S['r2bias'] = val.r2bias.mean()
            S['zs'] = val.zs.mean()
#            S['cdiff'] = val[field].mean()
            S[field] = val[field].mean()
            S['c2'] = val.c2.mean()

            df = df.append(S, ignore_index=True)



        reg2 = svm.NuSVR()
        reg2.fit(df[[field, 'zs', 'c2']].values, df.r2bias)

        bpred = reg2.predict(gc[[field, 'zs', 'c2']].values)
        gc['bpred'] = bpred
        gc['rup'] = gc.r2bias - gc.bpred

#        ipdb.set_trace()

        z = np.linspace(0.1, 1.8, 10)
        mean = gc.groupby(pd.cut(gc.zs, z)).mean()

        plt.plot(mean.zs, mean.r2bias, 'o-', label='Original')
        plt.plot(mean.zs, mean.rup, 'o-', label='rup')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.legend()
        plt.show()



#        mean_all = gc.groupby(gr1).mean()
#
#        for key, val in df.groupby('zbin'):
#            if key == 'all':
#                continue
#
#            plt.plot(val[field], val.r2bias, 'o-', label=key)
#
#        plt.plot(mean_all[field], mean_all.r2bias, 'o-', lw=2., label='All')
##            ipdb.set_trace()
#
#        ax = plt.gca()
#        ax.xaxis.grid(True, which='both')
#        ax.yaxis.grid(True, which='both')
#
##        plt.xlabel('Col difference [{0}]'.format(xlblD[field]), size=16)
#        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
#
#        plt.legend()
#        plt.show()




#        ipdb.set_trace()
#        np.polyfit(Adiff[:,0], gc.r2bias, 1)
#        ipdb.set_trace()

if __name__ == '__main__':
    Plot().plot2()
