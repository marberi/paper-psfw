#!/usr/bin/env python

from __future__ import print_function
from collections import OrderedDict
import ipdb
import os
import time
import numpy as np
import pandas as pd

from bokeh.client import push_session
from bokeh.io import show, output_server #, output_file
#from bokeh.plotting import Session
from bokeh.plotting import curdoc, Figure
from bokeh.models import ColumnDataSource, Plot, MultiLine, DataRange1d, Circle
from bokeh.models import HoverTool
from bokeh.models import CustomJS
import bokeh.charts as ch
import bokeh.palettes as pl

from matplotlib import pyplot as plt

import test11
import squid

# for the graph layout.
import networkx as nx



# Adding the nodes.
def add_nodes(graph, nodes):
#    nodes = nodes.set_index('taskid')
    # Remove the duplicated entries.
    nodes['isdupl'] = nodes.duplicated(['taskid'])
    nodes = nodes[~nodes.isdupl]


    # The normal nodes.
    for i,row in nodes.iterrows():
        name = row['taskid']
        graph.add_node(name)

    # Adding the figures as nodes.
    for fig in nodes.fig.unique():
        graph.add_node(fig)


# Adding the edges.
def add_edges(graph, nodes, edges):
    edges = edges.join(nodes.taskid, on='from')
    edges = edges.join(nodes.taskid, on='to', rsuffix='_to')

    for i, row in edges.iterrows():
        graph.add_edge(row['taskid'], row['taskid_to'])

    for i, row in nodes[nodes.name == 'empty'].iterrows():
        graph.add_edge(row['fig'], row['taskid'])


def calc_pts(graph, nodes, edges):
    print('starting layout')
    t1 = time.time()
    #pts = nx.random_layout(g)
    pts = nx.spring_layout(graph)
    t2 = time.time()

    return pts

def edges_df(graph, pts):
    edges = pd.DataFrame()
    start_id, end_id = zip(*graph.edges())
   
    A = np.vstack([pts[x] for x in start_id])
    B = np.vstack([pts[x] for x in end_id])

    xs = zip(A[:,0], B[:,0])
    ys = zip(A[:,1], B[:,1])

    edges = pd.DataFrame({'start_id': start_id, 'end_id': end_id,
                          'xs': xs, 'ys': ys})

    return edges

def points_df(graph, pts):
    points = pd.DataFrame(pts).transpose().reset_index()
    points = points.rename(columns={
            0: 'x',
            1: 'y',
            'index': 'start_id',
        })

    return points

def calc():
    inuse = squid.find_inuse()
    nodes, edges = test11.get(inuse)

    graph = nx.Graph()
    add_nodes(graph, nodes)
    add_edges(graph, nodes, edges)

    pts = calc_pts(graph, nodes, edges)

    edges = edges_df(graph, pts)
    points = points_df(graph, pts)
    tasks = nodes[~nodes.isdupl]
    tasks = tasks.set_index('taskid')

    points = points.join(tasks.name, on='start_id')

    return points, edges, tasks

def get_info():
    d = '/data2/marberi/results/scratch'
    name = 'graph1.h5'

    path = os.path.join(d, name)
    if os.path.exists(path):
        with pd.HDFStore(path) as store:
            points = store['points']
            edges = store['edges']
            tasks = store['tasks']
    else:
        points, edges, tasks = calc()
        with pd.HDFStore(path, 'w') as store:
            store['points'] = points
            store['edges'] = edges
            store['tasks'] = tasks

    return points, edges, tasks

def iscached():
    d = '/data2/marberi/results/default'

    t1 = time.time()
    L = os.listdir(d)
    t2 = time.time()

#    print('time', t2-t1)
    return pd.Series(L)


#import sys
#sys.exit(1)

points, edges, tasks = get_info()
L = iscached()
points['iscached'] = points.start_id.isin(L)

# This should have been set earlier.
prob = map(lambda x: not isinstance(x, str), points.name)
points.ix[prob, 'name'] = 'fig'

PINK_400 = '#ec407a'
CYAN_800 = '#00838f'
points['color'] = CYAN_800
points['size'] = 5

spec = pl.Spectral11+pl.YlOrRd9+pl.BuGn4
for i,(key,sub) in enumerate(points.groupby('name')):
    if key == 'fig':
        points.ix[sub.index, 'color'] = PINK_400
        points.ix[sub.index, 'size'] = 10
    elif key == 'empty':
        points.ix[sub.index, 'color'] = pl.Blues9[0]
        points.ix[sub.index, 'size'] = 5
    else:
        points.ix[sub.index, 'color'] = pl.Oranges5[0]

# Color the calculated results.
points.ix[points.iscached, 'color'] = pl.Greys9[1]

# Should be replaced by the builtin paletts
GRAY_900 = '#212121'
CYAN_50 = '#e0f7fa'
CYAN_900 = '#006064'
PINK_50 = '#fce4ec'
PINK_400 = '#ec407a'
PINK_800 = '#ad1457'


points['taskname'] = points.name
edges['taskname'] = 'blah'

#points = points.transpose()

inner_circle_source = ColumnDataSource(points)
line_source = ColumnDataSource(edges)
outer_circle_source = ColumnDataSource(points)

#plot = Plot()
if True:
    plot = Plot(
        x_range=DataRange1d(), 
        y_range=DataRange1d(), 
        plot_height=600, 
        plot_width=800,
        background_fill=PINK_50,
        border_fill=GRAY_900,
        outline_line_color=None,
    )

#TOOLS = "resize,crosshair,pan,wheel_zoom,box_zoom,reset,tap,previewsave,box_select,poly_select,lasso_select"
hover = HoverTool(tooltips=[('Task type', '@taskname'),
                            ('ID', '@start_id')])

fig = Figure(tools=[hover,'resize','box_zoom', 'wheel_zoom', 'pan', 'box_select', 'poly_select', 'tap'])
fig.background_fill = "beige"

# Hiding the grid.
fig.grid.grid_line_color = None
fig.axis.axis_line_color = None

# See: http://stackoverflow.com/questions/27141240
fig.axis.major_tick_line_color = None
fig.axis.major_label_text_font_size = '0pt'
axis = fig.axis
axis.major_tick_line_color = None  # turn off major ticks
axis[0].ticker.num_minor_ticks = 0  # turn off minor ticks
axis[1].ticker.num_minor_ticks = 0


# Add some lines.
line_props = dict(xs='xs', ys='ys', line_width=.2, line_alpha=0.4)
nonselected_line = MultiLine(line_color=CYAN_900, **line_props)
selected_line = MultiLine(line_color=CYAN_50, **line_props)

fig.add_glyph(
    line_source, 
    glyph=nonselected_line,
    nonselection_glyph=nonselected_line,
    selection_glyph=selected_line
)

circle_props = dict(x='x', y='y', size='size', line_color=None, fill_alpha=0.5)
mycircle = fig.circle(fill_color='color', **circle_props)
src = ColumnDataSource(points)

mycircle.data_source = src


from bokeh.plotting import hplot

source2 = ColumnDataSource(dict(x=[], y=[]))
fig2 = Figure()
fig2.circle('x','y', fill_color='color', source=source2)

src.callback = CustomJS(args=dict(s2=source2), code="""
        var inds = cb_obj.get('selected')['1d'].indices;
        var d1 = cb_obj.get('data');
        var d2 = s2.get('data');
        d2['x'] = []
        d2['y'] = []
        d2['color'] = []
        for (i = 0; i < inds.length; i++) {
            d2['x'].push(d1['x'][inds[i]])
            d2['y'].push(d1['y'][inds[i]])
            d2['color'].push(d1['color'][inds[i]])
        }
        s2.trigger('change');
    """)



F = hplot(fig, fig2)

#ipdb.set_trace()

#print('finished..')
#show(plot)

def update():
    L = iscached()
    df = src.to_df()
    df['niscached'] = df.start_id.isin(L)
    df['new_nodes'] = df.niscached & ~df.iscached

    df.ix[df.new_nodes, 'color'] = pl.BuGn9[1]
    df.ix[df.new_nodes, 'size'] = 8

    src.data['iscached'] = df.niscached
    src.data['color'] = df.color

    if df.new_nodes.sum():
        print('found')
    else:
        print('finished update')

#    ipdb.set_trace()
#    pass
#    print('fisk')

#fig = Figure(plot)

#curdoc().add_root(plot)
curdoc().add_root(F) #fig)
curdoc().add_periodic_callback(update, 30)
session = push_session(curdoc())
session.show() #plot)
session.loop_until_closed()
