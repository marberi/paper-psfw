#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


config = {'ell': {
        'phi': [2.4, 1.1, -2.7],
        'Ms': [5.0, 1.6, -21.9],
        'alpha': [1.7, 1.6, -1]},

     'sp': {\
        'phi': [0.5, 0.1, -2.28],
        'Ms': [3.2, 2.5, -21.0],
        'alpha': [0.7, -0.9, -1.5]},

     'irr': {\
        'phi': [1.0, -3.5, -3.1],
        'Ms': [5.0, 1.3, -20.0],
        'alpha': [1.8, 0.9, -1.85]}}

def f(z, a,b,c):
#    ipdb.set_trace()

    return a*np.exp(-(1+z)**b) + c


z = np.linspace(0., 2.)

labels = ['ell', 'sp', 'irr']
if False:
    for lbl in labels:
        y = f(z, *config[lbl]['phi'])

        plt.plot(z, 10**y, label=lbl)


for lbl in labels:
    y = f(z, *config[lbl]['Ms'])

    plt.plot(z, y, label=lbl)



plt.legend()
plt.show()


#    ipdb.set_trace()
