#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from sklearn import svm, linear_model

import xdolphin as xd
import trainpipel

pipel = trainpipel.pipel()
pipel.config.update({
  'r2x.use_cal': False,
  'r2x.filters': ['r', 'i', 'z'],
#  'r2x.filters': ['r', 'i'],
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 10,
  'r2x.use_stars': True})


def plot1():
    top = xd.Topnode(pipel)
    top.add_layer_key('r2x.filters', [['r', 'i', 'z'], ['r', 'i']])
    top.add_layer_key('r2x.test_cat.r', [1., 1000000.])

    for key, pipel in top.iteritems():
        lbl_filters = ','.join(pipel.config['r2x.filters'])
        lbl_noise = 'Noiseless' if 10000 < pipel.config['r2x.test_cat.r'] \
                    else 'Noisy'

        lbl = '{0}: {1}'.format(lbl_filters, lbl_noise)

        cat = pipel.r2x.test_cat.result
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat['c0'] = cat.mag_r - cat.mag_i
        cat = cat[cat.mag_vis < 24.5]
        cat = cat[cat.zs < 0.3] # TEST.

        col = np.linspace(-1., 2., 20)
        mean = cat.groupby(pd.cut(cat.c0, col)).mean()
        Nboot = pipel.r2x.config['Nboot']
        r2bias = mean[range(Nboot)].mean(axis=1)

        plt.plot(mean.c0, r2bias, '.-', label=lbl)


    ax = plt.gca()
    ax.xaxis.grid(True, which='both')
    ax.yaxis.grid(True, which='both')

    plt.xlim((-0.2, 2.0))
    plt.ylim((-0.02, 0.02))
    plt.xlabel('r - i', size=16)
    plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

    plt.legend(loc=8)
    plt.show()


def plot2():
    cat = pipel.r2x.test_cat.result
    cat = cat.join(pipel.r2x.test_cat.magcat.result, rsuffix='_nl')
    cat = cat.join(pipel.r2x.get_store()['r2eff'])
    cat['c0'] = cat.mag_r - cat.mag_i
    cat['c0_nl'] = cat.mag_r_nl - cat.mag_i_nl
    cat = cat[cat.mag_vis < 24.5]

    z = np.linspace(0.1, 1.8, 7)
    K = {'histtype': 'step', 'normed': True}
    for key,val in cat.groupby(pd.cut(cat.zs, z)):
    #    val.c0.hist(bins=40, label='z: {0}'.format(key), **K)
        val.c0_nl.hist(bins=40, label='z: {0}'.format(key), **K)

    plt.xlabel('r-i', size=16)
    plt.legend()
    plt.show()


def plot3():
    pipel.r2x.config.update({
      'Ntrain': 2000,
      'filters': ['r', 'i'],
      'use_stars': False})

    pipel.r2x.test_cat.config['r'] = 1000000. #    top.add_layer_key('r2x.test_cat.r', [1., 1000000.])
    cat = pipel.r2x.test_cat.result
    cat = cat.join(pipel.r2x.test_cat.magcat.result, rsuffix='_nl')
    cat = cat.join(pipel.r2x.get_store()['r2true'])
    cat['c0'] = cat.mag_r - cat.mag_i
    cat['c0_nl'] = cat.mag_r_nl - cat.mag_i_nl
    cat = cat[cat.mag_vis < 24.5]

    z = np.linspace(0.1, 1.8, 7)
    for i,(key,val) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
        plt.plot(val.c0_nl, val[0]+i*5, '.', label=key)

    #plt.plot(cat
    plt.legend()
    plt.show()
    #ipdb.set_trace()

def plot4():
    cat = pipel.r2x.test_cat.result
    cat = cat.join(pipel.r2x.test_cat.magcat.result, rsuffix='_nl')
    cat = cat.join(pipel.r2x.get_store()['r2true'])
    cat['c0'] = cat.mag_r - cat.mag_i
    cat['c0_nl'] = cat.mag_r_nl - cat.mag_i_nl
    cat = cat[cat.mag_vis < 24.5]

    z = np.linspace(0.1, 1.8, 7)
    K = {'histtype': 'step', 'normed': False}# True}
    for key,val in cat.groupby(pd.cut(cat.zs, z)):
    #    val.c0.hist(bins=40, label='z: {0}'.format(key), **K)
        val[0].hist(bins=40, label='z: {0}'.format(key), **K)
        
    plt.xlabel('r-i', size=16)
    plt.legend()
    plt.show()

def plot5():
    cat = pipel.r2x.test_cat.result
    cat = cat.join(pipel.r2x.test_cat.magcat.result, rsuffix='_nl')
    cat = cat.join(pipel.r2x.get_store()['r2true'])
    cat['c0'] = cat.mag_r - cat.mag_i
    cat['c0_nl'] = cat.mag_r_nl - cat.mag_i_nl
    cat = cat[cat.mag_vis < 24.5]

    z = np.linspace(0.1, 1.8, 7)
    for i,(key, val) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
        plt.clf()
        val.plot(kind='hexbin', x='c0', y=0, legend=key)
        plt.title(key, size=16)

        plt.savefig('../run/r188_x50_{0}.pdf'.format(i))
