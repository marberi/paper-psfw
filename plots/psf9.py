#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job
sys.path.append('/home/marberi/xdolphin/xdolphin/lang')

import bootstrap
import trainpipel

#plt.rcParams['font.family'] = ['roman']
#plt.rcParams["font.family"] = "Times New Roman"

class Plot:
    """Redshift dependence for different photo-z R^2 estimations."""

    name = 'psf9'
    def pipel(self):
        pipel = trainpipel.pipel(toplevel='r2pz')
        pipel.config.update({\
#          'r2pz.use_ml': False,

          'r2pz.pzcat.out_pdf': True,
          'r2pz.pzcat.out_pdftype': True})

        # Mostly because of being tired of waiting for the
        # results.
#        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 30000
        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 100000

        return pipel

    def f_reduce(self, pipel):

        J = pipel.r2pz
        r2eff = J.r2gal.result
        r2pz = J.result
        galcat = J.pzcat.galcat.result
        mabs = J.pzcat.galcat.magcat.mabs.result
        pzcat = J.pzcat.result.unstack()

        df = pd.concat([r2eff,r2pz,galcat,mabs,pzcat], axis=1)
        df = df[df.mag_vis < 24.5]

        zs = df['zs'].icol(0)
        del df['zs']
        df['zs'] = zs

        df['px'] = (df.zb - zs) / (1+zs)

        # Starting to spliit
        sedl = [0,17,55,65]
        S = pd.cut(df.sed, sedl, labels=['ell', 'sp', 'irr'])

        nbins = 30 
        x = np.linspace(0.1, 2.0, nbins)

        res = {}
        for key, val in df.groupby(S):
            gr = pd.cut(val.zs, x)
            zs_mean = val.zs.groupby(gr).mean()
            mean_peak, std_peak = bootstrap.bootstrap(val.r2peak, gr)
            mean_pdf, std_pdf = bootstrap.bootstrap(val.r2pdf, gr)

            D = {'mean_peak': mean_peak, 'std_peak': std_peak,
                 'mean_pdf': mean_pdf, 'std_pdf': std_pdf,
                 'zs_mean': zs_mean}

            res[key] = pd.concat(D)

        return pd.concat(res)
 
    def plot(self, prepare):
        prepare = prepare.ix[0]

        gal_type = ['ell', 'sp', 'irr']
        lsD = {'ell': '-', 'sp': '--', 'irr': '-.'} # Not used..
        for gtype in gal_type:
            sub = prepare.ix[gtype]

            # This could probably be improved.
            zs_mean = sub.ix['zs_mean'][0]
            mean_peak = sub.ix['mean_peak'][0]
            std_peak = sub.ix['std_peak'][0]
            mean_pdf = sub.ix['mean_pdf'][0]
            std_pdf = sub.ix['std_pdf'][0]

            lbl1 = gtype.capitalize()
            ls = lsD[gtype]
            plt.errorbar(zs_mean, mean_peak, std_peak, label=lbl1+', Peak', ls=ls)
            plt.errorbar(zs_mean, mean_pdf, std_pdf, label=lbl1+', Pdf(z)', ls=ls)

        plt.xlabel('$z_{\mathrm{s}}$', size=14)
        plt.ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)

        # Shade the requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')
        plt.xlim(0.1, 2.0)
        plt.ylim(-0.008, 0.008)
        plt.legend(loc=4)
        plt.subplots_adjust(left = 0.14, bottom=0.12)

if __name__ == '__main__':
    xd.plot(Plot).save()
