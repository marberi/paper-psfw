#!/usr/bin/env python
# encoding: UTF8

import copy
import ipdb
import pandas as pd
from matplotlib import pyplot as plt

import psf13
from xdolphin import Job

class psf14(psf13.psf13):
    """Varying the SN in the star sample."""

    config = copy.deepcopy(psf13.psf13.config)
    config['myconf']['galnoise'] =  {'const_sn': False}
    config['myconf']['starnoise'] = {'const_sn': True}

    config['myconf']['r2train']['Ntrain'] = 400
    config['myconf']['r2train']['Nboot'] = 1000
    config['xkey'] = 'starnoise.sn'
    config['xvals'] = [1, 5, 8, 10, 12, 15, 20, 30, 40, 100]

#    ipdb.set_trace()

    def mylines(self):
        # One line for each of the different filter sets?


        noisy = {'galnoise.const_sn': False,
                 'starnoise.const_sn': False,
                 'starnoise.sn': 100}

        def hmm(X, val,label, isnoisy):
            """Merging different values"""
            myconf = copy.deepcopy(X)
            myconf['r2train.use_stars'] = val
            myconf['_label'] = label
            myconf['_isnoisy'] = isnoisy

            return myconf


        myconf = hmm(noisy, True, 'Stars, noisy', True)
        myconf['r2train.filters'] = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']

        lines = [myconf]

        return lines #[2:]

    def run(self):
        topnode, lines = self.create_topnode()

        # Not too changed from psf13....
        tbl_data = pd.DataFrame()
        for ids, job in topnode.kids.iteritems():
            D = {}
            D['use_stars'] = job.config['use_stars']

            D['sn'] = job.kids['starcat'].kids['magcat'].config['sn']

#            ipdb.set_trace()
#            D['SN'] = job.config['Ntrain']

            this_line = lines[ids[0][0]]
            D['isnoisy'] = this_line['_isnoisy']
            D['lbl'] = this_line['_label']

            D['line_nr'] = ids[0][0]
            D['xval_nr'] = ids[0][1]
            X = job['r2train']['r2eff']
            D.update(X.mean().abs().describe(percentiles=[0.68,.95]))

            tbl_data = tbl_data.append(pd.Series(D), ignore_index=True)

        for key, val in tbl_data.groupby('line_nr'):
            val = val.sort('xval_nr')

            lbl = val.lbl.irow(0)
            plt.plot(val.sn, val['mean'], 'o-', label=lbl)

        plt.xlabel('SN for the stars', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.xscale('log')
        plt.yscale('log')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

#        plt.title('Number of objects in the training sample', size=16)
        plt.legend()
        plt.show()

if __name__ == '__main__':
    Job('psf14').run()

