#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

class psf61:
    name = 'psf61'
    config = {}

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def d1(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')

        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
        reg = svm.NuSVR()
        reg.fit(Astar[inds], r2star[inds,0])

        delta = np.linspace(-0.2, 0.1, 20)

        for i,di in enumerate(list(delta)):
            print('predict', i)
            col_tmp = col_gal.copy()

            col_tmp[:,0] += di


            r2pred = reg.predict(col_tmp)
            cat['r2eff'+str(i)] = (r2pred - r2true) /r2true

            #ipdb.set_trace()

        r2pred = reg.predict(col_gal)
        cat['r2eff'] = (r2pred - r2true) /r2true

        z = np.linspace(0.1, 2.0, 15)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        keys = ['r2eff'+str(i) for i in range(delta.shape[0])]
        A = mean[keys].values
        delta_arg = np.abs(A).argmin(axis=1)

        # The optimal r-band offset.
        if True: #False:
            plt.plot(mean.zs, delta[delta_arg], 'o-')
            plt.xlabel('z', size=16)
            plt.ylabel('$\Delta r$', size=16)

            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            plt.show()

        plt.plot(mean.zs, A[range(A.shape[0]),delta_arg], label='Corrected')
        plt.plot(mean.zs, mean.r2eff, label='Normal')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.legend()
        plt.show()

#        k2 = 

        ipdb.set_trace()

        # The bias for the different corrections.
        if False:
            for i in range(delta.shape[0]):
                plt.plot(mean.zs, mean['r2eff'+str(i)], 'o-', label=str(delta[i]))

            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            plt.legend()
            plt.show()

#        ipdb.set_trace()

    def d2(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B[key]['galnoise'].depend['magcat'].depend['mabs']['mabs']['cat'][['sed']])


#        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')

        fL = ['r', 'i', 'z']

        Ntrain = 2000 #400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
#        reg = svm.NuSVR()
#        reg.fit(Astar[inds], r2star[inds,0])

        reg = neighbors.KNeighborsRegressor(n_neighbors=1)
        reg.fit(Astar[inds], r2star[inds,0])

        r2pred = reg.predict(col_gal)
        cat['r2pred'] = r2pred
        cat['r2eff'] = (r2pred - r2true) / r2true

        if False:
            sedx = np.arange(66)
            mean = cat.groupby(pd.cut(cat.sed, sedx)).mean()
    
            plt.plot(mean.sed, mean.r2eff, 'o-')
    
            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')
    
            plt.show()

        # Looking at which star that is closest. Using the index from the
        # full catalog.
        neigh_ind = inds[reg.kneighbors(col_gal)[1][:,0]]
        col_neigh = Astar[neigh_ind]

        delta_col = col_neigh - col_gal

        cat['c1'] = delta_col[:,0]
        cat['c2'] = delta_col[:,1]


        sedx = np.arange(66)
        mean = cat.groupby(pd.cut(cat.sed, sedx)).mean()
    
#        plt.plot(mean.sed, mean.r2eff, 'o-')
        plt.plot(mean.sed, mean.c1, 'o-', label='r-i')
        plt.plot(mean.sed, mean.c2, 'o-', label='i-z')
    
        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')
  
        plt.xlabel('SED number', size=16) 
        plt.ylabel('color(Star) - color(Gal)', size=16)

        plt.legend() 
        plt.show()


#        ipdb.set_trace()



    def run(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 50000


            B[lbl] = A


        self.d2(B)

if __name__ == '__main__':
    Job('psf61').run()
