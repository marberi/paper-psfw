#!/usr/bin/env python
# encoding: UTF8

import xdolphin as xd

def gen_star_pipel(photoz_data, filter_dir):
    """Generate the pipeline for the star catalogs."""

#    photoz_data = '/data2/photoz/run/data/'
#    filter_dir = '/data2/photoz/run/data/des_euclid2'

    abkids = xd.Job('ab')
    abstar = xd.Job('ab')
    abkids.task.config.update({'data_dir': photoz_data,
                               'filter_dir': 'kids_filters',
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})


    abstar.task.config.update({'data_dir': photoz_data,
                               'filter_dir': filter_dir,
                               'zmax': 1.,
                               'sed_dir': 'seds_stars'})

    j1 = xd.Job('kidscat')


    j2 = xd.Job('starsel')
    j2.depend['catalog'] = j1

    j3 = xd.Job('starfit')
    j3.depend['catalog'] = j2
    j3.depend['ab'] = abkids

    j4 = xd.Job('starabs')
    j4.depend['starcat'] = j2
    j4.depend['starfit'] = j3


    j5 = xd.Job('starobs')
    j5.depend['starcat'] = j4
    j5.depend['ab'] = abstar

    starcatN = j5

    # R2 stars
    r2vz_star = xd.Job('r2vz')
    r2vz_star.config['filter_dir'] = filter_dir
    r2vz_star.config['data_dir'] = photoz_data
    r2vz_star.config['obj_type'] = 'star'

    r2star = xd.Job('r2starx')
    r2star.depend['magcat'] = starcatN
    r2star.depend['r2vz'] = r2vz_star

    star_noise = xd.Job('magnoise')
    star_noise.depend['magcat'] = starcatN

    starcat = xd.Job('offset')
    starcat.depend['magcat'] = star_noise

    return starcat, r2star

def replace_job(pipel, loc, new_task, copy_conf=False, copy_dep=False):
    """General code for replacing tasks."""

    # This type of functionality could be added directly to the
    # xdolphin job.
    to_replace = pipel.depend[loc]
    # Copying the configuration items.
    if copy_conf:
        for key, val in to_replace.config.task_config.iteritems():
            new_task.config[key] = val

    # Add the dependencies
    if copy_dep:
        for name, j in to_replace.depend.iteritems():
            new_task.depend[name] = j

    # This functionality should be in xdolphin. I temporarily
    # implement this here.
    if not '.' in loc:
        pipel.depend[loc] = new_task
    else:
        spl = loc.split('.')
        part = pipel
        for x in spl[:-1]:
            part = part.depend[x]

        part.depend[spl[-1]] = new_task

def add_pipel(pipel, task_name, photoz_data, filter_dir):
    """Add the new star pipeline."""

    starcat, r2star = gen_star_pipel(photoz_data, filter_dir)

    replace_job(pipel, 'r2x', xd.Job(task_name), copy_conf=True, copy_dep=True)
    replace_job(pipel, 'r2x.train_star_r2', r2star)
    replace_job(pipel, 'r2x.train_star_cat', starcat)
