#!/usr/bin/env python

# Effect of using an observed star distribution.
import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import trainpipel

pipel = trainpipel.pipel(star_fit=True)

job = pipel.r2train.train_star_cat.magcat.magcat.sed_prob

job.config['fieldnr'] = '129p0_m0p5'
A = job.result


ipdb.set_trace()
