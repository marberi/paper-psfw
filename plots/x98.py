#!/usr/bin/env python

# Testing the json dump.

import ipdb
import json
import os
import trainpipel

import pandas as pd

import xdolphin as xd

pipel = trainpipel.pipel()
d = '/tmp/mydump'

#layers = xd.Layers()
#layers.add_pipeline(pipel)
#ipdb.set_trace()
#pipel.save(d)

#pipel.save(d)

if False:
    taskid = pipel.taskid()
    task_dir = os.path.join(d, taskid)

    job = xd.load_pipel(task_dir, taskid)

def f_reduce(pipel):
    df = pd.DataFrame()

    print('running reduce')

    return df

def test():
    session = xd.create_session()

    layers = xd.Layers()
    layers.add_pipeline(pipel)

    layers.add_layer_key('mag_lim', 'r2x.lim_vis', [22.5, 23., 24.])

    top = layers.to_topnode()
    top.session = session

    res = top.reduce(f_reduce)


import psf6
psf6.Plot().run()

#ipdb.set_trace()
