#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import xdolphin as xd
from matplotlib import pyplot as plt

class Plot:
    name = 'psf82'

    def pipel(self):
        import trainpipel

        pipel = trainpipel.pipel()
        pipel.config.update({
          'r2x.reg': 'kneigh',
          'r2x.use_cal': False,
          'r2x.filters': ['r', 'i', 'z'],
        #  'r2x.filters': ['r', 'i'],
          'r2x.all_test': True,
          'r2x.Ntrain': 400,
          'r2x.Nboot': 100,
          'r2x.use_stars': True})

        return pipel

    def load(self, pipel):
        cat = pipel.r2x.get_store()['r2eff']
        cat = cat.join(pipel.r2x.test_cat.result)
        cat = cat[cat.mag_vis < 24.5]
        cat = cat.join(pipel.r2x.test_pz.result.unstack(), rsuffix='A')

        data = {'cat': cat, 'Nboot': pipel.config['r2x.Nboot']}

        return data

    def plot(self, data):

        xa = 0.4
        xb = xa+0.1

        cat = data['cat']
        sub1 = cat[(xa < cat.zs) & (cat.zs < xb)]
        sub2 = cat[(xa < cat.zb) & (cat.zb < xb)]

        K = {'histtype': 'step', 'normed': True, 'bins': 50}
        c1 = 'black'
        c2 = 'blue'
        ls1 = '-'
        ls2 = '--'
#        sub1[0].hist(label='Spec-z', color=c1, ls='solid', **K)
#        sub2[0].hist(label='Photo-z', color=c2, ls='dashed', **K)

        Nboot = data['Nboot'] #pipel.config['r2x.Nboot']
        sub1[range(Nboot)].unstack().hist(label='Spec-z', color=c1, ls='solid', **K)
        sub2[range(Nboot)].unstack().hist(label='Photo-z', color=c2, ls='dashed', **K)
#        ipdb.set_trace()

        plt.axvline(sub1[0].mean(), color=c1, ls=ls1)
        plt.axvline(sub2[0].mean(), color=c2, ls=ls2)

        plt.title('Redshift: [{0:.1f}, {1:.1f}]'.format(xa, xb), size=14)
        plt.legend()

        xlim = 0.03
        plt.xlim((-xlim, xlim))
        plt.ylim((1e-3, 5e2))
        plt.yscale('log')

        plt.xlabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=13)
        plt.ylabel('Probability', size=14)

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, -3e-4, 3e-4, color='black', alpha=0.1)

if __name__ == '__main__':
    xd.plot(Plot).save()
