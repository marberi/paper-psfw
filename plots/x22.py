#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import xdolphin as xd
from matplotlib import pyplot as plt

class Plot:
    def topnode(self):
        import trainpipel
        top = xd.Topnode(trainpipel.pipel())

        f_riz = ['r', 'i', 'z']
        f_all = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']
        top.add_layer_key('r2sed.test_cat.r', [1, 10])
        top.add_layer_key('r2sed.filters', [f_riz, f_all]) #, f_ri])

        return top

    def run(self):
#        topnode = self.topnode()

        import trainpipel
        pipel = trainpipel.pipel()
        pipel.config.update({\
          'r2sed.test_cat.r': 1,
          'r2sed.filters': ['r', 'i', 'z']})



        cat = pipel.r2sed.result
        cat = cat.join(pipel.r2sed.test_cat.result)

        cat = cat[cat.mag_vis < 24.5]

        import psf67
        inst = psf67.Plot()
        D2,cat2 = inst.get_cat(xd.Topnode(pipel))
#        cat.r2eff.hist(bins=100)
#        plt.show()

#        ipdb.set_trace()

        z = np.linspace(0.1, 2.0, 10)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()
        mean2 = cat.groupby(pd.cut(cat2.zs, z)).mean()

        ipdb.set_trace() 

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.ylim((-0.002, 0.002))
        plt.plot(mean.zs, mean.r2eff, 'o-')
        plt.show()



Plot().run()
