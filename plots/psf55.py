#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

class psf55:
    name = 'psf55'
    config = {}

    def d1(self, B):

        noisy = B['noisy']['galnoise']['magcat']['cat']

        nl = B['noiseless']['galnoise']['magcat']['cat']

        #noisy.mag_r - noisy.mag_i).hist(bins=200, label='noisy')
        #(nl.mag_r - nl.mag_i).hist(bins=200, label='noiseless')

        if False:
            noisy.mag_r.hist(bins=200, histtype='step', normed=True, label='noisy')
            nl.mag_r.hist(bins=200, histtype='step', normed=True, label='noiseless')

            plt.legend()
            plt.show()

        # Plotting the magnitude distributions.
        Hnoisy, xedges = np.histogram(noisy.mag_r, bins=200)
        Hnl, _ = np.histogram(nl.mag_r, bins=xedges)

        xm = 0.5*(xedges[:-1] + xedges[1:])

        if False:
            plt.plot(xm, Hnoisy, label='Noisy')
            plt.plot(xm, Hnl, label='Noiseless')

            plt.legend()
            plt.show()

        for key,val in noisy.groupby(pd.cut(nl.mag_r, xedges)):
            ipdb.set_trace()

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def d2(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
        reg1 = svm.NuSVR()
        reg1.fit(Astar[inds], r2star[inds,0])
        r2pred1 = reg1.predict(col_gal)

        reg2 = neighbors.KNeighborsRegressor(n_neighbors=10) #, weights='distance')
        reg2.fit(Astar[inds], r2star[inds,0])
        r2pred2 = reg2.predict(col_gal)

        r2eff1 = (r2pred1 - r2true) / r2true
        r2eff2 = (r2pred2 - r2true) / r2true

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        # Scatter plot.
        if False:
            xline = np.linspace(np.min(r2eff1), np.max(r2eff2))
            plt.plot(xline, xline, '--', lw=2)
            plt.plot(r2eff1, r2eff2, '.')
            plt.show()

        cat['r2eff1'] = r2eff1
        cat['r2eff2'] = r2eff2
        cat['r2eff3'] = 0.5*(r2eff1 + r2eff2)

        # The normal histogram.
        if False:
            cat = cat[cat.mag_vis < 24.5]
    #        cat = cat[ca
            K = {'bins': 200, 'histtype': 'step', 'normed': True}     
            cat.r2eff1.hist(label='SVR', **K)
            cat.r2eff2.hist(label='Neigh', **K)
            cat.r2eff3.hist(label='Combined', **K)

            plt.legend()
            plt.show()



        # Trying to use the nearest neighbors approach to remove the 
        # redshift dependence.
        if False:
            cat = cat[cat.mag_vis < 24.5]
            z = np.linspace(0.1, 2.0, 10)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            plt.plot(mean.zs, mean.r2eff1, label='SVR')
            plt.plot(mean.zs, mean.r2eff2, label='Neigh')
            plt.plot(mean.zs, mean.r2eff3, label='Combined')

            plt.legend()
            plt.show()

        cat['r2pred1'] = r2pred1
        cat['r2pred2'] = r2pred2
        cat['K'] = (cat.r2pred2 - cat.r2pred1) / cat.r2pred1
        cat = cat[cat.mag_vis < 24.5]
        z = np.linspace(0.1, 2.0, 10)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        plt.plot(mean.zs, mean.K, 'o-')
        plt.show()

#        ipdb.set_trace()

    def d3(self, B):
#        key = 'noisy'
        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
        reg = svm.NuSVR()
        reg.fit(Astar[inds], r2star[inds,0])
        r2pred = reg.predict(col_gal)


        if False:
            for x in [-0.1, 0, 0.1]:
                col_tmp = col_gal.copy()
                col_tmp[:,1] += x

                r2pred = reg.predict(col_tmp)
                r2eff = pd.Series((r2pred - r2true) / r2true)

                r2eff.hist(bins=40, histtype='step', normed=True, label=str(x))

            plt.legend()
            plt.show()

    
        # Seeing the effect on the redshift dependence.
        if False:
            xL = [-0.1, 0, 0.1]
            for i,x in enumerate(xL):
                col_tmp = col_gal.copy()
                col_tmp[:,1] += x

                r2pred = reg.predict(col_tmp)
                r2eff = pd.Series((r2pred - r2true) / r2true)

                cat['l'+str(i)] = pd.Series((r2pred - r2true) / r2true)
                
    #            r2eff.hist(bins=40, histtype='step', normed=True, label=str(x))

            z = np.linspace(0.1, 2.0, 10)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            for i in range(len(xL)):   
                plt.plot(mean.zs, mean['l'+str(i)], 'o-', label=str(xL[i]))

    #            ipdb.set_trace()
     
            plt.legend()
            plt.show()


#        r2pred = reg.predict(col_gal)
#        cat['r2pred'] = r2pred
        cat['r2true'] = r2true
        cat['col0'] = col_gal[:,0]
        cat['col1'] = col_gal[:,1]

        cat = cat[cat.mag_vis < 24.5]
        for i,lbl in enumerate(['r-i', 'i-z'][:-1]):
#            ipdb.set_trace()

            plt.plot(cat.col0, cat.r2true, '.')
            plt.plot(Astar[:,i], r2star[:,0], '.', label='Star')

        plt.show()

#            ipdb.set_trace()
#            plt.show()

    def d4(self, B):
        key = 'noisy'
#        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal


        starcat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(starcat, fL)
#        ipdb.set_trace()
        starcat_nl = B[key]['r2train'].depend['starcat'].depend['magcat'].depend['magcat']['magcat']['magcat']
        Astar_nl, _ = self.get_col(starcat_nl, fL)

        if False:
            plt.plot(Astar[inds,0], r2star[inds], '.', label='Normal')
            plt.plot(Astar_nl[inds,0], r2star[inds], '.', label='Noiseless')

            plt.legend()
            plt.show()


            ipdb.set_trace()

        from sklearn import svm, neighbors
        for i,(lbl,Afit) in enumerate([('Noisy star', Astar), ('Noiseless star', Astar_nl)]):
            reg = svm.NuSVR()
            reg.fit(Afit[inds], r2star[inds,0])

            r2pred = reg.predict(col_gal)

            cat['l'+str(i)] = (r2pred-r2true)/ r2true

        z = np.linspace(0.1, 2., 10)
#        for key,val in 
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        plt.plot(mean.zs, mean.l0, 'o-', label='Noiseless stars')
        plt.plot(mean.zs, mean.l1, 'o-', label='Noisy stars')

        plt.legend()
        plt.show()

        ipdb.set_trace()

    def d5(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal


#        plt.plot(col_gal[:,0], r2true, '.')

        cat['col0'] = col_gal[:,0]
        cat['col1'] = col_gal[:,1]

#        plt.plot(col_gal[:,0], col_gal[:,1], '.')
        for za,zb in [(0.1,0.12),(0.3,0.32), (0.5,0.52), (0.6, 0.62), (1.,1.02), (1.3, 1.32), (1.5, 1.52)]:
            sub = cat[(za < cat.zs) & (cat.zs < zb)]
       

            plt.plot(sub.col0, sub.col1, '.', label=str(za)) 

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('r-i', size=16)
        plt.ylabel('i-z', size=16)

        plt.legend()
        plt.show()


    def d6(self, B):
        key = 'noisy'
#        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['g', 'r', 'i', 'z', 'vis']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal


        from sklearn import svm, neighbors
        reg = svm.NuSVR()

        if False:
            S = []
            S.append(['r','i','z'])
    #        S.append(['r','i'])
    #        S.append(['r','i', 'vis'])
    #        S.append(['g', 'r','i', 'z', 'vis'])


            for i,fL in enumerate(S):
                Astar, _ = self.get_col(star_cat, fL)
                col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)

                reg.fit(Astar[inds], r2star[inds,0])
                r2pred = reg.predict(col_gal)
                cat['r2eff'+str(i)] = (r2pred - r2true) #/ r2true

            z = np.linspace(0.1, 2., 15)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            for i,fL in enumerate(S):
                lbl = ','.join(fL)
                plt.plot(mean.zs, mean['r2eff'+str(i)], 'o-', label=lbl)

            plt.legend()
            plt.show()


        fL = ['r', 'i', 'z']

        Astar, _ = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)

        reg.fit(Astar[inds], r2star[inds,0])
        r2pred = reg.predict(col_gal)
#                cat['r2eff'+str(i)] = (r2pred - r2true) #/ r2true

        if False:
            cat['r2true'] = r2true
            cat['r2eff'] = (r2pred - r2true) / r2true
    #        cat['r2diff'] = (r2pred - r2true) 


    #        cat = cat[(1.0 < cat.zs) & (cat.zs < 1.02)]
            zL = [0.2, 0.5, 1.0, 1.5]

            width = 0.1
            for z in zL:
                za = z - 0.5*width
                zb = z + 0.5*width
                sub = cat[(za < cat.zs) & (cat.zs < zb)]

                plt.plot(sub.r2true, sub.r2eff, '.', label=str(z))

            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')
            plt.legend()
            plt.show()


        cat['r2true'] = r2true
        cat['r2eff'] = (r2pred - r2true) / r2true

        r2mean = cat.r2true.mean()
        z = np.linspace(0.1, 2., 10)
        cat1 = cat[cat.r2true < r2mean]
        cat2 = cat[cat.r2true > r2mean]

        for lbl, C in [('All', cat), ('Low $"R^2_{\\rm{PSF}}"$', cat1), ('High $"R^2_{\\rm{PSF}}"$', cat2)]:
            mean = C.groupby(pd.cut(C.zs, z)).mean()

            plt.plot(mean.zs, mean.r2eff, 'o-', label=lbl)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('z', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ rel. bias', size=18)
        plt.legend(loc=0)
        plt.show()

#        cat.r2true.hist()
#        plt.show()


#        ipdb.set_trace()

    def d7(self, B):
        key = 'noisy'
#        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['g', 'r', 'i', 'z', 'vis']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors
        reg = svm.NuSVR()

        fL = ['r', 'i', 'z']

        Astar, mag_star = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)

        flux_star = 10**(-0.4*mag_star)

#        Atr = Astar[inds]
        Btr = r2star[inds, 0]

#        reg.fit(Atr, Btr)
        N = 200 #50 #000
        ind_i = np.random.randint(0, len(inds), N)
        ind_j = np.random.randint(0, len(inds), N)

#        ipdb.set_trace()

        print('Duplicates:', (ind_i == ind_j).sum())

        per = 15
        Bval = pd.Series(0.5*(Btr[ind_i] + Btr[ind_i]))
        cut_val = Bval.describe(percentiles=[per/100.])[str(per)+'%']

        is_small = Bval < cut_val
#        ipdb.set_trace()
#        is_small = 0.5*(Btr[ind_i] + Btr[ind_i]) < 0.9*Btr.mean()
        if False:
            ind_i = ind_i[is_small]
            ind_j = ind_j[is_small]


        rr = np.arange(len(inds))
        ind_i = np.hstack([rr, ind_i])
        ind_j = np.hstack([rr, ind_j])

        reg2 = svm.NuSVR()


        ipdb.set_trace()

#        Ax = 0.5*(Atr[ind_i]+Atr[ind_j])
#        Bx = 0.5*(Btr[ind_i]+Btr[ind_j])

        # Experimenting with cutting away some of the stars.
#        to_use = Btr < (0.995)*r2true.max()
#        Ax = Atr[to_use]
#        Bx = Btr[to_use]
    
#        ipdb.set_trace()


        reg2.fit(Ax, Bx)
        r2pred_x = reg2.predict(col_gal)




#        ind_i = inds[i]
#        ind_j = inds[j]

#        reg2.fit(Atr[i_ind], Btr[j
        S_x = pd.Series((r2pred_x - r2true) / r2true)

        reg.fit(Atr, Btr)
        r2pred = reg.predict(col_gal)
        S = pd.Series((r2pred - r2true)/r2true)

        # The ordinary histogram.
        if False:
            K = {'bins': 40, 'histtype': 'step', 'normed': True}
            S.hist(label='Orig', **K)
            S_x.hist(label='Hmm', **K) #Orig')
            plt.legend()
            plt.show()

        cat['r2eff'] = S
        cat['r2eff_x'] = S_x

        if True: #False: #True: #False: #False:
            z = np.linspace(0.1, 2., 10)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            plt.plot(mean.zs, mean.r2eff, 'o-', label='Normal')
            plt.plot(mean.zs, mean.r2eff_x, 'o-', label='Corrected')

            plt.legend()
            plt.show()

        cat['r2true'] = r2true
        if False:
            K = {'histtype': 'step', 'normed': True}
            pd.Series(Bx).hist(bins=50, label='Comb', **K)
            pd.Series(Btr).hist(bins=20, label='Normal', **K)
            cat.r2true.hist(bins=50, label='Galaxies', **K)

            plt.legend()
            plt.show()

#        pd.Series( 
#        ipdb.set_trace()


#Astar[inds], r2star[inds,0])


    def d8(self, B):
        key = 'noisy'
        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z', 'vis']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal


        fL = ['r', 'i'] #, 'z']

        Astar, mag_star = self.get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)

        flux_star = 10**(-0.4*mag_star)

#        Atr = Astar[inds]

#        reg.fit(Atr, Btr)
        N = 200 #50 #000
        ind_i = np.random.randint(0, len(inds), N)
        ind_j = np.random.randint(0, len(inds), N)

        print('Duplicates', (ind_i == ind_j).sum())
        if False:
            to_use = np.abs(Astar[ind_i, 0] - Astar[ind_j,0]) < 0.1
            ind_i = ind_i[to_use]
            ind_j = ind_j[to_use]

#        ipdb.set_trace()

        rr = np.arange(len(inds))
        ind_i = np.hstack([rr, ind_i])
        ind_j = np.hstack([rr, ind_j])

        flux_comb = 0.5*(flux_star[inds[ind_i]] + flux_star[inds[ind_j]])
        mag_comb = -2.5*np.log10(flux_comb)

        # Test that the magnitudes convert back to the original
        # values.
        assert np.allclose(mag_comb[:Ntrain], mag_star[inds])

        Ax = mag_comb[:,:-1] - mag_comb[:,1:]
        Btr = r2star[inds, 0]
        Bx = pd.Series(0.5*(Btr[ind_i] + Btr[ind_i]))

        Atr = Astar[inds]

        from sklearn import svm, neighbors
        if True: #False:
            reg = svm.NuSVR()
            regx = svm.NuSVR()
        else:
            reg = neighbors.KNeighborsRegressor(n_neighbors=1)
            regx = neighbors.KNeighborsRegressor(n_neighbors=1)

#        ipdb.set_trace()

        reg.fit(Astar[inds], r2star[inds,0])
        r2pred = reg.predict(col_gal) #Atr, Btr)
        cat['r2eff'] = (r2pred - r2true)/r2true

        regx.fit(Ax, Bx)

        r2predx = regx.predict(col_gal)

        cat['r2effx'] = (r2predx - r2true)/r2true

        z = np.linspace(0.1, 2.0, 15)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        plt.plot(mean.zs, mean.r2eff, 'o-', label='Orig')
        plt.plot(mean.zs, mean.r2effx, 'o-', label='Linear combination')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.legend()
        plt.show()

#        Ax 
#        ipdb.set_trace()



    def run(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 50000


            B[lbl] = A


        self.d8(B)

if __name__ == '__main__':
    Job('psf55').run()
