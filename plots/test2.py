#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from xdolphin import Job
import xdolphin as xd

import trainpipel

def topnode_from_empty(job):
    topnode = xd.Topnode()

    for key,val in job.depend.iteritems():
        topnode.depend[key] = val

    return topnode

pipel = trainpipel.pipel()
topnode = topnode_from_empty(pipel)
topnode.config.update({'r2train': {'use_stars': True}})
topnode.depend.add_layer_key('r2train.use_stars', [False, True])



#print(topnode.depend._data)
ipdb.set_trace()

topnode.depend['fisk'] = pipel
