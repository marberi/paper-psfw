#!/usr/bin/env python
# encoding: UTF8

# Simple implementation of bootstrap functionality.
import numpy as np
import pandas as pd

def bootstrap(S, gr):
    """Straight forward fuction for estimating the bootstrap error
       of the mean.
    """

    # Unfortunately this fuctionality was not directly implemented
    # in Pandas.
    Nboot = 100

    mean = S.groupby(gr).mean()
    std = pd.Series()
    for key, val in S.groupby(gr):
        if not len(val):
            std[key] = np.nan
            continue

        # This part is not exactly elegant, but should be straight
        # forward.
        r = np.random.randint(0, len(val), size=(Nboot, len(val)))
        tmp = pd.Series()
        for i,row in enumerate(r):
            mean_sample = val.ix[val.index.values[row]].mean()
            tmp[str(i)] = mean_sample

        std[key] = np.sqrt(tmp.var())

    return mean, std
