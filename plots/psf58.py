#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
from matplotlib import pyplot as plt
from xdolphin import Job

class psf58:
    name = 'psf58'

    config = {}

    def run(self):
        import trainpipel

        pipel = trainpipel.pipel()

        pipel.depend['r2train'].run()

        ipdb.set_trace()

if __name__ == '__main__':
    Job('psf58').run()
