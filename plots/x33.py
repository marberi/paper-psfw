#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

def add_r2cal(pipel):
    r2cal = xd.Job('r2cal')
    r2cal.depend['r2train'] = pipel.r2train

    cal = pipel.r2train.clone()
    cal.depend['galcat'] = cal.train_gal_cat
    cal.depend['r2gal'] = cal.train_gal_r2

    r2cal.depend['cal'] = cal

    pipel.depend['r2cal'] = r2cal

    return pipel

def topnode():
    pipel = trainpipel.pipel()
    pipel.config.update({\
      'r2train.all_test': True,
      'r2train.use_stars': True,
      'r2train.Ntrain': 400,
      'r2train.filters': ['r','i','z']}) #400,

    pipel = add_r2cal(pipel)

    return pipel

def get_col(cat, filtersL, errors=False):
    mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
    col = mag[:,:-1] - mag[:,1:]

    if errors:
        err = cat[['err_{0}'.format(x) for x in filtersL]].values
        return col, mag, err
    else:
        return col, mag

def add_colors(cat):
    filters = ['r', 'i', 'z']
    new_cols = map('c{0}'.format, range(len(filters)-1))

    fmt = 'mag_{0}'.format
    for col, f1, f2 in zip(new_cols, filters[:-1], filters[1:]):
        cat[col] = cat[fmt(f1)] - cat[fmt(f2)]

# Here we start the normal code.
pipel = topnode()

star_cat = pipel.r2train.train_star_cat.result
star_cat = star_cat.join(pipel.r2train.train_star_r2.result)
gc = pipel.r2train.galcat.result.join(pipel.r2train.r2gal.result)
gc['r2true'] = gc.r2eff

Ntrain = 400
fL = ['r', 'i', 'z']

Astar, _ = get_col(star_cat, fL)
col_gal, mag_gal, err_gal = get_col(gc, fL, True)

inds = np.random.permutation(range(len(Astar)))[:Ntrain]

Atrain = Astar[inds]
Btrain = star_cat.r2eff.values[inds]
from sklearn import svm

# A galaxy mock for calibrating..
#pipel.
mock = pipel.r2cal.cal.galcat.result
mock = mock.join(pipel.r2cal.cal.r2gal.result)
mock['r2true'] = mock['r2eff']

cal_cat = mock
test_cat = gc

class Calibration_test:
    config = {
      'coff_start': 0.05
    }

    def next_calc_step(self, df_cal):
        """Determine the next step using a Newton downhill method."""

        sub = df_cal.groupby('zbin').mean()
        sub['a'] = (sub.r2bias1 - sub.r2bias0) / (sub.coff1 - sub.coff0)
        sub['b'] = sub.r2bias0 - sub.a*sub.coff0
        sub['col_off'] = -sub.b/sub.a

        return sub.col_off

    def core_find_offset(self, reg, df_cal):
        """Determine the optimal r-band offset."""

        train_cols = ['c0_tmp', 'c1']

        Nsteps = 3
        assert Nsteps
        for istep in range(Nsteps+1):
            if istep == 0:
                df_cal['coff0'] = 0.
                df_cal['c0_tmp'] = df_cal.c0
            elif istep == 1: 
                df_cal['coff1'] = self.config['coff_start']
                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1
            elif istep < Nsteps+1:
                coff = self.next_calc_step(df_cal)

                if 'col_off' in df_cal:
                    del df_cal['col_off']

                df_cal = df_cal.join(coff, on='zbin')
                df_cal.ix[np.isnan(df_cal.col_off), 'col_off'] = 0. # Setting default value.
                df_cal['coff1'] = df_cal['col_off']

                df_cal['c0_tmp'] = df_cal.c0 + df_cal.coff1

            col_tmp = df_cal[['c0_tmp', 'c1']].values
            r2pred = pd.Series(reg.predict(col_tmp), index=df_cal.index)
            r2bias = (r2pred - df_cal.r2true)/df_cal.r2true

            nr = 0 if istep == 0 else 1
            df_cal['r2pred{0}'.format(nr)] = r2pred
            df_cal['r2bias{0}'.format(nr)] = r2bias

        return coff

    def next_find_offset(self):

        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        test_cat = gc
        add_colors(cal_cat)
        add_colors(test_cat)

        zedges_cal = np.linspace(0.1, 1.8, 10)
        cal_cat['zbin'] = pd.cut(cal_cat.zs, zedges_cal)
        test_cat['zbin'] = pd.cut(test_cat.zs, zedges_cal)

        coff = self.core_find_offset(reg, cal_cat)

        # Evaluating the "R^2_{\\rm{PSF}}" bias for the test catalog.
        # In most cases we will need to evaluate the "R^2_{\\rm{PSF}}" value another time...
        test_cat = test_cat.join(coff, on='zbin') 
        test_cat['coff'] = test_cat['col_off']
        test_cat.ix[np.isnan(test_cat.col_off), 'coff'] = 0. # Setting default value.

        test_cat['c0_tmp'] = test_cat.c0 + test_cat.coff
        col_tmp = test_cat[['c0_tmp', 'c1']].values

        test_cat['r2pred1'] = pd.Series(reg.predict(col_tmp), index=test_cat.index)
        test_cat['r2bias1'] = (test_cat.r2pred1 - test_cat.r2true) / test_cat.r2true

        return test_cat

    def plot(self, test_cat):
        zedges_cal = np.linspace(0.1, 1.8, 10)
        mean = test_cat.groupby(pd.cut(test_cat.zs, zedges_cal)).mean()

    #    plt.plot(mean.zs, mean.r2bias0, 'o-', label='Uncorrected')
        plt.plot(mean.zs, mean.r2bias1, 'o-', label='Corrected')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.legend()
        plt.show()

    #    ipdb.set_trace()

        return mean

    def run(self):
        test_cat = self.next_find_offset()
        self.plot(test_cat)

def test_code():
    test = Calibration_test()
    test.run()

def test_task():
    # Configuring the new task. It has all the input of
    # the ordinary r2train and in a mock catalog copied
    # from r2cal.
    r2x = xd.Job('r2x')

    name_map = {'galcat': 'test_cat', 'r2gal': 'test_r2'}
    for dep_name, dep_job in pipel.r2train.depend.iteritems():
        new_name = name_map[dep_name] if dep_name in name_map \
                   else dep_name

        r2x.depend[new_name] = dep_job

    r2x.depend['cal_cat'] = pipel.r2cal.cal.galcat
    r2x.depend['cal_r2'] = pipel.r2cal.cal.r2gal

    r2x.config.update({
      'use_cal': True,
      'Ntrain': 400,
      'filters': ['r', 'i', 'z']})

    #r2x.run()
    r2bias = r2x.get_store()['r2eff']

    ipdb.set_trace()

test_task()
#next_find_offset()
#find_offset()
