#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import statsmodels.api as sm
from sklearn import linear_model
from scipy.interpolate import splrep, splev

import xdolphin as xd

import psf4
import tosave

class Plot(psf4.Plot):
    """Test the "R^2_{\\rm{PSF}}" expansion in the paper."""

    def _load(self, var, vals, N = 50000):

        # Should later be in the topnode function
        r2gauss = xd.Job('r2gauss')

        A = []
        for x in vals:
            r2gauss.task.config[var] = x
            r2gauss.task.config['N'] = N
            r2gauss.config['z0'] = 0.499

            df = r2gauss.result
            df[var] = x
            A.append(df)

        df = pd.concat(A)

        return df

    def expanded(self, zs):
        # This is not the usual approach...
        r2vz_job = xd.Job('r2vz')
        r2vz_job.config['dz_ab'] = 0.0001
        r2vz_job.config['data_dir'] = '/data2/photoz/run/data'
        r2vz_job.config['filter_dir'] = '/data2/photoz/run/data/des_euclid'
        r2vz = r2vz_job.result

        # Testing first with the Elliptical galaxy (Ell_01).
        z = r2vz['z']
        y = r2vz['/vis/Ell_01']
        
        assert z[0] == 0., 'Something went wrong'
        norm = y[0]

        spl = splrep(z, y, k=5)

        der = pd.DataFrame()
        for i in range(5):
            der[i] = splev(zs, spl, der=i)

        return der 
#        ipdb.set_trace()

    def plot1(self, df):
        # Only move, not tested.

        # Prediction using the expansion.
        df['dz'] = df.zp - df.zs
        t1 = time.time()
        df['r2pred_ana'] = df.der0 + df.der1*df.dz + 0.5*df.der2*df.dz**2 
#                           (1./6.)*df.der3*df.dz**3 + (1./24.)*df.der4*df.dz**4
        t2 = time.time()
        print('time pred', t2-t1, 'len', len(df))
        df['r2bias_ana'] = (df.r2pred_ana - df.der0)/df.der0
        print('Found predictions')

        mean = df.groupby('pz').mean()
        mean = mean.reset_index()
        plt.plot(mean.pz, mean.r2bias.abs(), label='Normal')
        plt.plot(mean.pz, mean.r2bias_ana.abs(), label='Expanded $"R^2_{\\rm{PSF}}"$')

        plt.xlabel('$\sigma_z$/(1+z)', size=16) 
        plt.ylabel('$\delta "R^2_{\\rm{PSF}}"$/ $"R^2_{\\rm{PSF}}"$', size=16)
        
        plt.grid(True)
        plt.legend()
        plt.show()
        ipdb.set_trace()



    def plot2(self, df):

        ipdb.set_trace()


    def run(self):
        #pzL = np.logspace(-2.3, -1.)
        N = int(5e5)
        pzL = np.logspace(-3.3, -1.3)
#        pzL = np.logspace(-3.3, -1.3, 5)

        df = self._load('pz', pzL) #, N)
        df['r2bias'] = df.Ell_01

        # Add the derivative.
        der = self.expanded(df.zs)
        der = der.rename(columns=lambda x: 'der'+str(x))
        df = df.join(der)

        self.plot2(df)




if __name__ == '__main__':
    Plot().run()
