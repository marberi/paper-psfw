#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import xdolphin as xd
from matplotlib import pyplot as plt

import psf22
import tosave

class Plot:
    def topnode(self):
        import trainpipel
        pipel = trainpipel.pipel()

        pipel.config.update({\
          'r2x.Ntrain': 400,
          'r2x.Nboot': 10,
#          'r2x.filters': ['r','i','z']})
          'r2x.z_cal': 'zb',
          'r2x.z_test': 'zb'})

#          'r2x.filters': ['r','i']})

        dsl = psf22.Plot()

        topnode = xd.Topnode(pipel)

        topnode.add_layer_key('r2x.use_stars', [True, False])
        #topnode.add_layer_key('r2x.filters', [['r','i','z'], ['r','z']])
        topnode.add_layer(map(dsl.zband, [True, False]))

        return topnode

    def plot(self, topnode):
        z = np.linspace(0.1, 1.8, 10)

        for key, pipel in topnode.iteritems():
            cat = pipel.r2x.get_store()['r2eff']
            cat = cat.join(pipel.r2x.test_cat.result)
            cat = cat[cat.mag_vis < 24.5]
            cat = cat.join(pipel.r2x.test_pz.result, rsuffix='A')

            mean = cat.groupby(pd.cut(cat.zb, z)).mean()
            Nboot = pipel.config['r2x.Nboot']

            r2bias = mean[range(Nboot)].mean(axis=1)
            err = np.sqrt(mean[range(Nboot)].var(axis=1))

            lbl_star = 'Star train' if pipel.config['r2x.use_stars'] else 'Gal train'
            lbl_f = ','.join(pipel.config['r2x.filters'])
            lbl = '{0}: {1}'.format(lbl_f, lbl_star)

            plt.errorbar(mean.zb, r2bias, err, label=lbl)
#            ipdb.set_trace()

        plt.grid(True)
        plt.legend()

        plt.xlabel('Redshift [zb]', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.subplots_adjust(left=0.15)

        plt.xlim((0., 1.9))

        # Shaded requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        plt.savefig(tosave.path('x59'))

    def run(self):
        topnode = self.topnode()
        self.plot(topnode)

if __name__ == '__main__':
    Plot().run()
