#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave
import xdolphin as xd
from xdolphin import Job
sys.path.append('/home/marberi/xdolphin/xdolphin/lang')

import bootstrap
import trainpipel

class Plot:
    """Redshift dependence for different photo-z R^2 estimations."""

    def topnode(self):
        pipel = trainpipel.pipel(toplevel='r2pz')
        pipel.config.update({\
#          'r2pz.use_ml': False,

          'r2pz.pzcat.out_pdf': True,
          'r2pz.pzcat.out_pdftype': True})

        # Mostly because of being tired of waiting for the
        # results.
#        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 35000
        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 30000
#        ipdb.set_trace()

        return pipel

    def run(self):

        pipel = self.topnode()

        J = pipel.r2pz
        r2eff = J.r2gal.result
        r2pz = J.result
        galcat = J.pzcat.galcat.result
        mabs = J.pzcat.galcat.magcat.mabs.result
        pzcat = J.pzcat.result

        df = pd.concat([r2eff,r2pz,galcat,mabs,pzcat], axis=1)
        
        df = df[(24 < df.mag_vis) & df.mag_vis < 24.5]

        sedl = [0,17,55,65]
        S = pd.cut(df.sed, sedl, labels=['ell', 'sp', 'irr'])

        bins = np.arange(0., 2.0, 0.1)
        K = {'histtype': 'step', 'bins': bins, 'normed': False}
        for key, sub in df.groupby(S):
            lbl = key

            #ipdb.set_trace()
            zs = sub['zs'].icol(0)
            zs.hist(label=lbl, **K)

        plt.legend()
        plt.show()
#            ipdb.set_trace()

if __name__ == '__main__':
    Plot().run()
