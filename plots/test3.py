#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import xdolphin as xd

import sys
sys.path.append('/home/marberi/xdolphin/xdolphin')

from pack import Pack



# Test setup
j = xd.Job('psf11')
E = j.task.topnode()

J = E.values()[0]

p1 = Pack()
p1.add(J)

taskids = p1.taskids()

print('p1', taskids.values())
