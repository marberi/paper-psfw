#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    """The correlations between the R^2_{\\rm{PSF}} and photo-z estimation."""

    name = 'psf17'

    def topnode(self):
        import trainpipel

        config = {
          'r2x.filters': ['r','i','z'],
          'r2x.use_cal': False,
#          'r2x.use_stars': False, #True,
          'r2x.all_test': True,
#          'r2x.Nboot': 400,
          'r2x.Nboot': 100,
          'r2x.Ntrain': 400}

        pipelD = {}
        for bj in [False, True]:
            pipel = trainpipel.pipel(ind_noise_test=bj)
            pipelD[bj] = pipel


        layers = xd.Layers()
        layers.add_config(config)
        layers.add_pipeline_layer(pipelD)
        layers.add_layer_key('use_stars', 'r2x.use_stars', [False, True])

        topnode = layers.to_topnode()

        return topnode

    def f_reduce(self, pipel):
        """Reduction step."""

        cat = pipel.r2x.test_cat.result[['mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat.join(pipel.r2x.test_pz.result.unstack())
        cat_zs = pipel.r2x.test_pz.galcat.magcat.mabs.result[['zs']]
        cat['zs'] = cat_zs.zs

        cat['zx'] = (cat.zb - cat.zs) / (1 + cat.zs)
        cat = cat[cat.mag_vis < 24.5]

        Nboot = pipel.r2x.config['Nboot']

        z = np.linspace(0.1, 2.0, 15)
        df = pd.DataFrame()
        for key, sub in cat.groupby(pd.cut(cat.zs, z)):
            # The dataframe.corr function is correlation all collumns
            # with all columns. This is too slow with many bootstrap
            # samples.

            # This is for the different pointings.
            corr = np.nan*np.ones(Nboot)
            for i in range(Nboot):
                corr[i] = sub[[i, 'zx']].corr()[i]['zx']

            S = pd.Series()
            S['corr_val'] = corr.mean()
            S['err'] = np.sqrt(corr.var())
            S['zb'] = sub.zb.mean()
            S['zs'] = sub.zs.mean()
   
            df = df.append(S, ignore_index=True)

        return df

    def plot(self, prepare):

#        plt.rcParams.update({'axes.labelsize': 19})
        styles = gen_styles()

        order = [(0,False), (1,False),
                 (0,True), (1,True)]

        for base_pipel, use_stars in order:
            sub = prepare[(prepare.base_pipel == base_pipel) & \
                          (prepare.use_stars == use_stars)]


            isindep = base_pipel

            lbl_stars = 'Stars train' if use_stars else 'Gal train'

            lbl = lbl_stars if not isindep else \
                  '{0} - Indep'.format(lbl_stars)

            ls = '--' if isindep else '-'
            col = 'red' if use_stars else 'blue'
            plt.errorbar(sub.zs, sub.corr_val, sub.err, color=col, ls=ls, 
                         label=lbl)


        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

<<<<<<< HEAD
        plt.xlabel('$z_{\mathrm{s}}$', size=14)
        plt.ylabel('r($\delta R^2_{\\rm{PSF}}\, , \, \delta z$)', size=13)
=======
        plt.xlabel('Redshift [$z_s$]')
        plt.ylabel('r($\delta R^2_{\\rm{PSF}}\, , \, \delta z$)')
>>>>>>> 0a69a82160a549d0a1ad619323851fad7563238a
        plt.legend()


if __name__ == '__main__':
    xd.plot(Plot).save()
