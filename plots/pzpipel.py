#!/usr/bin/env python
# encoding: UTF8

import ipdb
import os
import numpy as np
import pandas as pd
import xdolphin as xd
from xdolphin import Job

from matplotlib import pyplot as plt
#from euclid_setup import lim_ext
import euclid_setup

data_dir = '/nfs/pic.es/user/e/eriksen/data/psfw/'

def pipel():
    """Pipeline for testing estimating the PSF size using the output
       from photo-z codes.
    """

# yes, this is used by psf31.
#    raise NotImplementedError('Is this still in use??')

    euclid_conf = euclid_setup.config()

    config = {'hack': 1, 'pzcat': {'use_priors': True}} 
    mabs = Job('galabs')
    galcatN = Job('magobs')
    galcatN.depend['mabs'] = mabs

#    ab_dir = '/data2/photoz/run/data/'
    ab_dir = data_dir
#    filter_dir = 'des_euclid'
#    filter_dir = '/data2/photoz/run/data/des_euclid'
#    data_dir = '/data2/photoz/run/data/'

    abgal = Job('ab')
    abgal.config.update({'data_dir': ab_dir,
                         'filter_dir': os.path.join(data_dir, 'des_euclid2'),
#                         'dz_ab': 0.001,
                         'sed_dir': os.path.join(data_dir, 'seds')})

    galcatN.depend['ab'] = abgal

    galcat = Job('magnoise')
    galcat.depend['magcat'] = galcatN
#    galcat.config['sn'] = 50.1
    galcat.config['const_sn'] = False
    galcat.config.task_config['sn_lims'].update(euclid_conf['lim_ext'])

    pzcat = Job('pzcat')
    pzcat.depend['galcat'] = galcat
    pzcat.depend['ab'] = abgal

    pz_conf = {'add_noise': False, #True, 
               'out_pdf': True,
               'filter_dir': os.path.join(data_dir, 'des_euclid2'),
               'sed_dir': os.path.join(data_dir, 'seds'),
               'sky_spec': os.path.join(data_dir, 'sky_spectrum.txt'),
               'zmax': 5.}


    pzcat.config.update(pz_conf)

    r2vz = Job('r2vz')
    r2vz.config['filter_dir'] = os.path.join(data_dir, 'des_euclid2')
    r2vz.config['data_dir'] = data_dir


    r2gal = Job('r2gal', depend={'r2vz': r2vz, 'mabs': mabs})

    r2pz = Job('r2pz')
    r2pz.depend['pzcat'] = pzcat
    r2pz.depend['r2vz'] = r2vz
    r2pz.depend['r2gal'] = r2gal

    pipel = xd.Job()
    pipel['r2pz'] = r2pz

    return pipel
