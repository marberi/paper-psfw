#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import trainpipel

def _add_offset(pipel):
    """Add a task for the offset."""

    offset = xd.Job('offset')
    offset.depend['magcat'] = pipel.r2train.galcat

    pipel.r2train.depend['galcat'] = offset

    return pipel


pipel = trainpipel.pipel()
pipel.config.update({
  'r2train.filters': ['r', 'i', 'z'],
#  'r2train.filters': ['r', 'i'],
  'r2train.all_test': True,
  'r2train.Ntrain': 400,
  'r2train.Nboot': 10,
  'r2train.use_stars': False})

pipel = _add_offset(pipel)
#per = np.array([-2,-1,0,1,2])
#gamma = 0.55*(1. + per/100.) 

top = xd.Topnode(pipel)

# Shifts \gamma in the observations, but not in the mocks.
#layer = [{'r2train.r2gal.r2vz.r2_exp': x,
#          'r2train.train_star_r2.r2vz.r2_exp': x} for x in gamma]

layer2 = [{'r2train.use_stars': True, 'r2train.Ntrain': 400}, \
          {'r2train.use_stars': False, 'r2train.Ntrain': 1500}]

top.add_layer(layer2) 


off = [-0.01, 0., 0.01]
off = [-0.1, 0., 0.1]
layer = [{'r2train.galcat.r': x} for x in off]

top.add_layer(layer) 
#top.add_layer_key('r2train.r2gal.r2vz.r2_exp', gamma)


#for key, pipel in top.iteritems():
#    pipel.r2train.run()


def plot1():
    # Ugly way of enforcing 
    for field in [True, False]:
        for key,pipel in top.iteritems():
            use_stars = pipel.r2train.config['use_stars']
            if not use_stars == field:
                continue

            r2train = pipel.r2train
            print('galaxies', r2train.r2gal.r2vz.config['r2_exp'])
            print('stars', r2train.train_star_r2.r2vz.config['r2_exp'])
            print('mock', pipel.r2train.train_gal_r2.r2vz.config['r2_exp'])

    #        ipdb.set_trace()

            A = pipel.r2train.get_store()['r2pred']
            gc = pipel.r2train.galcat.result[['zs', 'mag_vis']]
            gc = gc.join(A)
            gc = gc[gc.mag_vis < 24.0]
            gc = gc[gc[0] < 140]

            lbl_star = 'Stars' if use_stars else 'Galaxies'
            gamma = pipel.r2train.r2gal.r2vz.config['r2_exp']
            off = pipel.r2train.galcat.config['r']
            lbl = '{0}, off = {1}'.format(lbl_star, off)

            K = {'histtype': 'step', 'bins': 150, 'normed': True}
            gc[0].hist(label=lbl, **K)

    plt.legend()
    plt.show()

def get_cat(pipel):
    store = pipel.r2train.get_store()
    gc = pipel.r2train.galcat.result #[['zs', 'mag_vis']]
    gc = gc.join(store['r2pred'])
    gc = gc.join(store['r2true'], rsuffix='_true')
    store.close()

    # here it also includes the offset task.
    gc = gc.join(pipel.r2train.galcat.magcat.magcat.mabs.result, rsuffix='_x')

    # Tryint to cut in color.
    gc['col_ri'] = gc.mag_r - gc.mag_i
    gc = gc[gc.col_ri > 1.1]

#    gc = gc[gc.mag_vis < 23.0]
#    gc = gc[(0.3 < gc.zs) & (gc.zs < 0.7)]
#    gc = gc[(0.45 < gc.zs) & (gc.zs < 0.55)]
#    gc = gc[(10 < gc.sed) & (gc.sed < 17)]
#    gc = gc[(20 < gc.sed) & (gc.sed < 30)]
#    gc = gc[(30 < gc.sed) & (gc.sed < 40)]


    return gc

# A bit ad hoc.
for f in range(len(off)):
    p0 = top[f,0]
    p1 = top[f,1]
    gamma0 = p0.r2train.r2gal.r2vz.config['r2_exp']
    gamma1 = p1.r2train.r2gal.r2vz.config['r2_exp']
    off0 = p0.r2train.galcat.config['r']

    gc0 = get_cat(p0)
    gc1 = get_cat(p1)


    K = {'histtype': 'step', 'bins': 450, 'normed': True}
    lbl = str(off0) #str(gamma0)

    field = '1'
    (gc1[field] - gc0[field]).hist(label=lbl, **K)

plt.legend()
plt.show()
