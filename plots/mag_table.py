#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
from euclid_setup import lim_ext, lim_point

filters = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

print('Filter &' +'& '.join(filters)+'\\\\')

fmt = '{0}'
A = [fmt.format(lim_point[x]) for x in filters]
B = [fmt.format(lim_ext[x]) for x in filters]

print('Stars &' + '& '.join(A) + '\\\\')
print('Galaxies &' + '& '.join(B) + '\\\\')

ipdb.set_trace()
