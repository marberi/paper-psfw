#!/usr/bin/env python
# encoding: UTF8

import ipdb
import pandas as pd
from matplotlib import pyplot as plt
from xdolphin import Job

import psf19

class Plot(psf19.Plot):
    """Recovered offsets in gamma."""

    expL = [0.55, 0.551]
    def gamma_plot(self, df):

        S = self._gamma_deriv(df)
        dg = self._measure_gamma(S)

        # For cosmetics
        dg = dg[dg.dg.abs() < 0.005]

        title = 'Recovered $\gamma$ offsets.'
        dg.hist(bins=15, normed=True) #, title='Recovered $\gamma$ offsets.')
        plt.xlim(-0.002, 0.002)
        plt.xlabel('$\Delta \gamma$', fontsize=16)
        plt.ylabel('Probability', fontsize=16)
        plt.title(title)
        plt.show()

    def run(self):
        df = self.get_data()
        self.gamma_plot(df)


if __name__ == '__main__':
    Plot().run()
