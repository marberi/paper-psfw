#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import trainpipel
import xdolphin as xd
from matplotlib import pyplot as plt

class Plot:
    def add_r2cal(self, pipel):
        r2cal = xd.Job('r2cal')
        r2cal.depend['r2train'] = pipel.r2train

        cal = pipel.r2train.clone()
        cal.depend['galcat'] = cal.train_gal_cat
        cal.depend['r2gal'] = cal.train_gal_r2

        r2cal.depend['cal'] = cal

        pipel.depend['r2cal'] = r2cal

        return pipel

    def topnode(self):
        pipel = trainpipel.pipel()
        pipel.config.update({\
          'r2train.all_test': True,
#          'r2train.use_stars': True, #False,
          'r2train.use_stars': False,
          'r2train.Ntrain': 400,
          'r2train.filters': ['r','i','z']}) #400,

        pipel = self.add_r2cal(pipel)
        top = xd.Topnode(pipel)
        top.add_layer([{'r2train.use_stars': True, 'r2train.Ntrain': 400}, \
                       {'r2train.use_stars': False, 'r2train.Ntrain': 1500}])


        return top

    def prepare(self, pipel):

        cat = pipel.r2cal.r2train.galcat.result
        store = pipel.r2cal.r2train.get_store()
        cat = cat.join(store['r2eff'])
        store.close()
        cat = cat.join(pipel.r2cal.result, rsuffix='_cal')

        Nboot = 2 # HACK... fix this.

        prepare = pd.DataFrame()
        z = np.linspace(0.1, 1.8, 10)
        for key, sub in cat.groupby(pd.cut(cat.zs, z)):
            cols = map(str, range(Nboot))
            cols_corr = map('{0}_cal'.format, range(Nboot))

            S = pd.Series()
            S['r2bias'] = sub[cols].mean().mean()
            S['r2bias_corr'] = sub[cols_corr].mean().mean()
            S['zs' ] = sub.zs.mean()
            S['use_stars'] = pipel.r2train.config['use_stars']
 
            prepare = prepare.append(S, ignore_index=True)

        return prepare

    def plot(self, prepare):
        for key, sub in prepare.groupby('key'):
            sub = sub.sort('zs')

            lbl_stars = 'Stars' if sub.irow(0).use_stars else 'Galaxies'
            plt.plot(sub.zs, sub.r2bias, 'o-', label=lbl_stars)
            plt.plot(sub.zs, sub.r2bias_corr, 'o-', label='{0}, corrected'.format(lbl_stars))

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4, ls='--', color='black', lw=2., alpha=0.5)
        plt.axhline(3e-4, ls='--', color='black', lw=2., alpha=0.5)

        plt.legend(loc=0)
        plt.show()

    def prep_df(self, top, f_prep):
        prepare = pd.DataFrame()
        for key, pipel in top.iteritems():
            part = f_prep(pipel)
            part['key'] = len(part)*[key] # needed hack
            prepare = prepare.append(part, ignore_index=True)

        return prepare

    def run(self):
        top = self.topnode()
#        prepare = top.get_prepare(self.prepare)
        prepare = self.prep_df(top, self.prepare)
        self.plot(prepare)


if __name__ == '__main__':
    Plot().run()
