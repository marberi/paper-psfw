#!/usr/bin/env python
# encoding: UTF8

# Check which plots that are missing the topnode function.

from __future__ import print_function
import ipdb
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import imp

import squid
inuse = squid.find_inuse()

# Exceptions.
ex = []
ex += ['psf1','psf4','psf5'] # Very simple plots
ex += ['psf19', 'psf20'] # Offset detection


for pl in inuse:
    # Exceptions which needs to be fixed.
    if pl in ex:
        continue

    mod = imp.load_module(pl, *imp.find_module(pl))
    inst = getattr(mod, pl)()
   
    print(pl, hasattr(inst, 'topnode'))

#    ipdb.set_trace()
