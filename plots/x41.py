#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import trainpipel

pipel = trainpipel.pipel()
pipel.config.update({\
  'r2x.Ntrain': 400,
  'r2x.all_test': True,
  'r2x.filters': ['r','i', 'z'],
  'r2x.use_stars': True})


# Then looking at the results.
cat = pipel.r2x.test_cat.result.join(pipel.r2x.get_store()['r2eff'])
cat = cat[cat.mag_vis < 24.5]

cat = cat[1.0 < cat.zs]

z = np.linspace(0.1, 1.8, 7)
for key, val in cat.groupby(pd.cut(cat.zs, z)):
    if not len(val): continue

    val[0].hist(label='z: {0}'.format(key), bins=100,histtype='step', normed=True)

plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
plt.ylabel('Probability', size=16)
plt.legend()
plt.show()
