#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
import time
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

class Plot:
    """Main tables with the PSF bias when training on galaxies and stars
       for different filter configurations.
    """


    all_filters = ['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H']

    def _rows(self):
        I1 = [['r', 'i', 'z'],
              ['r', 'i'],
              ['i', 'z']]


        I2 = [['vis', 'r', 'i', 'z'],
              ['vis', 'r', 'i'],
              ['vis', 'i', 'z']]

#        I2 = [['vis', 'i', 'z'],
#              ['vis', 'r', 'i', 'z'],
#              ['vis', 'r', 'i']
#             ]

        I3 = [['g', 'r', 'i', 'z', 'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i', 'z',        'Y', 'J', 'H'],
              ['g',      'i', 'z', 'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i',      'vis', 'Y', 'J', 'H'],
              ['g', 'r', 'i',             'Y', 'J', 'H']
             ]

        I4 = [['g', 'r', 'i'],
              ['g', 'r', 'i', 'z']]

        I4 = [['r', 'i', 'z', 'Y'],
              ['r', 'i', 'z', 'J'],
              ['r', 'i', 'z', 'H']]

        I = I1 + I2 + I3

#        I = [I[0]] # For testing.

        return I

    def topnode(self):

        import trainpipel
        pipl = trainpipel.pipel()
        top = xd.Topnode(pipl)
        top.add_layer_key('r2train.filters', self._rows())
        top.add_layer_key('r2train.use_stars', [False, True])
#        top.update_config(self.flatten_conf(config['myconf']))

        top.update_config({\
          'r2train.Ntrain': 1000,
          'r2train.Nboot': 100,
          'r2train.Ntest': 540000,
          'galnoise.const_sn': True,
          'galnoise.sn': 1000000,
          'starnoise.const_sn': True,
          'starnoise.sn': 1000000})

        return top

    def prepare_f(self, pipel):
        print('Staring prepare')
        tbl_data = pd.DataFrame()
        levels = ['filters', 'stars']
        perc = [.68, .95]

        mag_cat = pipel.galnoise.result[['mag_vis', 'zs']]
        r2eff = pipel.r2train.get_store()['r2eff'] 

        N = r2eff.shape[1]
        cat = r2eff.join(mag_cat)
        cat = cat[cat.mag_vis < 24.5]

        X = cat[range(N)].mean().abs()
        
        S = X.describe(percentiles=perc)

        # Should be moved.
        S['r2train.use_stars'] = pipel.r2train.config['use_stars']
        S['r2train.filters'] = pipel.r2train.config['filters']

        print('Finish prepare')
        return S


    def row_name(self, filters):
        """Because one need special configuration for the filters."""

        fspl = filters
        if len(fspl) <= 4:
            return ','.join(filters)

        diff = list(set(self.all_filters) - set(fspl))
        if diff:
            return 'All - {0}'.format(','.join(diff))
        else:
            return 'All'

    def create_tbl(self, prepare):
        prepare['filters'] = map(self.row_name, prepare['r2train.filters'])
        prepare['f_key'] = [3, 7, 6, 10, 3, 2, 5, 4, 10, 1, 0, 4, 5, 9, 0, 8, 1, 6, 8, 7, 9, 2]

        tbl = pd.DataFrame()

        for key, sub in prepare.groupby('f_key'): 
            sub = sub.set_index('r2train.use_stars')

            row = pd.Series()
            for n,lbl in [(0, 'Gal'), (1, 'Star')]:
                row['Filters'] = sub.irow(0).filters
                for field in ['mean', '68%', '95%']:
                    row['{0} {1}'.format(lbl, field)] = sub.ix[n, field]

            row.name = key
            tbl = tbl.append(row) #, ignore_index=True)

        return tbl


    def run(self):

        topnode = self.topnode()
        prepare = topnode.get_prepare(self.prepare_f)
        tbl = self.create_tbl(prepare)

        latex = tbl.to_latex(float_format='{0:.1e}'.format, index=False)

        print(latex)

    def fast(self, top):
        top.update_config({\
          'r2train.Ntrain': 100,
          'r2train.Nboot': 4,
          'r2train.galcat.magcat.mabs.Ngal': 1000})

if __name__ == '__main__':
    Plot().run()
