#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import copy

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from xdolphin import Job

import trainpipel

class Plot:
    """North-south corrections."""

    def topnode(self):
        myconf_base = {'Ntrain': 400, 'Nboot': 100, 'use_stars': True, 'filters': ['r', 'i', 'z']}

        myconfS = copy.deepcopy(myconf_base)
        myconfN = copy.deepcopy(myconf_base)

        myconfS['filters'] = ['r', 'i', 'z']
        myconfN['filters'] = ['r', 'i']


        south = trainpipel.pipel()
#        south.config['r2train.galcat.magcat.mabs.Ngal'] = 100
        south.r2train.config.update(myconfS)
#        south.galabs.config['HACK'] = 10 # This was used to signal independent galaxy samples.

        north = trainpipel.pipel()
#        north.config['r2train.galcat.magcat.mabs.Ngal'] = 100

        north.r2train.config.update(myconfN)

        southX = south.clone()
        southX.r2train.config['filters'] = ['r', 'i']

        r2corr = Job('r2corr')
        r2corr.depend['north'] = north
        r2corr.depend['south'] = south
        r2corr.depend['southX'] = southX

        return r2corr

    def getcat(self, pipel):
        names = ['r2eff', 'r2pred', 'r2true']
        store = pipel.r2train.get_store()

    #    A = pipel.kids['r2train']['r2train']
        cat = pipel.galcatN.result
        for name in names:
            part = store[name]
            part.columns = ['{0}_{1}'.format(name, x) for x in range(len(part.columns))]

            cat[name] = part.mean(axis=1)
            cat = cat.join(part)

        return cat

    def run(self):
        r2corr = self.topnode()

        Nboot = r2corr.north.r2train.config['Nboot']
        res = r2corr.result

        catS = self.getcat(r2corr.south)
        catSX = self.getcat(r2corr.southX)
        catN = self.getcat(r2corr.north)

        cat_corr = catN.join(res)

        L = [('South', catS, 'r2eff_{0}'),
             ('North', catN, 'r2eff_{0}'),
             ('North, corrected', cat_corr, 'r2eff_corr_{0}')]


        for lbl, cat, fmt in L:
            keys = map(fmt.format, range(Nboot))

            sub = cat[cat.mag_vis < 24.5]
            sub[keys].mean().hist(histtype='step', normed=True, label=lbl)

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.ylabel('Probability', size=16)

        plt.legend(loc=0)
        plt.show()

if __name__ == '__main__':
    Plot().run()
