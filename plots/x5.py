#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import copy

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from xdolphin import Job

import trainpipel
import tosave

class x5:
    name = 'x5'
    config = {'myconf_base': {'Ntrain': 400, 'Nboot': 100, 'use_stars': True, 'filters': ['r', 'i', 'z']}}

    def topnode(self):
        myconf_base = self.config['myconf_base']

        myconfS = copy.deepcopy(myconf_base)
        myconfN = copy.deepcopy(myconf_base)

        myconfS['filters'] = ['r', 'i', 'z']
        myconfN['filters'] = ['r', 'i']


        south = trainpipel.pipel()
        south.kids['r2train'].config.update(myconfS)
        south.kids['galabs'].config['HACK'] = 10

        north = trainpipel.pipel()
        north.kids['r2train'].config.update(myconfN)

        southX = south.clone()
        southX.kids['r2train'].config['filters'] = ['r', 'i']

        r2corr = Job('r2corr')
        r2corr.depend('north', north)
        r2corr.depend('south', south)
        r2corr.depend('southX', southX)

        return r2corr

    def getcat(self, pipel):
        names = ['r2eff', 'r2pred', 'r2true']
        store = pipel.kids['r2train']['r2train']

    #    A = pipel.kids['r2train']['r2train']
        cat = pipel.kids['galcatN']['magcat']['magcat']
        for name in names:
            part = store[name]
            part.columns = ['{0}_{1}'.format(name, x) for x in range(len(part.columns))]

            cat[name] = part.mean(axis=1)
            cat = cat.join(part)

        return cat

    def run(self):
        # This structure is weird...
        r2corr = self.topnode()
        kids = r2corr.kids

        Nboot = kids['north'].kids['r2train'].config['Nboot']

        catS = self.getcat(kids['south'])
        catSX = self.getcat(kids['southX'])
        catN = self.getcat(kids['north'])

        cat_corr1 = catN.join(r2corr['r2corr']['r2corr'])
       
        r2corr.config['z_corr'] = True
        cat_corr2 = catN.join(r2corr['r2corr']['r2corr'])
 
        r2corr.config['use_zb'] = False
        cat_corr3 = catN.join(r2corr['r2corr']['r2corr'])

#        ipdb.set_trace()

        L = [('South', catS, 'r2eff_{0}'),
#             ('North', catN, 'r2eff_{0}'),
#             ('North, corrected', cat_corr1, 'r2eff_corr_{0}'),
             ('North, corrected (zb)', cat_corr2, 'r2eff_corr_{0}'),
             ('North, corrected (zs)', cat_corr3, 'r2eff_corr_{0}')]


        for lbl, cat_in, fmt in L:
            keys = map(fmt.format, range(Nboot))
            cat = cat_in[cat_in.mag_vis < 24.5]

            S = pd.Series(cat_in[keys[:1]].values.flatten())
            S.hist(bins=200, histtype='step', normed=True, label=lbl)

        plt.legend()
        plt.show()


if __name__ == '__main__':
    Job('x5').run()
