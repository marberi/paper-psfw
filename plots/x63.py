#!/usr/bin/env python
# encoding: UTF8

import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import psf9
import tosave

class Plot:
    """Showing the redshift distribution of the different
       galaxy types.
    """

    def topnode(self):
        inst = psf9.Plot()
        pipel = inst.topnode()
        pipel.r2pz.pzcat.galcat.magcat.mabs.config['Ngal'] = 10000

        return pipel

    def plot(self, pipel):
        cat = pipel.r2pz.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result)
        cat = cat.join(pipel.r2pz.pzcat.galcat.magcat.mabs.result, rsuffix='ignore_')

        cat = cat[cat.mag_vis < 24.5]
        #ipdb.set_trace()

        z = np.linspace(0.1, 2.0, 30)
        gr = pd.cut(cat.zs, z)

        sedl = [0,17,55,65]
        S = pd.cut(cat.sed, sedl, labels=['ell', 'sp', 'irr'])

        bins = np.linspace(0., 3, 40)
        for key, val in cat.groupby(S):
            val.zs.hist(label=key, bins=bins, histtype='step')

        plt.legend()
        plt.xlabel('Redshift', size=16)
        plt.savefig(tosave.path('x63'))

    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
