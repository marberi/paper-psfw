#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
from matplotlib import pyplot as plt

import psf10
import psf12

class psf47:
    name = 'psf47'
    config = {}

    def d1(self, B):
        r2eff = B['noisy']['r2train']['r2train']['r2eff']
        var = r2eff[range(200)].var(axis=1)

        var.hist(bins=100)

        plt.show()

    def _bias(self, val):
        hmm = val[range(30)].mean()
        r2bias = hmm.abs().describe(percentiles=[0.68])['68%'] #mean()
        r2bias *= np.sign(hmm.mean()) 

        return pd.Series({'z': val.zs.mean(), 'r2bias': r2bias})

    def d2(self, B):
        r2eff = B['noisy']['r2train']['r2train']['r2eff']
        magcat = B['noisy']['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)

        df = pd.DataFrame()

        z = np.linspace(0.1, 1.5, 10)
        for key, val in cat.groupby(pd.cut(cat.zs, z)):
            r2bias = val[range(30)].mean().mean()
            df = df.append(self._bias(val), ignore_index=True)

        df.plot(x='z', y='r2bias')
        plt.show()


    def d3(self, B):
        r2eff = B['noisy']['r2train']['r2train']['r2eff']
        magcat = B['noisy']['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)
        df = pd.DataFrame()

        cat['ri'] = cat['mag_r'] - cat['mag_i']

        kwds = {'histtype': 'step', 'normed':True}
        cat.ri.hist(bins=100, label='Full cat', **kwds)
        cat[cat.mag_vis < 24.5].ri.hist(bins=100, label='vis < 24.5', **kwds)

        plt.xlabel('r-i', size=16)
        plt.ylabel('Probability', size=16)

        plt.legend()
        plt.show()


        ipdb.set_trace()


    def d4(self, B):
        df = pd.DataFrame()
        for lbl in ['noiseless', 'noisy']:
            r2eff = B[lbl]['r2train']['r2train']['r2eff']
            magcat = B[lbl]['galnoise']['magcat']['cat']
            cat = magcat.join(r2eff)
            cat['ri'] = cat['mag_r'] - cat['mag_i']

            cat = cat[cat.mag_vis < 24.5]

            ri = np.linspace(-1, 2, 20)
            for key, val in cat.groupby(pd.cut(cat.ri, ri)):
                r2bias = val[range(30)].mean().mean()
                S = self._bias(val)
                S['lbl'] = lbl
                df = df.append(S, ignore_index=True)
             
        for key, val in df.groupby('lbl'):
            plt.plot(val.ri, val.r2bias, 'o-', label=key) 

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        plt.xlabel('r-i', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend(loc=0)
        plt.show()

    def d5(self, B):
        df = pd.DataFrame()
#        for lbl in ['noiseless', 'noisy']:
        lbl = 'noisy'
        r2eff = B[lbl]['r2train']['r2train']['r2eff']
        magcat = B[lbl]['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)
        cat['ri'] = cat.mag_r - cat.mag_i

        K = B[lbl]['galnoise'].depend['magcat']['magcat']['magcat']
        cat['ri_true'] = K.mag_r - K.mag_i
        
        cat = cat[cat.mag_vis < 24.5]

        if False:
            plt.plot(cat.ri, cat.ri_true, '.')
            plt.xlim(-0.5, 2.5)
            plt.ylim(-0.5, 2.5)

            x = np.linspace(-0.5, 2.5)

            plt.plot(x, x)

        if False:
            (cat.ri - cat.ri_true).hist(bins=200)

        if False:
            plt.plot(cat.ri_true, cat.ri - cat.ri_true, '.')

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        if False:
            ri = np.linspace(-1, 2, 20)
            gr = pd.cut(cat.ri, ri)

            R = cat.groupby(gr).mean()
            #for key, val in 

            plt.plot(R.ri_true, R.ri, 'o-')

            plt.xlabel('r-i True', size=16)
            plt.ylabel('r-i', size=16)

            plt.xlim(-1, 2)
            plt.ylim(-1, 2)

            x = np.linspace(-1, 2)
            plt.plot(x, x, '-', lw=2, alpha=0.5, color='k')

        ri = np.linspace(-0.8, 1.8, 6)
        gr = pd.cut(cat.ri, ri)

        ri = np.linspace(-0.1, 1.0, 6)
        gr = pd.cut(cat.ri_true, ri)



        nbins = 20
        kwds = {'histtype': 'step', 'normed': True} #False}
        for key, val in cat.groupby(gr):
            (val.ri - val.ri_true).hist(label=key, bins=nbins, **kwds)

        (cat.ri - cat.ri_true).hist(label='Total', bins=50, lw=2, color='k', **kwds)

        plt.xlabel('ri - ri_true', size=16)
        plt.ylabel('Probability', size=16)

#        ipdb.set_trace()

        plt.legend()
        plt.show()


#        plt.show()

    def get_cat(self, B):
        df = pd.DataFrame()
#        for lbl in ['noiseless', 'noisy']:
        lbl = 'noisy'
        r2eff = B[lbl]['r2train']['r2train']['r2eff']
        magcat = B[lbl]['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)
        cat['ri'] = cat.mag_r - cat.mag_i

        K = B[lbl]['galnoise'].depend['magcat']['magcat']['magcat']
        cat['ri_true'] = K.mag_r - K.mag_i
        
        cat = cat[cat.mag_vis < 24.5]

        return cat

    def d6(self, B):
        cat = self.get_cat(B)

        if False:
            nbins = 100
            cat.err_r.hist(bins=nbins, label='r')
            cat.err_i.hist(bins=nbins, label='i')



        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


#        cat = cat[cat.ri < 0]

        ri = np.linspace(-0.8, 1.8, 6)
        gr = pd.cut(cat.ri, ri)
        for key, val in cat.groupby(gr):
            plt.plot(val.err_r, val.err_i, '.', label=key, alpha=0.2)


        plt.xlim(0.,0.4)
        plt.ylim(0.,0.4)

        x = np.linspace(0., 1)
        plt.plot(x, x, lw=2, color='k')

        plt.legend()
        plt.show()
#        ipdb.set_trace()

    def d7(self):
        N = 10000
        r1 = np.random.normal(scale=2, size=N)
        r2 = np.random.normal(scale=3, size=N)
       
        s1 = pd.Series(r1) 
        s2 = pd.Series(r2) 

        s3 = s1 + s2
        s4 = s1 - s2

        nbins = 30
        K = {'histtype': 'step', 'normed': True}
        s1.hist(bins=nbins, label='s1', **K)
        s2.hist(bins=nbins, label='s2', **K)

        s3.hist(bins=nbins, label='s1+s2', **K)
        s4.hist(bins=nbins, label='s1-s2', **K)

        plt.legend()
        plt.show()

        import sys
        sys.exit(1)
#        ipdb.set_trace()


    def d8(self, B):
        lbl = 'noisy'
        r2eff = B[lbl]['r2train']['r2train']['r2eff']
        magcat = B[lbl]['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)
        cat['ri'] = cat.mag_r - cat.mag_i

        K = B[lbl]['galnoise'].depend['magcat']['magcat']['magcat']
        di = cat.mag_i - K.mag_i
        dr = cat.mag_r - K.mag_r


        plt.plot(dr, di, '.')
        plt.show()


    def d9(self, B):
        df = pd.DataFrame()

        for lbl in ['noisy', 'noiseless']:

            r2eff = B[lbl]['r2train']['r2train']['r2eff']
            magcat = B[lbl]['galnoise']['magcat']['cat']
            cat = magcat.join(r2eff)


            z = np.linspace(0.1, 2.0, 20)
            for key, val in cat.groupby(pd.cut(cat.zs, z)):
                r2bias = val[range(30)].mean().mean()
                S = self._bias(val)
                S['lbl'] = lbl
                df = df.append(S, ignore_index=True)

        for key, val in df.groupby('lbl'):
            plt.plot(val.z, val.r2bias, 'o-', label=key)


        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(-3e-4)
        plt.axhline(3e-4)

        plt.legend()
        plt.show()


    def d10(self, B):

        lbl = 'noiseless'
#        lbl = 'noisy'
        r2eff = B[lbl]['r2train']['r2train']['r2eff']
        magcat = B[lbl]['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)

        r2true = B[lbl]['r2train']['r2train']['r2true']
        r2pred = B[lbl]['r2train']['r2train']['r2pred']

        cat['tr'] = r2true[0]
        cat['pred'] = r2pred[0]
        cat = cat[cat.mag_vis < 24.5]

        if False:
            r2 = np.linspace(132, 140, 5)
            r2 = np.linspace(133, 138, 5)

            nbins = 200
            K = {'histtype': 'step', 'normed': True}
            for key,val in cat.groupby(pd.cut(cat.pred, r2)):
                print(val[0].mean())
                val[0].hist(bins=nbins, label=key, **K)

            cat[0].hist(bins=nbins, label='Total', lw=2, **K)

            plt.legend()
            plt.show()

        # Selecting on the true redshift.
#        cat = cat[(1.5 < cat.zs) & (cat.zs < 1.6)]

        #nbins = 100 #30
        nbins = 2000 #300
        K = {'histtype': 'step', 'normed': False} #True}
        cat.tr.hist(bins=nbins, label='True', **K)
#        cat.pred.hist(bins=nbins, label='Predicted', **K)

        plt.legend()
        plt.show()


    def d11(self, B):

        lbl = 'noiseless'
#        lbl = 'noisy'
        r2eff = B[lbl]['r2train']['r2train']['r2eff']
        magcat = B[lbl]['galnoise']['magcat']['cat']
        cat = magcat.join(r2eff)

        r2true = B[lbl]['r2train']['r2train']['r2true']
        r2pred = B[lbl]['r2train']['r2train']['r2pred']

        mabs = B[lbl]['galnoise'].depend['magcat'].depend['mabs']['mabs']['cat']

#        r2true = r2true.join(mabs)
        r2eff = r2eff.join(mabs)

        if False:
            r2true[r2true.sed == 16][0].hist(bins=1000, label='16')
            r2true[r2true.sed == 17][0].hist(bins=1000, label='17')
            plt.legend()
            plt.show()

        if False:
            M = r2true.groupby(r2true.sed)[0].max()
            N = M.values
            K = (N[1:] - N[:-1]) / N[:-1]
            plt.plot(range(len(K)), K, 'o-')
            plt.show()

        r2eff['dt'] = r2eff.sed - np.floor(r2eff.sed)
#        ipdb.set_trace()
        r2eff = r2eff[1.5 < r2eff.zs]

        df = pd.DataFrame()
        sed = np.linspace(0., 65, 400)
        sed0 = np.linspace(0., 1, 400)

        for i,(key, val) in enumerate(r2eff.groupby(pd.cut(r2eff.dt, sed0))):
            r2bias = val[range(30)].mean().mean()

            df = df.append(pd.Series({'i': i, 'r2bias': r2bias}),
                           ignore_index=True)

        plt.plot(df.i, df.r2bias, 'o-')
        plt.show()


    def d12(self, B):
        magcat = B['noisy']['galnoise']['magcat']['cat']

        from sklearn import svm
        reg = svm.NuSVR()

        dep = B['noisy']['r2train'].depend
        starcat = dep['starcat']['magcat']['cat']

        fL = ['r', 'i', 'z']
        cols = map('mag_{0}'.format, fL)

        A = starcat[cols].values
        A_col = A[:,:-1] - A[:,1:]
        r2star = dep['r2star']['r2eff']['r2eff']

        inds = np.arange(len(r2star))
        inds = np.random.permutation(inds)

        N = 400
        inds = inds[:N]

        reg.fit(A[inds], r2star.ix[inds,0])

        ipdb.set_trace()

    def run(self):

        inst = psf10.psf10()
#        inst = psf12.psf12()
#        topnode = inst.create_topnode()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['use_stars'] = True
#            A['r2train'].config['Nboot'] = 200 #use_stars'] = True
            A['r2train'].config['Nboot'] = 30 #use_stars'] = True

#            A['galabs'].config['Ngal'] = 1000000 #2e6 #200007
#            A['r2train'].config['Ntest'] = 1000000
#            ipdb.set_trace()

#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['Ntest'] = 200000
            if lbl == 'noiseless':
                A['galabs'].config['Ngal'] = 200008

            B[lbl] = A


        self.d12(B)

if __name__ == '__main__':
    Job('psf47').run()
