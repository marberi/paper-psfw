#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

class x13:
    name = 'x13'
    config = {}

    def get_col(self, cat, filtersL, errors=False):
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else:
            return col, mag

    def d1(self, B):
        key = 'noisy'
#        key = 'noiseless'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'vis', 'i', 'z']
#        fL = ['r', 'vis']  #'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm, neighbors, tree, ensemble
        reg1 = svm.NuSVR()
        reg1.fit(Astar[inds], r2star[inds,0])
        r2pred1 = reg1.predict(col_gal)
        cat['eff1'] = (r2pred1 - r2true) / r2true

        reg2 = ensemble.RandomForestRegressor(n_estimators=100)
#        reg2 = tree.ExtraTreeRegressor(min_samples_split=5) #max_depth=14)
        reg2 = neighbors.KNeighborsRegressor(n_neighbors=1) #, weights='distance')

#        sel = True
        reg2.fit(Astar[inds][:,:1], r2star[inds,0])

        A, B = reg2.kneighbors(col_gal[:,:1])
        A = A[:,:1]

        R = A.T - col_gal.T[0]
        R = R.T
        M = R.mean(axis=1)
        cat['M'] = M

#        ipdb.set_trace()

        if False:
            z = np.linspace(0.1, 2.0, 10)
            K = {'histtype': 'step', 'bins': 10, 'normed': True}
            for i,(key, val) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
                if not i in [1, 3,5, 7]: continue

                val.M.hist(label=key, **K)

            plt.legend()
            plt.show()

        cat = cat[cat.mag_vis < 24.5]

        z = np.linspace(0.1, 2.0, 8) #10)
        m = np.linspace(-1.5, 1.5, 20)

#        mean = cat.groupby([pd.cut(cat.zs, z), pd.cut(cat.M, m)]).mean()

        for key, val in cat.groupby(pd.cut(cat.zs, z)):
            mean = val.groupby(pd.cut(val.M, m)).mean()

            plt.plot(mean.M, mean.eff1, 'o-', label=key)


        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.axhline(0, ls='--', lw=2., color='black')
        plt.axvline(0, ls='--', lw=2., color='black')

        plt.xlabel('(Nearest star - Gal)${|}_{r-vis}$', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)

        plt.legend()
        plt.show()


        import sys
        sys.exit(1)


        ipdb.set_trace()




        for key, sub in cat.groupby(pd.cut(cat.zs, z)):
            plt.plot(sub.M, sub.eff1, '.', label=key)

        plt.legend()
        plt.show()


#            ipdb.set_trace()

        if False:
            z = np.linspace(0.1, 2.0, 15)
            mean = cat.groupby(pd.cut(cat.zs, z)).mean()
            plt.plot(mean.zs, mean.M, 'o-')
            plt.show()


        rx1 = reg1.predict(col_gal)
        rx2 = reg1.predict(col_gal-M.mean())
        rx3 = reg1.predict(col_gal+M.mean())

        r2true = pd.Series(r2true)
        re1 = (rx1 - r2true) / r2true
        re2 = (rx2 - r2true) / r2true
        re3 = (rx3 - r2true) / r2true

        K = {'histtype': 'step', 'bins': 100, 'normed': True}
        re1.hist(label='Normal', **K)
        re2.hist(label='Minus', **K)
        re3.hist(label='Plus', **K)

        plt.legend()
        plt.show()


        import sys
        sys.exit(1)
#        ipdb.set_trace()



        #### Below is the basic check plots. ####

        cat['eff1'] = (r2pred1 - r2true) / r2true
        cat['eff2'] = (r2pred2 - r2true) / r2true

        if False:
            K = {'histtype': 'step', 'bins': 100, 'normed': True}
            cat.eff1.hist(label='NuSVR', **K)
            cat.eff2.hist(label='Changed', **K)

            plt.legend()
            plt.show()

        if False:
            z = np.linspace(0.1, 2.0, 10)

            mean = cat.groupby(pd.cut(cat.zs, z)).mean()

            ax = plt.gca()
            ax.xaxis.grid(True, which='both')
            ax.yaxis.grid(True, which='both')

            plt.plot(mean.zs, mean['eff1'], 'o-', label='NuSVR')
            plt.plot(mean.zs, mean['eff2'], 'o-', label='Changed')

            plt.legend()
            plt.show()

#        ipdb.set_trace()



    def run(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 50000


            B[lbl] = A


        self.d1(B)

if __name__ == '__main__':
    Job('x13').run()
