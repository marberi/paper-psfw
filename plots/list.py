#!/usr/bin/env python
# encoding: UTF8

import glob
import imp
import ipdb
import numpy as np

for file_name in glob.glob('*.py'):
    if 'list' in file_name: continue

    cls = file_name.replace('.py', '')

    mod = imp.load_source(cls, file_name)
    if not hasattr(mod, cls):
        continue

    print(cls, ':', getattr(mod,cls).__doc__)
#    ipdb.set_trace()
