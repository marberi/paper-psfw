#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from xdolphin import Job
import xdolphin as xd

from matplotlib import pyplot as plt
import trainpipel

class x10:
    name = 'x10'
    config = {}

    def d1(self):

        pipel = trainpipel.pipel()
        ab = pipel.depend['galcat.magcat.ab']

#        ipdb.set_trace()
        ab_star = pipel.depend['starcatN.ab']
        ab_nb = Job('ab_nb')
        ab_nb_star = Job('ab_nb')

        # Adjusting the wavelength range:
        for j in [ab_nb, ab_nb_star]:
#            pass
            j.config['xa'] -= 400
            j.config['xb'] += 400

        # Copy over some configuration options...
        for f in ['data_dir', 'sed_dir', 'filter_dir']:
            ab_nb.config[f] = ab.config[f]
            ab_nb_star.config[f] = ab_star.config[f]

        N = 20 # 11 #3# 37 #11 #3 #7
        ab_nb.depend['ab'] = ab
        ab_nb.config['N'] = N
        galcat_nb = pipel.depend['galcatN'] #.clone()
        galcat_nb.depend['ab'] =  ab_nb

        ab_nb_star.depend['ab'] = ab_star
        ab_nb_star.config['N'] = N
        starcat_nb = pipel.depend['starcatN'] #.clone()
        starcat_nb.depend['ab'] =  ab_nb_star

        nb = map(str, range(N))
        filters = galcat_nb.config['filters']
        filters += nb

        starcat_nb.config['filters'] += nb

#        galcat_nb.config['filters'] += map(str, range(N))
        use_stars = True

        galcat = galcat_nb['magcat']['magcat']
        galcat = galcat.join(pipel.depend['r2gal']['r2eff']['r2eff'])

        if False: #True:
            gc = galcat
            for fname in ['r', '0', '5', '10']:
                plt.plot(gc.mag_vis - gc['mag_'+fname], gc.r2eff, '.', label=fname)
    #        plt.plot(gc.mag_vis - gc.mag_r, '.')
            plt.xlabel('Color', size=16)
            plt.ylabel('$"R^2_{\\rm{PSF}}"$', size=16)

            plt.legend()
            plt.show()

#        ipdb.set_trace()

        if use_stars:
            starcat = starcat_nb['magcat']['magcat']
            starcat = starcat.join(pipel.depend['r2star']['r2eff']['r2eff'])

            sc = starcat
#            ipdb.set_trace()


        bb = ['r', 'i', 'z']
        toselL = [('bb', bb), ('nb', nb), ('all', bb+nb)]

        from sklearn import svm, neighbors, ensemble
        Ntrain = 400 #400 #400 #400 #400

        K = {'bins':100, 'histtype': 'step', 'normed': True}

        galcat['r2true'] = galcat['r2eff']
        starcat['r2true'] = starcat['r2eff']

        for lbl, tosel in toselL:
            train_cat = starcat if use_stars else galcat

            mag_keys = map('mag_{0}'.format, tosel)
            sub = train_cat[mag_keys]

            inds_train = np.random.permutation(np.arange(len(train_cat)))[:Ntrain]
            mag_train = sub.ix[inds_train].values

            Atrain = mag_train[:,1:] - mag_train[:,:-1]
            Btrain = train_cat.r2true[inds_train].values

#            reg = svm.NuSVR(degree=15, C=2.)
#            reg = svm.NuSVR() #degree=15, C=2.)
#            reg = neighbors.KNeighborsRegressor(n_neighbors=40, weights='distance')
            reg = svm.NuSVR()
#            reg = ensemble.GradientBoostingRegressor(n_estimators=1000) #, subsample=.1) #loss='exponential') #200)

            reg.fit(Atrain, Btrain) 

            mag_test = galcat[mag_keys].values
            Atest = mag_test[:,1:] - mag_test[:,:-1]

#            ipdb.set_trace()

            r2pred = reg.predict(Atest)

            r2eff = (r2pred - galcat.r2true) / galcat.r2true
#            r2eff.hist(label=lbl, **K)

            galcat['r2eff_'+lbl] = r2eff
            galcat['r2pred_'+lbl] = r2pred

#        galcat = galcat[1. < galcat.zs]

        galcat = galcat.join(pipel.depend['galcat.magcat.mabs']['mabs']['cat'][['sed']])
        mean_sed = galcat.groupby(pd.cut(galcat.sed,range(65))).mean()

        for lbl,_ in toselL:
            plt.plot(mean_sed.sed, mean_sed['r2eff_'+lbl], 'o-', label=lbl)

        plt.legend()
        plt.show()

        ipdb.set_trace()

        if True: #urFalse: #True: #False:
            z = np.linspace(0.1, 2., 20)
            mean = galcat.groupby(pd.cut(galcat.zs, z)).mean()
            for lbl, _ in toselL:
                plt.plot(mean.zs, mean['r2eff_'+lbl], 'o-', label=lbl)

            plt.legend()
            plt.show()

        if False:
            keys = ['r2pred_'+x for (x,y) in toselL]+['r2true']
            for key in keys:
                galcat[key].hist(bins=100, histtype='step', normed=True, label=key)

        for lbl,_ in toselL:
            galcat['r2eff_'+lbl].hist(bins=100, histtype='step', normed=True, label=lbl)


        plt.legend()
        plt.show()

#        for lbl, _ in toselL:
#        ipdb.set_trace()


    def run(self):
        self.d1()

if __name__ == '__main__':
    Job('x10').run()
