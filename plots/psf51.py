#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from xdolphin import Job

import psf10
import psf12

class psf51:
    name = 'psf51'
    config = {}

    def d1(self, B):
        E = B['noiseless']['r2train']
        E.config['reg'] = 'linear'

        galcat = B['noiseless']['galnoise']['magcat']['cat']

        ans = E['r2train']['r2eff']
        cat = galcat.join(ans)

        cat = cat[cat.mag_vis < 24.5]
#        cat[0].hist(bins=100)
#        cat.me
#        plt.show()
#        ans.mean().hist()

        ipdb.set_trace()

    def get_col(self, cat, filtersL, errors=False): 
        mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
        col = mag[:,:-1] - mag[:,1:]

        if errors:
            err = cat[['err_{0}'.format(x) for x in filtersL]].values
            return col, mag, err
        else: 
            return col, mag
#        ipdb.set_trace()

    def der_step(self, col, offsets, fL):
        off_mag = np.array([offsets[x] for x in fL])
        off_col = off_mag[:-1] - off_mag[1:]

        col_new = col.copy()
        col_new = col_new + off_col

        return col_new

    def d2(self, B):
        key = 'noisy'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal = self.get_col(galcat, fL)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm,neighbors
        reg = svm.NuSVR()
        reg.fit(Astar[inds], r2star[inds,0])


        dens_noise = True 
#        dens_noise = False


        # Fit the density estimation.
        regL = []
        dens_cat = galcat if dens_noise else B['noiseless']['galnoise']['magcat']['cat']

        dz = 0.1 #1
        dp5 = 0.5*dz

        zedges = np.linspace(0., 2.5, 100)
        zmean = 0.5*(zedges[:-1] + zedges[1:])

        # It might have slightly strange edge effects.
        za = zmean - dp5
        zb = zmean + dp5

#        za = zedges[:-1]
#        zb = zedges[1:]

        for i in range(len(za)):
            zai = float(za[i])
            zbi = float(zb[i])
            cat_tmp = dens_cat[(zai <= dens_cat.zs) & (dens_cat.zs < zbi)]
#            cat_tmp = cat_tmp[cat_tmp.mag_vis < 25.0]

            _, mag_tmp = self.get_col(cat_tmp, fL)

            reg_dens = neighbors.NearestNeighbors(n_neighbors=10)
            reg_dens.fit(mag_tmp) 

            regL.append(reg_dens)


        # Function evaluation.
        ev0 = reg.predict(col_gal)
        r2pred = ev0

        # 1 Step
        nf = len(fL)
        ev1 = np.zeros((nf, len(cat)))
        delta_mag = 0.01 
        for i,fname in enumerate(fL):
            offsets = {'r': 0., 'i': 0., 'z': 0.}
            offsets[fname] = delta_mag

            col_ev = self.der_step(col_gal, offsets, fL)
            ev1[i] = reg.predict(col_ev)


        # 2 steps
        ev2 = np.zeros((nf, nf, len(cat)))
        for i,fname1 in enumerate(fL):
            for j,fname2 in enumerate(fL):
                print('second', i, j)
                offsets = {'r': 0., 'i': 0., 'z': 0.}

                # Includes two steps along the same filter.
                offsets[fname1] = delta_mag
                offsets[fname2] += delta_mag

                col_ev = self.der_step(col_gal, offsets, fL)
                ev2[i,j] = reg.predict(col_ev)

        # Generate some Gaussian realizations...
        n_real = 100
        R = np.random.normal(size=(len(fL), len(cat), n_real))
        err = cat[['err_{0}_noisy'.format(x) for x in fL]].values.T
        R = np.einsum('in,ink->ink', err, R)



        # Trying to evaluate the "R^2_{\\rm{PSF}}" in all the different points...
        R2_eval = np.zeros((len(cat), n_real))
        for i in range(n_real):
            print('eval i', i)
            mag_tmp = mag_gal + R[:,:,i].T
            col_tmp = mag_tmp[:,:-1] - mag_tmp[:,1:]

            R2_eval[:,i] = reg.predict(col_tmp)

        if False:
            # Looking at how the terms look.
            B = (R2_eval.T - r2true).T

            ipdb.set_trace()


        # Estimate the volume at the different locations in magnitude
        # space.
        n_dim = mag_gal.shape[1]
        vol = np.zeros((mag_gal.shape[0], n_real))
        reg_ind = np.digitize(cat.zs, zedges) - 1

        # Doing two loops might be suboptimal... I wanted some
        # quick results. 


        # One less to compare with vol0
        dist_ind = -1 if dens_noise else -2
        for i in range(n_real):
            print('real', i)
            mag_tmp = mag_gal + R[:,:,i].T

            for j,reg in enumerate(regL):
                print('vol', i, j)
                to_update = reg_ind == j

                A = reg_dens.kneighbors(mag_tmp[to_update]) 
                n_neigh = A[0].shape[1]
                dist = A[0][:,dist_ind] 

                vol[to_update,i] = dist**n_dim






        # Add a correct normalization.
        vol0 = np.zeros(len(mag_gal))
        for j,reg in enumerate(regL):
            to_update = reg_ind == j

            A = reg_dens.kneighbors(mag_gal)
            dist = A[0][:,-1]
            vol0 = dist**n_dim

        if False: #True:
            S0 = pd.Series(vol0)
            S1 = pd.Series(vol.flatten())

            ipdb.set_trace()
         
            lim = 0.01
            S0 = S0[S0 < lim] 
            S1 = S1[S1 < lim] 
            K = {'bins': 100, 'histtype': 'step', 'normed': True} 
            S0.hist(label='Vol0', **K)
            S1.hist(label='Sample', **K)

            plt.legend()
            plt.show()

        if False:
            B = (vol.T / vol0).T
            pd.Series(B.flatten()).hist(bins=2000)

            ipdb.set_trace()
            plt.show()


        vol = (vol.T / vol0). T

#        vol = 1./vol

        rmax = 10. #104.
        vol = np.clip(vol, 0./rmax, rmax) #0.2, 5)

#        vol = vol*(vol < rmax)
        # A second normalization.
#        vol_norm = vol.sum(axis=1)
#        vol = (vol.T / vol_norm).T

        # Estimate the first and second derivative.
        der1 = np.zeros_like(ev1)
        for i, fname in enumerate(fL):
            der1[i] = (ev1[i] - ev0) / delta_mag

        der2 = np.zeros_like(ev2)
        for i, fname1 in enumerate(fL):
            for j, fname2 in enumerate(fL):
                der2[i,j] = (ev2[i,j] - ev1[i] - ev1[j] + ev0) / delta_mag**2

        # Testing the volume weighting.
        if False:
            H = np.einsum('ink,in->nk', R, der1)
            corr_tot = H.mean(axis=1)



        wL = [False, True]
        sL = [False, True]
        for dens_weight in wL:
#        dens_weight = False
#        dens_weight = True #False

        #corr_tot = np.einsum('ink,in,nk->nk', R, der1,vol).mean(axis=1)
            for has_sec in sL:
                corr_tot = np.einsum('ink,in->nk', R, der1)
                if has_sec:
                    corr_tot += 0.5*np.einsum('ink,jnk,ijn->nk', R, R, der2)


                if dens_weight:
                    corr_tot *= vol


                corr_tot = corr_tot.mean(axis=1)

                # Testing removing the most extreme corrections. Remove
                # later..
#                rel_corr = corr_tot / r2pred
#                no_corr = 0.01 < np.abs(rel_corr)
#                corr_tot[no_corr] = 1.5*corr_tot[no_corr]
       
 
                lbl = '{0} {1}'.format('Dens weight' if dens_weight else 'No dens weight', 
                                       'Second order' if has_sec else 'First order')

                key = '{0}_{1}'.format(1*dens_weight, 1*has_sec)
                k = -1.0 #1.0 #-0.4 #1.0 #-2.5 # HACK
                cat[key] = (k*corr_tot + r2pred - r2true) / r2true
                cat[key+'_k'] = corr_tot
                cat[key+'_rel'] = corr_tot / r2true
                cat['lbl'] = lbl

        # Then try to estimate the "R^2_{\\rm{PSF}}" by using "R^2_{\\rm{PSF}}" estimated at various
        # locations in magnitude space.

        cat['xt_eff'] = (R2_eval.mean(axis=1) - r2true) / r2true

        r2_weighted = (R2_eval * vol).sum(axis=1) / vol.sum(axis=1)
        cat['xt_vol_eff'] = (r2_weighted  - r2true) / r2true

#        ipdb.set_trace()



        cat['r2pred'] = r2pred
        cat['r2true'] = r2true
        cat['r2diff'] = r2pred-r2true

        cat['r2eff'] = cat.r2diff / cat.r2true

        store = pd.HDFStore('/data2/marberi/tmp/to_debug.h5', 'w')
        store['cat'] = cat
        store.close()

        import sys
        sys.exit(1)

        ipdb.set_trace()

#        ipdb.set_trace()
#        cat = cat[cat

        kl = np.linspace(-0.05, 0.05)

        # Testing also the redshift dependence of the correction.
        mean = cat[1.5 < cat.zs].groupby(pd.cut(cat.r2eff, kl)).mean()

        for dens_weight in wL:
            for has_sec in sL:
#                key = '{0}_{1}_k'.format(1*dens_weight, 1*has_sec)
#                plt.plot(mean['r2diff'], mean[key], 'o-', label=key)

                key = '{0}_{1}_rel'.format(1*dens_weight, 1*has_sec)
                lbl = '{0} {1}'.format('Dens weight' if dens_weight else 'No dens weight',
                                       'Second order' if has_sec else 'First order')

#                ipdb.set_trace()
                y = mean[key] #/ cat['r2true']
                plt.plot(mean['r2eff'], y, '-', label=lbl)


        plt.plot(kl, kl)
#        plt.plot(mean['r2diff'], mean['0_0_k'], 'o-')
#        plt.plot(mean['r2diff'], mean['0_1_k'], 'o-')

#        plt.plot(xline, xline)
#        plt.plot(-xline, xline)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xlabel('$"R^2_{\\rm{PSF}}"$ pred - $"R^2_{\\rm{PSF}}"$ true', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ correction', size=16)

        plt.legend() 
        plt.show()


        import sys
        sys.exit(1)
        # 

        cat = cat[(0.5 < cat.zs) & (cat.zs < 0.51)]
        xline = np.linspace(0., 2.)

        plt.plot(cat['r2pred'] - cat['r2true'], cat['1_1_k'], '.')

        plt.plot(xline, xline)
        plt.show()


        ipdb.set_trace()


        cat['r2eff'] = (r2pred - r2true) / r2true

#        ipdb.set_trace()

#        cat = cat[cat.mag_vis <24.5] # =

        z = np.linspace(0.1, 2.0, 15)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        plt.plot(mean.zs, mean.r2eff, 'o-', label='Orig')
        for i,w in enumerate(wL):
            for j,s in enumerate(sL):
                key = '{0}_{1}'.format(i,j)

                plt.plot(mean.zs, mean[key], 'o-', label=key)


        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')


        plt.legend()
        plt.show()

        import sys
        sys.exit(1)

#                ipdb.set_trace()

        for i,k in enumerate(kL):
            plt.plot(mean.zs, mean['l'+str(i)], 'o-', label='k = {0:.2f}'.format(k))

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.legend()
        plt.show()



        ipdb.set_trace()


#            ipdb.set_trace()
        #ipdb.set_trace()


        if False:
            H = np.einsum('ink,jnk,ijn->nk', R, R, der2) 
            corr_tot = 0.5*H.mean(axis=1)

        cat['r2eff'] = (r2pred - r2true) / r2true

        if False:
            k = 0.2
            c1 = (k*corr_tot + r2pred - r2true) / r2true
            c2 = (-k*corr_tot + r2pred - r2true) / r2true

            cat['c1'] = c1
            cat['c2'] = c2
                
            K = {'bins': 20, 'histtype': 'step', 'normed': True}
            cat.r2eff.hist(label='Orig', **K)
            cat.c1.hist(label='c1', **K)
            cat.c2.hist(label='c2', **K)
            plt.legend()
            plt.show()


            ipdb.set_trace()

        kL = [-1, -0.2,0.2,1.] # 0.5, 0.8, 1.0, 1.5]
#        kL = [-0.1, -0.01,0.01, 0.1] # 0.5, 0.8, 1.0, 1.5]
        for i,k in enumerate(kL):
            c = (k*corr_tot + r2pred - r2true) / r2true
#            c = (-k*corr_tot + cat['0_pred_noisy'] - cat['0_true_noisy']) / cat['0_true_noisy']
            cat['l'+str(i)] = c

        cat = cat[cat.mag_vis < 24.5]
 
        z = np.linspace(0.1, 2.0, 15)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        plt.plot(mean.zs, mean.r2eff, 'o-', label='Orig')
        for i,k in enumerate(kL):
            plt.plot(mean.zs, mean['l'+str(i)], 'o-', label='k = {0:.2f}'.format(k))

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.legend()
        plt.show()

        import sys
        sys.exit()
#        cat = cat[(0.5 < cat.zs) & (cat.zs < 0.6)]

        # Testing the results. 

        # The first test.
        if False: #True: #False:
            K = {'bins': 100, 'histtype': 'step', 'normed': True}
            cat['0_eff_noisy'].hist(label='Orig', **K)
            cat.c1.hist(label='c1', **K)
            cat.c2.hist(label='c2', **K)
            plt.legend()
            plt.show()


        z = np.linspace(0.1, 2.0, 15)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.plot(mean.zs, mean['0_eff_noisy'], 'o-', label='Orig')
        plt.plot(mean.zs, mean['c1'], 'o-', label='c1')
#        plt.plot(mean.zs, mean['c2'], label='c2')

        plt.legend()
        plt.show()


    def d3(self, B):
        key = 'noisy'
        F = B[key]

        galcat = B[key]['galnoise']['magcat']['cat']
#        ans = F['r2train']['r2train']['r2eff']

#        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')

#        for N in [100,200,500,1000,10000, 100000, 1000000000]:
#            t1 = time.time()
#            reg.kneighbors(col_gal[:N])
#            t2 = time.time()
#
#            print('time', N, t2-t1)

#        for n in [3,5,7,15]:
#            reg = neighbors.NearestNeighbors(n_neighbors=n)
#
#            t1 = time.time()
#            reg.fit(col_gal)
#            t2 = time.time()
#
#            reg.kneighbors(col_gal)
#
#            t3 = time.time()
#            print(n, t2-t1, t3-t2)


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
#        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
#        Astar, _ = self.get_col(star_cat, fL)
#        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
#        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal = self.get_col(galcat, fL)

        from sklearn import svm,neighbors
        reg = neighbors.NearestNeighbors()
        reg.fit(col_gal)

        if False:
            import time
            n = 100
            reg = neighbors.NearestNeighbors(n_neighbors=n)
            reg.fit(mag_gal)

            A = reg.kneighbors(mag_gal)

            dist = A[0]
            mean = dist.mean(axis=0)

            plt.plot(np.arange(len(mean)), mean, 'o-', label='All')

            plt.legend()
            plt.show()


        if False:
            ker = neighbors.KernelDensity()
            ker.fit(mag_gal)

            import time
            for n in [100, 200]:
                t1 = time.time()
                ker.score_samples(mag_gal[:n])
                t2 = time.time()

                print(n, t2-t1)


        reg = neighbors.RadiusNeighborsRegressor()
        reg.fit(mag_gal, np.ones(mag_gal.shape[0]))

        import time
        for n in [100, 200, 1000]:
            t1 = time.time()
#            ker.score_samples(mag_gal[:n])
            reg.radius_neighbors(mag_gal[:n])
            t2 = time.time()

            print(n, t2-t1)

        ipdb.set_trace()


    def d3(self, B):
        key = 'noisy'
        F = B[key]

        galcat = B[key]['galnoise']['magcat']['cat']

        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
#        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
#        Astar, _ = self.get_col(star_cat, fL)
#        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
#        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal = self.get_col(galcat, fL)

        from sklearn import svm,neighbors
        reg = neighbors.NearestNeighbors()
        reg.fit(col_gal)


        ipdb.set_trace()


    def d4(self, B):
        key = 'noisy'
        F = B[key]

        delta = np.linspace(-0.01, 0.01)
#        fname = 'r'
        galcat = B[key]['galnoise']['magcat']['cat']
        ans = F['r2train']['r2train']['r2eff']

        cat = galcat.join(ans)
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2eff'].rename(columns=lambda x: str(x)+'_eff_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2pred'].rename(columns=lambda x: str(x)+'_pred_noisy'))
#        cat = cat.join(B['noisy']['r2train']['r2train']['r2true'].rename(columns=lambda x: str(x)+'_true_noisy'))
        cat = cat.join(B['noisy']['galnoise']['magcat']['cat'], rsuffix='_noisy')


        fL = ['r', 'i', 'z']

        Ntrain = 400

        # Setting up the training method.
        star_cat = F['r2train'].depend['starcat']['magcat']['cat']
        Astar, _ = self.get_col(star_cat, fL)
        r2star = F['r2train'].depend['r2star']['r2eff']['r2eff'].values
        inds = np.random.permutation(np.arange(len(r2star)))[:Ntrain]

        col_gal, mag_gal, err_gal = self.get_col(galcat, fL, True)
        r2gal = F['r2train'].depend['r2gal']['r2eff']['r2eff']['r2eff'].values
        r2true = r2gal

        from sklearn import svm    
        reg = svm.NuSVR()
        reg.fit(Astar[inds], r2star[inds,0])

        # The unweighted predictions.
        r2pred = reg.predict(col_gal)

        W = 1./err_gal**2.
        R = mag_gal[:1000, :]

        ipdb.set_trace()



    def run(self):
        inst = psf10.psf10()

        topo = {'add_pz': True}
        task_names = ['galnoise', 'r2train', 'galabs', 'pzcat']
        B = {}
        for lbl, inst in [('noiseless', psf10.psf10()),
                          ('noisy',     psf12.psf12())]:
            myconfL, topnode = inst.create_topnode(topo, task_names)
            A = dict([(key[1],task) for (key,task) in topnode.depend.iteritems() if key[0]==(0,1)])
            A['r2train'].config['Nboot'] = 3 #0 #use_stars'] = True
#            A['r2train'].config['filters'] = ['r', 'i']

#            A['r2train'].config['use_stars'] = False
            A['r2train'].config['all_test'] = True #use_stars'] = False

            A['galabs'].config['Ngal'] = 50000

#            ipdb.set_trace()
#            A['r2train'].config['Ntest'] = 200000
#            if lbl == 'noiseless':
#                A['galabs'].config['Ngal'] = 200008

            B[lbl] = A


        self.d4(B)

if __name__ == '__main__':
    Job('psf51').run()
