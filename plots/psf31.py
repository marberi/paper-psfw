#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import sys

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick

from xdolphin import Job
import xdolphin as xd

import matplotlib as mpl


def find_seds():
    """Read in the list of all SEDs."""

    fjc_file = '/nfs/pic.es/user/e/eriksen/papers/psfw/data/fjc_temp'
    seds = map(str.strip, open(fjc_file).readlines())

    return seds

def gen_styles():
    i = 0
    styles = ['-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1

def top_concat(D, name='top'):
    """Concat together different topnode objects."""

    # This functionality is more general.
    nodes = {}
    labels = pd.DataFrame()

    pipel_counter = 0
    for top_name, top in D.iteritems():
        label_part = top.labels.copy().reset_index()
        label_part[name] = top_name

        L = [(key+pipel_counter, val) for key,val in top.nodes.iteritems()]
        nodes.update(dict(L))

        label_part.pipel += pipel_counter
        labels = labels.append(label_part, ignore_index=True)

        pipel_counter += len(label_part)

    labels = labels.set_index('pipel')
    top = xd.Topnode(nodes, labels)

    return top


class Plot:
    """The 65 SED plot."""

    name = 'psf31'

    def topnode(self):
        import pzpipel

#        xtop_base = xd.Topnode(pzpipel.pipel())

#        xtop_base.update_config({\
#          'r2pz.pzcat.galcat.not_scale': ['Y', 'J', 'H'],
#          'r2pz.pzcat.dz': 0.001,
#          'r2pz.pzcat.out_pdf': False,
#          'r2pz.pzcat.out_pdftype': False,
#          'r2pz.pzcat.use_priors': False,
#          'r2pz.pzcat.zmax': 5.,
#          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 10002,
#          'r2pz.use_ml': False
#        })


        config = {\
          'r2pz.pzcat.galcat.not_scale': ['Y', 'J', 'H'],
          'r2pz.pzcat.dz': 0.001,
          'r2pz.pzcat.out_pdf': False,
          'r2pz.pzcat.out_pdftype': False,
          'r2pz.pzcat.use_priors': False,
          'r2pz.pzcat.zmax': 5.,
          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 10002,
          'r2pz.use_ml': False
        }

        pipel = pzpipel.pipel()
        layers = xd.Layers()
        layers.add_config(config)
        layers.add_pipeline(pipel)

        # The x-axis and panel split.
        R = [0.5, 0.6, 0.7, 0.75, 0.8, 0.9, 1, 2,3,4, 5,7, 10, 20, 50, 70, 100]

        layers.add_layer_key('limit_sed', 'r2pz.pzcat.galcat.magcat.mabs.limit_seds', [True, False])
        layers.add_layer_key('r', 'r2pz.pzcat.galcat.r', R)

        all_filters = ['g', 'r', 'i', 'z', 'Y', 'J', 'H']

        part = 1

        # Left panel
        layers1 = layers.copy()
        layers1.add_layer_key('filters', 'r2pz.pzcat.filters', [['r', 'i', 'z'], all_filters])
        layers1.add_layer_key('priors', 'r2pz.pzcat.use_priors', [True, False])

        layers2 = layers.copy()
        layers2.add_layer('fake', [{'r2pz.pzcat.seds': find_seds(), 'r2pz.pzcat.use_priors': False}])
        layers2.add_layer_key('filters', 'r2pz.pzcat.filters', [['r', 'i', 'z'], all_filters])

        top1 = layers1.to_topnode()
        top2 = layers2.to_topnode()
        top = top_concat({'left': top1, 'right': top2}, name='panel')

        return top

    def f_reduce(self, pipel):

        # Move this layer of indirection outside of the prepare
        # object.
        ptop = pipel

#        ipdb.set_trace()
#        df = pd.DataFrame()
        galcat = pipel.r2pz.pzcat.galcat.result
        r2pz = pipel.r2pz.result

        cat = galcat.join(r2pz)
        cat = cat[cat.mag_vis < 24.5]

        mean = cat.mean()
        std = np.sqrt(cat.var())

        filters = pipel.r2pz.pzcat.config['filters']
        priors = pipel.r2pz.pzcat.config['use_priors']

#        D = pd.Series()
        D = {}
        D['r2peak'] = mean['r2peak']
#        D['priors'] = pipel.r2pz.pzcat.config['use_priors']
        D['filters_flat'] = pipel.r2pz.pzcat.config['filters']
        D['limit_seds'] = pipel.r2pz.pzcat.config['galcat.magcat.mabs.limit_seds']
        D['sed_interp'] = pipel.r2pz.config['pzcat.interp']
        D['nrseds'] = len(pipel.r2pz.config['pzcat.seds'])

        D['filters_flat'] = [','.join(D['filters_flat'])]

        res = pd.DataFrame(D)

#        ipdb.set_trace()

        return res

    def set_labels(self, prepare):

        # This is not the right way of setting the labels, but
        # changing the labeling technique is something which will
        # first happen later.

        df = prepare

        # This code is blindly copied from an earlier
        # version.
        has_priors = df.priors == 1
        df.loc[has_priors, 'lbl_priors'] = 'Priors'
        df.loc[~has_priors, 'lbl_priors'] = 'No priors'

        # Ok, maybe not the best..
        df['lbl_filters'] = df.filters_flat
        inds_all = np.array([len(x.split(',')) == 7 for x in df.lbl_filters])
        df.loc[inds_all, 'lbl_filters'] = 'All'


    def plot(self, prepare):

        # Version only including two panels, one for all SEDS and another
        # when limiting the SEDs in the template fitting.
        col_data  = [(True, 'Limited SEDs [6 SEDs]'),
                     (False, 'Full simulations [66 SEDs]')]

        fig, A = plt.subplots(1, len(col_data), sharey='row', sharex=True)

        self.set_labels(prepare)

        fig.set_size_inches(15, 6.)

        for i,(col,col_title) in enumerate(col_data):
            sub = prepare[prepare.limit_seds == col]

            ax = A[i]
            styles = gen_styles()
            for key, Y in sub.groupby(['nrseds', 'lbl_priors', 'lbl_filters', 'sed_interp']):
                Y = Y.sort_values('r')
                frow = Y.iloc[0]

                lbl = '{0: <6} - {1: <4}SEDs - {2}'.format(frow.lbl_filters, str(int(frow['nrseds'])), frow.lbl_priors)


                ls = next(styles)
                print('line style', ls)
                ax.plot(Y['r'], Y.r2peak, lw=2, label=lbl, ls=ls)

#            ax.set_yscale('log')
            ax.set_ylim(-0.002, 0.002)
            ax.set_ylim(-0.0015, 0.001)
            ax.xaxis.grid(which='both')
            ax.yaxis.grid(which='both')
            ax.legend(loc=4, prop={'size': 12}, framealpha=1.0)

            t = ax.set_title(col_title, size=16) #, y=0.8)
            t.set_y(1.01)

#        for i,title in enumerate(titles):
#            A[i].set_title(title)

        # x-label
        for ax in A: 
            ax.set_xlabel('Exposure times / Fiducial value', size=18)


        for ax in A:
            ax.set_xscale('log')
#            ax.set_xlim(0.4, 12)

        # Requirement band.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        for ax in A:
            ax.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

            ax.axvline(1, color='k', lw=3, alpha=0.5)

        # Fix both axis.
        for ax in A:
#            ax.xaxis.set_major_locator(MaxNLocator(prune='both', nbins=10))
            ax.set_xticks([1.,10.])
            ax.set_xlim((0.4, 12)) #ax.get_xlim()[1])) # Only change the lower limit.

#            ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2), useOffset=False)
#            ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.1e'))
            plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            ax.yaxis.major.formatter._useMathText = True

            ax.xaxis.set_tick_params(size=4)

        params = {\
          'xtick.labelsize': 12,
          'ytick.labelsize': 1}

        for ax in A:
            ax.tick_params(axis='both', which='major', labelsize=12)

        plt.tight_layout()
        plt.subplots_adjust(left=0.09, wspace=0.1, right=0.97) #, wspace=0.15)
        
        A[0].set_ylabel('$\delta R^2_{\\rm{PSF}}\, /\, R^2_{\\rm{PSF}}$', size=15)

if __name__ == '__main__':
    xd.plot(Plot).save()
