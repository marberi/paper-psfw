#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np
import pandas as pd
from scipy.interpolate import splrep, splev
from matplotlib import pyplot as plt

import trainpipel
import tosave

def get_col(cat, filtersL, errors=False):
    mag = cat[['mag_{0}'.format(x) for x in filtersL]].values
    col = mag[:,:-1] - mag[:,1:]

    if errors:
        err = cat[['err_{0}'.format(x) for x in filtersL]].values
        return col, mag, err
    else:
        return col, mag

def gen_styles():
    i = 0
    styles = [':', '-','-.','--']
    while True:
        yield styles[i % len(styles)]
        i += 1


class Plot:
    def topnode(self):
        pipel = trainpipel.pipel()

        return pipel

    def get_df(self, pipel, rL):
        # Then looking 
        star_cat = pipel.r2x.train_star_cat.result
        star_cat = star_cat.join(pipel.r2x.train_star_r2.result)

        gc = pipel.r2x.test_cat.result
        gc = gc.join(pipel.r2x.test_r2.result)
        gc['r2true'] = gc.r2eff

        # Move to topnode..
        pipel.r2x.cal_cat.magcat.mabs.config['Ngal'] = 500000

        Ntrain = 400
        fL = ['r', 'i', 'z']

        Astar, _ = get_col(star_cat, fL)
        col_gal, mag_gal, err_gal = get_col(gc, fL, True)

        inds = np.random.permutation(range(len(Astar)))[:Ntrain]

        Atrain = Astar[inds]
        Btrain = star_cat.r2eff.values[inds]
        from sklearn import svm


        #if False:
        cal_cat = pipel.r2x.cal_cat.result
        cal_cat = cal_cat.join(pipel.r2x.cal_r2.result)
        cal_cat['r2true'] = cal_cat.r2eff
        cal_cat = cal_cat[cal_cat.mag_vis < 24.5]

        col_cal, mag_cal, err_cal = get_col(cal_cat, fL, True)

        print('Training')
        reg = svm.NuSVR()
        reg.fit(Atrain, Btrain)

        df = pd.DataFrame({'zs': cal_cat.zs})

        print('Predicting')
        for i, ri in enumerate(rL):
            col_tmp = col_cal.copy()
            col_tmp[:,0] += ri
            r2pred = reg.predict(col_tmp)
            r2bias = (r2pred - cal_cat.r2true) / cal_cat.r2true

            df[i] = r2bias

        # Experiment with estimating the optimal r-band shift as a
        # function of redshift.
        print('Determine the optimal shift')
        zsplit = np.arange(0.0, 2.2, 0.05)

        tmp = pd.DataFrame()
        for key, sub in df.groupby(pd.cut(df.zs, zsplit)):
            r2bias = sub[range(len(rL))].mean(axis=0)
            part = pd.DataFrame({'r2bias': r2bias, 'lum': rL, 'z': sub.zs.mean()})
            tmp = tmp.append(part, ignore_index=True)

        from scipy.interpolate import SmoothBivariateSpline
        spl = SmoothBivariateSpline(tmp.z, tmp.r2bias, tmp.lum)

        # Evaluate the spline...
        zev = np.arange(0.0, 2.2, 0.05)
        v2 = np.zeros_like(zev)

        r2shift = spl.ev(zev, v2)

        return df, r2shift, zev
 
    def plot(self, pipel):
        rL = np.linspace(-0.2, 0.2, 50)
        #rL = np.linspace(-0.1, 0.1, 10)
        rL = np.linspace(-0.1, 0.1, 6)
        
        df, r2shift, zev = self.get_df(pipel, rL)

        ax1 = plt.gca()
        ax2 = ax1.twinx()

        z = np.arange(0.0, 2.2, 0.4)
        mean = df.groupby(pd.cut(df.zs, z)).mean()

        all_line = [df[x].mean() for x in range(len(rL))]
        ax1.plot(rL, all_line, ls='-', color='black', lw=1.5, label='All')

        T = mean.T
        styles = gen_styles()
        for key,col in mean[range(len(rL))].T.iteritems():
            lbl ='z = {0}'.format(key)
            ls = next(styles)
            ax1.plot(rL, col, ls=ls, label=lbl)

        # Shade the requirement region.
        xline = np.linspace(*plt.xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        ax1.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        ax2.plot(r2shift, zev)

        plt.grid(True)

        xlim = 0.1
        plt.xlim((-xlim,xlim))
        plt.xlabel('r-band offset', size=16)
        plt.ylabel('$"R^2_{\\rm{PSF}}"$ bias', size=16)
        plt.legend(loc=2)
        plt.subplots_adjust(left=0.15)

        plt.savefig(tosave.path('psf83'))

    def run(self):
        pipel = self.topnode()
        self.plot(pipel)

if __name__ == '__main__':
    Plot().run()
