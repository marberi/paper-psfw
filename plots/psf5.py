#!/usr/bin/env python
# encoding: UTF8

import ipdb
import copy
import numpy as np
from matplotlib import pyplot as plt

import xdolphin as xd
import psf4

class Plot(psf4.Plot):
    """Gaussian bias."""

    name = 'psf5'

    def topnode(self):
        r2gauss = xd.Job('r2gauss')
        pipel = xd.Job('empty')
        pipel.depend['r2gauss'] = r2gauss
        pipel.config.update({'r2gauss.N': 50000})

        biasL = np.logspace(-3., -1.)

        layers = xd.Layers()
        layers.add_pipeline(pipel)

        layers.add_layer_key('bias', 'r2gauss.bias', biasL)

        top = layers.to_topnode()

        return top

    def plot(self, prepare):
        labels = {'bias': 'r2gauss.bias'}
        seds = ['Ell_01', 'Sbc_01', 'Scd_01', 'Irr_01', 'Irr_14']
#        pz = np.array(mean.index) # Not the best...
        colors = ['b', 'r', 'k', 'm', 'g', 'm', 'purple']
        styles = ['-',':','-.','-', '--']

        for i,sed in enumerate(seds):
            sub = prepare[prepare.sed == sed].sort_values('bias')

            plt.plot(sub['bias'], np.abs(sub['mean']), label=sed, ls=styles[i], color=colors[i])

        self.pretty()

        plt.xlabel('$|\Delta z_p|$', size=14)
        plt.ylabel('$|\delta R^2_{\\rm{PSF}}|\, /\, R^2_{\\rm{PSF}}$', size=14)
        plt.ylim(3e-5, 1e-2)

        xline = np.linspace(*plt.xlim())
        y_lower = 0*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        plt.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        yline = np.linspace(*plt.ylim())
        plt.fill_betweenx(yline, 0, 0.002, color='b', alpha=0.1, hatch='/')

        plt.xlim((9e-4, 1.1e-1))
#        plt.axvline(0.002)


if __name__ == '__main__':
    xd.plot(Plot).save()
