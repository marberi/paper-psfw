#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import numpy as np

z = np.linspace(0.1, 2., 10)
z = np.linspace(0.1, 1.8, 6)

for i,(za,zb) in enumerate(zip(z[:-1], z[1:])):
    #print('Bin {0}: {1:.2f}, {2:.2f}'.format(i, za, zb))
    print('[{1:.2f}, {2:.2f})'.format(i, za, zb), end=', ')

#    ipdb.set_trace()
