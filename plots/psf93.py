#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import sys
import ipdb
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

import xdolphin as xd

import intersect

class Plot:
    """Offset in gamma."""

    name = 'psf93'

    def topnode(self):
        import trainpipel

        pipel = trainpipel.pipel()
        pipel.config.update({\
#          'r2x.use_stars': True, #False, #True,
          'r2x.use_stars': False, #True, #False, #True,
          'r2x.filters': ['r','i','z'],
          'r2x.all_test': True,
          'r2x.Ntrain': 4000})
#          'r2x.Ntrain': 400})

        layers = xd.Layers()
        layers.add_pipeline(pipel)

        # Its simpler thinking in term of percentages.
        per = np.array([-2.,-1., 0, 1.,2.])
        gamma = 0.55*(1+per/100.)

        layers.add_layer_key('r2_exp', 'r2x.test_r2.r2vz.r2_exp', gamma)

        fields = ['r2x.use_stars', 'r2x.Ntrain']
        X = [(True, 400), (False, 4000)]

        layer = [dict(zip(fields, x)) for x in X]

        layers.add_layer('use_stars', layer)

        topnode = layers.to_topnode()

        return topnode

    def f_reduce(self, pipel):
        cat = pipel.r2x.test_cat.result[['zs', 'mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]
        N = pipel.r2x.config['Nboot']

        # ..

        df = pd.DataFrame()
        S = pd.Series()
        S['zbin'] = -1
        S['r2bias'] = cat[range(N)].mean().mean() #abs().describe(percentiles=[0.68])['68%']
        df = df.append(S, ignore_index=True)

        z = np.linspace(0.1, 1.8, 7)
        z = np.arange(0.0, 2.2, 0.4)
        z = np.linspace(0.1, 1.8, 6)

        for i,(key,sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
            S = pd.Series()
            S['zbin'] = i
            S['r2bias'] = sub[range(N)].mean().mean()

            S['zlbl'] = key
            df = df.append(S, ignore_index=True)


        return df

    def per(self, r2_exp):
        return 100.*(-1 + r2_exp/0.55)

    def plot(self, prepare):
        labels = {'use_stars': 'r2x.use_stars'}
#        prepare = topnode.reduce(self.prepare)
        inter = intersect.find_inter(prepare, 'r2_exp', ['zbin', 'use_stars'])

        # Very similar to psf92.
        s1 = 0.13
        width = 0.3
        w2 = 0.25

        inter['rxa'] = inter.xa / 0.55 - 1.
        inter['rxb'] = inter.xb / 0.55 - 1.
        sub = inter[['rxa', 'rxb']]
        inter['y0'] = sub.min(axis=1)
        inter['y1'] = sub.max(axis=1)

        binnr = inter.zbin.sort_values().unique().astype(int)
        xlabels = map('bin {0}'.format, binnr+1)
        xlabels[0] = 'all'

#        print(inter)

        stars = inter[inter.use_stars == 1]
        gal = inter[inter.use_stars == 0]

        ax = plt.gca()
        ax.grid(True)

        fstar = {'color': 'black'}
        fgal = {'color': 'blue', 'hatch': '/'}

        # Previously it worked with Series, but not now..
        ax.bar(stars.zbin.values + s1, stars.y0.values, width=w2, label='Star', **fstar)
        ax.bar(stars.zbin.values + s1, stars.y1.values, width=w2, **fstar)
        ax.bar((gal.zbin + width + s1).values, gal.y0.values, width=w2, label='Gal', **fgal)
        ax.bar((gal.zbin + width + s1).values, gal.y1.values, width=w2, **fgal)

        ax.set_ylim((-0.07, 0.07))
        ax.legend()

        # x-axis 
        xpad = 0.1 
        plt.xlim((-1-xpad, binnr[-1]+width+w2+xpad))
        ax.set_xticks(binnr + 0.5*(width+w2))
        ax.set_xticklabels(xlabels, size=15)

        ax.set_ylabel('$\delta \gamma \quad /\quad 0.55 $', size=13)
        plt.subplots_adjust(hspace=0.0, top=0.95, left=0.14, right=0.92)

if __name__ == '__main__':
    xd.plot(Plot).save()
