#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave
import psf90

class Plot(psf90.Plot):
    set_lim = False
    def prepare(self, pipel):

        Nboot = pipel.r2train.config['Nboot']
        cat = pipel.r2train.get_store()['r2eff']
        cat = cat.join(pipel.r2train.galcat.result)

        z = np.linspace(0.1, 1.8, 7)
        mean = cat.groupby(pd.cut(cat.zs, z)).mean()

        prep = pd.DataFrame()
        prep['zs'] = mean['zs']
        prep['r2bias'] = mean[range(Nboot)].mean(axis=1)
        prep['r2bias_std'] = np.sqrt(mean[range(Nboot)].var(axis=1))

        return prep

    def after_plot(self, A):
        path = tosave.path('psf91')
        plt.savefig(path)

    def add_lines(self, top, ax, sub):
        sub = sub.sort('filters')
        for key,row in sub.iterrows():
            prep = self.prepare(top[key])

            lbl = row['filters']
            ax.errorbar(prep.zs, prep.r2bias, prep.r2bias_std, label=lbl)

        # Shading the region.
        xline = np.linspace(*ax.get_xlim())
        y_lower = -3e-4*np.ones_like(xline)
        y_upper = 3e-4*np.ones_like(xline)
        ax.fill_between(xline, y_lower, y_upper, color='black', alpha=0.1, label='fill')

        ax.grid(True)
        ax.legend(prop={'size': 12}, loc=3)

if __name__ == '__main__':
    Plot().run()
