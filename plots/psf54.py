#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


path = '/data2/marberi/tmp/vol_free.h5'
store = pd.HDFStore(path)
cat = store['cat']
store.close()

ipdb.set_trace()


cat = cat[cat.mag_vis < 24.5]

if False:
    diff = cat['r2pred_w'] - cat['r2pred']
    plt.plot(cat.r2true, cat.r2pred, '.')
    plt.plot(cat.r2true, cat.r2pred_w, '.')
    plt.show()

if False:# True: #True: #True: #eFalse: #False:
    z = np.linspace(0.2, 2.0, 10)
    mean = cat.groupby(pd.cut(cat.zs, z)).mean()
    #plt.plot(mean['zs'], mean['r2eff'], label='Orig')
    for key in ['r2eff', 'r2eff_w']:
        plt.plot(mean['zs'], mean[key], label=key)


    plt.legend()
    plt.show()

#ipdb.set_trace()

if True:
    N = 120
    normed = False
    cat.r2eff.hist(bins=N, histtype='step', normed=normed, label='Orig')
    cat[cat.r2eff_w.abs() < 0.1].r2eff_w.hist(bins=N, histtype='step', normed=normed, label='Weighted')

    plt.legend()
    plt.show()




#### OLD CODE ####

if False:
    x = np.linspace(-0.06, 0.06)
    for key in L:
    #for key in ['1_0', '1_1']:
        plt.plot(cat.r2eff, cat[key+'_rel'], '.', label=key)

    #plt.plot(cat.r2eff, cat['1_1_rel'], '.', label='1 1')
    plt.plot(x, x, '-', lw=2)

    plt.legend()
    plt.show()


# Looking at the histogram after correcting for the noise effect.
if False:
    cat['r2eff'].hist(bins=50, histtype='step', normed=True, label='Orig')

    for key in L:
        cat[key].hist(bins=50, histtype='step', normed=True, label=lblD[key])

    plt.legend()
    plt.show()

if False:
    cat = cat[(1.5 <  cat.zs) & (cat.zs < 1.7)]

    N = 200
    cat['r2eff'].hist(bins=N, histtype='step', normed=True, label='Orig')
    for key in ['xt_eff', 'xt_vol_eff']:
        cat[key].hist(bins=N, histtype='step', normed=True, label=key)

    plt.legend()
    plt.show()

#cat = cat[cat.mag_vis < 24.0]


# ------------
if False:
    z = np.linspace(0.2, 2.0, 10)
    mean = cat.groupby(pd.cut(cat.zs, z)).mean()
    plt.plot(mean['zs'], mean['r2eff'], label='Orig')
    for key in L:
        plt.plot(mean['zs'], mean[key], label=key)


    plt.legend()
    plt.show()

#ipdb.set_trace()
