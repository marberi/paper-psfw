#!/usr/bin/env python
# encoding: UTF8

# Showing how many jobs remain.

import ipdb
import os
import numpy as np
import pandas as pd

import psf22

dsl = psf22.psf22()
topnode = dsl.topnode()


# The tasks needed.
tocalc = pd.DataFrame()
for k1,pipel in topnode.iteritems():
    for task_name, job in pipel.depend.iteritems():
        S = pd.Series({'key': k1, 'task_name': task_name, 
                       'taskid': job.taskid()})

        tocalc = tocalc.append(S, ignore_index=True)


d = '/data2/marberi/results/default'
finished = os.listdir(d)

# Not completely Pandas style, but works.
tocalc['isfinished'] = pd.Series([(x in finished) for x in tocalc.taskid])

A = tocalc[tocalc.task_name == 'r2train'].isfinished.value_counts()

print('psf22')
print(A)
