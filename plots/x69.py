#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import scipy
import statsmodels

import trainpipel
pipel = trainpipel.pipel()
pipel.config.update({
  'r2x.use_cal': False,
#  'r2x.filters': ['r', 'i', 'z'],
  'r2x.filters': ['r', 'i'],
  'r2x.all_test': True,
  'r2x.Ntrain': 400,
  'r2x.Nboot': 2,
  'r2x.use_stars': False})

del pipel.r2x.depend['cal_pz']
del pipel.r2x.depend['test_pz']

xvals = np.linspace(-0.02, 0.02)
for flts in [['r','i'], ['r', 'i', 'z']]:
    pipel.config['r2x.filters'] = flts
    cat = pipel.r2x.get_store()['r2eff']
    cat = cat.join(pipel.r2x.test_cat.result)
    cat = cat[cat.mag_vis < 24.5]

    fit = scipy.stats.gennorm.fit(cat[0])
    print('fit vals', fit)
    y = scipy.stats.gennorm.pdf(xvals, *fit)
    plt.plot(xvals, y, '--')
#    ipdb.set_trace()

    cat[0].hist(bins=100, histtype='step', normed=True, label=','.join(flts))

xlim = 0.02
plt.xlim((-xlim, xlim))

plt.legend()
plt.savefig('test.pdf')
#ipdb.set_trace()
