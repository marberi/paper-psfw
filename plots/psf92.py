#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import copy
import ipdb
import time
import numpy as np
import pandas as pd
import itertools as it
import scipy.stats

from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import matplotlib.ticker as mtick

import xdolphin as xd

import intersect

session = xd.create_session()

def gen_styles():
    i = 0
    styles = ['-.','-','--', ':']
    while True:
        yield styles[i % len(styles)]
        i += 1

class Plot:
    """Magnitude offsets."""

    name = 'psf92'

    def _add_offset(self, pipel):
        """Add a task for the offset."""

        offset = xd.Job('offset')
        offset.depend['magcat'] = pipel.r2x.test_cat

        pipel.r2x.depend['test_cat'] = offset

        return pipel

    def _extend(self, topnode, new_key, toadd):
        for key, val in toadd.iteritems():
            topnode[(new_key,)+key] = val

    def topnode(self):
        import trainpipel

        pipel = self._add_offset(trainpipel.pipel())
        pipel.config.update({\
          'r2x.filters': ['r', 'i', 'z'], 
          'r2x.all_test': True
        })

        # Combined filter and magnitude list.
        fL = ['r','i','z']
        mag_list = [-0.02, -0.015, -0.01, -0.005, 0., 0.005, 0.01, 0.015, 0.02]


        layer1 = []
        for f,val in it.product(fL, mag_list):
            layer1.append({'r2x.test_cat.{0}'.format(f): val})


        # Mor2 compact than it should be...
        fields = ['r2x.use_stars', 'r2x.Ntrain']
        X = [(True, 400), (False, 4000)]
        layer2 = [dict(zip(fields, x)) for x in X]


        layers = xd.Layers()
        layers.add_pipeline(pipel)
        layers.add_layer('comb', layer1)
        layers.add_layer('use_stars', layer2, labels=[True,False])

        top = layers.to_topnode()

        return top

    def f_reduce(self, pipel):
        cat = pipel.r2x.test_cat.result[['zs', 'mag_vis']]
        cat = cat.join(pipel.r2x.get_store()['r2eff'])
        cat = cat[cat.mag_vis < 24.5]

        N = pipel.r2x.config['Nboot']
       
        df = pd.DataFrame()
        S = pd.Series({'zbin': -1})
        S['r2bias'] = cat[range(N)].mean().mean()
        df = df.append(S, ignore_index=True)

        z = np.linspace(0.1, 1.8, 6)

        for i,(key,sub) in enumerate(cat.groupby(pd.cut(cat.zs, z))):
            S = pd.Series()
            S['zbin'] = i
            S['r2bias'] = sub[range(N)].mean().mean()
            S['zlbl'] = 'z = {0}'.format(key)
            S['zs'] = sub.zs.mean()

            df = df.append(S, ignore_index=True)

        return df


    def labels(self, pipel):
        """Offset label."""

        offS = pd.Series(dict(pipel.r2x.test_cat.config))

        has_offset = offS[offS != 0.]
        assert len(has_offset) <= 1, 'Offsets in multiple filters: {0}'.\
               format(','.join(has_offset.index.tolist()))


        S = pd.Series()
        S['fname'] = has_offset.index[0] if len(has_offset) else 'none'
        S['off_val'] = float(has_offset.values) if len(has_offset) else 0.

        return S


    def plot(self, prepare):

        # This step should technically be elsewhere, but only take 0.5 seconds.
        inter = intersect.find_inter(prepare, 'off_val', ['fname', 'zbin', 'use_stars'])

        s1 = 0.13
        width = 0.3
        w2 = 0.25
        inter['y0'] = inter[['xa', 'xb']].min(axis=1)
        inter['y1'] = inter[['xa', 'xb']].max(axis=1)

        binnr = inter.zbin.sort_values().unique().astype(int)
        xlabels = map('bin {0}'.format, binnr+1)
        xlabels[0] = 'all'


        # For testing..
        fig, A = plt.subplots(3, sharex=True)

        fL = ['r', 'i', 'z']

        for fname, ax in zip(fL, A):

            sub = inter[inter.fname == fname]
            stars = sub[sub.use_stars == 1]
            gal = sub[sub.use_stars == 0]

            ax.grid(True)

            # This part of Matplotlib fails if being sent a series.
            fstar = {'color': 'black'}
            fgal = {'color': 'blue', 'hatch': '/'}
            ax.bar(stars.zbin.values+s1, stars.y0.values, width=w2, label='Star', **fstar)
            ax.bar(stars.zbin.values+s1, stars.y1.values, width=w2, **fstar)
            ax.bar((gal.zbin + s1 + width).values, gal.y0.values, width=w2, label='Gal', **fgal)
            ax.bar((gal.zbin + s1 + width).values, gal.y1.values, width=w2, **fgal)

            ax.set_ylim((-0.035, 0.035))
            if fname == 'r':
                print('adding legend')
#                ax.legend(loc=(0.32, 0.05), ncol=2, prop={'size': 11}) 
                ax.legend(loc=(0.32, 0.05), ncol=2) 

            ax.set_yticks([-0.02, -0.01, 0., 0.01, 0.02])

            ax.text(3.77, 0.022, 'Band: {0}'.format(fname), fontsize=12,
                      horizontalalignment='center', weight='bold')



        # x-axis 
        xpad = 0.1 
        plt.xlim((-1-xpad, binnr[-1]+width+w2+xpad))
        ax.set_xticks(binnr + 0.5*(width+w2))
        ax.set_xticklabels(xlabels, size=15)

        A[1].set_ylabel('$\delta m$', size=14)

        plt.subplots_adjust(hspace=0.0, top=0.95, left=0.14, right=0.92)

if __name__ == '__main__':
    xd.plot(Plot).save()
