#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import sys

import numpy as np
import pandas as pd
import x84

import xdolphin as xd

def trim_pipel(pipel):
    # To avoid changing the dictionary we iterate
    # over.
    keys = pipel.depend.keys()
    todel = filter(lambda x: x != 'r2train', keys)

    for k2 in todel:
        del pipel.depend[k2]

def find_bug(top):
    D = top._par_items()

    df = pd.DataFrame(D.items())
    counts = df[1].value_counts()
    counts.name = 'counts'
    df = df.join(counts, on=1)

    print('Duplicates', (df.counts > 1).sum())
    sys.exit()

    ipdb.set_trace()

def get_nodes():
    inst = x84.Plot()
    top = inst.topnode()
    top.update_config({'r2train.galcat.magcat.mabs.Ngal': int(3.5e5)})
    for k1,pipel in top.iteritems():
        trim_pipel(pipel)


#    find_bug(top)
#    ipdb.set_trace()


    # Testing new interface.
    nodes, dep = top.gecko()

    cached = top.gecko_cached()
    nodes['is_cached'] = nodes.taskid.isin(cached)
    nodes = nodes[nodes.name != 'empty']

    return nodes, dep


from IPython.parallel import Client
from IPython.parallel.util import interactive

has_connection = False
def setup_connection():
    global has_connection
    global dview
    rc = Client()
#    rc[:]['_fetch_local'] = _fetch_local
#    rc[:]['_find_info'] = _find_info
#    rc[:]['cachedir'] = cachedir
    rc[:].execute('import xdolphin as xd')

    return rc

def runme(root):
#    global dep

    # This should be set elsewhere.
    import sys
    sys.path.append('/home/marberi/xdolphin')
    sys.path.append('/home/marberi/drv/work/papers/psfw/tasks')
    sys.path.append('/home/marberi/drv/work/papers/psfw/plots')

    import gecko
    import sklearn
    job = gecko.build(nodes, dep, root)

#    return job.jobid, len(nodes), len(dep)
#    txt = 'Should run: {0}'.format(job.name)

    txt = job.runme()

    return txt

if False:
    import trainpipel
    pipel = trainpipel.pipel()
    nodes, dep = pipel.r2train.gecko_pack()

    import gecko
    J = gecko.build(nodes, dep, pipel.r2train.galcat.jobid)



nodes, dep = get_nodes()
#ipdb.set_trace()


rc = setup_connection()
rc[:]['nodes'] = nodes
rc[:]['dep'] = dep
dview = rc.load_balanced_view()


torun = nodes[~nodes.is_cached]
isedge = ~torun.index.isin(dep['from'])

# get the unique tasks to run.
L = []
unique = set(torun.taskid.unique())
for jobid,row in torun[isedge].iterrows():
    if row.taskid in unique:
        L.append(jobid)
        unique.remove(row.taskid)
    
ipdb.set_trace()

y = dview.map_sync(runme, L)

#print(y)
#ipdb.set_trace()
