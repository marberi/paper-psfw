#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
import scipy
import scipy.stats
from matplotlib import pyplot as plt

import xdolphin as xd
from xdolphin import Job

class Plot:
    """Requirement for the photo-z redshift resolution."""

    name = 'psf81'
    def topnode(self):

        import trainpipel
        pipel = trainpipel.pipel(toplevel='r2pz')

        interp = 2 
        #interp = 0
        pzconf = {'use_priors': False, 'filters': ['r', 'i', 'z']} #, 'zmax': 4.5}
        #pzconf = {'use_priors': False, 'filters': ['r', 'i']}
        pzconf['interp'] = interp
        #pzconf['zmax'] = 3.

        pipel.r2pz.pzcat.galcat.config['sn'] = 100
        pipel.r2pz.pzcat.galcat.config['sn'] = 1e7


        dzL = [0.02, 0.01, 0.007]
        dzL = [.315, 0.2, 0.15, 0.1, 0.05, 0.02, 0.015, 0.01, 0.007]
        dzL = [0.0005, 0.001, 0.003, 0.005, 0.01]
#        dzL += [0.007, 0.01, 0.015, 0.02, 0.03, 0.035, 0.04, 0.045, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]#, 0.12, 0.13,0.14, 0.15, 0.2]

#        dzL = [0.1] # FAST


        pipel.config.update({\
          'r2pz.pzcat.galcat.magcat.mabs.limit_seds': True,
          'r2pz.pzcat.galcat.const_sn': True})

        layers = xd.Layers()
        layers.add_pipeline(pipel)

        layers.add_layer_key('dz', 'r2pz.pzcat.dz', dzL)

#        topnode = xd.Topnode(pipel)
#        topnode.add_layer_key('r2pz.pzcat.dz', dzL)

        topnode = layers.to_topnode()

        return topnode

    def f_reduce(self, pipel):
        f_gauss = scipy.stats.norm()
        frac = 2.0*(np.array(map(f_gauss.cdf, [1,2,3])) - 0.5)

        cat = pipel.r2pz.result
        cat = cat.join(pipel.r2pz.pzcat.galcat.result)
        cat = cat[cat.mag_vis < 24.5]

        S = cat.r2peak.abs().describe(percentiles=list(frac))
        S['dz'] = pipel.config['r2pz.pzcat.dz']

        return S
#        ipdb.set_trace()

    def plot(self, data):
        df = data.unstack()['r2peak']

        touse = ['68.3%', '95.4%', '99.7%']
        styles = ['-','-.','--']

        for col,ls in zip(touse, styles):
            plt.plot(df.dz, df[col], ls=ls, label=col)

        ax = plt.gca()
        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')

        plt.xscale('log')
        plt.yscale('log')

        xlim = (0.0005, 0.01)
        xdiff = np.log10(xlim[1] / xlim[0])
        fac = 10**(0.05*xdiff)

        plt.xlim((xlim[0]/fac, fac*xlim[1]))
        plt.ylim((5e-6, 5e-4))
        plt.xlabel('$\Delta z$ - SED fit redshift resolution', size=14)
        plt.ylabel('$|\delta R^2_{\\rm{PSF}}|\, /\, R^2_{\\rm{PSF}}$', size=13)

        plt.legend(loc=2)

if __name__ == '__main__':
    xd.plot(Plot).save()
