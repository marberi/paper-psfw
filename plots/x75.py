#!/usr/bin/env python
# encoding: UTF8

from __future__ import print_function
import ipdb
import time
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import xdolphin as xd
import tosave

photoz_data = '/data2/photoz/run/data/'
abkids = xd.Job('ab')
abkids.task.config.update({'data_dir': photoz_data,
                           'filter_dir': 'kids_filters',
                           'zmax': 1.,
                           'sed_dir': 'seds_stars'})


j1 = xd.Job('kidscat')


j2 = xd.Job('starsel')
j2.depend['catalog'] = j1

j3 = xd.Job('starfit')
j3.depend['catalog'] = j2
j3.depend['ab'] = abkids


def load_fit():
    cat = j3.result

    # Only look at the objects with 3 colors.
    cat = cat[cat.nobs == 3.]
    del cat['nobs']

    #xmin = np.argmin(cat.values, axis=1)
    best_fit = cat.idxmin(axis=1)

    return cat, best_fit

def plot1():
    """Distribution of the chi2 values."""

    chi2 = cat.lookup(best_fit.index, best_fit)
    chi2 = pd.Series(chi2)
    np.sqrt(chi2[chi2< 1000]).hist(bins=200, normed=True) #, lw=1, histtype='step')

    plt.xlabel('$\chi$', size=16)
    plt.ylabel('Probability', size=16)
    plt.savefig(tosave.path('x75_chidistr'))


def plot2():
    """Check the SED distribution for different chi2 cuts."""

    chi2 = cat.lookup(best_fit.index, best_fit)
    df = pd.DataFrame({'sed': best_fit, 'chi2': chi2})

    aL = [('All', df.sed.value_counts())]
    for lim in [100, 20, 10, 3]:
        aL.append((lim, df[df.chi2 < lim].sed.value_counts()))

    #B = pd.concat([A1,A2], axis=1)
    bL = []
    for lbl, a in aL:
        a.name = lbl
        bL.append(a)

    C = pd.concat(bL, axis=1)
    toshow = C.sort(columns='All', ascending=False)[:10]
    toshow.plot(kind='bar')
    plt.savefig(tosave.path('x75_sedtypes'))

def plot3():
    """Look at the probability distribution."""

    prob = np.exp(-0.2*cat) # HACK.
    prob = prob.divide(prob.sum(axis=1), axis='rows')
    torem = np.isnan(prob).any(axis=1)

    # After removing the fits which give NaNs.
    prob = prob[~torem]

def plot4():
    """Number of objects in the different catalogs."""

    print('Loading galaxies')
    store_all = j1.get_store()
    Ngal = 0
    reader = store_all.select('default', columns=[], chunksize=1e5)
    for X in reader:
        Ngal += len(X)

        print(Ngal)

    print('Loading stars')
    cat_stars = j2.result

    print('# All', Ngal)
    print('# Stars', len(cat_stars))


# Checking the 
#cat_stars = j2.result
#plt.plot(cat_stars.RAJ2000, cat_stars.DECJ2000, '.')

cat = j2.result
sub = cat[cat.fieldnr == 0]

plt.plot(sub.RAJ2000, sub.DECJ2000, '.')
plt.show()
#ipdb.set_trace()
