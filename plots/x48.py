#!/usr/bin/env python
# encoding: UTF8

import ipdb
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from sklearn import svm, linear_model

import xdolphin as xd
import trainpipel

pipel = trainpipel.pipel()

#ipdb.set_trace()
ell_01 = pipel.r2train.train_gal_r2.r2vz.result['/vis/Ell_01'][0]
pipel.r2x.train_star_cat.magcat.magcat.config['Nstars'] = 10000


orig = pipel.r2x.train_star_cat.result
orig = orig.join(pipel.r2x.train_star_r2.result)
orig['c0'] = orig.mag_r - orig.mag_i
orig['c1'] = orig.mag_i - orig.mag_z

#orig = orig[orig.c0 < 0.]


Nstars = 5000
rL = np.linspace(-0.01, 0.01, 10)
df = pd.DataFrame()

cmap = {'c0': [1, -1, None], 
        'c1': [None, 1, -1]}

for i,col_field in enumerate(['c0', 'c1']):
    A = np.array([orig[col_field]]).T
    reg = linear_model.LinearRegression()
    reg.fit(A, orig.r2eff)

    for j, fname in enumerate(['r', 'i', 'z']):

        for k, ri in enumerate(rL):
            pipel.config['r2x.train_star_cat.magcat.magcat.Nstars'] = 5000+i
            off = {'r': 0., 'i': 0., 'z': 0.}
            off[fname] = ri

            pipel.r2x.train_star_cat.config.update(off)
#            pipel.config['r2x.train_star_cat.{0}'.format(fname)] = ri
#            ipdb.set_trace()

            cat = pipel.r2x.train_star_cat.result
            cat = cat.join(pipel.r2x.train_star_r2.result)
            cat['c0'] = cat.mag_r - cat.mag_i
            cat['c1'] = cat.mag_i - cat.mag_z
            #cat = cat[cat.c0 < 0.]

            A = np.array([cat[col_field]]).T
            pred = reg.predict(A)
            X = (pred - cat.r2eff)

            fac = cmap[col_field][j]
            if not fac:
                continue

            S = pd.Series()
            S['col'] = col_field
            S['fname'] = fname
            S['dmag'] = ri
            S['xmean'] = fac*(pred - cat.r2eff).mean() / ell_01
            S.name = k



            df = df.append(S)

ax = plt.gca()
ax.xaxis.grid(True, which='both')
ax.yaxis.grid(True, which='both')

toshowL = [['c0', 'r'], ['c0','i'], ['c1', 'i'], ['c1', 'z']]
#toshowL = [toshowL[-1]]

col_lblD = {'c0': 'r-i', 'c1': 'i-z'}
for col, fname in toshowL:
    sub = df[(df.col == col) & (df.fname == fname)]
#for key, val in df.groupby(['fname', 'col']):

    lbl = '{0} from {1}'.format(fname, col_lblD[col])
    plt.plot(sub.dmag, sub.xmean, 'o-', label=lbl)

#line = np.linspace(-0.01, 0.01)
#plt.plot(line, line, '--', lw=2.5, color='black', alpha=0.5)

plt.axhline(0., ls='--', lw=2.5, color='black', alpha=0.5)
plt.axvline(0., ls='--', lw=2.5, color='black', alpha=0.5)

plt.xlabel('Magnitude offset', size=16)
plt.ylabel('<$"R^2_{\\rm{PSF}}"$ diff> / "R^2_{\\rm{PSF}}"(Ell_01)', size=16)

plt.xlim((-0.01, 0.01))
plt.legend(loc=4)
plt.show()
