#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import ColorConverter

from xdolphin import Job

import tosave

z = np.linspace(0.1, 2., 5)

def plot4(galcat, starcat, x1, y1, ax=False, vis=True):
    col = lambda cat: cat['mag_'+x1] - cat['mag_'+y1]

    col_gal = col(galcat)
    col_star = col(starcat)

#    ax.plot(col_gal, galcat.r2eff, '.', color='orange', label='Galaxy', visible=vis)

    gr = pd.cut(galcat.zs, z)
    for key, val in galcat.groupby(gr):
        colx = col(val)

        ax.plot(colx, val.r2eff, '.', label='Galaxy'+key, visible=vis, alpha=0.5)

    ax.plot(col_star, starcat.r2eff, 'ko', label='Star', visible=vis) #, alpha=0.5)
#        ipdb.set_trace()


def catalogs(Ngal, Nstar):
    import trainpipel
    pipl = trainpipel.pipel() 

    galmag = pipl.depend['galcatN']['magcat']['magcat']
    r2gal = pipl.depend['r2gal']['r2eff']['r2eff']
    galcat = galmag.join(r2gal)

    starmag = pipl.depend['starcatN']['magcat']['magcat']
#    ipdb.set_trace()
#    starmag = pipl.depend['starnoise']['magcat']['cat']
    r2star = pipl.depend['r2star']['r2eff']['r2eff']
    starcat = starmag.join(r2star)

    # Only use a subset. 
    r_gal = np.random.randint(0, len(galcat), Ngal)
    r_star = np.random.randint(0, len(starcat), Nstar)

    galcat = galcat.ix[r_gal]
    starcat = starcat.ix[r_star]

    return galcat, starcat

galcat, starcat = catalogs(10000, 100)

# Only plot a subset

#ipdb.set_trace()
#F = ['g', 'r', 'i', 'z', 'vis']
#F += ['Y', 'J', 'H']

F = ['r', 'i', 'z']
F = ['z', 'i', 'r']
#F = ['i', 'z']
#F = ['r', 'i']
#F = ['r', 'z']
F = ['vis', 'r']

N = len(F)
fig, A = plt.subplots(N-1,N-1, sharex='col', sharey=True)

if len(F) == 2:
    A = [[A]]

conv = ColorConverter()
col = conv.to_rgb('#b0e0e6') ##87ceeb') #6495ed')

#optical =  ['g', 'r', 'i', 'z', 'vis']
over_vis = ['r', 'i', 'z', 'vis']
over_vis = []

#plt.locator_params(axis='x', nbins=4)

for k1,(f,B) in enumerate(zip(F[1:], A)):
    for k2, (g,ax) in enumerate(zip(F[:-1], B)):
        if k2 < k1+1:
            plot4(galcat, starcat, f, g, ax)
        else:
            plot4(galcat, starcat, f, g, ax, False)
            ax.axis('off')

        if f in over_vis and g in over_vis:
            ax.set_axis_bgcolor(col)


        ax.xaxis.grid(True, which='both')
        ax.yaxis.grid(True, which='both')
        ax.set_yticks([])

        ax.locator_params(axis='x', nbins=5)
        plt.setp(ax.get_xticklabels(), fontsize=10, rotation='vertical')

        if k2 <= k1:
            ax.set_title('{0}-{1}'.format(f, g), fontsize=16)

A[0][0].set_xlim(-0.25, 1.25)
A[0][0].legend(loc=0)

# y-label
fig.text(0.07, 0.5, '$"R^2_{\\rm{PSF}}"$', ha='center', va='center', rotation='vertical', size=16)

# Add legend
#lines = ax.lines
#labels = [x.get_label() for x in lines]
#plt.figlegend(ax.lines, labels, loc=0) #loc=(0.65, 0.6))

plt.subplots_adjust(wspace=0.2, hspace=0.42)
#title = 'Color versus $"R^2_{\\rm{PSF}}"$'
#fig.text(0.6, 0.8, title, fontsize=20)
#plt.suptitle('Color versus $"R^2_{\\rm{PSF}}"$', size=18)
#plt.tight_layout()
#plt.savefig(tosave.path('psf23'))
plt.show()
