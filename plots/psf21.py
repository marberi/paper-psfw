#!/usr/bin/env python
# encoding: UTF8

import copy
import ipdb
from matplotlib import pyplot as plt

import psf10
from xdolphin import Job

class psf21(psf10.psf10):
    """Main table with noise and training with more realistic
       star catalogs.
    """

    config = copy.deepcopy(psf10.psf10.config)
    config['myconf']['galnoise'] =  {'const_sn': False}
    config['myconf']['starnoise'] = {'const_sn': False}
    config['topo']['star_fit'] = True

if __name__ == '__main__':
    Job('psf21').run()
