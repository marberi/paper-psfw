#!/usr/bin/env python
# encoding: UTF8

import ipdb
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tosave
from xdolphin import Job
import trainpipel

class Plot:
    """Table with results from the photo-z method.""" 

    def topnode(self):
        pipel = trainpipel.pipel(toplevel='r2pz')
        pipel.config.update({\
          'r2pz.pzcat.out_pdf': True,
          'r2pz.pzcat.out_pdftype': True,
          'r2pz.pzcat.galcat.magcat.mabs.Ngal': 10000})

        return pipel

    def prepare(self):
        pipel = self.topnode()

        # This could have used some fancy joing methond.
        r2eff = pipel.r2pz.r2gal.result
        r2pz = pipel.r2pz.result
        galcat = pipel.r2pz.pzcat.galcat.result
        mabs = pipel.r2pz.pzcat.galcat.magcat.mabs.result 

        cat = pd.concat([r2eff,r2pz,galcat,mabs], axis=1)
        cat = cat[cat.mag_vis < 24.5]


        sedl = [0,17,55,65]
        gr = pd.cut(cat.sed, sedl, labels=['ell', 'sp', 'irr'])

        df = pd.DataFrame()

        S = cat[['r2peak', 'r2pdf', 'r2pdf_type']].mean()
        S['lbl'] = 'All'
        df = df.append(S, ignore_index=True)

        for key, val in cat.groupby(gr):
            lbl = key.capitalize()

            S = val[['r2peak', 'r2pdf', 'r2pdf_type']].mean() #.abs()
            S['lbl'] = lbl

            df = df.append(S, ignore_index=True)

#        ipdb.set_trace()

        return df

    def run(self):
        prepare = self.prepare()

        header = 'Sample &' + '& '.join(prepare.lbl) +'\\\\'
        print(header)

        for lbl,key in [('Peak','r2peak'), 
                        ('Pdf(z)', 'r2pdf'),
                        ('Pdf(z,sed)', 'r2pdf_type')]:

            row_txt = '{0} &'.format(lbl) + \
                      '& '.join(map('{0:.2e}'.format, prepare[key])) + \
                      '\\\\'

            print(row_txt)


if __name__ == '__main__':
    Plot().run()
